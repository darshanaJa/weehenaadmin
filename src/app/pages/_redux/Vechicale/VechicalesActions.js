import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./VechicalesCrud";
import {VechicalesSlice, callTypes} from "./VechicalesSlice";

const {actions} = VechicalesSlice;

export const fetchVechicales = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findVechicales(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.VechicalesFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Vechicales";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchVechicale = id => dispatch => {
  if (!id) {
    return dispatch(actions.VechicaleFetched({ VechicaleForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getVechicaleById(id)
    .then((res) => {
      let Vechicale = res.data; 

      dispatch(actions.VechicaleFetched({ VechicaleForEdit: Vechicale }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Vechicale";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteVechicale = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteVechicale(id)

    .then((res) => {
      let VechicaleId = res.data;
      console.log(VechicaleId);
      dispatch(actions.VechicaleDeleted({ VechicaleId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete Vechicale";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createVechicale = VechicaleForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createVechicale(VechicaleForCreation)

    .then((res) => {
      let Vechicale = res.data;
      console.log(Vechicale);
      dispatch(actions.VechicaleCreated({ Vechicale }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Vechicale";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateVechicale = (Vechicale,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Vechicale)
  return requestFromServer
    .updateVechicale(Vechicale,id)
    .then((res) => {
      dispatch(actions.VechicaleUpdated({ Vechicale }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Vechicale";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateVechicalePassword = (Vechicale,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Vechicale)
  return requestFromServer
    .updateVechicalePassword(Vechicale,id)
    .then(() => {
      dispatch(actions.VechicaleUpdated({ Vechicale }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Vechicale";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateVechicalesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForVechicales(ids, status)
    .then(() => {
      dispatch(actions.VechicalesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Vechicales status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteVechicales = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteVechicales(ids)
    .then(() => {
      dispatch(actions.VechicalesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Vechicales";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
