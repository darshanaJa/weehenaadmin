import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Vechicales_URL = Api_Login + '/api/vehicle/search';
export const Vechicales_CREATE_URL = Api_Login + '/api/vehicle/register';
export const Vechicales_DELETE_URL = Api_Login + '/api/vehicle/delete';
export const Vechicales_GET_ID_URL = Api_Login + '/api/vehicle';
export const Vechicales_UPDATE = Api_Login + '/api/vehicle/update';
export const Vechicales_PASSWORD_RESET = Api_Login + '/api/sales-Vechicale/password/update';

// CREATE =>  POST: add a new Vechicale to the server
export function createVechicale(Vechicale) {
  return axios.post(Vechicales_CREATE_URL, Vechicale)
}
// READ
export function getAllVechicales() {
  // return axios.get(Vechicales_URL);
}

export function getVechicaleById(id) {
  // console.log(id);
  return axios.get(`${Vechicales_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findVechicales(queryParams) {
  console.log(queryParams.vehi_reg_number);
  if (queryParams.vehi_reg_number === null || queryParams.vehi_reg_number === "") {
    return axios.post(Vechicales_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Vechicales_URL, { 
        "filter": [ {"vehi_reg_number" : queryParams.vehi_reg_number}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateVechicale(Vechicale,id) {
  console.log(id)
  return axios.put(`${Vechicales_UPDATE}/${id}`, Vechicale,{headers:{'Content-Type':'multipart/form-data'}} );
}

export function updateVechicalePassword(Vechicale,id) {
  console.log(id)
  return axios.put(`${Vechicales_PASSWORD_RESET}/${id}`, Vechicale );
}

// UPDATE Status
export function updateStatusForVechicales(ids, status) {
}

// DELETE => delete the Vechicale from the server
export function deleteVechicale(VechicaleId) {
  console.log(VechicaleId)
  return axios.delete(`${Vechicales_DELETE_URL}/${VechicaleId}`);
}

// DELETE Vechicales by ids
export function deleteVechicales(ids) {
}
