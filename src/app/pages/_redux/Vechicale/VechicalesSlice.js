import {createSlice} from "@reduxjs/toolkit";

const initialVechicalesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  VechicaleForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const VechicalesSlice = createSlice({
  name: "Vechicales",
  initialState: initialVechicalesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getVechicaleById
    VechicaleFetched: (state, action) => {
      state.actionsLoading = false;
      state.VechicaleForEdit = action.payload.VechicaleForEdit;
      state.error = null;
    },
    // findVechicales
    VechicalesFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createVechicale
    VechicaleCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Vechicale);
    },
    // updateVechicale
    VechicaleUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Vechicale.id) {
          return action.payload.Vechicale;
        }
        return entity;
      });
    },
    // deleteVechicale
    VechicaleDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Vechicale_id !== action.payload.sales_Vechicale_id);
    },
    // deleteVechicales
    VechicalesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // VechicalesUpdateState
    VechicalesStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
