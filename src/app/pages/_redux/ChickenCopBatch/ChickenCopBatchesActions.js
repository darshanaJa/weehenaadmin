import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./ChickenCopBatchesCrud";
import {ChickenCopBatchesSlice, callTypes} from "./ChickenCopBatchesSlice";

const {actions} = ChickenCopBatchesSlice;

export const fetchChickenCopBatches = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findChickenCopBatches(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.ChickenCopBatchesFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find ChickenCopBatches";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchChickenCopBatch = id => dispatch => {
  if (!id) {
    return dispatch(actions.ChickenCopBatchFetched({ ChickenCopBatchForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getChickenCopBatchById(id)
    .then((res) => {
      let ChickenCopBatch = res.data; 
      dispatch(actions.ChickenCopBatchFetched({ ChickenCopBatchForEdit: ChickenCopBatch }));
    })
    .catch(error => {
      error.clientMessage = "Can't find ChickenCopBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteChickenCopBatch = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChickenCopBatch(id)
    .then((res) => {
      let ChickenCopBatchId = res.data;
      console.log(ChickenCopBatchId);
      dispatch(actions.ChickenCopBatchDeleted({ ChickenCopBatchId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete ChickenCopBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createChickenCopBatch = ChickenCopBatchForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createChickenCopBatch(ChickenCopBatchForCreation)
    .then((res) => {
      let ChickenCopBatch = res.data;
      console.log(ChickenCopBatch);
      dispatch(actions.ChickenCopBatchCreated({ ChickenCopBatch }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create ChickenCopBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChickenCopBatch = (ChickenCopBatch,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(ChickenCopBatch)
  return requestFromServer
    .updateChickenCopBatch(ChickenCopBatch,id)
    .then((res) => {
      dispatch(actions.ChickenCopBatchUpdated({ ChickenCopBatch }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickenCopBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChickenCopBatchPassword = (ChickenCopBatch,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(ChickenCopBatch)
  return requestFromServer
    .updateChickenCopBatchPassword(ChickenCopBatch,id)
    .then(() => {
      dispatch(actions.ChickenCopBatchUpdated({ ChickenCopBatch }));
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickenCopBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateChickenCopBatchesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForChickenCopBatches(ids, status)
    .then(() => {
      dispatch(actions.ChickenCopBatchesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickenCopBatches status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteChickenCopBatches = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChickenCopBatches(ids)
    .then(() => {
      dispatch(actions.ChickenCopBatchesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete ChickenCopBatches";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
