import {createSlice} from "@reduxjs/toolkit";

const initialChickenCopBatchesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  ChickenCopBatchForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const ChickenCopBatchesSlice = createSlice({
  name: "ChickenCopBatches",
  initialState: initialChickenCopBatchesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getChickenCopBatchById
    ChickenCopBatchFetched: (state, action) => {
      state.actionsLoading = false;
      state.ChickenCopBatchForEdit = action.payload.ChickenCopBatchForEdit;
      state.error = null;
    },
    // findChickenCopBatches
    ChickenCopBatchesFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createChickenCopBatch
    ChickenCopBatchCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.ChickenCopBatch);
    },
    // updateChickenCopBatch
    ChickenCopBatchUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.ChickenCopBatch.id) {
          return action.payload.ChickenCopBatch;
        }
        return entity;
      });
    },
    // deleteChickenCopBatch
    ChickenCopBatchDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_ChickenCopBatch_id !== action.payload.sales_ChickenCopBatch_id);
    },
    // deleteChickenCopBatches
    ChickenCopBatchesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // ChickenCopBatchesUpdateState
    ChickenCopBatchesStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
