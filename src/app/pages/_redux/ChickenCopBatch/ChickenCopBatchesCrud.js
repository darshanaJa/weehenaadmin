import axios from "axios";
import { Api_Login } from "../../../config/config";

export const ChickenCopBatches_URL = Api_Login + '/api/chickcoop-batch/search';
export const ChickenCopBatches_CREATE_URL = Api_Login + '/api/chickcoop-batch/add';
export const ChickenCopBatches_DELETE_URL = Api_Login + '/api/chickcoop-batch/soft-delete';
export const ChickenCopBatches_GET_ID_URL = Api_Login + '/api/chickcoop-batch/get';
export const ChickenCopBatches_UPDATE = Api_Login + '/api/chickcoop-batch/update';
export const ChickenCopBatches_PASSWORD_RESET = Api_Login + '/api/sales-ChickenCopBatch/password/update';

// CREATE =>  POST: add a new ChickenCopBatch to the server
export function createChickenCopBatch(ChickenCopBatch) {
  return axios.post(ChickenCopBatches_CREATE_URL, ChickenCopBatch)
}
// READ
export function getAllChickenCopBatches() {
  // return axios.get(ChickenCopBatches_URL);
}

export function getChickenCopBatchById(id) {
  // console.log(id);
  return axios.get(`${ChickenCopBatches_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findChickenCopBatches(queryParams) {
  console.log(queryParams.buyback_name);
  if (queryParams.buyback_name === null || queryParams.buyback_name === "") {
    return axios.post(ChickenCopBatches_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(ChickenCopBatches_URL, { 
        "filter": [ {"buyback_name" : queryParams.buyback_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateChickenCopBatch(ChickenCopBatch,id) {
  console.log(id)
  return axios.put(`${ChickenCopBatches_UPDATE}/${id}`, ChickenCopBatch );
}

export function updateChickenCopBatchPassword(ChickenCopBatch,id) {
  console.log(id)
  return axios.put(`${ChickenCopBatches_PASSWORD_RESET}/${id}`, ChickenCopBatch );
}

// UPDATE Status
export function updateStatusForChickenCopBatches(ids, status) {
}

// DELETE => delete the ChickenCopBatch from the server
export function deleteChickenCopBatch(ChickenCopBatchId) {
  console.log(ChickenCopBatchId)
  return axios.put(`${ChickenCopBatches_DELETE_URL}/${ChickenCopBatchId}`);
}

// DELETE ChickenCopBatches by ids
export function deleteChickenCopBatches(ids) {
}
