import axios from "axios";
import { Api_Login } from "../../../config/config";

// export const Employees_URL = Api_Login + "/api/main-stock/search";
export const Employees_URL = Api_Login + "/api/employee/search";
// export const Employees_URL_GET = Api_Login + "/api/main-stock/all?Page=1&limit=10";

export const Employees_URL_CREATE = Api_Login + "/api/employee/register";
export const Employees_URL_DELETE = Api_Login + "/api/employee/delete";
export const Employees_URL_GETBYID = Api_Login + "/api/employee";
export const Employees_URL_UPDATE = Api_Login + "/api/employee/update";

// CREATE =>  POST: add a new Employee to the server
export function createEmployee(Employee) {
  return axios.post(Employees_URL_CREATE, Employee, {headers:{'Content-Type': 'application/json'}});
}

export function getEmployeeById(EmployeeId) {
  return axios.get(`${Employees_URL_GETBYID}/${EmployeeId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findEmployees(queryParams) {
  console.log(queryParams.emp_fname);
  if (queryParams.emp_fname === null || queryParams.emp_fname === "") {
    return axios.post(Employees_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Employees_URL, { 
        "filter": [ {"emp_fname" : queryParams.emp_fname}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateEmployee(Employee,id) {
  return axios.put(`${Employees_URL_UPDATE}/${id}`, Employee ,{headers:{'Content-Type': 'application/json'}});
}

// UPDATE Status
export function updateStatusForEmployees(ids, status) {
  // return axios.post(`${Employees_URL}/updateStatusForEmployees`, {
  //   ids,
  //   status
  // });
}

// DELETE => delete the Employee from the server
export function deleteEmployee(EmployeeId) {
  return axios.delete(`${Employees_URL_DELETE}/${EmployeeId}`);
}

// DELETE Employees by ids
export function deleteEmployees(ids) {
  // return axios.post(`${Employees_URL}/deleteEmployees`, { ids });
}
