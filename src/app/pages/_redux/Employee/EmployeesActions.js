import { notify,notifyError } from "../../../config/Toastify";
// import { EmployeesCard } from "../../pages/Employee/EmployeesCard";
import * as requestFromServer from "./EmployeesCrud";
import {EmployeesSlice, callTypes} from "./EmployeesSlice";

const {actions} = EmployeesSlice;

export const fetchEmployees = queryParams => dispatch => {
  console.log(queryParams);
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
  // console.log(queryParams)
    .findEmployees(queryParams)

    .then((res) => {
      let data = res.data;
      dispatch(actions.EmployeesFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
    })
    .catch(error => {
      console.log("Error Axios")
      error.clientMessage = "Can't find Employees";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchEmployee = id => dispatch => {
  if (!id) {
    return dispatch(actions.EmployeeFetched({ EmployeeForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getEmployeeById(id)
    .then(response => {
      const Employee = response.data;
      dispatch(actions.EmployeeFetched({ EmployeeForEdit: Employee }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Employee";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteEmployee = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteEmployee(id)
    .then(res => {
      dispatch(actions.EmployeeDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
     

    })
    .catch(error => {
      error.clientMessage = "Can't delete Employee";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      console.log("cant Delete")
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createEmployee = EmployeeForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createEmployee(EmployeeForCreation)
    
    .then((res) => {
      let Employee = res.data;
      console.log(Employee);
      dispatch(actions.EmployeeCreated({ Employee }));
      let msg = res.data.message;
      notify(msg)
      const errormsg = res.data.data.error_message ? res.data.data.error_message: null;
      console.log(errormsg)
      notifyError(errormsg)
    
      // dispatch(actions.SalesTicketUpdated({ SalesTicket }));
    })

    .catch(error => {
      error.clientMessage = "Can't create Employee";
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateEmployee = (Employee,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateEmployee(Employee,id)
    .then((res) => {
      dispatch(actions.EmployeeUpdated({ Employee }));
      let msg = res.data.message;
      console.log(msg)
      // EmployeesCard(msg)
      notify((msg))
    })
    .catch(error => {
      error.clientMessage = "Can't update Employee";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateEmployeesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForEmployees(ids, status)
    .then(() => {
      dispatch(actions.EmployeesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Employees status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteEmployees = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteEmployees(ids)
    .then(() => {
      dispatch(actions.EmployeesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Employees";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
