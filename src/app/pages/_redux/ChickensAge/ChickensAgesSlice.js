import {createSlice} from "@reduxjs/toolkit";

const initialChickensAgesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  ChickensAgeForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const ChickensAgesSlice = createSlice({
  name: "ChickensAges",
  initialState: initialChickensAgesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getChickensAgeById
    ChickensAgeFetched: (state, action) => {
      state.actionsLoading = false;
      state.ChickensAgeForEdit = action.payload.ChickensAgeForEdit;
      state.error = null;
    },
    // findChickensAges
    ChickensAgesFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createChickensAge
    ChickensAgeCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.ChickensAge);
    },
    // updateChickensAge
    ChickensAgeUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.ChickensAge.id) {
          return action.payload.ChickensAge;
        }
        return entity;
      });
    },
    // deleteChickensAge
    ChickensAgeDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_ChickensAge_id !== action.payload.sales_ChickensAge_id);
    },
    // deleteChickensAges
    ChickensAgesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // ChickensAgesUpdateState
    ChickensAgesStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
