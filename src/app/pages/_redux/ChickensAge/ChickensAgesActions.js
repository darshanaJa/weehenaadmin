import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./ChickensAgesCrud";
import {ChickensAgesSlice, callTypes} from "./ChickensAgesSlice";

const {actions} = ChickensAgesSlice;

export const fetchChickensAges = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findChickensAges(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.ChickensAgesFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find ChickensAges";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchChickensAge = id => dispatch => {
  if (!id) {
    return dispatch(actions.ChickensAgeFetched({ ChickensAgeForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getChickensAgeById(id)
    .then((res) => {
      let ChickensAge = res.data; 
      dispatch(actions.ChickensAgeFetched({ ChickensAgeForEdit: ChickensAge }));
    })
    .catch(error => {
      error.clientMessage = "Can't find ChickensAge";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteChickensAge = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChickensAge(id)
    .then((res) => {
      let ChickensAgeId = res.data;
      console.log(ChickensAgeId);
      dispatch(actions.ChickensAgeDeleted({ ChickensAgeId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete ChickensAge";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createChickensAge = ChickensAgeForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createChickensAge(ChickensAgeForCreation)
    .then((res) => {
      let ChickensAge = res.data;
      console.log(ChickensAge);
      dispatch(actions.ChickensAgeCreated({ ChickensAge }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create ChickensAge";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChickensAge = (ChickensAge,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(ChickensAge)
  return requestFromServer
    .updateChickensAge(ChickensAge,id)
    .then((res) => {
      dispatch(actions.ChickensAgeUpdated({ ChickensAge }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickensAge";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChickensAgePassword = (ChickensAge,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(ChickensAge)
  return requestFromServer
    .updateChickensAgePassword(ChickensAge,id)
    .then(() => {
      dispatch(actions.ChickensAgeUpdated({ ChickensAge }));
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickensAge";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateChickensAgesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForChickensAges(ids, status)
    .then(() => {
      dispatch(actions.ChickensAgesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickensAges status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteChickensAges = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChickensAges(ids)
    .then(() => {
      dispatch(actions.ChickensAgesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete ChickensAges";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
