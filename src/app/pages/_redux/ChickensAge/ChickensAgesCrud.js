import axios from "axios";
import { Api_Login } from "../../../config/config";

export const ChickensAges_URL = Api_Login + '/api/chicken/search';
export const ChickensAges_CREATE_URL = Api_Login + '/api/chicken/add';
export const ChickensAges_DELETE_URL = Api_Login + '/api/chickcoop-batch/delete';
export const ChickensAges_GET_ID_URL = Api_Login + '/api/chicken';
export const ChickensAges_UPDATE = Api_Login + '/api/chicken/update';
export const ChickensAges_PASSWORD_RESET = Api_Login + '/api/sales-ChickensAge/password/update';

// CREATE =>  POST: add a new ChickensAge to the server
export function createChickensAge(ChickensAge) {
  return axios.post(ChickensAges_CREATE_URL, ChickensAge)
}
// READ
export function getAllChickensAges() {
  // return axios.get(ChickensAges_URL);
}

export function getChickensAgeById(id) {
  // console.log(id);
  return axios.get(`${ChickensAges_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findChickensAges(queryParams) {
  console.log(queryParams.buyback_name);
  if (queryParams.buyback_name === null || queryParams.buyback_name === "") {
    return axios.post(ChickensAges_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(ChickensAges_URL, { 
        "filter": [ {"buyback_name" : queryParams.buyback_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateChickensAge(ChickensAge,id) {
  console.log(id)
  return axios.put(`${ChickensAges_UPDATE}/${id}`, ChickensAge );
}

export function updateChickensAgePassword(ChickensAge,id) {
  console.log(id)
  return axios.put(`${ChickensAges_PASSWORD_RESET}/${id}`, ChickensAge );
}

// UPDATE Status
export function updateStatusForChickensAges(ids, status) {
}

// DELETE => delete the ChickensAge from the server
export function deleteChickensAge(ChickensAgeId) {
  console.log(ChickensAgeId)
  return axios.delete(`${ChickensAges_DELETE_URL}/${ChickensAgeId}`);
}

// DELETE ChickensAges by ids
export function deleteChickensAges(ids) {
}
