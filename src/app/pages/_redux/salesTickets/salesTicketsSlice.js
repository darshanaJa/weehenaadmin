import {createSlice} from "@reduxjs/toolkit";

const initialSalesTicketsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  SalesTicketForEdit: undefined,
  lastError: null,
  salesItems:[],
  totalQuaniy:0
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const salesTicketsSlice = createSlice({
  name: "SalesTickets",
  initialState: initialSalesTicketsState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getSalesTicketById
    SalesTicketFetched: (state, action) => {
      state.actionsLoading = false;
      state.SalesTicketForEdit = action.payload.SalesTicketForEdit;
      state.error = null;
    },
    // findSalesTickets
    SalesTicketsFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createSalesTicket
    SalesTicketCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.SalesTicket);
    },
    // updateSalesTicket
    SalesTicketUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.SalesTicket.id) {
          return action.payload.SalesTicket;
        }
        return entity;
      });
    },
    // deleteSalesTicket
    SalesTicketDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.id !== action.payload.id);
    },
    // deleteSalesTickets
    SalesTicketsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // SalesTicketsUpdateState
    SalesTicketsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
    SalesTicketItemCrated:(state,action) => {
      const saleItem = action.payload;
      state.salesItems.push({
        mstock:saleItem.mstock,
        ticket_item_opening_quantity:saleItem.ticket_item_opening_quantity,
        ticket_item_price:saleItem.ticket_item_price
      }); 
      state.totalQuaniy = state.totalQuaniy + saleItem.ticket_item_opening_quantity

    },
    RemvesalesTickeItem:(state,action) => {
      const saleItem = action.payload;
      state.salesItems = state.salesItems.filter((el) => el.mstock !== saleItem)
    },
    EmptySalesTickeet:(state) => {
      state.salesItems =[]
      state.totalQuaniy=0
    }
  }
});
