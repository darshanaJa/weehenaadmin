// import React, { useEffect, useState, useRef } from "react";
import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./salesTicketsCrud";
import {salesTicketsSlice, callTypes} from "./salesTicketsSlice";
// notify
const {actions} = salesTicketsSlice;

export const fetchSalesTickets = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findSalesTickets(queryParams)
    .then((res) => {
      let data = res.data;
      console.log(data)
      dispatch(actions.SalesTicketsFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
    })
    .catch(error => {
      error.clientMessage = "Can't find SalesTickets";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchSalesTicket = id => dispatch => {
  if (!id) {
    return dispatch(actions.SalesTicketFetched({ SalesTicketForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSalesTicketById(id)
    .then(response => {
      const SalesTicket = response.data;
      console.log(SalesTicket)
      dispatch(actions.SalesTicketFetched({ SalesTicketForEdit: SalesTicket }));
    })
    .catch(error => {
      error.clientMessage = "Can't find SalesTicket";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSalesTicket = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSalesTicket(id)
    .then(res => {
      dispatch(actions.SalesTicketDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete SalesTicket";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Something went Wrong";
      notifyError(msg)
    });
};

export const createSalesTicket = SalesTicketForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSalesTicket(SalesTicketForCreation)
    .then((res) => {
      let SalesTicket = res.data;
      console.log(SalesTicket);
      // let msg = res.data.message;
      // console.log(msg)
      // {notify(msg)}
      
      let msg = res.data.message ? res.data.message : null
      console.log(msg)
      notify(msg)
      const errormsg = res.data.data.error_message ? res.data.data.error_message: null;
      console.log(errormsg)
      notifyError(errormsg)
      dispatch(actions.SalesTicketCreated({SalesTicket}));
    
    })
    
    .catch(error => {
      error.clientMessage = "Can't create SalesTicket";
      let msg = error.message ? error.message :"Something went Wrong";
      notifyError(msg)
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      
    });
};

export const updateSalesTicket = (SalesTicket,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateSalesTicket(SalesTicket,id)
    .then((res) => {
      dispatch(actions.SalesTicketUpdated({ SalesTicket }));
      let msg = res.data.message ? res.data.message : null
      console.log(msg)
      notify(msg)
      const errormsg = res.data.data.error_message ? res.data.data.error_message: null;
      console.log(errormsg)
      notifyError(errormsg)
    })
    .catch(error => {
      error.clientMessage = "Can't update SalesTicket";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Something went Wrong";
      notifyError(msg)
    });
};
// notifyError
export const updateSalesTicketsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForSalesTickets(ids, status)
    .then(() => {
      dispatch(actions.SalesTicketsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update SalesTickets status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSalesTickets = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSalesTickets(ids)
    .then(() => {
      dispatch(actions.SalesTicketsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete SalesTickets";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
