import { notify,notifyError } from "../../../config/Toastify";
// import { PositionsCard } from "../../pages/Position/PositionsCard";
import * as requestFromServer from "./PositionsCrud";
import {PositionsSlice, callTypes} from "./PositionsSlice";

const {actions} = PositionsSlice;

export const fetchPositions = queryParams => dispatch => {
  console.log(queryParams);
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
  // console.log(queryParams)
    .findPositions(queryParams)

    .then((res) => {
      let data = res.data;
      dispatch(actions.PositionsFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
    })
    .catch(error => {
      console.log("Error Axios")
      error.clientMessage = "Can't find Positions";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchPosition = id => dispatch => {
  if (!id) {
    return dispatch(actions.PositionFetched({ PositionForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getPositionById(id)
    .then(response => {
      const Position = response.data;
      dispatch(actions.PositionFetched({ PositionForEdit: Position }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Position";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deletePosition = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deletePosition(id)
    .then(res => {
      dispatch(actions.PositionDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
     

    })
    .catch(error => {
      error.clientMessage = "Can't delete Position";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      console.log("cant Delete")
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createPosition = PositionForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createPosition(PositionForCreation)
    
    .then((res) => {
      let Position = res.data;
      console.log(Position);
      dispatch(actions.PositionCreated({ Position }));
      let msg = res.data.message;
      notify(msg)
      const errormsg = res.data.data.error_message ? res.data.data.error_message: null;
      console.log(errormsg)
      notifyError(errormsg)
    
      // dispatch(actions.SalesTicketUpdated({ SalesTicket }));
    })

    .catch(error => {
      error.clientMessage = "Can't create Position";
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updatePosition = Position => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updatePosition(Position)
    .then((res) => {
      dispatch(actions.PositionUpdated({ Position }));
      let msg = res.data.message;
      console.log(msg)
      // PositionsCard(msg)
      notify((msg))
    })
    .catch(error => {
      error.clientMessage = "Can't update Position";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updatePositionsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForPositions(ids, status)
    .then(() => {
      dispatch(actions.PositionsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Positions status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deletePositions = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deletePositions(ids)
    .then(() => {
      dispatch(actions.PositionsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Positions";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
