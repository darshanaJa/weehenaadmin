import axios from "axios";
import { Api_Login } from "../../../config/config";

// export const Positions_URL = Api_Login + "/api/main-stock/search";
export const Positions_URL = Api_Login + "/api/position/search";
export const Positions_URL_GET = Api_Login + "/api/main-stock/all?Page=1&limit=10";

export const Positions_URL_CREATE = Api_Login + "/api/position/create";
export const Positions_URL_DELETE = Api_Login + "/api/position/delete";
export const Positions_URL_GETBYID = Api_Login + "/api/position";
export const Positions_URL_UPDATE = Api_Login + "/api/position/update";

// CREATE =>  POST: add a new Position to the server
export function createPosition(Position) {
  return axios.post(Positions_URL_CREATE, Position, {headers:{'Content-Type': 'application/json'}});
}

// READ
export function getAllPositions() {
  return axios.get(Positions_URL_GET);
}

export function getPositionById(PositionId) {
  return axios.get(`${Positions_URL_GETBYID}/${PositionId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findPositions(queryParams) {
  console.log(queryParams.position_name);
  if (queryParams.position_name === null || queryParams.position_name === "") {
    return axios.post(Positions_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Positions_URL, { 
        "filter": [ {"position_name" : queryParams.position_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updatePosition(Position) {
  return axios.put(`${Positions_URL_UPDATE}/${Position.id}`, Position);
}

// UPDATE Status
export function updateStatusForPositions(ids, status) {
}

// DELETE => delete the Position from the server
export function deletePosition(PositionId) {
  return axios.delete(`${Positions_URL_DELETE}/${PositionId}`);
}

// DELETE Positions by ids
export function deletePositions(ids) {
}
