import {createSlice} from "@reduxjs/toolkit";

const initialPaymentsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  PaymentForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const PaymentsSlice = createSlice({
  name: "Payments",
  initialState: initialPaymentsState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getPaymentById
    PaymentFetched: (state, action) => {
      state.actionsLoading = false;
      state.PaymentForEdit = action.payload.PaymentForEdit;
      state.error = null;
    },
    // findPayments
    PaymentsFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createPayment
    PaymentCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Payment);
    },
    // updatePayment
    PaymentUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Payment.id) {
          return action.payload.Payment;
        }
        return entity;
      });
    },
    // deletePayment
    PaymentDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Payment_id !== action.payload.sales_Payment_id);
    },
    // deletePayments
    PaymentsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // PaymentsUpdateState
    PaymentsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
