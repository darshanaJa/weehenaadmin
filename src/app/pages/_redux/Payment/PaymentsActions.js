import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./PaymentsCrud";
import {PaymentsSlice, callTypes} from "./PaymentsSlice";

const {actions} = PaymentsSlice;

export const fetchPayments = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findPayments(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.PaymentsFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Payments";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchPayment = id => dispatch => {
  if (!id) {
    return dispatch(actions.PaymentFetched({ PaymentForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getPaymentById(id)
    .then((res) => {
      let Payment = res.data; 
      dispatch(actions.PaymentFetched({ PaymentForEdit: Payment }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Payment";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deletePayment = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deletePayment(id)
    .then((res) => {
      let PaymentId = res.data;
      console.log(PaymentId);
      dispatch(actions.PaymentDeleted({ PaymentId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete Payment";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createPayment = PaymentForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createPayment(PaymentForCreation)
    .then((res) => {
      let Payment = res.data;
      console.log(Payment);
      dispatch(actions.PaymentCreated({ Payment }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Payment";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updatePayment = (Payment,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Payment)
  return requestFromServer
    .updatePayment(Payment,id)
    .then((res) => {
      dispatch(actions.PaymentUpdated({ Payment }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Payment";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updatePaymentPassword = (Payment,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Payment)
  return requestFromServer
    .updatePaymentPassword(Payment,id)
    .then(() => {
      dispatch(actions.PaymentUpdated({ Payment }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Payment";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updatePaymentsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForPayments(ids, status)
    .then(() => {
      dispatch(actions.PaymentsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Payments status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deletePayments = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deletePayments(ids)
    .then(() => {
      dispatch(actions.PaymentsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Payments";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
