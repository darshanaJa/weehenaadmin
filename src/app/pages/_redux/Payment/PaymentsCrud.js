import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Payments_URL = Api_Login + '/api/payment-method/search';
export const Payments_CREATE_URL = Api_Login + '/api/payment-method/register';
export const Payments_DELETE_URL = Api_Login + '/api/payment-method/delete';
export const Payments_GET_ID_URL = Api_Login + '/api/payment-method';
export const Payments_UPDATE = Api_Login + '/api/payment-method/update';
export const Payments_PASSWORD_RESET = Api_Login + '/api/sales-Payment/password/update';

// CREATE =>  POST: add a new Payment to the server
export function createPayment(Payment) {
  return axios.post(Payments_CREATE_URL, Payment)
}
// READ
export function getAllPayments() {
  // return axios.get(Payments_URL);
}

export function getPaymentById(id) {
  // console.log(id);
  return axios.get(`${Payments_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findPayments(queryParams) {
  console.log(queryParams.buyback_name);
  if (queryParams.buyback_name === null || queryParams.buyback_name === "") {
    return axios.post(Payments_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Payments_URL, { 
        "filter": [ {"type" : queryParams.buyback_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updatePayment(Payment,id) {
  console.log(id)
  return axios.put(`${Payments_UPDATE}/${id}`, Payment );
}

export function updatePaymentPassword(Payment,id) {
  console.log(id)
  return axios.put(`${Payments_PASSWORD_RESET}/${id}`, Payment );
}

// UPDATE Status
export function updateStatusForPayments(ids, status) {
}

// DELETE => delete the Payment from the server
export function deletePayment(PaymentId) {
  console.log(PaymentId)
  return axios.delete(`${Payments_DELETE_URL}/${PaymentId}`);
}

// DELETE Payments by ids
export function deletePayments(ids) {
}
