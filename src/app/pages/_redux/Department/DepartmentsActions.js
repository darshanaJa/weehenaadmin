import { notify,notifyError } from "../../../config/Toastify";
// import { DepartmentsCard } from "../../pages/department/DepartmentsCard";
import * as requestFromServer from "./DepartmentsCrud";
import {DepartmentsSlice, callTypes} from "./DepartmentsSlice";

const {actions} = DepartmentsSlice;

export const fetchDepartments = queryParams => dispatch => {
  console.log(queryParams);
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
  // console.log(queryParams)
    .findDepartments(queryParams)

    .then((res) => {
      let data = res.data;
      dispatch(actions.DepartmentsFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
    })
    .catch(error => {
      console.log("Error Axios")
      error.clientMessage = "Can't find Departments";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchDepartment = id => dispatch => {
  if (!id) {
    return dispatch(actions.DepartmentFetched({ DepartmentForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getDepartmentById(id)
    .then(response => {
      const Department = response.data;
      dispatch(actions.DepartmentFetched({ DepartmentForEdit: Department }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Department";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteDepartment = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteDepartment(id)
    .then(res => {
      dispatch(actions.DepartmentDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
     

    })
    .catch(error => {
      error.clientMessage = "Can't delete Department";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      console.log("cant Delete")
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createDepartment = DepartmentForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createDepartment(DepartmentForCreation)
    // .then(response => {
    //   const { Department } = response.data;
    //   dispatch(actions.DepartmentCreated({ Department }));
    
    .then((res) => {
      let Department = res.data;
      console.log(Department);
      dispatch(actions.DepartmentCreated({ Department }));
      let msg = res.data.message;
      notify(msg)
      const errormsg = res.data.data.error_message ? res.data.data.error_message: null;
      console.log(errormsg)
      notifyError(errormsg)
    
      // dispatch(actions.SalesTicketUpdated({ SalesTicket }));
    })

    .catch(error => {
      error.clientMessage = "Can't create Department";
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateDepartment = (Department,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateDepartment(Department,id)
    .then((res) => {
      dispatch(actions.DepartmentUpdated({ Department }));
      let msg = res.data.message;
      console.log(msg)
      // DepartmentsCard(msg)
      notify((msg))
    })
    .catch(error => {
      error.clientMessage = "Can't update Department";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateDepartmentsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForDepartments(ids, status)
    .then(() => {
      dispatch(actions.DepartmentsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Departments status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteDepartments = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteDepartments(ids)
    .then(() => {
      dispatch(actions.DepartmentsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Departments";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
