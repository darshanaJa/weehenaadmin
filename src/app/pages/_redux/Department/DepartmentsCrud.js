import axios from "axios";
import { Api_Login } from "../../../config/config";

// export const Departments_URL = Api_Login + "/api/main-stock/search";
export const Departments_URL = Api_Login + "/api/department/search";
export const Departments_URL_GET = Api_Login + "/api/main-stock/all?Page=1&limit=10";

export const Departments_URL_CREATE = Api_Login + "/api/department/create";
export const Departments_URL_DELETE = Api_Login + "/api/department/delete";
export const Departments_URL_GETBYID = Api_Login + "/api/department";
export const Departments_URL_UPDATE = Api_Login + "/api/department/update";

// CREATE =>  POST: add a new Department to the server
export function createDepartment(Department) {
  return axios.post(Departments_URL_CREATE, Department, {headers:{'Content-Type': 'application/json'}});
}

// READ
export function getAllDepartments() {
  return axios.get(Departments_URL_GET);
}

export function getDepartmentById(DepartmentId) {
  return axios.get(`${Departments_URL_GETBYID}/${DepartmentId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findDepartments(queryParams) {
  console.log(queryParams.department_name);
  if (queryParams.department_name === null || queryParams.department_name === "") {
    return axios.post(Departments_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Departments_URL, { 
        "filter": [ {"department_name" : queryParams.department_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateDepartment(Department,id) {
  return axios.put(`${Departments_URL_UPDATE}/${id}`, Department,{headers:{'Content-Type': 'application/json'}});
}

// UPDATE Status
export function updateStatusForDepartments(ids, status) {
}

// DELETE => delete the Department from the server
export function deleteDepartment(DepartmentId) {
  return axios.delete(`${Departments_URL_DELETE}/${DepartmentId}`);
}

// DELETE Departments by ids
export function deleteDepartments(ids) {
  // return axios.post(`${Departments_URL}/deleteDepartments`, { ids });
}
