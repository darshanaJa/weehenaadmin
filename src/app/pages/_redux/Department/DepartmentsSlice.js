import {createSlice} from "@reduxjs/toolkit";

const initialDepartmentsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  DepartmentForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const DepartmentsSlice = createSlice({
  name: "Departments",
  initialState: initialDepartmentsState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getDepartmentById
    DepartmentFetched: (state, action) => {
      state.actionsLoading = false;
      state.DepartmentForEdit = action.payload.DepartmentForEdit;
      state.error = null;
    },
    // findDepartments
    DepartmentsFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createDepartment
    DepartmentCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Department);
    },
    // updateDepartment
    DepartmentUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Department.id) {
          return action.payload.Department;
        }
        return entity;
      });
    },
    // deleteDepartment
    DepartmentDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.id !== action.payload.id);
    },
    // deleteDepartments
    DepartmentsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // DepartmentsUpdateState
    DepartmentsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
