import * as requestFromServer from "./AccessCrud";
import {AccessSlice, callTypes} from "./AccessSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = AccessSlice;

export const fetchAccess = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findAccess(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.AccessFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Access";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchAcces = id => dispatch => {
  if (!id) {
    return dispatch(actions.AccesFetched({ AccesForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getAccesById(id)
    .then((res) => {
      let Acces = res.data; 
      dispatch(actions.AccesFetched({ AccesForEdit: Acces }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Acces";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteAcces = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteAcces(id)
    .then((res) => {
      let AccesId = res.data;
      console.log(AccesId);
      dispatch(actions.AccesDeleted({ AccesId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete Acces";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createAcces = AccesForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createAcces(AccesForCreation)
    .then((res) => {
      let Acces = res.data;
      console.log(Acces);
      dispatch(actions.AccesCreated({ Acces }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Acces";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateAcces = (Acces,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Acces)
  return requestFromServer
    .updateAcces(Acces,id)
    .then((res) => {

      dispatch(actions.AccesUpdated({ Acces }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Acces";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateAccesPassword = (Acces,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Acces)
  return requestFromServer
    .updateAccesPassword(Acces,id)
    .then((res) => {
      dispatch(actions.AccesUpdated({ Acces }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Acces";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateAccessStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForAccess(ids, status)
    .then(() => {
      dispatch(actions.AccessStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Access status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteAccess = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteAccess(ids)
    .then(() => {
      dispatch(actions.AccessDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Access";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
