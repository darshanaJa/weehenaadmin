import {createSlice} from "@reduxjs/toolkit";

const initialAccessState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  AccesForEdit: undefined,
  lastError: null,
  item:[]
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const AccessSlice = createSlice({
  name: "Access",
  initialState: initialAccessState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getAccesById
    AccesFetched: (state, action) => {
      state.actionsLoading = false;
      state.AccesForEdit = action.payload.AccesForEdit;
      state.error = null;
    },
    // findAccess
    AccessFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createAcces
    AccesCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Acces);
    },
    // updateAcces
    AccesUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Acces.id) {
          return action.payload.Acces;
        }
        return entity;
      });
    },
    // deleteAcces
    AccesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Acces_id !== action.payload.sales_Acces_id);
    },
    // deleteAccess
    AccessDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // AccessUpdateState
    AccessStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
    Accessitem: (state, action) => {
      state.item=action.payload
    }
  }
});
