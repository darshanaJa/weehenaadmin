import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Access_URL = Api_Login + '/api/user-permissions/search';
export const Access_GET_URL = Api_Login + '/api/sales-Acces/all/sales-Access';
export const Access_CREATE_URL = Api_Login + '/api/user-permissions/create';
export const Access_DELETE_URL = Api_Login + '/api/sales-Acces/delete';
export const Access_GET_ID_URL = Api_Login + '/api/user-permissions';
export const Access_UPDATE = Api_Login + '/api/user-permissions/update';
export const Access_PASSWORD_RESET = Api_Login + '/api/sales-Acces/password/update';

// CREATE =>  POST: add a new Acces to the server
export function createAcces(Acces) {
  return axios.post(Access_CREATE_URL, Acces)
}
// READ
export function getAllAccess() {
  return axios.get(Access_GET_URL);
}

export function getAccesById(id) {
  // console.log(id);
  return axios.get(`${Access_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findAccess(queryParams) {
  console.log(queryParams.sales_Acces_fname);
  if (queryParams.sales_Acces_fname === null || queryParams.sales_Acces_fname === "") {
    return axios.post(Access_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Access_URL, { 
        "filter": [ {"sales_Acces_fname" : queryParams.sales_Acces_fname}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateAcces(Acces,id) {
  console.log(id)
  return axios.put(`${Access_UPDATE}/${id}`, Acces);
}

export function updateAccesPassword(Acces,id) {
  console.log(id)
  return axios.put(`${Access_PASSWORD_RESET}/${id}`, Acces );
}

// UPDATE Status
export function updateStatusForAccess(ids, status) {
}

// DELETE => delete the Acces from the server
export function deleteAcces(AccesId) {
  console.log(AccesId)
  return axios.delete(`${Access_DELETE_URL}/${AccesId}`);
}

// DELETE Access by ids
export function deleteAccess(ids) {
  // return axios.post(`${Access_URL}/deleteAccess`, { ids });
}
