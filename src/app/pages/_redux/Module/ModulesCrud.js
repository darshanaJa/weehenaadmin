import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Modules_URL = Api_Login + '/api/access-modules/search';
export const Modules_CREATE_URL = Api_Login + '/api/access-modules/add';
export const Modules_DELETE_URL = Api_Login + '/api/buyback-Module/delete';
export const Modules_GET_ID_URL = Api_Login + '/api/buyback-Module';
export const Modules_UPDATE = Api_Login + '/api/access-modules/update';
export const Modules_PASSWORD_RESET = Api_Login + '/api/sales-Module/password/update';

// CREATE =>  POST: add a new Module to the server
export function createModule(Module) {
  return axios.post(Modules_CREATE_URL, Module)
}
// READ
export function getAllModules() {
  // return axios.get(Modules_URL);
}

export function getModuleById(id) {
  // console.log(id);
  return axios.get(`${Modules_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findModules(queryParams) {
  console.log(queryParams.buyback_name);
  if (queryParams.buyback_name === null || queryParams.buyback_name === "") {
    return axios.post(Modules_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Modules_URL, { 
        "filter": [ {"buyback_name" : queryParams.buyback_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateModule(Module,id) {
  console.log(id)
  return axios.put(`${Modules_UPDATE}/${id}`, Module );
}

export function updateModulePassword(Module,id) {
  console.log(id)
  return axios.put(`${Modules_PASSWORD_RESET}/${id}`, Module );
}

// UPDATE Status
export function updateStatusForModules(ids, status) {
}

// DELETE => delete the Module from the server
export function deleteModule(ModuleId) {
  console.log(ModuleId)
  return axios.delete(`${Modules_DELETE_URL}/${ModuleId}`);
}

// DELETE Modules by ids
export function deleteModules(ids) {
}
