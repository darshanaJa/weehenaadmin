import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./ModulesCrud";
import {ModulesSlice, callTypes} from "./ModulesSlice";

const {actions} = ModulesSlice;

export const fetchModules = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findModules(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.ModulesFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Modules";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchModule = id => dispatch => {
  if (!id) {
    return dispatch(actions.ModuleFetched({ ModuleForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getModuleById(id)
    .then((res) => {
      let Module = res.data; 
      dispatch(actions.ModuleFetched({ ModuleForEdit: Module }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Module";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteModule = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteModule(id)
    .then((res) => {
      let ModuleId = res.data;
      console.log(ModuleId);
      dispatch(actions.ModuleDeleted({ ModuleId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete Module";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createModule = ModuleForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createModule(ModuleForCreation)
    .then((res) => {
      let Module = res.data;
      console.log(Module);
      dispatch(actions.ModuleCreated({ Module }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Module";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateModule = (Module,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Module)
  return requestFromServer
    .updateModule(Module,id)
    .then((res) => {
      dispatch(actions.ModuleUpdated({ Module }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Module";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateModulePassword = (Module,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Module)
  return requestFromServer
    .updateModulePassword(Module,id)
    .then(() => {
      dispatch(actions.ModuleUpdated({ Module }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Module";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateModulesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForModules(ids, status)
    .then(() => {
      dispatch(actions.ModulesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Modules status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteModules = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteModules(ids)
    .then(() => {
      dispatch(actions.ModulesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Modules";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
