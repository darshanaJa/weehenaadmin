import {createSlice} from "@reduxjs/toolkit";

const initialModulesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  ModuleForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const ModulesSlice = createSlice({
  name: "Modules",
  initialState: initialModulesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getModuleById
    ModuleFetched: (state, action) => {
      state.actionsLoading = false;
      state.ModuleForEdit = action.payload.ModuleForEdit;
      state.error = null;
    },
    // findModules
    ModulesFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createModule
    ModuleCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Module);
    },
    // updateModule
    ModuleUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Module.id) {
          return action.payload.Module;
        }
        return entity;
      });
    },
    // deleteModule
    ModuleDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Module_id !== action.payload.sales_Module_id);
    },
    // deleteModules
    ModulesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // ModulesUpdateState
    ModulesStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
