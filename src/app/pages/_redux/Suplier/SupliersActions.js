import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./SupliersCrud";
import {SupliersSlice, callTypes} from "./SupliersSlice";

const {actions} = SupliersSlice;
// notify
export const fetchSupliers = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findSupliers(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.SupliersFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Supliers";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const fetchSuplier = id => dispatch => {
  if (!id) {
    return dispatch(actions.SuplierFetched({ SuplierForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSuplierById(id)
    .then((res) => {
      let Suplier = res.data; 
      dispatch(actions.SuplierFetched({ SuplierForEdit: Suplier }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Suplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSuplier = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSuplier(id)
    .then((res) => {
      let SuplierId = res.data;
      console.log(SuplierId);
      dispatch(actions.SuplierDeleted({ SuplierId })); 
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete Suplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createSuplier = SuplierForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSuplier(SuplierForCreation)

    .then((res) => {
      let Suplier = res.data;
      console.log(Suplier);
      dispatch(actions.SuplierCreated({ Suplier }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
       })
    
    .catch(error => {
      error.clientMessage = "Can't create Suplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSuplier = (Suplier,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Suplier)
  return requestFromServer
    .updateSuplier(Suplier,id)
    .then((res) => {
      dispatch(actions.SuplierUpdated({ Suplier }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Suplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};
// notifyError
export const updateSuplierPassword = (Suplier,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Suplier)
  return requestFromServer
    .updateSuplierPassword(Suplier,id)
    .then(() => {
      dispatch(actions.SuplierUpdated({ Suplier }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Suplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateSupliersStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForSupliers(ids, status)
    .then(() => {
      dispatch(actions.SupliersStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Supliers status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSupliers = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSupliers(ids)
    .then(() => {
      dispatch(actions.SupliersDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Supliers";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
