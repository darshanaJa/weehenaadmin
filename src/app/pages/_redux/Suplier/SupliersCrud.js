import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Supliers_URL_SEARCH = Api_Login + '/api/supplier/search';
export const Supliers_CREATE_URL = Api_Login + '/api/supplier/register';
export const Supliers_DELETE_URL = Api_Login + '/api/supplier/delete';
export const Supliers_GET_ID_URL = Api_Login + '/api/supplier';
export const Supliers_UPDATE = Api_Login + '/api/supplier/update';
export const Supliers_PASSWORD_RESET = Api_Login + '/api/sales-Suplier/password/update';

// CREATE =>  POST: add a new Suplier to the server
export function createSuplier(Suplier) {
  return axios.post(Supliers_CREATE_URL, Suplier)
}
// READ
export function getAllSupliers() {
  // return axios.get(Supliers_URL);
}

export function getSuplierById(id) {
  // console.log(id);
  return axios.get(`${Supliers_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findSupliers(queryParams) {
  console.log(queryParams.supplier_name);
  if (queryParams.supplier_name == null || queryParams.supplier_name == null) {
    return axios.post(Supliers_URL_SEARCH, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Supliers_URL_SEARCH, { 
        "filter": [ {"supplier_name" : queryParams.supplier_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateSuplier(Suplier,id) {
  console.log(id)
  return axios.put(`${Supliers_UPDATE}/${id}`, Suplier );
}

export function updateSuplierPassword(Suplier,id) {
  console.log(id)
  return axios.put(`${Supliers_PASSWORD_RESET}/${id}`, Suplier );
}

// UPDATE Status
export function updateStatusForSupliers(ids, status) {
}

// DELETE => delete the Suplier from the server
export function deleteSuplier(SuplierId) {
  console.log(SuplierId)
  return axios.delete(`${Supliers_DELETE_URL}/${SuplierId}`);
}

// DELETE Supliers by ids
export function deleteSupliers(ids) {
}
