import {createSlice} from "@reduxjs/toolkit";

const initialSupliersState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  SuplierForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const SupliersSlice = createSlice({
  name: "Supliers",
  initialState: initialSupliersState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getSuplierById
    SuplierFetched: (state, action) => {
      state.actionsLoading = false;
      state.SuplierForEdit = action.payload.SuplierForEdit;
      state.error = null;
    },
    // findSupliers
    SupliersFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createSuplier
    SuplierCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Suplier);
    },
    // updateSuplier
    SuplierUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Suplier.id) {
          return action.payload.Suplier;
        }
        return entity;
      });
    },
    // deleteSuplier
    SuplierDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Suplier_id !== action.payload.sales_Suplier_id);
    },
    // deleteSupliers
    SupliersDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // SupliersUpdateState
    SupliersStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
