import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Items_URL = Api_Login + "/api/saleticket-item/search";
export const Items_CREATE_URL = Api_Login + "/api/saleticket-item/add";
export const Items_UPDATE_URL = Api_Login + "/api/saleticket-item/update";
export const Items_DELETE_URL = Api_Login + "/api/saleticket-item/delete";
export const Items_GETBYID_URL = Api_Login + "/api/saleticket-item";



// CREATE =>  POST: add a new Item to the server
export function createItem(Item) {
  return axios.post(Items_CREATE_URL, Item);
}

// READ
// Server should return filtered Items by ItemId
export function getAllItemItemsByItemId(ItemId) {
  return axios.get(`${Items_URL}?ItemId=${ItemId}`);
}

export function getItemById(ItemId) {
  return axios.get(`${Items_GETBYID_URL}/${ItemId}`);
}

// Server should return sorted/filtered Items and merge with items from state
// TODO: Change your URL to REAL API, right now URL is 'api/Itemsfind/{ItemId}'. Should be 'api/Items/find/{ItemId}'!!!
export function findItems(queryParams) {
  return axios.post(Items_URL, {"filter": [{"status": "1" }],
  "sort": "DESC",
  "limit": 10, 
  "skip": 0});
}

// UPDATE => PUT: update the Item
export function updateItem(Item) {
  return axios.put(`${Items_UPDATE_URL}/${Item.sales_ticket_item_id}`, Item);
}

// DELETE => delete the Item
export function deleteItem(ItemId) {
  return axios.delete(`${Items_DELETE_URL}/${ItemId}`);
  // return axios.delete(`${Items_URL}/${ItemId}`);
}

// DELETE Items by ids
export function deleteItems(ids) {
  // return axios.post(`${Items_URL}/deleteItems`, { ids });
}
