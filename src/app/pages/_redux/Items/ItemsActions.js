import * as requestFromServer from "./ItemsCrud";
import {ItemsSlice, callTypes} from "./ItemsSlice";

const {actions} = ItemsSlice;


export const fetchItems = (queryParams, ItemId) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer

    .findItems(queryParams)
    .then((res) => {
      let data = res.data;
      console.log(data)
      dispatch(actions.ItemsFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));

    })
    .catch(error => {
      error.clientMessage = "Can't find Items";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchItem = id => dispatch => {
  if (!id) {
    return dispatch(actions.ItemFetched({ ItemForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getItemById(id)
    .then(response => {
      const Item = response.data;
      dispatch(actions.ItemFetched({ ItemForEdit: Item }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Item";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteItem = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteItem(id)
    .then(response => {
      dispatch(actions.ItemDeleted({ id }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Item";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const createItem = ItemForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createItem(ItemForCreation)

    .then((res) => {
      let Item = res.data;
      console.log(Item);
      dispatch(actions.ItemCreated({ Item }));
    })
    .catch(error => {
      error.clientMessage = "Can't create Item";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateItem = Item => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateItem(Item)
    .then(() => {
      dispatch(actions.ItemUpdated({ Item }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Item";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteItems = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteItems(ids)
    .then(() => {
      console.log("delete return");
      dispatch(actions.ItemsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Items";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
