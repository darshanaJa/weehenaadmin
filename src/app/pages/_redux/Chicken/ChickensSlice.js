import {createSlice} from "@reduxjs/toolkit";

const initialChickensState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  ChickenForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const ChickensSlice = createSlice({
  name: "Chickens",
  initialState: initialChickensState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getChickenById
    ChickenFetched: (state, action) => {
      state.actionsLoading = false;
      state.ChickenForEdit = action.payload.ChickenForEdit;
      state.error = null;
    },
    // findChickens
    ChickensFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createChicken
    ChickenCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Chicken);
    },
    // updateChicken
    ChickenUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Chicken.id) {
          return action.payload.Chicken;
        }
        return entity;
      });
    },
    // deleteChicken
    ChickenDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Chicken_id !== action.payload.sales_Chicken_id);
    },
    // deleteChickens
    ChickensDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // ChickensUpdateState
    ChickensStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
