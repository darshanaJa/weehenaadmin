import * as requestFromServer from "./ChickensCrud";
import {ChickensSlice, callTypes} from "./ChickensSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = ChickensSlice;

export const fetchChickens = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findChickens(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.ChickensFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Chickens";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchChicken = id => dispatch => {
  if (!id) {
    return dispatch(actions.ChickenFetched({ ChickenForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getChickenById(id)
    .then((res) => {
      let Chicken = res.data; 
      dispatch(actions.ChickenFetched({ ChickenForEdit: Chicken }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Chicken";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteChicken = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChicken(id)
    .then((res) => {
      let ChickenId = res.data;
      console.log(ChickenId);
      dispatch(actions.ChickenDeleted({ ChickenId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete Chicken";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createChicken = ChickenForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createChicken(ChickenForCreation)

    .then((res) => {
      let Chicken = res.data;
      console.log(Chicken);
      dispatch(actions.ChickenCreated({ Chicken }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Chicken";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChicken = (Chicken,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Chicken)
  return requestFromServer
    .updateChicken(Chicken,id)
    .then((res) => {

      dispatch(actions.ChickenUpdated({ Chicken }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Chicken";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChickenPassword = (Chicken,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Chicken)
  return requestFromServer
    .updateChickenPassword(Chicken,id)
    .then((res) => {
      dispatch(actions.ChickenUpdated({ Chicken }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Chicken";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateChickensStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForChickens(ids, status)
    .then(() => {
      dispatch(actions.ChickensStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Chickens status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteChickens = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChickens(ids)
    .then(() => {
      dispatch(actions.ChickensDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Chickens";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
