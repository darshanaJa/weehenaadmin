import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Chickens_URL = Api_Login + '/api/chickencoop/search';
export const Chickens_GET_URL = Api_Login + '/api/sales-Chicken/all/sales-Chickens';
export const Chickens_CREATE_URL = Api_Login + '/api/chickencoop/add';
export const Chickens_DELETE_URL = Api_Login + '/api/chickencoop/delete';
export const Chickens_GET_ID_URL = Api_Login + '/api/chickencoop';
export const Chickens_UPDATE = Api_Login + '/api/chickencoop/update';
export const Chickens_PASSWORD_RESET = Api_Login + '/api/sales-Chicken/password/update';

// CREATE =>  POST: add a new Chicken to the server
export function createChicken(Chicken) {
  return axios.post(Chickens_CREATE_URL, Chicken)
}
// READ
export function getAllChickens() {
  return axios.get(Chickens_GET_URL);
}

export function getChickenById(id) {
  // console.log(id);
  return axios.get(`${Chickens_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findChickens(queryParams) {
  // console.log(queryParams.chickcoop_name);
  if (queryParams.chickcoop_name === null || queryParams.chickcoop_name === "") {
    return axios.post(Chickens_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": 10, 
      "skip":0
  });
    } else {
      return axios.post(Chickens_URL, { 
        "filter": [ {"chickcoop_name" : queryParams.chickcoop_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}


// UPDATE => PUT: update the procuct on the server
export function updateChicken(Chicken,id) {
  console.log(id)
  return axios.put(`${Chickens_UPDATE}/${id}`, Chicken);
}

export function updateChickenPassword(Chicken,id) {
  console.log(id)
  return axios.put(`${Chickens_PASSWORD_RESET}/${id}`, Chicken );
}

// UPDATE Status
export function updateStatusForChickens(ids, status) {
}

// DELETE => delete the Chicken from the server
export function deleteChicken(ChickenId) {
  console.log(ChickenId)
  return axios.delete(`${Chickens_DELETE_URL}/${ChickenId}`);
}

// DELETE Chickens by ids
export function deleteChickens(ids) {
  // return axios.post(`${Chickens_URL}/deleteChickens`, { ids });
}
