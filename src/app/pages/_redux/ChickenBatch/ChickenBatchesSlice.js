import {createSlice} from "@reduxjs/toolkit";

const initialChickenBatchesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  ChickenBatchForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const ChickenBatchesSlice = createSlice({
  name: "ChickenBatches",
  initialState: initialChickenBatchesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getChickenBatchById
    ChickenBatchFetched: (state, action) => {
      state.actionsLoading = false;
      state.ChickenBatchForEdit = action.payload.ChickenBatchForEdit;
      state.error = null;
    },
    // findChickenBatches
    ChickenBatchesFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createChickenBatch
    ChickenBatchCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.ChickenBatch);
    },
    // updateChickenBatch
    ChickenBatchUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.ChickenBatch.id) {
          return action.payload.ChickenBatch;
        }
        return entity;
      });
    },
    // deleteChickenBatch
    ChickenBatchDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_ChickenBatch_id !== action.payload.sales_ChickenBatch_id);
    },
    // deleteChickenBatches
    ChickenBatchesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // ChickenBatchesUpdateState
    ChickenBatchesStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
