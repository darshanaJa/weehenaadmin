import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./ChickenBatchesCrud";
import {ChickenBatchesSlice, callTypes} from "./ChickenBatchesSlice";

const {actions} = ChickenBatchesSlice;

export const fetchChickenBatches = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findChickenBatches(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.ChickenBatchesFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find ChickenBatches";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchChickenBatch = id => dispatch => {
  if (!id) {
    return dispatch(actions.ChickenBatchFetched({ ChickenBatchForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getChickenBatchById(id)
    .then((res) => {
      let ChickenBatch = res.data; 
      dispatch(actions.ChickenBatchFetched({ ChickenBatchForEdit: ChickenBatch }));
    })
    .catch(error => {
      error.clientMessage = "Can't find ChickenBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteChickenBatch = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChickenBatch(id)
    .then((res) => {
      let ChickenBatchId = res.data;
      console.log(ChickenBatchId);
      dispatch(actions.ChickenBatchDeleted({ ChickenBatchId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete ChickenBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createChickenBatch = ChickenBatchForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createChickenBatch(ChickenBatchForCreation)
    .then((res) => {
      let ChickenBatch = res.data;
      console.log(ChickenBatch);
      dispatch(actions.ChickenBatchCreated({ ChickenBatch }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create ChickenBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChickenBatch = (ChickenBatch,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(ChickenBatch)
  return requestFromServer
    .updateChickenBatch(ChickenBatch,id)
    .then((res) => {
      dispatch(actions.ChickenBatchUpdated({ ChickenBatch }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickenBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateChickenBatchPassword = (ChickenBatch,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(ChickenBatch)
  return requestFromServer
    .updateChickenBatchPassword(ChickenBatch,id)
    .then(() => {
      dispatch(actions.ChickenBatchUpdated({ ChickenBatch }));
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickenBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateChickenBatchesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForChickenBatches(ids, status)
    .then(() => {
      dispatch(actions.ChickenBatchesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update ChickenBatches status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteChickenBatches = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteChickenBatches(ids)
    .then(() => {
      dispatch(actions.ChickenBatchesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete ChickenBatches";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
