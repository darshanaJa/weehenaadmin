import axios from "axios";
import { Api_Login } from "../../../config/config";

export const ChickenBatches_URL = Api_Login + '/api/chick-batch/search';
export const ChickenBatches_CREATE_URL = Api_Login + '/api/chick-batch/add';
export const ChickenBatches_DELETE_URL = Api_Login + '/api/chick-batch/soft-delete';
export const ChickenBatches_GET_ID_URL = Api_Login + '/api/chick-batch';
export const ChickenBatches_UPDATE = Api_Login + '/api/chick-batch/update';
export const ChickenBatches_PASSWORD_RESET = Api_Login + '/api/sales-ChickenBatch/password/update';

// CREATE =>  POST: add a new ChickenBatch to the server
export function createChickenBatch(ChickenBatch) {
  return axios.post(ChickenBatches_CREATE_URL, ChickenBatch)
}
// READ
export function getAllChickenBatches() {
  // return axios.get(ChickenBatches_URL);
}

export function getChickenBatchById(id) {
  // console.log(id);
  return axios.get(`${ChickenBatches_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findChickenBatches(queryParams) {
  console.log(queryParams.buyback_name);
  if (queryParams.buyback_name === null || queryParams.buyback_name === "") {
    return axios.post(ChickenBatches_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(ChickenBatches_URL, { 
        "filter": [ {"batchname" : queryParams.buyback_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateChickenBatch(ChickenBatch,id) {
  console.log(id)
  return axios.put(`${ChickenBatches_UPDATE}/${id}`, ChickenBatch );
}

export function updateChickenBatchPassword(ChickenBatch,id) {
  console.log(id)
  return axios.put(`${ChickenBatches_PASSWORD_RESET}/${id}`, ChickenBatch );
}

// UPDATE Status
export function updateStatusForChickenBatches(ids, status) {
}

// DELETE => delete the ChickenBatch from the server
export function deleteChickenBatch(ChickenBatchId) {
  console.log(ChickenBatchId)
  return axios.put(`${ChickenBatches_DELETE_URL}/${ChickenBatchId}`);
}

// DELETE ChickenBatches by ids
export function deleteChickenBatches(ids) {
}
