import {createSlice} from "@reduxjs/toolkit";

const initialServicesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  ServiceForEdit: undefined,
  lastError: null,
  spareItems:[]
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const ServicesSlice = createSlice({
  name: "Services",
  initialState: initialServicesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getServiceById
    ServiceFetched: (state, action) => {
      state.actionsLoading = false;
      state.ServiceForEdit = action.payload.ServiceForEdit;
      state.error = null;
    },
    // findServices
    ServicesFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createService
    ServiceCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Service);
    },
    // updateService
    ServiceUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Service.id) {
          return action.payload.Service;
        }
        return entity;
      });
    },
    // deleteService
    ServiceDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Service_id !== action.payload.sales_Service_id);
    },
    // deleteServices
    ServicesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // ServicesUpdateState
    ServicesStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    },
    AddSpareItem: (state, action) => {
      const spareItem = action.payload
      state.spareItems.push({
        grnsparestock:spareItem.id,
        sp_quantity:spareItem.quantity
      })
    },
    SpareItemReset:(state) => {
      state.spareItems=[]
    },
    SpareDeleteHandler:(state,action) => {
      const spareItemID = action.payload
      state.spareItems = state.spareItems.filter((el) => el.grnsparestock !== spareItemID)

    }
  }
});

export const serviceSliceActions = ServicesSlice.actions
