import * as requestFromServer from "./ServicesCrud";
import {ServicesSlice, callTypes} from "./ServicesSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = ServicesSlice;

export const fetchServices = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findServices(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.ServicesFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Services";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchService = id => dispatch => {
  if (!id) {
    return dispatch(actions.ServiceFetched({ ServiceForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getServiceById(id)
    .then((res) => {
      let Service = res.data; 
      dispatch(actions.ServiceFetched({ ServiceForEdit: Service }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Service";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteService = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteService(id)
    .then((res) => {
      let ServiceId = res.data;
      console.log(ServiceId);
      dispatch(actions.ServiceDeleted({ ServiceId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete Service";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createService = ServiceForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createService(ServiceForCreation)

    .then((res) => {
      let Service = res.data;
      console.log(Service);
      dispatch(actions.ServiceCreated({ Service }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Service";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateService = (Service,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Service)
  return requestFromServer
    .updateService(Service,id)
    .then((res) => {

      dispatch(actions.ServiceUpdated({ Service }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Service";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateServicePassword = (Service,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Service)
  return requestFromServer
    .updateServicePassword(Service,id)
    .then((res) => {
      dispatch(actions.ServiceUpdated({ Service }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Service";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateServicesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForServices(ids, status)
    .then(() => {
      dispatch(actions.ServicesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Services status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteServices = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteServices(ids)
    .then(() => {
      dispatch(actions.ServicesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Services";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
