import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Services_URL = Api_Login + '/api/vehicle-service/search';
export const Services_GET_URL = Api_Login + '/api/sales-Service/all/sales-Services';
export const Services_CREATE_URL = Api_Login + '/api/vehicle-service/create';
export const Services_DELETE_URL = Api_Login + '/api/vehicle-service/delete';
export const Services_GET_ID_URL = Api_Login + '/api/vehicle-service';
export const Services_UPDATE = Api_Login + '/api/vehicle-service/update';
export const Services_PASSWORD_RESET = Api_Login + '/api/sales-Service/password/update';

// CREATE =>  POST: add a new Service to the server
export function createService(Service) {
  return axios.post(Services_CREATE_URL, Service, {headers:{'Content-Type':'multipart/form-data'}})
}
// READ
export function getAllServices() {
  return axios.get(Services_GET_URL);
}

export function getServiceById(id) {
  // console.log(id);
  return axios.get(`${Services_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findServices(queryParams) {
  console.log(queryParams.sales_Service_fname);
  if (queryParams.sales_Service_fname === null || queryParams.sales_Service_fname === "") {
    return axios.post(Services_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Services_URL, { 
        "filter": [ {"sales_Service_fname" : queryParams.sales_Service_fname}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateService(Service,id) {
  console.log(id)
  console.log(Service)
  return axios.put(`${Services_UPDATE}/${id}`, Service, {headers:{'Content-Type':'multipart/form-data'}});
}

export function updateServicePassword(Service,id) {
  console.log(id)
  return axios.put(`${Services_PASSWORD_RESET}/${id}`, Service );
}

// UPDATE Status
export function updateStatusForServices(ids, status) {
}

// DELETE => delete the Service from the server
export function deleteService(ServiceId) {
  console.log(ServiceId)
  return axios.delete(`${Services_DELETE_URL}/${ServiceId}`);
}

// DELETE Services by ids
export function deleteServices(ids) {
}
