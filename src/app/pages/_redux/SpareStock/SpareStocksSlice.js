import {createSlice} from "@reduxjs/toolkit";

const initialSpareStocksState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  SpareStockForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const SpareStocksSlice = createSlice({
  name: "SpareStocks",
  initialState: initialSpareStocksState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getSpareStockById
    SpareStockFetched: (state, action) => {
      state.actionsLoading = false;
      state.SpareStockForEdit = action.payload.SpareStockForEdit;
      state.error = null;
    },
    // findSpareStocks
    SpareStocksFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createSpareStock
    SpareStockCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.SpareStock);
    },
    // updateSpareStock
    SpareStockUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.SpareStock.id) {
          return action.payload.SpareStock;
        }
        return entity;
      });
    },
    // deleteSpareStock
    SpareStockDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_SpareStock_id !== action.payload.sales_SpareStock_id);
    },
    // deleteSpareStocks
    SpareStocksDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // SpareStocksUpdateState
    SpareStocksStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
