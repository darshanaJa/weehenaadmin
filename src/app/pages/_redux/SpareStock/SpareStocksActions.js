import * as requestFromServer from "./SpareStocksCrud";
import {SpareStocksSlice, callTypes} from "./SpareStocksSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = SpareStocksSlice;

export const fetchSpareStocks = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findSpareStocks(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.SpareStocksFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find SpareStocks";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchSpareStock = id => dispatch => {
  if (!id) {
    return dispatch(actions.SpareStockFetched({ SpareStockForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSpareStockById(id)
    .then((res) => {
      let SpareStock = res.data
      dispatch(actions.SpareStockFetched({ SpareStockForEdit: SpareStock }));
    })
    .catch(error => {
      error.clientMessage = "Can't find SpareStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSpareStock = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSpareStock(id)
    .then((res) => {
      let SpareStockId = res.data;
      console.log(SpareStockId);
      dispatch(actions.SpareStockDeleted({ SpareStockId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)
        // SpareStocksCard("abcd")

    })
    .catch(error => {
      error.clientMessage = "Can't delete SpareStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createSpareStock = SpareStockForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSpareStock(SpareStockForCreation)

    .then((res) => {
      let SpareStock = res.data;
      console.log(SpareStock);
      dispatch(actions.SpareStockCreated({ SpareStock }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
     })
    
    .catch(error => {
      error.clientMessage = "Can't create SpareStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSpareStock = (SpareStock,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(SpareStock)
  return requestFromServer
    .updateSpareStock(SpareStock,id)
    .then((res) => {

      dispatch(actions.SpareStockUpdated({ SpareStock }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update SpareStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSpareStockPassword = (SpareStock,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(SpareStock)
  return requestFromServer
    .updateSpareStockPassword(SpareStock,id)
    .then((res) => {
      dispatch(actions.SpareStockUpdated({ SpareStock }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update SpareStock";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateSpareStocksStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForSpareStocks(ids, status)
    .then(() => {
      dispatch(actions.SpareStocksStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update SpareStocks status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSpareStocks = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSpareStocks(ids)
    .then(() => {
      dispatch(actions.SpareStocksDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete SpareStocks";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
