import axios from "axios";
import { Api_Login } from "../../../config/config";

export const SpareStocks_URL = Api_Login + '/api/spare-parts/search';
export const SpareStocks_GET_URL = Api_Login + '/api/sales-SpareStock/all/sales-SpareStocks';
export const SpareStocks_CREATE_URL = Api_Login + '/api/spare-parts/create';
export const SpareStocks_DELETE_URL = Api_Login + '/api/spare-parts/delete';
export const SpareStocks_GET_ID_URL = Api_Login + '/api/spare-parts';
export const SpareStocks_UPDATE = Api_Login + '/api/spare-parts/update';
export const SpareStocks_PASSWORD_RESET = Api_Login + '/api/sales-SpareStock/password/update';

// CREATE =>  POST: add a new SpareStock to the server
export function createSpareStock(SpareStock) {
  return axios.post(SpareStocks_CREATE_URL, SpareStock)
}
// READ
export function getAllSpareStocks() {
  return axios.get(SpareStocks_GET_URL);
}

export function getSpareStockById(id) {
  // console.log(id);
  return axios.get(`${SpareStocks_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findSpareStocks(queryParams) {
  console.log(queryParams.spare_part_name);
  if (queryParams.spare_part_name === null || queryParams.spare_part_name === "") {
    return axios.post(SpareStocks_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(SpareStocks_URL, { 
        "filter": [ {"spare_part_name" : queryParams.spare_part_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateSpareStock(SpareStock,id) {
  console.log(id)
  return axios.put(`${SpareStocks_UPDATE}/${id}`, SpareStock);
}

export function updateSpareStockPassword(SpareStock,id) {
  console.log(id)
  return axios.put(`${SpareStocks_PASSWORD_RESET}/${id}`, SpareStock );
}

// UPDATE Status
export function updateStatusForSpareStocks(ids, status) {
}

// DELETE => delete the SpareStock from the server
export function deleteSpareStock(SpareStockId) {
  console.log(SpareStockId)
  return axios.delete(`${SpareStocks_DELETE_URL}/${SpareStockId}`);
}

// DELETE SpareStocks by ids
export function deleteSpareStocks(ids) {
  // return axios.post(`${SpareStocks_URL}/deleteSpareStocks`, { ids });
}
