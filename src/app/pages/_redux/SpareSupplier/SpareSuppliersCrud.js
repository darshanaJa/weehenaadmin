import axios from "axios";
import { Api_Login } from "../../../config/config";

export const SpareSuppliers_URL = Api_Login + '/api/spart-supplier/search';
export const SpareSuppliers_GET_URL = Api_Login + '/api/sales-SpareSupplier/all/sales-SpareSuppliers';
export const SpareSuppliers_CREATE_URL = Api_Login + '/api/spart-supplier/create';
export const SpareSuppliers_DELETE_URL = Api_Login + '/api/spart-supplier/delete';
export const SpareSuppliers_GET_ID_URL = Api_Login + '/api/spart-supplier';
export const SpareSuppliers_UPDATE = Api_Login + '/api/spart-supplier/update';
export const SpareSuppliers_PASSWORD_RESET = Api_Login + '/api/sales-SpareSupplier/password/update';

// CREATE =>  POST: add a new SpareSupplier to the server
export function createSpareSupplier(SpareSupplier) {
  return axios.post(SpareSuppliers_CREATE_URL, SpareSupplier)
}
// READ
export function getAllSpareSuppliers() {
  return axios.get(SpareSuppliers_GET_URL);
}

export function getSpareSupplierById(id) {
  // console.log(id);
  return axios.get(`${SpareSuppliers_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findSpareSuppliers(queryParams) {
  console.log(queryParams.supplier_name);
  if (queryParams.supplier_name === null || queryParams.supplier_name === "") {
    return axios.post(SpareSuppliers_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(SpareSuppliers_URL, { 
        "filter": [ {"supplier_name" : queryParams.supplier_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateSpareSupplier(SpareSupplier,id) {
  console.log(id)
  return axios.put(`${SpareSuppliers_UPDATE}/${id}`, SpareSupplier);
}

export function updateSpareSupplierPassword(SpareSupplier,id) {
  console.log(id)
  return axios.put(`${SpareSuppliers_PASSWORD_RESET}/${id}`, SpareSupplier );
}

// UPDATE Status
export function updateStatusForSpareSuppliers(ids, status) {
}

// DELETE => delete the SpareSupplier from the server
export function deleteSpareSupplier(SpareSupplierId) {
  console.log(SpareSupplierId)
  return axios.delete(`${SpareSuppliers_DELETE_URL}/${SpareSupplierId}`);
}

// DELETE SpareSuppliers by ids
export function deleteSpareSuppliers(ids) {
}
