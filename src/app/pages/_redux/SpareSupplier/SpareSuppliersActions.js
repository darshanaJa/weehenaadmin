import * as requestFromServer from "./SpareSuppliersCrud";
import {SpareSuppliersSlice, callTypes} from "./SpareSuppliersSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = SpareSuppliersSlice;

export const fetchSpareSuppliers = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findSpareSuppliers(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.SpareSuppliersFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find SpareSuppliers";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchSpareSupplier = id => dispatch => {
  if (!id) {
    return dispatch(actions.SpareSupplierFetched({ SpareSupplierForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSpareSupplierById(id)
    .then((res) => {
      let SpareSupplier = res.data; 
      dispatch(actions.SpareSupplierFetched({ SpareSupplierForEdit: SpareSupplier }));
    })
    .catch(error => {
      error.clientMessage = "Can't find SpareSupplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSpareSupplier = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSpareSupplier(id)
    .then((res) => {
      let SpareSupplierId = res.data;
      console.log(SpareSupplierId);
      dispatch(actions.SpareSupplierDeleted({ SpareSupplierId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete SpareSupplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createSpareSupplier = SpareSupplierForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSpareSupplier(SpareSupplierForCreation)

    .then((res) => {
      let SpareSupplier = res.data;
      console.log(SpareSupplier);
      dispatch(actions.SpareSupplierCreated({ SpareSupplier }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create SpareSupplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSpareSupplier = (SpareSupplier,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(SpareSupplier)
  return requestFromServer
    .updateSpareSupplier(SpareSupplier,id)
    .then((res) => {

      dispatch(actions.SpareSupplierUpdated({ SpareSupplier }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update SpareSupplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSpareSupplierPassword = (SpareSupplier,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(SpareSupplier)
  return requestFromServer
    .updateSpareSupplierPassword(SpareSupplier,id)
    .then((res) => {
      dispatch(actions.SpareSupplierUpdated({ SpareSupplier }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update SpareSupplier";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateSpareSuppliersStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForSpareSuppliers(ids, status)
    .then(() => {
      dispatch(actions.SpareSuppliersStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update SpareSuppliers status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSpareSuppliers = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSpareSuppliers(ids)
    .then(() => {
      dispatch(actions.SpareSuppliersDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete SpareSuppliers";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
