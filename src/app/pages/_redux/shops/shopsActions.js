import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./shopsCrud";
import {shopsSlice, callTypes} from "./shopsSlice";

// notify
const {actions} = shopsSlice;

export const fetchShops = queryParams => dispatch => {
  console.log(queryParams);
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findShops(queryParams)
    .then((res) => {
      let data = res.data;
      dispatch(actions.ShopsFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Shops";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchShop = id => dispatch => {
  if (!id) {
    return dispatch(actions.ShopFetched({ ShopForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getShopById(id)
      .then((res) => {
        //notify()
      let Shop = res.data; 
      console.log(Shop)
      dispatch(actions.ShopFetched({ ShopForEdit: Shop }));
     })
    .catch(error => {
      error.clientMessage = "Can't find Shop";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteShop = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteShop(id)
    .then(res => {
      dispatch(actions.ShopDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete Shop";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      
    });
};

export const createShop = ShopForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));

  return requestFromServer

    .createShop(ShopForCreation)
      .then((res) => {
        let Shop = res.data;
        console.log(Shop);
        dispatch(actions.ShopCreated({ Shop })); 
        let msg = res.data.message;
        console.log(msg)
        notify(msg)
    })
    .catch(error => {
      alert("Can't create Shop");
      error.clientMessage = "Can't create Shop";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateShop = (Shop,id) => dispatch => {
  console.log(id)
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateShop(Shop,id)
    // .then(() => {
    //   dispatch(actions.ShopUpdated({ Shop }));
    .then((res) => {
      let Shop = res.data;
      dispatch(actions.ShopUpdated({ Shop })); 
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      //notify();
      
      
    })
    .catch(error => {
      //notify();
      error.clientMessage = "Can't update Shop";
      alert("Can't update Shop");
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

// notifyError

export const updateShopsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForShops(ids, status)
    .then(() => {
      dispatch(actions.ShopsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Shops status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteShops = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteShops(ids)
    .then(() => {
      dispatch(actions.ShopsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Shops";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
