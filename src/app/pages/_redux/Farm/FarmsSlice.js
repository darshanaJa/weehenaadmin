import {createSlice} from "@reduxjs/toolkit";

const initialFarmsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  FarmForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const FarmsSlice = createSlice({
  name: "Farms",
  initialState: initialFarmsState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getFarmById
    FarmFetched: (state, action) => {
      state.actionsLoading = false;
      state.FarmForEdit = action.payload.FarmForEdit;
      state.error = null;
    },
    // findFarms
    FarmsFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createFarm
    FarmCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Farm);
    },
    // updateFarm
    FarmUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Farm.id) {
          return action.payload.Farm;
        }
        return entity;
      });
    },
    // deleteFarm
    FarmDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Farm_id !== action.payload.sales_Farm_id);
    },
    // deleteFarms
    FarmsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // FarmsUpdateState
    FarmsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
