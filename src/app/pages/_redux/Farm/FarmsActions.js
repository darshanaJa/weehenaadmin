import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./FarmsCrud";
import {FarmsSlice, callTypes} from "./FarmsSlice";

const {actions} = FarmsSlice;

export const fetchFarms = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findFarms(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.FarmsFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Farms";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchFarm = id => dispatch => {
  if (!id) {
    return dispatch(actions.FarmFetched({ FarmForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getFarmById(id)
    .then((res) => {
      let Farm = res.data; 
      dispatch(actions.FarmFetched({ FarmForEdit: Farm }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Farm";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteFarm = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteFarm(id)
    .then((res) => {
      let FarmId = res.data;
      console.log(FarmId);
      dispatch(actions.FarmDeleted({ FarmId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete Farm";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createFarm = FarmForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createFarm(FarmForCreation)
    .then((res) => {
      let Farm = res.data;
      console.log(Farm);
      dispatch(actions.FarmCreated({ Farm }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Farm";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateFarm = (Farm,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Farm)
  return requestFromServer
    .updateFarm(Farm,id)
    .then((res) => {
      dispatch(actions.FarmUpdated({ Farm }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Farm";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateFarmPassword = (Farm,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Farm)
  return requestFromServer
    .updateFarmPassword(Farm,id)
    .then(() => {
      dispatch(actions.FarmUpdated({ Farm }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Farm";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateFarmsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForFarms(ids, status)
    .then(() => {
      dispatch(actions.FarmsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Farms status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteFarms = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteFarms(ids)
    .then(() => {
      dispatch(actions.FarmsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Farms";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
