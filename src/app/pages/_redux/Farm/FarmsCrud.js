import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Farms_URL = Api_Login + '/api/buyback-farm/search';
export const Farms_CREATE_URL = Api_Login + '/api/buyback-farm/register';
export const Farms_DELETE_URL = Api_Login + '/api/buyback-farm/delete';
export const Farms_GET_ID_URL = Api_Login + '/api/buyback-farm';
export const Farms_UPDATE = Api_Login + '/api/buyback-farm/update';
export const Farms_PASSWORD_RESET = Api_Login + '/api/sales-Farm/password/update';

// CREATE =>  POST: add a new Farm to the server
export function createFarm(Farm) {
  return axios.post(Farms_CREATE_URL, Farm)
}
// READ
export function getAllFarms() {
  // return axios.get(Farms_URL);
}

export function getFarmById(id) {
  // console.log(id);
  return axios.get(`${Farms_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findFarms(queryParams) {
  console.log(queryParams.buyback_name);
  if (queryParams.buyback_name === null || queryParams.buyback_name === "") {
    return axios.post(Farms_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Farms_URL, { 
        "filter": [ {"buyback_name" : queryParams.buyback_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateFarm(Farm,id) {
  console.log(id)
  return axios.put(`${Farms_UPDATE}/${id}`, Farm );
}

export function updateFarmPassword(Farm,id) {
  console.log(id)
  return axios.put(`${Farms_PASSWORD_RESET}/${id}`, Farm );
}

// UPDATE Status
export function updateStatusForFarms(ids, status) {
}

// DELETE => delete the Farm from the server
export function deleteFarm(FarmId) {
  console.log(FarmId)
  return axios.delete(`${Farms_DELETE_URL}/${FarmId}`);
}

// DELETE Farms by ids
export function deleteFarms(ids) {
}
