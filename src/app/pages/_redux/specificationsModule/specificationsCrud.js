import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Items_URL = Api_Login + "/api/access-modules/search";
export const Items_CREATE_URL = Api_Login + "/api/access-modules/add";
export const Items_DELETE_URL = Api_Login + "/api/saleticket-item/delete";
export const Items_GETBYID_URL = Api_Login + "/api/saleticket-item";
export const Items_UPDATE_URL = Api_Login + "/api/access-modules/update";


// CREATE =>  POST: add a new specifications to the server
export function createSpecification(specification) {
  return axios.post(Items_CREATE_URL, specification);
}

// READ
// Server should return filtered specifications by productId
export function getAllProductSpecificationsByProductId(productId) {
  // return axios.get(`${SPECIFICATIONS_URL}?productId=${productId}`);
}

export function getSpecificationById(specificationId) {
  return axios.get(`${Items_GETBYID_URL}/${specificationId}`);
}

// Server should return sorted/filtered specifications and merge with items from state
// TODO: Change your URL to REAL API, right now URL is 'api/specificationsfind/{productId}'. Should be 'api/specifications/find/{productId}'!!!
export function findSpecifications(queryParams, productId) {
  console.log(productId)

  return axios.post(Items_URL , {
    "filter": [{"status": "1"}],
    "sort": "DESC",
    "limit": queryParams.pageSize, 
    "skip":(queryParams.pageNumber-1)*queryParams.pageSize
});
}

// UPDATE => PUT: update the specification
export function updateSpecification(specification,id) {
  console.log(id)
  return axios.put(`${Items_UPDATE_URL}/${id}`, specification);
}

// DELETE => delete the specification
export function deleteSpecification(specificationId) {
  return axios.delete(`${Items_DELETE_URL}/${specificationId}`);
}

// DELETE specifications by ids
export function deleteSpecifications(ids) {
  // return axios.post(`${SPECIFICATIONS_URL}/deleteSpecifications`, { ids });
}
