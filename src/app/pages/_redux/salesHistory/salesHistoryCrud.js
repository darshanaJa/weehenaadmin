import axios from "axios";

export const SalesHistory_URL = "api/SalesHistory";

// CREATE =>  POST: add a new SaleHistory to the server
export function createSaleHistory(SaleHistory) {
  return axios.post(SalesHistory_URL, { SaleHistory });
}

// READ
export function getAllSalesHistory() {
  return axios.get(SalesHistory_URL);
}

export function getSaleHistoryById(SaleHistoryId) {
  return axios.get(`${SalesHistory_URL}/${SaleHistoryId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findSalesHistory(queryParams) {
  return axios.post(`${SalesHistory_URL}/find`, { queryParams });
}

// UPDATE => PUT: update the procuct on the server
export function updateSaleHistory(SaleHistory) {
  return axios.put(`${SalesHistory_URL}/${SaleHistory.id}`, { SaleHistory });
}

// UPDATE Status
export function updateStatusForSalesHistory(ids, status) {
  return axios.post(`${SalesHistory_URL}/updateStatusForSalesHistory`, {
    ids,
    status
  });
}

// DELETE => delete the SaleHistory from the server
export function deleteSaleHistory(SaleHistoryId) {
  return axios.delete(`${SalesHistory_URL}/${SaleHistoryId}`);
}

// DELETE SalesHistory by ids
export function deleteSalesHistory(ids) {
  return axios.post(`${SalesHistory_URL}/deleteSalesHistory`, { ids });
}
