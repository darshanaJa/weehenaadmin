import {createSlice} from "@reduxjs/toolkit";

const initialSalesHistoryState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  SaleHistoryForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const salesHistorySlice = createSlice({
  name: "SalesHistory",
  initialState: initialSalesHistoryState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getSaleHistoryById
    SaleHistoryFetched: (state, action) => {
      state.actionsLoading = false;
      state.SaleHistoryForEdit = action.payload.SaleHistoryForEdit;
      state.error = null;
    },
    // findSalesHistory
    SalesHistoryFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createSaleHistory
    SaleHistoryCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.SaleHistory);
    },
    // updateSaleHistory
    SaleHistoryUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.SaleHistory.id) {
          return action.payload.SaleHistory;
        }
        return entity;
      });
    },
    // deleteSaleHistory
    SaleHistoryDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.id !== action.payload.id);
    },
    // deleteSalesHistory
    SalesHistoryDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // SalesHistoryUpdateState
    SalesHistoryStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
