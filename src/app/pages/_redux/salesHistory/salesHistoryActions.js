import { notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./salesHistoryCrud";
import {salesHistorySlice, callTypes} from "./salesHistorySlice";

const {actions} = salesHistorySlice;

export const fetchSalesHistory = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findSalesHistory(queryParams)
    .then(response => {
      const { totalCount, entities } = response.data;
      dispatch(actions.SalesHistoryFetched({ totalCount, entities }));
    })
    .catch(error => {
      error.clientMessage = "Can't find SalesHistory";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchSaleHistory = id => dispatch => {
  if (!id) {
    return dispatch(actions.SaleHistoryFetched({ SaleHistoryForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSaleHistoryById(id)
    .then(response => {
      const SaleHistory = response.data;
      dispatch(actions.SaleHistoryFetched({ SaleHistoryForEdit: SaleHistory }));
    })
    .catch(error => {
      error.clientMessage = "Can't find SaleHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSaleHistory = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSaleHistory(id)
    .then(response => {
      dispatch(actions.SaleHistoryDeleted({ id }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete SaleHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const createSaleHistory = SaleHistoryForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSaleHistory(SaleHistoryForCreation)
    .then(response => {
      const { SaleHistory } = response.data;
      dispatch(actions.SaleHistoryCreated({ SaleHistory }));
    })
    .catch(error => {
      error.clientMessage = "Can't create SaleHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSaleHistory = SaleHistory => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateSaleHistory(SaleHistory)
    .then(() => {
      dispatch(actions.SaleHistoryUpdated({ SaleHistory }));
    })
    .catch(error => {
      error.clientMessage = "Can't update SaleHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};
// notifyError

export const updateSalesHistoryStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForSalesHistory(ids, status)
    .then(() => {
      dispatch(actions.SalesHistoryStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update SalesHistory status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSalesHistory = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSalesHistory(ids)
    .then(() => {
      dispatch(actions.SalesHistoryDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete SalesHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
