import axios from "axios";
import { Api_Login } from "../../../config/config";

export const Sparegrns_URL = Api_Login + '/api/spart-grn/search';
export const Sparegrns_GET_URL = Api_Login + '/api/sales-Sparegrn/all/sales-Sparegrns';
export const Sparegrns_CREATE_URL = Api_Login + '/api/spart-grn/add-grn';
export const Sparegrns_DELETE_URL = Api_Login + '/api/sales-Sparegrn/delete';
export const Sparegrns_GET_ID_URL = Api_Login + '/api/sales-Sparegrn/get-by-id';
export const Sparegrns_UPDATE = Api_Login + '/api/sales-Sparegrn/update';
export const Sparegrns_PASSWORD_RESET = Api_Login + '/api/sales-Sparegrn/password/update';

// CREATE =>  POST: add a new Sparegrn to the server
export function createSparegrn(Sparegrn) {
  return axios.post(Sparegrns_CREATE_URL, Sparegrn)
}
// READ
export function getAllSparegrns() {
  return axios.get(Sparegrns_GET_URL);
}

export function getSparegrnById(id) {
  // console.log(id);
  return axios.get(`${Sparegrns_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findSparegrns(queryParams) {
  console.log(queryParams.sales_Sparegrn_fname);
  if (queryParams.sales_Sparegrn_fname === null || queryParams.sales_Sparegrn_fname === "") {
    return axios.post(Sparegrns_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(Sparegrns_URL, { 
        "filter": [ {"sales_Sparegrn_fname" : queryParams.sales_Sparegrn_fname}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateSparegrn(Sparegrn,id) {
  console.log(id)
  return axios.put(`${Sparegrns_UPDATE}/${id}`, Sparegrn , {headers:{'Content-Type':'multipart/form-data'}} );
}

export function updateSparegrnPassword(Sparegrn,id) {
  console.log(id)
  return axios.put(`${Sparegrns_PASSWORD_RESET}/${id}`, Sparegrn );
}

// UPDATE Status
export function updateStatusForSparegrns(ids, status) {
}

// DELETE => delete the Sparegrn from the server
export function deleteSparegrn(SparegrnId) {
  console.log(SparegrnId)
  return axios.delete(`${Sparegrns_DELETE_URL}/${SparegrnId}`);
}

// DELETE Sparegrns by ids
export function deleteSparegrns(ids) {
  // return axios.post(`${Sparegrns_URL}/deleteSparegrns`, { ids });
}
