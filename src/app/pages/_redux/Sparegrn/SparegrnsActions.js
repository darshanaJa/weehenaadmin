import * as requestFromServer from "./SparegrnsCrud";
import {SparegrnsSlice, callTypes} from "./SparegrnsSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = SparegrnsSlice;

export const fetchSparegrns = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findSparegrns(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.SparegrnsFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Sparegrns";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchSparegrn = id => dispatch => {
  if (!id) {
    return dispatch(actions.SparegrnFetched({ SparegrnForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSparegrnById(id)
    // .then(response => {
    //   const Sparegrn = response.data;
    .then((res) => {
      let Sparegrn = res.data.sales_Sparegrn; 
      // console.log(Sparegrn)
      dispatch(actions.SparegrnFetched({ SparegrnForEdit: Sparegrn }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Sparegrn";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSparegrn = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSparegrn(id)
    // .then(response => {
    //   dispatch(actions.SparegrnDeleted({ id }));
    .then((res) => {
      let SparegrnId = res.data;
      console.log(SparegrnId);
      dispatch(actions.SparegrnDeleted({ SparegrnId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)
        // SparegrnsCard("abcd")

    })
    .catch(error => {
      error.clientMessage = "Can't delete Sparegrn";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
      // SparegrnsCard("abcd")
    });
};

export const createSparegrn = SparegrnForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSparegrn(SparegrnForCreation)
    // .then(response => {
    //   const { Sparegrn } = response.data;
    //   dispatch(actions.SparegrnCreated({ Sparegrn }));
    // })

    .then((res) => {
      let Sparegrn = res.data;
      console.log(Sparegrn);
      dispatch(actions.SparegrnCreated({ Sparegrn }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      // setMessage("abcd")
      // dispatch(actions.SparegrnsFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })
    
    .catch(error => {
      error.clientMessage = "Can't create Sparegrn";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSparegrn = (Sparegrn,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Sparegrn)
  return requestFromServer
    .updateSparegrn(Sparegrn,id)
    .then((res) => {

      dispatch(actions.SparegrnUpdated({ Sparegrn }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Sparegrn";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSparegrnPassword = (Sparegrn,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Sparegrn)
  return requestFromServer
    .updateSparegrnPassword(Sparegrn,id)
    .then((res) => {
      dispatch(actions.SparegrnUpdated({ Sparegrn }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Sparegrn";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateSparegrnsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForSparegrns(ids, status)
    .then(() => {
      dispatch(actions.SparegrnsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Sparegrns status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSparegrns = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSparegrns(ids)
    .then(() => {
      dispatch(actions.SparegrnsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Sparegrns";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
