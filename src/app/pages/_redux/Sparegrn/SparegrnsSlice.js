import {createSlice} from "@reduxjs/toolkit";

const initialSparegrnsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  SparegrnForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const SparegrnsSlice = createSlice({
  name: "Sparegrns",
  initialState: initialSparegrnsState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getSparegrnById
    SparegrnFetched: (state, action) => {
      state.actionsLoading = false;
      state.SparegrnForEdit = action.payload.SparegrnForEdit;
      state.error = null;
    },
    // findSparegrns
    SparegrnsFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createSparegrn
    SparegrnCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Sparegrn);
    },
    // updateSparegrn
    SparegrnUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Sparegrn.id) {
          return action.payload.Sparegrn;
        }
        return entity;
      });
    },
    // deleteSparegrn
    SparegrnDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Sparegrn_id !== action.payload.sales_Sparegrn_id);
    },
    // deleteSparegrns
    SparegrnsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // SparegrnsUpdateState
    SparegrnsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
