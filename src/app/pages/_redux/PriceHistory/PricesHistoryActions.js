import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./PricesHistoryCrud";
import {PricesHistorySlice, callTypes} from "./PricesHistorySlice";

const {actions} = PricesHistorySlice;

export const fetchPricesHistory = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findPricesHistory(queryParams)
    .then((res) => {
      let data = res.data;
      console.log(data)
      dispatch(actions.PricesHistoryFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));
    })
    .catch(error => {
      error.clientMessage = "Can't find PricesHistory";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchPriceHistory = id => dispatch => {
  if (!id) {
    return dispatch(actions.PriceHistoryFetched({ PriceHistoryForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getPriceHistoryById(id)
    .then(response => {
      const PriceHistory = response.data;
      dispatch(actions.PriceHistoryFetched({ PriceHistoryForEdit: PriceHistory }));
    })
    .catch(error => {
      error.clientMessage = "Can't find PriceHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
// notify
export const deletePriceHistory = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deletePriceHistory(id)
    .then(res => {
      dispatch(actions.PriceHistoryDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete PriceHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createPriceHistory = PriceHistoryForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createPriceHistory(PriceHistoryForCreation)
    .then((res) => {
      let PriceHistory = res.data;
      console.log(PriceHistory);
      dispatch(actions.PriceHistoryCreated({ PriceHistory })); 
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't create PriceHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updatePriceHistory = PriceHistory => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updatePriceHistory(PriceHistory)
    .then((res) => {
      dispatch(actions.PriceHistoryUpdated({ PriceHistory }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update PriceHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};
// notifyError

export const updatePricesHistoryStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForPricesHistory(ids, status)
    .then(() => {
      dispatch(actions.PricesHistoryStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update PricesHistory status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deletePricesHistory = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deletePricesHistory(ids)
    .then(() => {
      dispatch(actions.PricesHistoryDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete PricesHistory";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
