import axios from "axios";
import { Api_Login } from "../../../config/config";

export const PricesHistory_URL = "api/PricesHistory";
export const PricesHistory_URL_FIND = Api_Login + '/api/stkprice-history/search';
export const PricesHistory_URL_DELETE = Api_Login + '/api/stkprice-history/delete';
export const PricesHistory_URL_CREATE = Api_Login + '/api/stkprice-history/register';

// CREATE =>  POST: add a new PriceHistory to the server
export function createPriceHistory(PriceHistory) {
  return axios.post(PricesHistory_URL_CREATE, PriceHistory);
}

// READ
export function getAllPricesHistory() {
  return axios.get(PricesHistory_URL);
}

export function getPriceHistoryById(PriceHistoryId) {
  return axios.get(`${PricesHistory_URL}/${PriceHistoryId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findPricesHistory(queryParams) {
  console.log(queryParams.stk_history_id);
  if (queryParams.stk_history_id === null || queryParams.stk_history_id === "") {
  return axios.post(PricesHistory_URL_FIND , {
    "filter": [{"status": "1" }],
    "sort": "DESC",
    "limit": queryParams.pageSize, 
    "skip":(queryParams.pageNumber-1)*queryParams.pageSize
});
  } else {
    return axios.post(PricesHistory_URL_FIND , {
      "filter": [ {"stk_history_id" : queryParams.stk_history_id}],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
  }
}

// UPDATE => PUT: update the procuct on the server
export function updatePriceHistory(PriceHistory) {
  return axios.put(`${PricesHistory_URL}/${PriceHistory.id}`, { PriceHistory });
}

// UPDATE Status
export function updateStatusForPricesHistory(ids, status) {
  return axios.post(`${PricesHistory_URL}/updateStatusForPricesHistory`, {
    ids,
    status
  });
}

// DELETE => delete the PriceHistory from the server
export function deletePriceHistory(PriceHistoryId) {
  return axios.delete(`${PricesHistory_URL_DELETE}/${PriceHistoryId}`);
}

// DELETE PricesHistory by ids
export function deletePricesHistory(ids) {
  return axios.post(`${PricesHistory_URL}/deletePricesHistory`, { ids });
}
