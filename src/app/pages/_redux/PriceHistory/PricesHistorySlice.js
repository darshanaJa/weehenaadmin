import {createSlice} from "@reduxjs/toolkit";

const initialPricesHistoryState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  PriceHistoryForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const PricesHistorySlice = createSlice({
  name: "PricesHistory",
  initialState: initialPricesHistoryState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getPriceHistoryById
    PriceHistoryFetched: (state, action) => {
      state.actionsLoading = false;
      state.PriceHistoryForEdit = action.payload.PriceHistoryForEdit;
      state.error = null;
    },
    // findPricesHistory
    PricesHistoryFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createPriceHistory
    PriceHistoryCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.PriceHistory);
    },
    // updatePriceHistory
    PriceHistoryUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.PriceHistory.id) {
          return action.payload.PriceHistory;
        }
        return entity;
      });
    },
    // deletePriceHistory
    PriceHistoryDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.id !== action.payload.id);
    },
    // deletePricesHistory
    PricesHistoryDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // PricesHistoryUpdateState
    PricesHistoryStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
