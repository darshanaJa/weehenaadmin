import {createSlice} from "@reduxjs/toolkit";

const initialCreditsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  CreditForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const CreditsSlice = createSlice({
  name: "Credits",
  initialState: initialCreditsState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getCreditById
    CreditFetched: (state, action) => {
      state.actionsLoading = false;
      state.CreditForEdit = action.payload.CreditForEdit;
      state.error = null;
    },
    // findCredits
    CreditsFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createCredit
    CreditCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.Credit);
    },
    // updateCredit
    CreditUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.Credit.id) {
          return action.payload.Credit;
        }
        return entity;
      });
    },
    // deleteCredit
    CreditDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_Credit_id !== action.payload.sales_Credit_id);
    },
    // deleteCredits
    CreditsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // CreditsUpdateState
    CreditsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
