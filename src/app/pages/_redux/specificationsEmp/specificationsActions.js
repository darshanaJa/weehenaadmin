import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./specificationsCrud";
import {specificationsSlice, callTypes} from "./specificationsSlice";

const {actions} = specificationsSlice;

export const fetchSpecifications = (queryParams, productId) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));

  return requestFromServer
    .findSpecifications(queryParams, productId)
    .then((res) => {
      let data = res.data;
      console.log(data)
      dispatch(actions.specificationsFetched({ totalCount:  data.data.pagination.total, entities: data.data.results }));

    })
    .catch(error => {
      error.clientMessage = "Can't find specifications";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchSpecification = id => dispatch => {
  if (!id) {
    return dispatch(
      actions.specificationFetched({ specificationForEdit: undefined })
    );
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getSpecificationById(id)
    .then(response => {
      const specification = response.data;
      dispatch(
        actions.specificationFetched({ specificationForEdit: specification })
      );
    })
    .catch(error => {
      error.clientMessage = "Can't find specification";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteSpecification = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSpecification(id)
    .then(res => {
      dispatch(actions.specificationDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete specification";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createSpecification = specificationForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createSpecification(specificationForCreation)
    .then(res => {
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      const { specification } = res.data;
      dispatch(actions.specificationCreated({ specification }));
    })
    .catch(error => {
      error.clientMessage = "Can't create specification";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateSpecification = specification => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateSpecification(specification)
    .then((res) => {
      dispatch(actions.specificationUpdated({ specification }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update specification";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const deleteSpecifications = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteSpecifications(ids)
    .then(() => {
      dispatch(actions.specificationsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete specifications";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
