import {createSlice} from "@reduxjs/toolkit";

const initialGateKeepersState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  GateKeeperForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const GateKeepersSlice = createSlice({
  name: "GateKeepers",
  initialState: initialGateKeepersState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getGateKeeperById
    GateKeeperFetched: (state, action) => {
      state.actionsLoading = false;
      state.GateKeeperForEdit = action.payload.GateKeeperForEdit;
      state.error = null;
    },
    // findGateKeepers
    GateKeepersFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createGateKeeper
    GateKeeperCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.GateKeeper);
    },
    // updateGateKeeper
    GateKeeperUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.GateKeeper.id) {
          return action.payload.GateKeeper;
        }
        return entity;
      });
    },
    // deleteGateKeeper
    GateKeeperDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_GateKeeper_id !== action.payload.sales_GateKeeper_id);
    },
    // deleteGateKeepers
    GateKeepersDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // GateKeepersUpdateState
    GateKeepersStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
