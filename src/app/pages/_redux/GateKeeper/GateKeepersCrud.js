import axios from "axios";
import { Api_Login } from "../../../config/config";

export const GateKeepers_URL = Api_Login + '/api/gate-keeper/search';
export const GateKeepers_GET_URL = Api_Login + '/api/sales-GateKeeper/all/sales-GateKeepers';
export const GateKeepers_CREATE_URL = Api_Login + '/api/gate-keeper/register';
export const GateKeepers_DELETE_URL = Api_Login + '/api/sales-GateKeeper/delete';
export const GateKeepers_GET_ID_URL = Api_Login + '/api/gate-keeper';
export const GateKeepers_UPDATE = Api_Login + '/api/gate-keeper/update';
export const GateKeepers_PASSWORD_RESET = Api_Login + '/api/sales-GateKeeper/password/update';

// CREATE =>  POST: add a new GateKeeper to the server
export function createGateKeeper(GateKeeper) {
  return axios.post(GateKeepers_CREATE_URL, GateKeeper, {headers:{'Content-Type':'multipart/form-data'}})
}
// READ
export function getAllGateKeepers() {
  return axios.get(GateKeepers_GET_URL);
}

export function getGateKeeperById(id) {
  // console.log(id);
  return axios.get(`${GateKeepers_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findGateKeepers(queryParams) {
  console.log(queryParams.sales_GateKeeper_fname);
  if (queryParams.sales_GateKeeper_fname === null || queryParams.sales_GateKeeper_fname === "") {
    return axios.post(GateKeepers_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(GateKeepers_URL, { 
        "filter": [ {"sales_GateKeeper_fname" : queryParams.sales_GateKeeper_fname}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateGateKeeper(GateKeeper,id) {
  console.log(id)
  return axios.put(`${GateKeepers_UPDATE}/${id}`, GateKeeper , {headers:{'Content-Type':'multipart/form-data'}} );
}

export function updateGateKeeperPassword(GateKeeper,id) {
  console.log(id)
  return axios.put(`${GateKeepers_PASSWORD_RESET}/${id}`, GateKeeper );
}

// UPDATE Status
export function updateStatusForGateKeepers(ids, status) {
 }

// DELETE => delete the GateKeeper from the server
export function deleteGateKeeper(GateKeeperId) {
  console.log(GateKeeperId)
  return axios.delete(`${GateKeepers_DELETE_URL}/${GateKeeperId}`);
}

// DELETE GateKeepers by ids
export function deleteGateKeepers(ids) {
  // return axios.post(`${GateKeepers_URL}/deleteGateKeepers`, { ids });
}
