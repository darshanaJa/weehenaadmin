import * as requestFromServer from "./GateKeepersCrud";
import {GateKeepersSlice, callTypes} from "./GateKeepersSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = GateKeepersSlice;

export const fetchGateKeepers = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findGateKeepers(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.GateKeepersFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find GateKeepers";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchGateKeeper = id => dispatch => {
  if (!id) {
    return dispatch(actions.GateKeeperFetched({ GateKeeperForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getGateKeeperById(id)
    .then((res) => {
      let GateKeeper = res.data.gate_keeper; 
      console.log(res.data.gate_keeper)
      dispatch(actions.GateKeeperFetched({ GateKeeperForEdit: GateKeeper }));
    })
    .catch(error => {
      error.clientMessage = "Can't find GateKeeper";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteGateKeeper = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteGateKeeper(id)
    .then((res) => {
      let GateKeeperId = res.data;
      console.log(GateKeeperId);
      dispatch(actions.GateKeeperDeleted({ GateKeeperId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete GateKeeper";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createGateKeeper = GateKeeperForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createGateKeeper(GateKeeperForCreation)

    .then((res) => {
      let GateKeeper = res.data;
      console.log(GateKeeper);
      dispatch(actions.GateKeeperCreated({ GateKeeper }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create GateKeeper";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateGateKeeper = (GateKeeper,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(GateKeeper)
  return requestFromServer
    .updateGateKeeper(GateKeeper,id)
    .then((res) => {

      dispatch(actions.GateKeeperUpdated({ GateKeeper }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update GateKeeper";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateGateKeeperPassword = (GateKeeper,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(GateKeeper)
  return requestFromServer
    .updateGateKeeperPassword(GateKeeper,id)
    .then((res) => {
      dispatch(actions.GateKeeperUpdated({ GateKeeper }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update GateKeeper";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateGateKeepersStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForGateKeepers(ids, status)
    .then(() => {
      dispatch(actions.GateKeepersStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update GateKeepers status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteGateKeepers = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteGateKeepers(ids)
    .then(() => {
      dispatch(actions.GateKeepersDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete GateKeepers";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
