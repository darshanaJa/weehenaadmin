import {createSlice} from "@reduxjs/toolkit";

const initialMainStocksState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  MainStockForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const MainStocksSlice = createSlice({
  name: "MainStocks",
  initialState: initialMainStocksState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getMainStockById
    MainStockFetched: (state, action) => {
      state.actionsLoading = false;
      state.MainStockForEdit = action.payload.MainStockForEdit;
      state.error = null;
    },
    // findMainStocks
    MainStocksFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createMainStock
    MainStockCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.MainStock);
    },
    // updateMainStock
    MainStockUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.MainStock.id) {
          return action.payload.MainStock;
        }
        return entity;
      });
    },
    // deleteMainStock
    MainStockDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.id !== action.payload.id);
    },
    // deleteMainStocks
    MainStocksDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // MainStocksUpdateState
    MainStocksStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
