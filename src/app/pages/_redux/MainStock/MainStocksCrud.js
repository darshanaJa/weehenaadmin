import axios from "axios";
import { Api_Login } from "../../../config/config";

// export const MainStocks_URL = Api_Login + "/api/main-stock/search";
export const MainStocks_URL = Api_Login + "/api/main-stock/search";
export const MainStocks_URL_GET = Api_Login + "/api/main-stock/all?Page=1&limit=10";

export const MainStocks_URL_CREATE = Api_Login + "/api/main-stock/register";
export const MainStocks_URL_DELETE = Api_Login + "/api/main-stock/delete";
export const MainStocks_URL_GETBYID = Api_Login + "/api/main-stock/getby";
export const MainStocks_URL_UPDATE = Api_Login + "/api/main-stock/update";

// CREATE =>  POST: add a new MainStock to the server
export function createMainStock(MainStock) {
  return axios.post(MainStocks_URL_CREATE, MainStock, {headers:{'Content-Type': 'application/json'}});
}

// READ
export function getAllMainStocks() {
  return axios.get(MainStocks_URL_GET);
}

export function getMainStockById(MainStockId) {
  return axios.get(`${MainStocks_URL_GETBYID}/${MainStockId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findMainStocks(queryParams) {
  console.log(queryParams.item_name);
  if (queryParams.item_name === null || queryParams.item_name === "") {
    return axios.post(MainStocks_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(MainStocks_URL, { 
        "filter": [ {"item_name" : queryParams.item_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateMainStock(MainStock) {
  return axios.put(`${MainStocks_URL_UPDATE}/${MainStock.item_id}`, MainStock);
}

// UPDATE Status
export function updateStatusForMainStocks(ids, status) {

}

// DELETE => delete the MainStock from the server
export function deleteMainStock(MainStockId) {
  return axios.delete(`${MainStocks_URL_DELETE}/${MainStockId}`);
}

// DELETE MainStocks by ids
export function deleteMainStocks(ids) {
  // return axios.post(`${MainStocks_URL}/deleteMainStocks`, { ids });
}
