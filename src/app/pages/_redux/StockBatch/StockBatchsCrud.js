import axios from "axios";
import { Api_Login } from "../../../config/config";

export const StockBatchs_URL = Api_Login + "/api/stock-batch/search";
export const StockBatchs_URL_DELETE = Api_Login + "/api/stock-batch/delete";
export const StockBatchs_URL_CREATE = Api_Login + "/api/stock-batch/create";
export const StockBatchs_URL_GETBYID = Api_Login + "/api/stock-batch";
export const StockBatchs_URL_UPDATE = Api_Login + "/api/stock-batch/update";

// CREATE =>  POST: add a new StockBatch to the server
export function createStockBatch(StockBatch) {
  return axios.post(StockBatchs_URL_CREATE, StockBatch);
}

// READ
export function getAllStockBatchs() {
  return axios.get(StockBatchs_URL);
}

export function getStockBatchById(StockBatchId) {
  return axios.get(`${StockBatchs_URL_GETBYID}/${StockBatchId}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findStockBatchs(queryParams) {
  console.log(queryParams.batch_id);
  if (queryParams.batch_id === null || queryParams.batch_id === "") {
    return axios.post(StockBatchs_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(StockBatchs_URL, { 
        "filter": [ {"batch_id" : queryParams.batch_id}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateStockBatch(StockBatch) {
  return axios.put(`${StockBatchs_URL_UPDATE}/${StockBatch.batch_id}`, StockBatch);
}

// UPDATE Status
export function updateStatusForStockBatchs(ids, status) {
}

// DELETE => delete the StockBatch from the server
export function deleteStockBatch(StockBatchId) {
  return axios.delete(`${StockBatchs_URL_DELETE}/${StockBatchId}`);
}

// DELETE StockBatchs by ids
export function deleteStockBatchs(ids) {
  // return axios.post(`${StockBatchs_URL}/deleteStockBatchs`, { ids });
}
