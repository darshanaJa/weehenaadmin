import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./StockBatchsCrud";
import {StockBatchsSlice, callTypes} from "./StockBatchsSlice";
// notifyError
const {actions} = StockBatchsSlice;
// notify
export const fetchStockBatchs = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findStockBatchs(queryParams)

      .then((res) => {
        let data = res.data;
        console.log(data);
        dispatch(actions.StockBatchsFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));


    })
    .catch(error => {
      error.clientMessage = "Can't find StockBatchs";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchStockBatch = id => dispatch => {
  if (!id) {
    return dispatch(actions.StockBatchFetched({ StockBatchForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getStockBatchById(id)
    .then(response => {
      const StockBatch = response.data;
      dispatch(actions.StockBatchFetched({ StockBatchForEdit: StockBatch }));
    })
    .catch(error => {
      error.clientMessage = "Can't find StockBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteStockBatch = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteStockBatch(id)
    .then(res => {
      dispatch(actions.StockBatchDeleted({ id }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete StockBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createStockBatch = StockBatchForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createStockBatch(StockBatchForCreation)
    .then((res) => {
      let StockBatch = res.data;
      console.log(StockBatch);
      dispatch(actions.StockBatchCreated({ StockBatch }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't create StockBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateStockBatch = StockBatch => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStockBatch(StockBatch)
    .then((res) => {
      dispatch(actions.StockBatchUpdated({ StockBatch }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update StockBatch";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateStockBatchsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForStockBatchs(ids, status)
    .then(() => {
      dispatch(actions.StockBatchsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update StockBatchs status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteStockBatchs = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteStockBatchs(ids)
    .then(() => {
      dispatch(actions.StockBatchsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete StockBatchs";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
