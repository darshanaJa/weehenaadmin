import {createSlice} from "@reduxjs/toolkit";

const initialStockBatchsState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  StockBatchForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const StockBatchsSlice = createSlice({
  name: "StockBatchs",
  initialState: initialStockBatchsState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getStockBatchById
    StockBatchFetched: (state, action) => {
      state.actionsLoading = false;
      state.StockBatchForEdit = action.payload.StockBatchForEdit;
      state.error = null;
    },
    // findStockBatchs
    StockBatchsFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createStockBatch
    StockBatchCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.StockBatch);
    },
    // updateStockBatch
    StockBatchUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.StockBatch.id) {
          return action.payload.StockBatch;
        }
        return entity;
      });
    },
    // deleteStockBatch
    StockBatchDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.id !== action.payload.id);
    },
    // deleteStockBatchs
    StockBatchsDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // StockBatchsUpdateState
    StockBatchsStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
