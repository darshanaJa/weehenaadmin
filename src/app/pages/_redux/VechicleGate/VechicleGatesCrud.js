import axios from "axios";
import { Api_Login } from "../../../config/config";

export const VechicleGates_URL = Api_Login + '/api/vehicle-gate/search-all';
export const VechicleGates_CREATE_URL = Api_Login + '/api/vehicle-gate/create';
export const VechicleGates_DELETE_URL = Api_Login + '/api/buyback-VechicleGate/delete';
export const VechicleGates_GET_ID_URL = Api_Login + '/api/vehicle-gate';
export const VechicleGates_UPDATE = Api_Login + '/api/vehicle-gate/svg-update';
export const VechicleGates_PASSWORD_RESET = Api_Login + '/api/sales-VechicleGate/password/update';

// CREATE =>  POST: add a new VechicleGate to the server
export function createVechicleGate(VechicleGate) {
  return axios.post(VechicleGates_CREATE_URL, VechicleGate)
}
// READ
export function getAllVechicleGates() {
  // return axios.get(VechicleGates_URL);
}

export function getVechicleGateById(id) {
  // console.log(id);
  return axios.get(`${VechicleGates_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findVechicleGates(queryParams) {
  console.log(queryParams.buyback_name);
  if (queryParams.buyback_name === null || queryParams.buyback_name === "") {
    return axios.post(VechicleGates_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(VechicleGates_URL, { 
        "filter": [ {"buyback_name" : queryParams.buyback_name}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
  }

// UPDATE => PUT: update the procuct on the server
export function updateVechicleGate(VechicleGate,id) {
  console.log(id)
  return axios.put(`${VechicleGates_UPDATE}/${id}`, VechicleGate );
}

export function updateVechicleGatePassword(VechicleGate,id) {
  console.log(id)
  return axios.put(`${VechicleGates_PASSWORD_RESET}/${id}`, VechicleGate );
}

// UPDATE Status
export function updateStatusForVechicleGates(ids, status) {
}

// DELETE => delete the VechicleGate from the server
export function deleteVechicleGate(VechicleGateId) {
  console.log(VechicleGateId)
  return axios.delete(`${VechicleGates_DELETE_URL}/${VechicleGateId}`);
}

// DELETE VechicleGates by ids
export function deleteVechicleGates(ids) {
}
