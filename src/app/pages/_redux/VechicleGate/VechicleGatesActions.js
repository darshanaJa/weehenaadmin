import { notify, notifyError } from "../../../config/Toastify";
import * as requestFromServer from "./VechicleGatesCrud";
import {VechicleGatesSlice, callTypes} from "./VechicleGatesSlice";

const {actions} = VechicleGatesSlice;

export const fetchVechicleGates = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findVechicleGates(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.VechicleGatesFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find VechicleGates";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchVechicleGate = id => dispatch => {
  if (!id) {
    return dispatch(actions.VechicleGateFetched({ VechicleGateForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getVechicleGateById(id)
    .then((res) => {
      let VechicleGate = res.data; 
      dispatch(actions.VechicleGateFetched({ VechicleGateForEdit: VechicleGate }));
    })
    .catch(error => {
      error.clientMessage = "Can't find VechicleGate";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      notifyError()
    });
};

export const deleteVechicleGate = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteVechicleGate(id)
    .then((res) => {
      let VechicleGateId = res.data;
      console.log(VechicleGateId);
      dispatch(actions.VechicleGateDeleted({ VechicleGateId }));  
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't delete VechicleGate";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createVechicleGate = VechicleGateForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createVechicleGate(VechicleGateForCreation)
    .then((res) => {
      let VechicleGate = res.data;
      console.log(VechicleGate);
      dispatch(actions.VechicleGateCreated({ VechicleGate }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create VechicleGate";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateVechicleGate = (VechicleGate,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(VechicleGate)
  return requestFromServer
    .updateVechicleGate(VechicleGate,id)
    .then((res) => {
      dispatch(actions.VechicleGateUpdated({ VechicleGate }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update VechicleGate";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateVechicleGatePassword = (VechicleGate,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(VechicleGate)
  return requestFromServer
    .updateVechicleGatePassword(VechicleGate,id)
    .then(() => {
      dispatch(actions.VechicleGateUpdated({ VechicleGate }));
    })
    .catch(error => {
      error.clientMessage = "Can't update VechicleGate";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateVechicleGatesStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForVechicleGates(ids, status)
    .then(() => {
      dispatch(actions.VechicleGatesStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update VechicleGates status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteVechicleGates = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteVechicleGates(ids)
    .then(() => {
      dispatch(actions.VechicleGatesDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete VechicleGates";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
