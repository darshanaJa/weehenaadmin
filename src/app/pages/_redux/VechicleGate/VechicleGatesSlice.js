import {createSlice} from "@reduxjs/toolkit";

const initialVechicleGatesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  VechicleGateForEdit: undefined,
  lastError: null
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const VechicleGatesSlice = createSlice({
  name: "VechicleGates",
  initialState: initialVechicleGatesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // getVechicleGateById
    VechicleGateFetched: (state, action) => {
      state.actionsLoading = false;
      state.VechicleGateForEdit = action.payload.VechicleGateForEdit;
      state.error = null;
    },
    // findVechicleGates
    VechicleGatesFetched: (state, action) => {
      const { totalCount, entities } = action.payload;
      state.listLoading = false;
      state.error = null;
      state.entities = entities;
      state.totalCount = totalCount;
    },
    // createVechicleGate
    VechicleGateCreated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      state.entities.push(action.payload.VechicleGate);
    },
    // updateVechicleGate
    VechicleGateUpdated: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.map(entity => {
        if (entity.id === action.payload.VechicleGate.id) {
          return action.payload.VechicleGate;
        }
        return entity;
      });
    },
    // deleteVechicleGate
    VechicleGateDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(el => el.sales_VechicleGate_id !== action.payload.sales_VechicleGate_id);
    },
    // deleteVechicleGates
    VechicleGatesDeleted: (state, action) => {
      state.error = null;
      state.actionsLoading = false;
      state.entities = state.entities.filter(
        el => !action.payload.ids.includes(el.id)
      );
    },
    // VechicleGatesUpdateState
    VechicleGatesStatusUpdated: (state, action) => {
      state.actionsLoading = false;
      state.error = null;
      const { ids, status } = action.payload;
      state.entities = state.entities.map(entity => {
        if (ids.findIndex(id => id === entity.id) > -1) {
          entity.status = status;
        }
        return entity;
      });
    }
  }
});
