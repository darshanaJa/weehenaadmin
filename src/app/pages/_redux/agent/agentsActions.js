import * as requestFromServer from "./agentsCrud";
import {agentsSlice, callTypes} from "./agentsSlice";
import { notify,notifyError } from "../../../config/Toastify";

const {actions} = agentsSlice;

export const fetchAgents = queryParams => dispatch => {

  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findAgents(queryParams)

    .then((res) => {
      let data = res.data;
      console.log(data);
      dispatch(actions.AgentsFetched({ totalCount:data.data.pagination.total, entities: data.data.results }));
    })

    .catch(error => {
      error.clientMessage = "Can't find Agents";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchAgent = id => dispatch => {
  if (!id) {
    return dispatch(actions.AgentFetched({ AgentForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getAgentById(id)
    .then((res) => {
      let Agent = res.data.sales_agent; 
      dispatch(actions.AgentFetched({ AgentForEdit: Agent }));
    })
    .catch(error => {
      error.clientMessage = "Can't find Agent";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteAgent = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteAgent(id)
    .then((res) => {
      let AgentId = res.data;
      console.log(AgentId);
      dispatch(actions.AgentDeleted({ AgentId })); 

        let msg = res.data.message;
        console.log(msg)
        notify(msg)

    })
    .catch(error => {
      error.clientMessage = "Can't delete Agent";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const createAgent = AgentForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createAgent(AgentForCreation)
    .then((res) => {
      let Agent = res.data;
      console.log(Agent);
      dispatch(actions.AgentCreated({ Agent }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
      })
    
    .catch(error => {
      error.clientMessage = "Can't create Agent";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateAgent = (Agent,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Agent)
  return requestFromServer
    .updateAgent(Agent,id)
    .then((res) => {

      dispatch(actions.AgentUpdated({ Agent }));

      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Agent";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      let msg = error.message ? error.message :"Some thing went Wrong";
      notifyError(msg)
    });
};

export const updateAgentPassword = (Agent,id) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  console.log(Agent)
  return requestFromServer
    .updateAgentPassword(Agent,id)
    .then((res) => {
      dispatch(actions.AgentUpdated({ Agent }));
      let msg = res.data.message;
      console.log(msg)
      notify(msg)
    })
    .catch(error => {
      error.clientMessage = "Can't update Agent";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateAgentsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForAgents(ids, status)
    .then(() => {
      dispatch(actions.AgentsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update Agents status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteAgents = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteAgents(ids)
    .then(() => {
      dispatch(actions.AgentsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete Agents";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
