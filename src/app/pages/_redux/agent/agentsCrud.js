import axios from "axios";
import { Api_Login } from "../../../config/config";

export const AGENTS_URL = Api_Login + '/api/sales-agent/search';
export const AGENTS_GET_URL = Api_Login + '/api/sales-agent/all/sales-agents';
export const AGENTS_CREATE_URL = Api_Login + '/api/sales-agent/register';
export const AGENTS_DELETE_URL = Api_Login + '/api/sales-agent/delete';
export const AGENTS_GET_ID_URL = Api_Login + '/api/sales-agent/get-by-id';
export const AGENTS_UPDATE = Api_Login + '/api/sales-agent/update';
export const AGENTS_PASSWORD_RESET = Api_Login + '/api/sales-agent/password/update';

// CREATE =>  POST: add a new Agent to the server
export function createAgent(agent) {
  return axios.post(AGENTS_CREATE_URL, agent, {headers:{'Content-Type':'multipart/form-data'}})
}
// READ
export function getAllAgents() {
  return axios.get(AGENTS_GET_URL);
}

export function getAgentById(id) {
  // console.log(id);
  return axios.get(`${AGENTS_GET_ID_URL}/${id}`);
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findAgents(queryParams) {
  console.log(queryParams.sales_agent_fname);
  if (queryParams.sales_agent_fname === null || queryParams.sales_agent_fname === "") {
    return axios.post(AGENTS_URL, { 
      "filter": [{"status": "1" }],
      "sort": "DESC",
      "limit": queryParams.pageSize, 
      "skip":(queryParams.pageNumber-1)*queryParams.pageSize
  });
    } else {
      return axios.post(AGENTS_URL, { 
        "filter": [ {"sales_agent_fname" : queryParams.sales_agent_fname}],
        "sort": "DESC",
        "limit": queryParams.pageSize, 
        "skip":(queryParams.pageNumber-1)*queryParams.pageSize
    });
    }
}

// UPDATE => PUT: update the procuct on the server
export function updateAgent(Agent,id) {
  console.log(id)
  return axios.put(`${AGENTS_UPDATE}/${id}`, Agent , {headers:{'Content-Type':'multipart/form-data'}} );
}

export function updateAgentPassword(Agent,id) {
  console.log(id)
  return axios.put(`${AGENTS_PASSWORD_RESET}/${id}`, Agent );
}

// UPDATE Status
export function updateStatusForAgents(ids, status) {
}

// DELETE => delete the Agent from the server
export function deleteAgent(AgentId) {
  console.log(AgentId)
  return axios.delete(`${AGENTS_DELETE_URL}/${AgentId}`);
}

// DELETE Agents by ids
export function deleteAgents(ids) {
  // return axios.post(`${AGENTS_URL}/deleteAgents`, { ids });
}
