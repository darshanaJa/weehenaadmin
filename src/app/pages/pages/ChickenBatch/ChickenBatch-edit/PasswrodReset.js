import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const ChickenBatchEditSchema = Yup.object().shape({
  // id: Yup.string(),
  curent_password: Yup.string()
    .required("Curent Passwrod is required")
    .min(2, "Curerent password"),
  new_password: Yup.string()
    .required("New Password is required")
    .min(2, "New password"),
  confirm_password: Yup.string()
    .required("Confirm Password is required")
    .min(2, "Confirm password")
});


export function PasswordReset({
  ChickenBatch,
  btnRef,
  savePassword,
}) {

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={ChickenBatch}
        validationSchema={ChickenBatchEditSchema}
        onSubmit={(values)  => {
          console.log(values);
          savePassword(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="curent_password"
                    component={Input}
                    placeholder="Current Password"
                    label="Current Password"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="new_password"
                    component={Input}
                    placeholder="New Password"
                    label="New Password"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="confirm_password"
                    component={Input}
                    placeholder="Confirm Password"
                    label="Confirm Password"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2">Reset Password</button>
                </div>
              </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>
   

  );
}
