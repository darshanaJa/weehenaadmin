/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/ChickenBatch/ChickenBatchesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { ChickenBatchEditForm } from "./ChickenBatchEditForm";
import { ChickenBatchCreateForm } from "./ChickenBatchCreateForm";
import { Specifications } from "../ChickenBatch-specifications/Specifications";
import { SpecificationsUIProvider } from "../ChickenBatch-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../ChickenBatch-remarks/RemarksUIContext";
import { Remarks } from "../ChickenBatch-remarks/Remarks";
import axios from "axios";
import { Supliers_URL_SEARCH } from "../../../_redux/Suplier/SupliersCrud";
import { ChickenBatches_GET_ID_URL } from "../../../_redux/ChickenBatch/ChickenBatchesCrud";


export function ChickenBatchEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  const [shop,setShop]=useState([]);
  const [item,setIem]=useState();
  const [quantiy,setquantiy]=useState();
  const [age, setAge]= useState();
  const [batch, setBatch]= useState();
  const [description,setdescriptttion]=useState();
  const [adminId, setadminId] = useState('')
  const [createDate, setcreateDate] = useState('')
  const [updateDate, setupdateDate] = useState('')
  
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, ChickenBatchForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.ChickenBatches.actionsLoading,
      ChickenBatchForEdit: state.ChickenBatches.ChickenBatchForEdit,
    }),
    shallowEqual
  );

  const initChickenBatch = {
    batchname:batch,
    chick_batch_quantity : quantiy,
    chick_batch_description : description,
    supplierId : item,
    chicken_days_from_birth:age
  };

  console.log(age)

  console.log(ChickenBatchForEdit)

  useEffect(() => {
    dispatch(actions.fetchChickenBatch(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Chicken Batch";
    if (ChickenBatchForEdit && id) {
      // _title = `Edit ChickenBatch - ${ChickenBatchForEdit.buyback_name} ${ChickenBatchForEdit.buyback_contact} - ${ChickenBatchForEdit.sales_ChickenBatch_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenBatchForEdit, id]);

  

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Supliers_URL_SEARCH,
      data:{
        "filter": [{"status": "1" }],
      }
      })
      .then((res) => {
        setShop(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: ChickenBatches_GET_ID_URL + `/${id}`,
      
      })
      .then((res) => {
        setIem(res.data.supp.id);
        setquantiy(res.data.chick_batch_quantity);
        setdescriptttion(res.data.chick_batch_description);
        setAge(res.data.chicken_days_from_birth);
        setBatch(res.data.batchname);
        setadminId(res.data.created_by);
        setcreateDate(res.data.chick_batch_registered_date);
        setupdateDate(res.data.chick_batch_updated_date);
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[id])

  console.log(item)

  console.log(shop)

  const saveChickenBatch = (values) => {
    if (!id) {
      dispatch(actions.createChickenBatch(values,id)).then(() => backToChickenBatchesList());
    } else {
      dispatch(actions.updateChickenBatch(values,id)).then(() => backToChickenBatchesList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateChickenBatchPassword(values,id)).then(() => backToChickenBatchesList());
  };

  const btnRef = useRef();

  const backToChickenBatchesList = () => {
    history.push(`/buyback/batch`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickenBatchesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Coop Lists
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickenBatchEditForm
                actionsLoading={actionsLoading}
                ChickenBatch={initChickenBatch}
                ChickenBatch2={{ChickenBatchForEdit}}
                btnRef={btnRef}
                saveChickenBatch={saveChickenBatch}
                supplier={shop}
                adminId={adminId}
                createDate={createDate}
                updateDate={updateDate}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentChickenBatchId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id} currentChickenBatchId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickenBatchesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Chicken Batch remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Chicken Batch specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickenBatchCreateForm
                actionsLoading={actionsLoading}
                ChickenBatch={ChickenBatchForEdit || initChickenBatch}
                btnRef={btnRef}
                saveChickenBatch={saveChickenBatch}
                savePassword={savePassword}
                supplier={shop}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentChickenBatchId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentChickenBatchId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
