import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const ChickenBatchEditSchema = Yup.object().shape({
  chick_batch_quantity: Yup.number()
    .required("Quantity is required")
    .min(2, "Quantity must be at least 2 characters"),
  chick_batch_description: Yup.string()
    .required("Descripin is required")
    .min(2, "Descripin must be at least 2 characters"),
  supplierId: Yup.string()
    .required("Supplier Name is required"),
});


export function ChickenBatchCreateForm({
  ChickenBatch,
  btnRef,
  saveChickenBatch,
  supplier
}) {


  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={ChickenBatch}
        validationSchema={ChickenBatchEditSchema}
        onSubmit={(values)  => {
          console.log(values);            
          saveChickenBatch(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
              <div className="col-lg-4">
                  <Field
                    type="text"
                    name="batchname"
                    component={Input}
                    placeholder="Batch Name"
                    label="Batch Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Select name="supplierId" label="Supplier Name">
                  <option>Choose One</option>
                  {supplier.map((item) => (
                      <option value={item.id} >
                        {item.supplier_name}
                      </option>    
                  ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="chick_batch_quantity"
                    component={Input}
                    placeholder="Quantity"
                    label="Quantity"
                  />
                </div>
                
              </div>

              <div className="form-group row">
              <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chicken_days_from_birth"
                    component={Input}
                    placeholder="Chicken Days From Birth"
                    label="Chicken Days From Birth"
                  />
                </div>
              <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chick_batch_description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
                </div>  
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
