import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./ChickenBatchesUIHelpers";

const ChickenBatchesUIContext = createContext();

export function useChickenBatchesUIContext() {
  return useContext(ChickenBatchesUIContext);
}

export const ChickenBatchesUIConsumer = ChickenBatchesUIContext.Consumer;

export function ChickenBatchesUIProvider({ ChickenBatchesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newChickenBatchButtonClick: ChickenBatchesUIEvents.newChickenBatchButtonClick,
    openEditChickenBatchPage: ChickenBatchesUIEvents.openEditChickenBatchPage,
    openDeleteChickenBatchDialog: ChickenBatchesUIEvents.openDeleteChickenBatchDialog,
    openDeleteChickenBatchesDialog: ChickenBatchesUIEvents.openDeleteChickenBatchesDialog,
    openFetchChickenBatchesDialog: ChickenBatchesUIEvents.openFetchChickenBatchesDialog,
    openUpdateChickenBatchesStatusDialog: ChickenBatchesUIEvents.openUpdateChickenBatchesStatusDialog,
  };

  return (
    <ChickenBatchesUIContext.Provider value={value}>
      {children}
    </ChickenBatchesUIContext.Provider>
  );
}
