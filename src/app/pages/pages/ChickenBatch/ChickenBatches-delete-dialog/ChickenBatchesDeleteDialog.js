/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/ChickenBatch/ChickenBatchesActions";
import { useChickenBatchesUIContext } from "../ChickenBatchesUIContext";

export function ChickenBatchesDeleteDialog({ show, onHide }) {
  // ChickenBatches UI Context
  const ChickenBatchesUIContext = useChickenBatchesUIContext();
  const ChickenBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenBatchesUIContext.ids,
      setIds: ChickenBatchesUIContext.setIds,
      queryParams: ChickenBatchesUIContext.queryParams,
    };
  }, [ChickenBatchesUIContext]);

  // ChickenBatches Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.ChickenBatches.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected ChickenBatches we should close modal
  useEffect(() => {
    if (!ChickenBatchesUIProps.ids || ChickenBatchesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenBatchesUIProps.ids]);

  const deleteChickenBatches = () => {
    // server request for deleting ChickenBatch by seleted ids
    dispatch(actions.deleteChickenBatches(ChickenBatchesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickenBatches(ChickenBatchesUIProps.queryParams)).then(() => {
        // clear selections list
        ChickenBatchesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          ChickenBatches Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected ChickenBatches?</span>
        )}
        {isLoading && <span>ChickenBatches are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChickenBatches}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
