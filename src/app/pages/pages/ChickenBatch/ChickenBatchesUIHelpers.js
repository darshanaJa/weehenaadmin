export const ChickenBatchestatusCssClasses = ["success", "info", ""];
export const ChickenBatchestatusTitles = ["Selling", "Sold"];
export const ChickenBatchConditionCssClasses = ["success", "danger", ""];
export const ChickenBatchConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_ChickenBatch_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    buyback_name: "",
    batchname:"",
  },
  batchname:null,
  buyback_name:null,
  sortOrder: "asc", // asc||desc
  sortField: "buyback_name",
  pageNumber: 1,
  pageSize: 10
};
