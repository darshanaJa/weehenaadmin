import React from "react";
import { Route } from "react-router-dom";
import { ChickenBatchesLoadingDialog } from "./ChickenBatches-loading-dialog/ChickenBatchesLoadingDialog";
import { ChickenBatchDeleteDialog } from "./ChickenBatch-delete-dialog/ChickenBatchDeleteDialog";
import { ChickenBatchesDeleteDialog } from "./ChickenBatches-delete-dialog/ChickenBatchesDeleteDialog";
import { ChickenBatchesFetchDialog } from "./ChickenBatches-fetch-dialog/ChickenBatchesFetchDialog";
import { ChickenBatchesUpdateStatusDialog } from "./ChickenBatches-update-status-dialog/ChickenBatchesUpdateStatusDialog";
import { ChickenBatchesCard } from "./ChickenBatchesCard";
import { ChickenBatchesUIProvider } from "./ChickenBatchesUIContext";

export function ChickenBatchesPage({ history }) {
  const ChickenBatchesUIEvents = {
    newChickenBatchButtonClick: () => {
      history.push("/buyback/batch/new");
    },
    openEditChickenBatchPage: (id) => {
      history.push(`/buyback/batch/${id}/edit`);
    },
    openDeleteChickenBatchDialog: (sales_ChickenBatch_id) => {
      history.push(`/buyback/batch/${sales_ChickenBatch_id}/delete`);
    },
    openDeleteChickenBatchesDialog: () => {
      history.push(`/buyback/batch/deleteChickenBatches`);
    },
    openFetchChickenBatchesDialog: () => {
      history.push(`/buyback/batch/fetch`);
    },
    openUpdateChickenBatchesStatusDialog: () => {
      history.push("/buyback/batch/updateStatus");
    },
  };

  return (
    <ChickenBatchesUIProvider ChickenBatchesUIEvents={ChickenBatchesUIEvents}>
      <ChickenBatchesLoadingDialog />
      <Route path="/buyback/batch/deleteChickenBatches">
        {({ history, match }) => (
          <ChickenBatchesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/batch");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/batch/:sales_ChickenBatch_id/delete">
        {({ history, match }) => (
          <ChickenBatchDeleteDialog
            show={match != null}
            sales_ChickenBatch_id={match && match.params.sales_ChickenBatch_id}
            onHide={() => {
              history.push("/buyback/batch");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/batch/fetch">
        {({ history, match }) => (
          <ChickenBatchesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/batch");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/batch/updateStatus">
        {({ history, match }) => (
          <ChickenBatchesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/batch");
            }}
          />
        )}
      </Route>
      <ChickenBatchesCard />
    </ChickenBatchesUIProvider>
  );
}
