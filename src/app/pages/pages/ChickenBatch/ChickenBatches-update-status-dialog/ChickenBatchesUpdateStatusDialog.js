import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ChickenBatchestatusCssClasses } from "../ChickenBatchesUIHelpers";
import * as actions from "../../../_redux/ChickenBatch/ChickenBatchesActions";
import { useChickenBatchesUIContext } from "../ChickenBatchesUIContext";

const selectedChickenBatches = (entities, ids) => {
  const _ChickenBatches = [];
  ids.forEach((id) => {
    const ChickenBatch = entities.find((el) => el.id === id);
    if (ChickenBatch) {
      _ChickenBatches.push(ChickenBatch);
    }
  });
  return _ChickenBatches;
};

export function ChickenBatchesUpdateStatusDialog({ show, onHide }) {
  // ChickenBatches UI Context
  const ChickenBatchesUIContext = useChickenBatchesUIContext();
  const ChickenBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenBatchesUIContext.ids,
      setIds: ChickenBatchesUIContext.setIds,
      queryParams: ChickenBatchesUIContext.queryParams,
    };
  }, [ChickenBatchesUIContext]);

  // ChickenBatches Redux state
  const { ChickenBatches, isLoading } = useSelector(
    (state) => ({
      ChickenBatches: selectedChickenBatches(state.ChickenBatches.entities, ChickenBatchesUIProps.ids),
      isLoading: state.ChickenBatches.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected ChickenBatches we should close modal
  useEffect(() => {
    if (ChickenBatchesUIProps.ids || ChickenBatchesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenBatchesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing ChickenBatch by ids
    dispatch(actions.updateChickenBatchesStatus(ChickenBatchesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchChickenBatches(ChickenBatchesUIProps.queryParams)).then(
          () => {
            // clear selections list
            ChickenBatchesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected ChickenBatches
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {ChickenBatches.map((ChickenBatch) => (
              <div className="list-timeline-item mb-3" key={ChickenBatch.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ChickenBatchestatusCssClasses[ChickenBatch.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {ChickenBatch.id}
                  </span>{" "}
                  <span className="ml-5">
                    {ChickenBatch.manufacture}, {ChickenBatch.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${ChickenBatchestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
