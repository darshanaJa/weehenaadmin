import React, {useEffect,useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form } from "formik";
import axios from "axios";
import Pr  from "../../../../../config/p1";
import { ChickenCopBatches_GET_ID_URL } from "../../../../_redux/ChickenCopBatch/ChickenCopBatchesCrud";
import Switch from '@material-ui/core/Switch';
import { Api_Login } from "../../../../../config/config";
import { Chickens_GET_ID_URL } from "../../../../_redux/Chicken/ChickensCrud";
import { notify } from "../../../../../config/Toastify";




export function SpecificationEditForm({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
  id,
  idedit
}) {
  const [day, setday] = useState('');
  const [createDate, setcreateDate] = useState('')
  const [chView, setView] = useState(false)
  const [coopId, setcoopId] = useState('')
  const [act, setact] = useState("")

  // const url = Api_Login + '/api/chick-batch/update';
  const url2 = Api_Login + '/api/chickencoop/update';

  useEffect(() => {
    axios({
      method: 'get',
      baseURL: ChickenCopBatches_GET_ID_URL + `/${idedit}`
      })
      .then((res) => {
        setday(res.data.coopbatch.chicken_days_from_birth)
        setcreateDate(res.data.coopbatch.chick_batch_registered_date)
        console.log(res.data.coop_name)
        setcoopId(res.data.coop_name)
        
      })
      .catch(function (response) {
      });
  }, [idedit])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Chickens_GET_ID_URL + `/${coopId}`
      })
      .then((res) => {
        setact(res.data.active)
      })
      .catch(function (response) {
          // console.log(response);
      });

      // console.log("Coop Name")
      
  },[coopId])

  useEffect(() => {
    if(act==="Active"){
    setView(true);
  }
  console.log(act)
  }, [act])

  console.log(idedit)

  const handleChangeView =(event)=>{
    if(event.target.checked===true)
    {
      axios.put(url2+`/${coopId}`, {"active":"Active"});
      setView(event.target.checked)
      notify("Sucessfully Active Coop....")
    }
    else{
      axios.put(url2+`/${coopId}`, {"active":"off"});
      notify("Sucessfully Closed Coop....")
      setView(event.target.checked)
    }
    console.log(event.target.checked)
  }

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={specification}
        onSubmit={(values) => {
          console.log(values)
          saveSpecification(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <Form className="form form-label-right">

                  <div className="form-group row">
                    <div className="col-lg-8 + downbtn">
                      <Pr 
                        day1={day} 
                        createDate={createDate} 
                      />
                    </div>
                    <div className="col-lg-4">
                      <p>Coop Status</p>
                      <Switch
                        checked={chView}
                        value={chView}
                        onChange={handleChangeView}
                        color="primary"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>  
                  </div>

            </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              {/* <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button> */}
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}
