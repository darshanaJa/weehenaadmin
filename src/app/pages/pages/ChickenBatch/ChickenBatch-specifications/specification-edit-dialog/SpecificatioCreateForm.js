// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form } from "formik";
// import * as Yup from "yup";
// import { SPECIFICATIONS_DICTIONARY } from "../SpecificationsUIHelper";
import axios from "axios";
// import { MainStocks_URL_GET } from "../../../../_redux/MainStock/MainStocksCrud";
// import { Shops_URL_GET } from "../../../../_redux/shops/shopsCrud";
import { Items_GETBYID_URL } from "../../../../_redux/specifications/specificationsCrud";
import Pr  from "../../../../../config/p1";
// import { SalesTickets_URL_FIND } from "../../../../_redux/salesTickets/salesTicketsCrud";

// Validation schema
export function SpecificationCreateForm({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
  id,
  idedit
}) {

  console.log(idedit)

  // const [shop,setShop]=useState([]);
  // const [saleTicketID,setsaleTicketID]=useState([]);
  const [items,setItems]=useState();
  // const [retailSale,setRetailSale]=useState([]);

  console.log(items)


  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Items_GETBYID_URL + `/${idedit}`
      })
      .then((res) => {
        setItems(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[idedit])

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={specification}
        // validationSchema={SpecificationEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveSpecification(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <Form className="form form-label-right">

                  <div className="form-group row">
                    <div className="col-lg-12">
                      <Pr />
                    </div>
                  </div>

            </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              {/* <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button> */}
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}
