import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { ChickenBatchesFilter } from "./ChickenBatches-filter/ChickenBatchesFilter";
import { ChickenBatchesTable } from "./ChickenBatches-table/ChickenBatchesTable";
// import { ChickenBatchesGrouping } from "./ChickenBatches-grouping/ChickenBatchesGrouping";
import { useChickenBatchesUIContext } from "./ChickenBatchesUIContext";
import { notify } from "../../../config/Toastify";

export function ChickenBatchesCard() {
  const ChickenBatchesUIContext = useChickenBatchesUIContext();
  const ChickenBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenBatchesUIContext.ids,
      queryParams: ChickenBatchesUIContext.queryParams,
      setQueryParams: ChickenBatchesUIContext.setQueryParams,
      newChickenBatchButtonClick: ChickenBatchesUIContext.newChickenBatchButtonClick,
      openDeleteChickenBatchesDialog: ChickenBatchesUIContext.openDeleteChickenBatchesDialog,
      openEditChickenBatchPage: ChickenBatchesUIContext.openEditChickenBatchPage,
      openUpdateChickenBatchesStatusDialog:ChickenBatchesUIContext.openUpdateChickenBatchesStatusDialog,
      openFetchChickenBatchesDialog: ChickenBatchesUIContext.openFetchChickenBatchesDialog,
    };
  }, [ChickenBatchesUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Chicken Batch list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={ChickenBatchesUIProps.newChickenBatchButtonClick}
          >
            New Chicken Batch
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ChickenBatchesFilter />
        {ChickenBatchesUIProps.ids.length > 0 && (
          <>
            {/* <ChickenBatchesGrouping /> */}
          </>
        )}
        <ChickenBatchesTable />
      </CardBody>
    </Card>
  );
}
