import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { ChickenBatchestatusCssClasses } from "../ChickenBatchesUIHelpers";
import { useChickenBatchesUIContext } from "../ChickenBatchesUIContext";

const selectedChickenBatches = (entities, ids) => {
  const _ChickenBatches = [];
  ids.forEach((id) => {
    const ChickenBatch = entities.find((el) => el.id === id);
    if (ChickenBatch) {
      _ChickenBatches.push(ChickenBatch);
    }
  });
  return _ChickenBatches;
};

export function ChickenBatchesFetchDialog({ show, onHide }) {
  // ChickenBatches UI Context
  const ChickenBatchesUIContext = useChickenBatchesUIContext();
  const ChickenBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenBatchesUIContext.ids,
      queryParams: ChickenBatchesUIContext.queryParams,
    };
  }, [ChickenBatchesUIContext]);

  // ChickenBatches Redux state
  const { ChickenBatches } = useSelector(
    (state) => ({
      ChickenBatches: selectedChickenBatches(state.ChickenBatches.entities, ChickenBatchesUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!ChickenBatchesUIProps.ids || ChickenBatchesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenBatchesUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {ChickenBatches.map((ChickenBatch) => (
              <div className="list-timeline-item mb-3" key={ChickenBatch.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ChickenBatchestatusCssClasses[ChickenBatch.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {ChickenBatch.id}
                  </span>{" "}
                  <span className="ml-5">
                    {ChickenBatch.manufacture}, {ChickenBatch.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
