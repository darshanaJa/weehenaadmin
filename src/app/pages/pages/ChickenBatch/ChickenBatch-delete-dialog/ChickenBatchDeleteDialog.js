/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/ChickenBatch/ChickenBatchesActions";
import { useChickenBatchesUIContext } from "../ChickenBatchesUIContext";

export function ChickenBatchDeleteDialog({ sales_ChickenBatch_id, show, onHide }) {
  // console.log(sales_ChickenBatch_id)
  // ChickenBatches UI Context
  const ChickenBatchesUIContext = useChickenBatchesUIContext();
  const ChickenBatchesUIProps = useMemo(() => {
    return {
      setIds: ChickenBatchesUIContext.setIds,
      queryParams: ChickenBatchesUIContext.queryParams,
    };
  }, [ChickenBatchesUIContext]);

  // ChickenBatches Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.ChickenBatches.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_ChickenBatch_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_ChickenBatch_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteChickenBatch = () => {
    // server request for deleting ChickenBatch by id
    dispatch(actions.deleteChickenBatch(sales_ChickenBatch_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickenBatches(ChickenBatchesUIProps.queryParams));
      // clear selections list
      ChickenBatchesUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          ChickenBatch Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this ChickenBatch?</span>
        )}
        {isLoading && <span>ChickenBatch is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChickenBatch}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
