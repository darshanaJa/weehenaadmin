// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/ChickenBatch/ChickenBatchesActions";
import * as uiHelpers from "../ChickenBatchesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useChickenBatchesUIContext } from "../ChickenBatchesUIContext";

export function ChickenBatchesTable() {
  // ChickenBatches UI Context
  const ChickenBatchesUIContext = useChickenBatchesUIContext();
  const ChickenBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenBatchesUIContext.ids,
      setIds: ChickenBatchesUIContext.setIds,
      queryParams: ChickenBatchesUIContext.queryParams,
      setQueryParams: ChickenBatchesUIContext.setQueryParams,
      openEditChickenBatchPage: ChickenBatchesUIContext.openEditChickenBatchPage,
      openDeleteChickenBatchDialog: ChickenBatchesUIContext.openDeleteChickenBatchDialog,
    };
  }, [ChickenBatchesUIContext]);

  // Getting curret state of ChickenBatches list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.ChickenBatches }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // ChickenBatches Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    ChickenBatchesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchChickenBatches(ChickenBatchesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenBatchesUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_ChickenBatch_id);

  const columns = [
    {
      dataField: "chick_batch_id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "batchname",
      text: "Batch Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chick_batch_quantity",
      text: "Quantity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chicken_days_from_birth",
      text: " Chicken Days From Birth",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chick_batch_description",
      text: "Descripion",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "supp.supplier_name",
      text: "Supplier Name",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    // {
    //   dataField: "buyback_passed_experiance",
    //   text: "Passed Experiance",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_ChickenBatch_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditChickenBatchPage: ChickenBatchesUIProps.openEditChickenBatchPage,
        openDeleteChickenBatchDialog: ChickenBatchesUIProps.openDeleteChickenBatchDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ChickenBatchesUIProps.queryParams.pageSize,
    page: ChickenBatchesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_ChickenBatch_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ChickenBatchesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ChickenBatchesUIProps.ids,
                  setIds: ChickenBatchesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
