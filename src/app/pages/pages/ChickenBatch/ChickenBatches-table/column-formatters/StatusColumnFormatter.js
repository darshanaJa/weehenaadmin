import React from "react";
import {
  ChickenBatchestatusCssClasses,
  ChickenBatchestatusTitles
} from "../../ChickenBatchesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      ChickenBatchestatusCssClasses[row.status]
    } label-inline`}
  >
    {ChickenBatchestatusTitles[row.status]}
  </span>
);
