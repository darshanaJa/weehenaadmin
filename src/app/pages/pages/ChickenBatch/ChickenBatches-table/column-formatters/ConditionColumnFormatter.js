import React from "react";
import {
  ChickenBatchConditionCssClasses,
  ChickenBatchConditionTitles
} from "../../ChickenBatchesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        ChickenBatchConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        ChickenBatchConditionCssClasses[row.condition]
      }`}
    >
      {ChickenBatchConditionTitles[row.condition]}
    </span>
  </>
);
