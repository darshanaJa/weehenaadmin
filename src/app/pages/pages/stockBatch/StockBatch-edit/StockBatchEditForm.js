import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { MainStocks_URL_GET } from "../../../_redux/MainStock/MainStocksCrud";
import axios from "axios";
import Admin from "../../../../config/Admin";
import { StockBatchs_URL_GETBYID } from "../../../_redux/StockBatch/StockBatchsCrud";

// Validation schema
const StockBatchEditSchema = Yup.object().shape({
  batch_quantity: Yup.number()
    .required("Quantity is required"),

});

export function StockBatchEditForm({
  StockBatch,
  btnRef,
  saveStockBatch,
  id
}) {

  const [shop,setShop]=useState([]);

  const [idAdmin,setIdAdmin]=useState('');

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: StockBatchs_URL_GETBYID + `/${id}`
      })
      .then((res) => {
        setIdAdmin(res.data.created_by.id)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[id])

  console.log(shop)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])
  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={StockBatch}
        validationSchema={StockBatchEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveStockBatch(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="batch_quantity"
                    component={Input}
                    placeholder="Quantity"
                    label="Quantity"
                  />
                </div>
                <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                </div>
              </div>
              
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={idAdmin}
                  createDate={StockBatch.batch_created_date} 
                  updateDate={StockBatch.batch_updated_date} 
                />

            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
