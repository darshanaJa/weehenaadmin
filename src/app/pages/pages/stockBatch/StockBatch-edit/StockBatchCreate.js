import React, {useEffect,useState} from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { MainStocks_URL_GET } from "../../../_redux/MainStock/MainStocksCrud";
import axios from "axios";

const StockBatchEditSchema = Yup.object().shape({
  batch_quantity: Yup.number()
    .required("Quantity is required"),
  mstockItemId: Yup.string()
    .required("Item Name is required"),

});

export function StockBatchCreateForm({
  StockBatch,
  btnRef,
  saveStockBatch,
}) {

  const [shop,setShop]=useState([]);

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])
  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={StockBatch}
        validationSchema={StockBatchEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveStockBatch(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Select name="mstockItemId"
                    error="mstockItemId"
                    label="Item Name">
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.item_id} >
                            {item.item_name}
                          </option>    
                      ))}
                  </Select>
                  <ErrorMessage name="mstockItemId" />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="batch_quantity"
                    component={Input}
                    placeholder="Quantity"
                    label="Quantity"
                  />
                </div>
                <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                </div>
              </div>
              
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
