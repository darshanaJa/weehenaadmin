/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/StockBatch/StockBatchsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { StockBatchEditForm } from "./StockBatchEditForm";
import { StockBatchCreateForm } from "./StockBatchCreate";
import { Specifications } from "../StockBatch-specifications/Specifications";
import { SpecificationsUIProvider } from "../StockBatch-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../StockBatch-remarks/RemarksUIContext";
import { Remarks } from "../StockBatch-remarks/Remarks";

const initStockBatch = {
  batch_quantity : "",
  mstockItemId:"",
};

export function StockBatchEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, StockBatchForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.StockBatchs.actionsLoading,
      StockBatchForEdit: state.StockBatchs.StockBatchForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchStockBatch(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New StockBatch";
    if (StockBatchForEdit && id) {
      // _title = `Edit StockBatch - ${StockBatchForEdit.sales_StockBatch_fname} ${StockBatchForEdit.sales_StockBatch_lname} - ${StockBatchForEdit.sales_StockBatch_mobile01}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [StockBatchForEdit, id]);

  const saveStockBatch = (values) => {
    if (!id) {
      dispatch(actions.createStockBatch(values)).then(() => backToStockBatchsList());
    } else {
      dispatch(actions.updateStockBatch(values)).then(() => backToStockBatchsList());
    }
  };

  const btnRef = useRef();  

  const backToStockBatchsList = () => {
    history.push(`/stocks/stockbatch`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToStockBatchsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <StockBatchEditForm
                actionsLoading={actionsLoading}
                StockBatch={StockBatchForEdit || initStockBatch}
                btnRef={btnRef}
                saveStockBatch={saveStockBatch}
                id={id}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentStockBatchId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentStockBatchId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToStockBatchsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    StockBatch remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    StockBatch specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <StockBatchCreateForm
                actionsLoading={actionsLoading}
                StockBatch={StockBatchForEdit || initStockBatch}
                btnRef={btnRef}
                saveStockBatch={saveStockBatch}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentStockBatchId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentStockBatchId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
