import React, { useMemo } from "react";
import { useStockBatchsUIContext } from "../StockBatchsUIContext";

export function StockBatchsGrouping() {
  // StockBatchs UI Context
  const StockBatchsUIContext = useStockBatchsUIContext();
  const StockBatchsUIProps = useMemo(() => {
    return {
      ids: StockBatchsUIContext.ids,
      setIds: StockBatchsUIContext.setIds,
      openDeleteStockBatchsDialog: StockBatchsUIContext.openDeleteStockBatchsDialog,
      openFetchStockBatchsDialog: StockBatchsUIContext.openFetchStockBatchsDialog,
      openUpdateStockBatchsStatusDialog:
        StockBatchsUIContext.openUpdateStockBatchsStatusDialog,
    };
  }, [StockBatchsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{StockBatchsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={StockBatchsUIProps.openDeleteStockBatchsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={StockBatchsUIProps.openFetchStockBatchsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={StockBatchsUIProps.openUpdateStockBatchsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
