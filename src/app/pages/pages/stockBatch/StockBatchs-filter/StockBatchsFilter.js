import React, { useMemo } from "react";
import { Formik } from "formik";
import { isEqual } from "lodash";
import { useStockBatchsUIContext } from "../StockBatchsUIContext";

const prepareFilter = (queryParams, values) => {
  const { status, condition, searchText } = values;
  const newQueryParams = { ...queryParams };
  const filter = {};
  // Filter by status
  filter.status = status !== "" ? +status : undefined;
  // Filter by condition
  filter.condition = condition !== "" ? +condition : undefined;
  // Filter by all fields
  filter.model = searchText;
  if (searchText) {
    filter.manufacture = searchText;
    filter.VINCode = searchText;
  }
  newQueryParams.filter = filter;
  return newQueryParams;
};

export function StockBatchsFilter({ listLoading }) {
  // StockBatchs UI Context
  const StockBatchsUIContext = useStockBatchsUIContext();
  const StockBatchsUIProps = useMemo(() => {
    return {
      setQueryParams: StockBatchsUIContext.setQueryParams,
      queryParams: StockBatchsUIContext.queryParams,
    };
  }, [StockBatchsUIContext]);

  const applyFilter = (values) => {
    const newQueryParams = prepareFilter(StockBatchsUIProps.queryParams, values);
    if (!isEqual(newQueryParams, StockBatchsUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      newQueryParams.batch_id = values.searchText;
      StockBatchsUIProps.setQueryParams(newQueryParams);
    }
  };

  return (
    <>
      <Formik
        initialValues={{
          status: "", // values => All=""/Selling=0/Sold=1
          condition: "", // values => All=""/New=0/Used=1
          searchText: "",
        }}
        onSubmit={(values) => {
          applyFilter(values);
        }}
      >
        {({
          values,
          handleSubmit,
          handleBlur,
          handleChange,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit} className="form form-label-right">
            <div className="form-group row">
              <div className="col-lg-12">
                <input
                  type="text"
                  className="form-control"
                  name="searchText"
                  placeholder="Search"
                  onBlur={handleBlur}
                  value={values.searchText}
                  onChange={(e) => {
                    setFieldValue("searchText", e.target.value);
                    handleSubmit();
                  }}
                />
                <small className="form-text text-muted">
                  <b>Search</b> in all fields
                </small>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );
}
