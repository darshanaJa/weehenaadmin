import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { StockBatchsFilter } from "./StockBatchs-filter/StockBatchsFilter";
import { StockBatchsTable } from "./StockBatchs-table/StockBatchsTable";
import { StockBatchsGrouping } from "./StockBatchs-grouping/StockBatchsGrouping";
import { useStockBatchsUIContext } from "./StockBatchsUIContext";
import { notify } from "../../../config/Toastify";

export function StockBatchsCard() {
  const StockBatchsUIContext = useStockBatchsUIContext();
  const StockBatchsUIProps = useMemo(() => {
    return {
      ids: StockBatchsUIContext.ids,
      queryParams: StockBatchsUIContext.queryParams,
      setQueryParams: StockBatchsUIContext.setQueryParams,
      newStockBatchButtonClick: StockBatchsUIContext.newStockBatchButtonClick,
      openDeleteStockBatchsDialog: StockBatchsUIContext.openDeleteStockBatchsDialog,
      openEditStockBatchPage: StockBatchsUIContext.openEditStockBatchPage,
      openUpdateStockBatchsStatusDialog:StockBatchsUIContext.openUpdateStockBatchsStatusDialog,
      openFetchStockBatchsDialog: StockBatchsUIContext.openFetchStockBatchsDialog,
    };
  }, [StockBatchsUIContext]);

  return (
    <Card>
      <CardHeader title="StockBatch list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={StockBatchsUIProps.newStockBatchButtonClick}
          >
            New StockBatch
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <StockBatchsFilter />
        {StockBatchsUIProps.ids.length > 0 && (
          <>
            <StockBatchsGrouping />
          </>
        )}
        <StockBatchsTable />
      </CardBody>
    </Card>
  );
}
