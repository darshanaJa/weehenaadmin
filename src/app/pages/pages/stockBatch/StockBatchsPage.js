import React from "react";
import { Route } from "react-router-dom";
import { StockBatchsLoadingDialog } from "./StockBatchs-loading-dialog/StockBatchsLoadingDialog";
import { StockBatchDeleteDialog } from "./StockBatch-delete-dialog/StockBatchDeleteDialog";
import { StockBatchsDeleteDialog } from "./StockBatchs-delete-dialog/StockBatchsDeleteDialog";
import { StockBatchsFetchDialog } from "./StockBatchs-fetch-dialog/StockBatchsFetchDialog";
import { StockBatchsUpdateStatusDialog } from "./StockBatchs-update-status-dialog/StockBatchsUpdateStatusDialog";
import { StockBatchsCard } from "./StockBatchsCard";
import { StockBatchsUIProvider } from "./StockBatchsUIContext";

export function StockBatchsPage({ history }) {
  const StockBatchsUIEvents = {
    newStockBatchButtonClick: () => {
      history.push("/stocks/stockbatch/new");
    },
    openEditStockBatchPage: (id) => {
      history.push(`/stocks/stockbatch/${id}/edit`);
    },
    openDeleteStockBatchDialog: (id) => {
      history.push(`/stocks/stockbatch/${id}/delete`);
    },
    openDeleteStockBatchsDialog: () => {
      history.push(`/stocks/stockbatch/deleteStockBatchs`);
    },
    openFetchStockBatchsDialog: () => {
      history.push(`/stocks/stockbatch/fetch`);
    },
    openUpdateStockBatchsStatusDialog: () => {
      history.push("/stocks/stockbatch/updateStatus");
    },
  };

  return (
    <StockBatchsUIProvider StockBatchsUIEvents={StockBatchsUIEvents}>
      <StockBatchsLoadingDialog />
      <Route path="/stocks/stockbatch/deleteStockBatchs">
        {({ history, match }) => (
          <StockBatchsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/stockbatch");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/stockbatch/:id/delete">
        {({ history, match }) => (
          <StockBatchDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/stocks/stockbatch");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/stockbatch/fetch">
        {({ history, match }) => (
          <StockBatchsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/stockbatch");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/stockbatch/updateStatus">
        {({ history, match }) => (
          <StockBatchsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/stockbatch");
            }}
          />
        )}
      </Route>
      <StockBatchsCard />
    </StockBatchsUIProvider>
  );
}