import React, {useEffect, useMemo} from 'react';
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/StockBatch/StockBatchsActions";
import * as uiHelpers from "../StockBatchsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useStockBatchsUIContext } from "../StockBatchsUIContext";

export function StockBatchsTable() {
  // StockBatchs UI Context
  const StockBatchsUIContext = useStockBatchsUIContext();
  const StockBatchsUIProps = useMemo(() => {
    return {
      ids: StockBatchsUIContext.ids,
      setIds: StockBatchsUIContext.setIds,
      queryParams: StockBatchsUIContext.queryParams,
      setQueryParams: StockBatchsUIContext.setQueryParams,
      openEditStockBatchPage: StockBatchsUIContext.openEditStockBatchPage,
      openDeleteStockBatchDialog: StockBatchsUIContext.openDeleteStockBatchDialog,
    };
  }, [StockBatchsUIContext]);

  // Getting curret state of StockBatchs list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.StockBatchs }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // StockBatchs Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    StockBatchsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchStockBatchs(StockBatchsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [StockBatchsUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: "batch_id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "batch_quantity",
      text: "Quantity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "batch_created_date",
      text: "Create Date",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "batch_updated_date",
      text: "Update Date",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditStockBatchPage: StockBatchsUIProps.openEditStockBatchPage,
        openDeleteStockBatchDialog: StockBatchsUIProps.openDeleteStockBatchDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: StockBatchsUIProps.queryParams.pageSize,
    page: StockBatchsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  StockBatchsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: StockBatchsUIProps.ids,
                  setIds: StockBatchsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
