import React from "react";
import {
  StockBatchConditionCssClasses,
  StockBatchConditionTitles
} from "../../StockBatchsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        StockBatchConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        StockBatchConditionCssClasses[row.condition]
      }`}
    >
      {StockBatchConditionTitles[row.condition]}
    </span>
  </>
);
