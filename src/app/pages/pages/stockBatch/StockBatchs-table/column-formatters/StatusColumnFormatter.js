import React from "react";
import {
  StockBatchstatusCssClasses,
  StockBatchstatusTitles
} from "../../StockBatchsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      StockBatchstatusCssClasses[row.status]
    } label-inline`}
  >
    {StockBatchstatusTitles[row.status]}
  </span>
);
