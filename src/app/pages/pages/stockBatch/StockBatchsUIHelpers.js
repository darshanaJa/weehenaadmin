export const StockBatchstatusCssClasses = ["success", "info", ""];
export const StockBatchstatusTitles = ["Selling", "Sold"];
export const StockBatchConditionCssClasses = ["success", "danger", ""];
export const StockBatchConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    batch_id: "",
  },
  batch_id:null,
  sortOrder: "asc", // asc||desc
  sortField: "sales_StockBatch_fname",
  pageNumber: 1,
  pageSize: 10
};