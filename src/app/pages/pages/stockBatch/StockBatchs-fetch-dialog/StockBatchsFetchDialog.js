import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { StockBatchstatusCssClasses } from "../StockBatchsUIHelpers";
import { useStockBatchsUIContext } from "../StockBatchsUIContext";

const selectedStockBatchs = (entities, ids) => {
  const _StockBatchs = [];
  ids.forEach((id) => {
    const StockBatch = entities.find((el) => el.id === id);
    if (StockBatch) {
      _StockBatchs.push(StockBatch);
    }
  });
  return _StockBatchs;
};

export function StockBatchsFetchDialog({ show, onHide }) {
  // StockBatchs UI Context
  const StockBatchsUIContext = useStockBatchsUIContext();
  const StockBatchsUIProps = useMemo(() => {
    return {
      ids: StockBatchsUIContext.ids,
      queryParams: StockBatchsUIContext.queryParams,
    };
  }, [StockBatchsUIContext]);

  // StockBatchs Redux state
  const { StockBatchs } = useSelector(
    (state) => ({
      StockBatchs: selectedStockBatchs(state.StockBatchs.entities, StockBatchsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!StockBatchsUIProps.ids || StockBatchsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [StockBatchsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {StockBatchs.map((StockBatch) => (
              <div className="list-timeline-item mb-3" key={StockBatch.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      StockBatchstatusCssClasses[StockBatch.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {StockBatch.id}
                  </span>{" "}
                  <span className="ml-5">
                    {StockBatch.manufacture}, {StockBatch.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
