import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./StockBatchsUIHelpers";

const StockBatchsUIContext = createContext();

export function useStockBatchsUIContext() {
  return useContext(StockBatchsUIContext);
}

export const StockBatchsUIConsumer = StockBatchsUIContext.Consumer;

export function StockBatchsUIProvider({ StockBatchsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newStockBatchButtonClick: StockBatchsUIEvents.newStockBatchButtonClick,
    openEditStockBatchPage: StockBatchsUIEvents.openEditStockBatchPage,
    openDeleteStockBatchDialog: StockBatchsUIEvents.openDeleteStockBatchDialog,
    openDeleteStockBatchsDialog: StockBatchsUIEvents.openDeleteStockBatchsDialog,
    openFetchStockBatchsDialog: StockBatchsUIEvents.openFetchStockBatchsDialog,
    openUpdateStockBatchsStatusDialog: StockBatchsUIEvents.openUpdateStockBatchsStatusDialog,
  };

  return (
    <StockBatchsUIContext.Provider value={value}>
      {children}
    </StockBatchsUIContext.Provider>
  );
}
