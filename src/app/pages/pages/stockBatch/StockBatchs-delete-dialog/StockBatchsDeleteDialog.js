/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/StockBatch/StockBatchsActions";
import { useStockBatchsUIContext } from "../StockBatchsUIContext";

export function StockBatchsDeleteDialog({ show, onHide }) {
  // StockBatchs UI Context
  const StockBatchsUIContext = useStockBatchsUIContext();
  const StockBatchsUIProps = useMemo(() => {
    return {
      ids: StockBatchsUIContext.ids,
      setIds: StockBatchsUIContext.setIds,
      queryParams: StockBatchsUIContext.queryParams,
    };
  }, [StockBatchsUIContext]);

  // StockBatchs Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.StockBatchs.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected StockBatchs we should close modal
  useEffect(() => {
    if (!StockBatchsUIProps.ids || StockBatchsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [StockBatchsUIProps.ids]);

  const deleteStockBatchs = () => {
    // server request for deleting StockBatch by seleted ids
    dispatch(actions.deleteStockBatchs(StockBatchsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchStockBatchs(StockBatchsUIProps.queryParams)).then(() => {
        // clear selections list
        StockBatchsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          StockBatchs Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected StockBatchs?</span>
        )}
        {isLoading && <span>StockBatchs are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteStockBatchs}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
