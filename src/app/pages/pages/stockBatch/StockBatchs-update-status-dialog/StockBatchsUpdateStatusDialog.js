import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { StockBatchstatusCssClasses } from "../StockBatchsUIHelpers";
import * as actions from "../../../_redux/StockBatch/StockBatchsActions";
import { useStockBatchsUIContext } from "../StockBatchsUIContext";

const selectedStockBatchs = (entities, ids) => {
  const _StockBatchs = [];
  ids.forEach((id) => {
    const StockBatch = entities.find((el) => el.id === id);
    if (StockBatch) {
      _StockBatchs.push(StockBatch);
    }
  });
  return _StockBatchs;
};

export function StockBatchsUpdateStatusDialog({ show, onHide }) {
  // StockBatchs UI Context
  const StockBatchsUIContext = useStockBatchsUIContext();
  const StockBatchsUIProps = useMemo(() => {
    return {
      ids: StockBatchsUIContext.ids,
      setIds: StockBatchsUIContext.setIds,
      queryParams: StockBatchsUIContext.queryParams,
    };
  }, [StockBatchsUIContext]);

  // StockBatchs Redux state
  const { StockBatchs, isLoading } = useSelector(
    (state) => ({
      StockBatchs: selectedStockBatchs(state.StockBatchs.entities, StockBatchsUIProps.ids),
      isLoading: state.StockBatchs.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected StockBatchs we should close modal
  useEffect(() => {
    if (StockBatchsUIProps.ids || StockBatchsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [StockBatchsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing StockBatch by ids
    dispatch(actions.updateStockBatchsStatus(StockBatchsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchStockBatchs(StockBatchsUIProps.queryParams)).then(
          () => {
            // clear selections list
            StockBatchsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected StockBatchs
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {StockBatchs.map((StockBatch) => (
              <div className="list-timeline-item mb-3" key={StockBatch.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      StockBatchstatusCssClasses[StockBatch.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {StockBatch.id}
                  </span>{" "}
                  <span className="ml-5">
                    {StockBatch.manufacture}, {StockBatch.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${StockBatchstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
