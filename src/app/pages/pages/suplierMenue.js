import React, { Suspense } from "react";
import { Switch } from "react-router-dom";

import { SupliersPage } from "./suplier/SupliersPage"; 
import {SuplierEdit} from "./suplier/Suplier-edit/SuplierEdit";

import { SpareSuppliersPage } from "./SpareSupplier/SpareSuppliersPage";
import { SpareSupplierEdit } from "./SpareSupplier/SpareSupplier-edit/SpareSupplierEdit";

import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";



export default function suplierMenue() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        <ContentRoute path="/suplier/suplier/new" component={SuplierEdit} />
        <ContentRoute path="/suplier/suplier/:id/edit" component={SuplierEdit} /> 
        <ContentRoute path="/suplier/suplier" component={SupliersPage} />

        <ContentRoute path="/suplier/sparepartsuplier/new" component={SpareSupplierEdit} />
        <ContentRoute path="/suplier/sparepartsuplier/:id/edit" component={SpareSupplierEdit} /> 
        <ContentRoute path="/suplier/sparepartsuplier" component={SpareSuppliersPage} />
      </Switch>
    </Suspense>
  );
}
