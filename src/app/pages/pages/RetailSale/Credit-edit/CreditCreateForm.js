import React, {useEffect,useState, useReducer} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select, Input2 } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import axios from "axios";
import { Shops_URL } from "../../../_redux/shops/shopsCrud";
import { getSalesTicketById, SalesTickets_URL_FIND } from "../../../_redux/salesTickets/salesTicketsCrud";
import { getSpecificationById } from "../../../_redux/specificationsItem/specificationsCrud";
// import { RequestForm } from "./RequestForm";
// import { bindActionCreators } from "redux";
// import { push } from "object-pasth";



const RequestEditSchema = Yup.object().shape({
  
});

export function CreditCreateForm({
  Credit,
  //   btnRef,
  saveCredit,
  Request,
  btnRef,
  saveRequest,
  todos,
  setTodos,
  setSaleId,
  saleId,
  setComment,
  comment,
  storeId,
  setStoreId,
  bag,
  setbag,
  price,
  setprice,
  qu,
  setqu
}) {

  // console.log(Request)
 

  const [shop,setShop]=useState([]);
  const [saleticketId,setSaleTicketdId]=useState([]);
  const [saleItemId,setSaleItemId]=useState([]);
  const [saleItId,setSaleItId]=useState([]);

  const [saleItem,setSaleItem]=useState([]);
  const [saleItemQuntity,setSaleItemQuntity]=useState([]);
  const [saleItemPrice,setSaleItemPrice]=useState([]);
  const [stockName,setStockName]=useState('');

  const itemInitialState = {
    items1 : [],
    count:0,
  }

  const itemReducer =(state, action)=> {
    if(action.type === 'add'){

        const updateItems = state.items1.concat(action.item);
        console.log(action.item)
        console.log(state.items1)
        console.log(updateItems)

      return {items1 : updateItems};

    }
    return itemInitialState;
  }
  const [itemState, itemDispatch] = useReducer(itemReducer, itemInitialState)

  console.log(itemState.items1);
  setTodos(itemState.items1)


const stockNameHandler=(e)=>{
    setSaleItem(e.target.value)
  }

  const quntityHandler=(e)=>{
    setSaleItemQuntity(e.target.value)
}

const priceHandler=(e)=>{
    setSaleItemPrice(e.target.value)    
}

console.log(shop)


  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Shops_URL,
      data: {
        "filter": [{"status": "1"}],
        // "limit": 10,
        // "skip": 0,
        // "sort":"DESC"
      }
      })
      .then((res) => {
        setShop(res.data.data.results)
      })
      .catch(function (response) {
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: SalesTickets_URL_FIND,
      data: {
        "filter": [{"status": "1"}],
        // "limit": 10,
        // "skip": 0,
        // "sort":"DESC"
      }
      })
      .then((res) => {
        console.log(res.data)
        setSaleTicketdId(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  useEffect(()=>{
    getSalesTicketById(saleId)
      .then((res) => {
        console.log(res.data)
        setSaleItemId(res.data.sticketitem)
        // setSaleTicketdId(res.data.data.results)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[saleId])

  useEffect(()=>{
    getSpecificationById(saleItId)
      .then((res) => {

        // setSaleItId('')
        setSaleItem('')
        setSaleItemPrice('')
        setSaleItemQuntity('')

        console.log(res.data)
        setSaleItemPrice(res.data.mstock.item_default_price)
        setSaleItemQuntity(res.data.mstock.item_current_quantity)
        setSaleItem(res.data.mstock.item_id)
        setStockName(res.data.mstock.item_name)
        // setSaleTicketdId(res.data.data.results)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[saleItId])

  const idHandler =(e)=>{
    setSaleId(e.target.value)
  }

  const storeIdHandler =(e)=>{
    setStoreId(e.target.value)
  }

  // const CommentHandler =(e)=>{
  //   setComment(e.target.value)
  // }

  const slaeItemhandler =(e)=>{
    setSaleItId(e.target.value)
}

const submitTodoHandler = (e) =>{

  e.preventDefault();

  const Items4= ({
    ticketitem:{sales_ticket_item_id:saleItId}, 
    itemId:{item_id:saleItem, item_name:stockName}, 
    quantity:saleItemQuntity, 
    price:saleItemPrice
  })

  itemDispatch({type:'add', item:Items4})

  // setStoreId('') 
  setSaleItId('Choose One')
  setStockName('')
  setSaleItemQuntity('') 
  setSaleItemPrice('')

}


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Credit}
        validationSchema={RequestEditSchema}
        onSubmit={(values) => {
          console.log(values);
          saveCredit(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Select name="retailstore" label="Retail Store Name"  onChange={storeIdHandler}>
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.retail_store_id} >
                            {item.store_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                <Select name="sales_ticekt_id" value={saleId} label="Sale Ticket ID" onChange={idHandler}>
                      <option>Choose One</option>
                      {saleticketId.map((item) => (
                          <option value={item.sales_ticket_id} >
                            {item.sales_ticket_id}
                          </option>    
                      ))}
                  </Select>
                </div>
                {/* <div className="col-lg-3" >
                  <Field
                    type="string"
                    name="comment"
                    component={Input}
                    placeholder="Comment"
                    label="Comment"
                    // value={name} 
                    onChange={CommentHandler}
                  />
                </div> */}
                <div className="col-lg-4">
                  <Select name="retailstore" label="sale ticket Item Id" value={saleItId} onChange={slaeItemhandler}>
                      <option>Choose One</option>
                      {saleItemId.map((item) => (
                          <option value={item.sales_ticket_item_id} >
                            {item.sales_ticket_item_id}
                          </option>    
                      ))}
                  </Select>
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-3">
                  <Field
                    type="string"
                    name="price"
                    value={stockName}
                    component={Input}
                    placeholder="Main stock Name"
                    label="Main stock Name"
                    onChange={stockNameHandler}
                  />
                </div>
                <div className="col-lg-3">
                  <Field
                    type="string"
                    name="qty"
                    value={saleItemQuntity}
                    component={Input}
                    placeholder="Quntity"
                    label="Quntity"
                    onChange={quntityHandler}
                  />
                </div>
                <div className="col-lg-3">
                  <Field
                    type="string"
                    name="price"
                    value={saleItemPrice}
                    component={Input}
                    placeholder="Price"
                    label="Price"
                    onChange={priceHandler}
                  />
                </div>
                <div className="col-lg-3">
                  <button type="submit"
                    hidden={ 
                      // setSaleItId('Choose One') ||
                      stockName === '' ||
                      saleItemQuntity === '' ||
                      saleItemPrice === ''
                      //  || setSaleItId('')
                  }
                    onClick={submitTodoHandler} className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                </div>
              </div>

              {itemState.items1.map((item) => (
                <div className="form-group row">
                  <div className="col-lg-3">
                  <Field
                    type="string"
                    name="price"
                    value={item.ticketitem.sales_ticket_item_id}
                    component={Input2}
                    placeholder="Sale Item Id"
                    label="Sale Item Id"
                    onChange={stockNameHandler}
                  />
                  </div>
                  <div className="col-lg-3">
                  <Field
                    type="string"
                    name="price"
                    value={item.itemId.item_name}
                    component={Input2}
                    placeholder="Main stock Name"
                    label="Main stock Name"
                    onChange={stockNameHandler}
                  />
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="string"
                      name="qty"
                      value={item.quantity}
                      component={Input2}
                      placeholder="Quntity"
                      label="Quntity"
                      onChange={quntityHandler}
                    />
                  </div>
                  {/* <div className="col-lg-3">
                    <Field
                      type="string"
                      name="price"
                      value={item.quantity}
                      component={Input}
                      placeholder="Price"
                      label="Price"
                      onChange={priceHandler}
                    />
                  </div> */}
                  <div className="col-lg-3">
                    <Field
                      type="string"
                      name="price"
                      value={item.price}
                      component={Input2}
                      placeholder="Price"
                      label="Price"
                      onChange={priceHandler}
                    />
                  </div>
                </div>
                ))}

              {/* {itemState.items1.map((item) => (
                <div className="form-group row">
                  <div className="col-lg-3">
                  <Field
                    type="string"
                    name="price"
                    value={item.sales_ticket_item}
                    component={Input}
                    placeholder="Main stock Name"
                    label="Main stock Name"
                    onChange={stockNameHandler}
                  />
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="string"
                      name="qty"
                      value={item.main_stock_item}
                      component={Input}
                      placeholder="Quntity"
                      label="Quntity"
                      onChange={quntityHandler}
                    />
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="string"
                      name="price"
                      value={item.qty}
                      component={Input}
                      placeholder="Price"
                      label="Price"
                      onChange={priceHandler}
                    />
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="string"
                      name="price"
                      value={item.price}
                      component={Input}
                      placeholder="Price"
                      label="Price"
                      onChange={priceHandler}
                    />
                  </div>
                </div>
                ))} */}

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="sales_item_quantity"
                    value={qu}
                    component={Input}
                    placeholder="Quanity"
                    label="Quanity"
                    onChange={(e)=>{setqu(e.target.value)}}
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="sales_price"
                    value={price}
                    component={Input}
                    placeholder="Price"
                    label="Price"
                    onChange={(e)=>{setprice(e.target.value)}}
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="bagCount"
                    value={bag}
                    component={Input}
                    placeholder="Bag Count"
                    label="Bag Coun"
                    onChange={(e)=>{setbag(e.target.value)}}
                  />
                </div>
              </div>

              {/* <RequestForm saleItId={saleItId} setSaleItId={setSaleItId} saleItemId={saleItemId} setSaleItemId={setSaleItemId} todos={todos} setTodos={setTodos} saleId={saleId}  />         */}
                <div className="form-group row">
                  <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2" >Save</button>
                 </div>  
                </div> 
              
            
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}




// import React, {useEffect,useState, useReducer} from "react";
// import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
// import { Input, Select } from "../../../../../_metronic/_partials/controls";
// import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
// import axios from "axios";
// import { Shops_URL_GET } from "../../../_redux/shops/shopsCrud";
// import { getSalesTicketById, SalesTickets_URL_FIND } from "../../../_redux/salesTickets/salesTicketsCrud";
// import { getSpecificationById } from "../../../_redux/specificationsItem/specificationsCrud";


// // Validation schema
// const CreditEditSchema = Yup.object().shape({
//   buyback_name: Yup.string()
//     .required("Name is required")
//     .min(2, "Name must be at least 2 characters"),
//     buyback_address: Yup.string()
//     .required("Address is required")
//     .min(2, "Address must be at least 2 characters"),
//   buyback_Credit_quantity: Yup.string()
//     .required("Quntity is required"),
//     buyback_email: Yup.string().email()
//     .required("Email is required"),
//     buyback_contact: Yup.string()
//     .required("Contact Required")
//     .matches(/^[0-9]+$/, "Must be only digits")
//     .min(10, "Contact number be at least 10 numbers")
//     .max(10, "Contact number be at least 10 numbers"),
//     buyback_passed_experiance: Yup.string()
//     .required("Passed Experiance Required")
// });


// export function CreditCreateForm({
//   Credit,
//   btnRef,
//   saveCredit,
// }) {

//   const [shop,setShop]=useState([]);
//   const [saleticketId,setSaleTicketdId]=useState([]);
//   const [saleItemId,setSaleItemId]=useState([]);
//   const [saleItId,setSaleItId]=useState([]);

//   const [saleItem,setSaleItem]=useState([]);
//   const [saleItemQuntity,setSaleItemQuntity]=useState([]);
//   const [saleItemPrice,setSaleItemPrice]=useState([]);
//   const [stockName,setStockName]=useState([]);

//   useEffect(()=>{
//     axios({
//       method: 'get',
//       baseURL: Shops_URL_GET
//       })
//       .then((res) => {
//         setShop(res.data)
//       })
//       .catch(function (response) {
//       });
      
//   },[])


//   return (
//     <div>
//       <Formik
//         enableReinitialize={true}
//         initialValues={Credit}
//         validationSchema={CreditEditSchema}
//         onSubmit={(values)  => {
//           console.log(values);            
//           saveCredit(values);
//         }}
//       >
//         {({ handleSubmit }) => (
//           <>
//             <Form className="form form-label-right">
//               <div className="form-group row">
//                 <div className="col-lg-3">
//                   <Select name="retailstore" label="Retail Store Name"  onChange={storeIdHandler}>
//                       <option>Choose One</option>
//                       {shop.map((item) => (
//                           <option value={item.retail_store_id} >
//                             {item.store_name}
//                           </option>    
//                       ))}
//                   </Select>
//                 </div>
//                 <div className="col-lg-3">
//                 {/* <Select name="sales_ticekt_id" value={saleId} label="Sale Ticket ID" onChange={idHandler}>
//                       <option>Choose One</option>
//                       {saleticketId.map((item) => (
//                           <option value={item.sales_ticket_id} >
//                             {item.sales_ticket_id}
//                           </option>    
//                       ))}
//                   </Select> */}
//                 </div>
//                 {/* <div className="col-lg-3" >
//                   <Field
//                     type="string"
//                     name="comment"
//                     component={Input}
//                     placeholder="Comment"
//                     label="Comment"
//                     // value={name} 
//                     onChange={CommentHandler}
//                   />
//                 </div> */}
//                 <div className="col-lg-3">
//                   {/* <Select name="retailstore" label="sale ticket Item Id" value={saleItId} onChange={slaeItemhandler}>
//                       <option>Choose One</option>
//                       {saleItemId.map((item) => (
//                           <option value={item.sales_ticket_item_id} >
//                             {item.sales_ticket_item_id}
//                           </option>    
//                       ))}
//                   </Select> */}
//                 </div>
//               </div>
//               <div className="form-group row">
//                 <div className="col-lg-3">
//                   <Field
//                     type="string"
//                     name="price"
//                     value={stockName}
//                     component={Input}
//                     placeholder="Main stock Name"
//                     label="Main stock Name"
//                     onChange={stockNameHandler}
//                   />
//                 </div>
//                 <div className="col-lg-3">
//                   <Field
//                     type="string"
//                     name="qty"
//                     value={saleItemQuntity}
//                     component={Input}
//                     placeholder="Quntity"
//                     label="Quntity"
//                     onChange={quntityHandler}
//                   />
//                 </div>
//                 <div className="col-lg-3">
//                   <Field
//                     type="string"
//                     name="price"
//                     value={saleItemPrice}
//                     component={Input}
//                     placeholder="Price"
//                     label="Price"
//                     onChange={priceHandler}
//                   />
//                 </div>
//                 <div className="col-lg-3">
//                   <button type="submit" onClick={submitTodoHandler} className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
//                 </div>
//               </div>

//               {/* {itemState.items1.map((item) => (
//                 <div className="form-group row">
//                   <div className="col-lg-3">
//                   <Field
//                     type="string"
//                     name="price"
//                     value={item.sales_ticket_item}
//                     component={Input}
//                     placeholder="Main stock Name"
//                     label="Main stock Name"
//                     onChange={stockNameHandler}
//                   />
//                   </div>
//                   <div className="col-lg-3">
//                     <Field
//                       type="string"
//                       name="qty"
//                       value={item.main_stock_item}
//                       component={Input}
//                       placeholder="Quntity"
//                       label="Quntity"
//                       onChange={quntityHandler}
//                     />
//                   </div>
//                   <div className="col-lg-3">
//                     <Field
//                       type="string"
//                       name="price"
//                       value={item.qty}
//                       component={Input}
//                       placeholder="Price"
//                       label="Price"
//                       onChange={priceHandler}
//                     />
//                   </div>
//                   <div className="col-lg-3">
//                     <Field
//                       type="string"
//                       name="price"
//                       value={item.price}
//                       component={Input}
//                       placeholder="Price"
//                       label="Price"
//                       onChange={priceHandler}
//                     />
//                   </div>
//                 </div>
//                 ))} */}

//               {/* <RequestForm saleItId={saleItId} setSaleItId={setSaleItId} saleItemId={saleItemId} setSaleItemId={setSaleItemId} todos={todos} setTodos={setTodos} saleId={saleId}  />         */}
//                 <div className="form-group row">
//                   <div className="col-lg-4">
//                   <button type="submit" className="btn btn-primary ml-2" >Save</button>
//                  </div>  
//                 </div> 
              
            
//               <button
//                 type="submit"
//                 style={{ display: "none" }}
//                 ref={btnRef}
//                 onSubmit={() => handleSubmit()}
//               ></button>
//             </Form>
//           </>
//         )}
//       </Formik>
      
//     </div>

//   );
// }
