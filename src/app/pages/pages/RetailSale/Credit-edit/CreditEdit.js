/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Credit/CreditsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { CreditEditForm } from "./CreditEditForm";
import { CreditCreateForm } from "./CreditCreateForm";
import { Specifications } from "../Credit-specifications/Specifications";
import { SpecificationsUIProvider } from "../Credit-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Credit-remarks/RemarksUIContext";
import { Remarks } from "../Credit-remarks/Remarks";
import { getCreditById } from "../../../_redux/Credit/CreditsCrud";

export function CreditEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const dispatch = useDispatch();
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const [todos,setTodos] = useState([]);
  const [storeId,setStoreId]=useState('');
  const [saleId,setSaleId]=useState([]);
  const [bag, setbag] = useState('')
  const [price, setprice] = useState('')
  const [qu, setqu] = useState('')

  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, CreditForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Credits.actionsLoading,
      CreditForEdit: state.Credits.CreditForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchCredit(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Retail Sale";
    if (CreditForEdit && id) {
      // _title = `Edit Credit - ${CreditForEdit.buyback_name} ${CreditForEdit.buyback_contact} - ${CreditForEdit.sales_Credit_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [CreditForEdit, id]);

  console.log(todos)

  const initCredit = {
    sales_item_quantity: qu,
    rstoreRetailStoreId:storeId,
    saletickets:saleId,
    retailsaleItms:todos,
    sales_price: price,
    bagCount:bag,
  
  };

  useEffect(()=>{
    getCreditById(id)
      .then((res) => {
        console.log(res.data.rstore.retail_store_id)
        setStoreId(res.data.rstore.retail_store_id)
        setqu(res.data.sales_item_quantity)
        setprice(res.data.bagprice)
        setbag(res.data.bagCount)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[id])

  const saveCredit = (values) => {
    if (!id) {
      dispatch(actions.createCredit(values,id)).then(() => backToCreditsList());
    } else {
      dispatch(actions.updateCredit(values,id)).then(() => backToCreditsList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateCreditPassword(values,id)).then(() => backToCreditsList());
  };

  const btnRef = useRef();

  const backToCreditsList = () => {
    history.push(`/sales/reatails`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToCreditsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Invoice PDF
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <CreditEditForm
                actionsLoading={actionsLoading}
                Credit={initCredit}
                btnRef={btnRef}
                setTodos={setTodos}
                setStoreId={setStoreId}
                saveCredit={saveCredit}
                setSaleId={setSaleId}
                saleId={saleId}
                bag={bag}
                setbag={setbag}
                price={price}
                setprice={setprice}
                qu={qu}
                setqu={setqu}
                storeId={storeId}
                id={id}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentCreditId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id} currentCreditId={id}>
                <Specifications id={id} />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToCreditsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Credit remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Credit specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <CreditCreateForm
                actionsLoading={actionsLoading}
                Credit={CreditForEdit || initCredit}
                btnRef={btnRef}
                setTodos={setTodos}
                setStoreId={setStoreId}
                saveCredit={saveCredit}
                setSaleId={setSaleId}
                saleId={saleId}
                savePassword={savePassword}
                setbag={setbag}
                price={price}
                setprice={setprice}
                qu={qu}
                setqu={setqu}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentCreditId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentCreditId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
