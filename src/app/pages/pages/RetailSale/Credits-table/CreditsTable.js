// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Credit/CreditsActions";
import * as uiHelpers from "../CreditsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useCreditsUIContext } from "../CreditsUIContext";

export function CreditsTable() {
  // Credits UI Context
  const CreditsUIContext = useCreditsUIContext();
  const CreditsUIProps = useMemo(() => {
    return {
      ids: CreditsUIContext.ids,
      setIds: CreditsUIContext.setIds,
      queryParams: CreditsUIContext.queryParams,
      setQueryParams: CreditsUIContext.setQueryParams,
      openEditCreditPage: CreditsUIContext.openEditCreditPage,
      openDeleteCreditDialog: CreditsUIContext.openDeleteCreditDialog,
    };
  }, [CreditsUIContext]);

  // Getting curret state of Credits list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Credits }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Credits Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    CreditsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchCredits(CreditsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [CreditsUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Credit_id);

  const columns = [
    // {
    //   dataField: "sales_Credit_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "rstore.store_name",
      text: "Customer Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "sales_item_quantity",
      text: "Quantity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "sales_price",
      text: "Price",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "bagCount",
      text: "Bag Count",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "bagprice",
      text: "Bag Price",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    // {
    //   dataField: "buyback_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_Credit_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditCreditPage: CreditsUIProps.openEditCreditPage,
        openDeleteCreditDialog: CreditsUIProps.openDeleteCreditDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: CreditsUIProps.queryParams.pageSize,
    page: CreditsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Credit_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  CreditsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: CreditsUIProps.ids,
                  setIds: CreditsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
