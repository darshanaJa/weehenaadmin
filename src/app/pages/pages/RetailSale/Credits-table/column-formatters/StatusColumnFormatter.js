import React from "react";
import {
  CreditstatusCssClasses,
  CreditstatusTitles
} from "../../CreditsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      CreditstatusCssClasses[row.status]
    } label-inline`}
  >
    {CreditstatusTitles[row.status]}
  </span>
);
