/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Credit/CreditsActions";
import { useCreditsUIContext } from "../CreditsUIContext";

export function CreditsDeleteDialog({ show, onHide }) {
  // Credits UI Context
  const CreditsUIContext = useCreditsUIContext();
  const CreditsUIProps = useMemo(() => {
    return {
      ids: CreditsUIContext.ids,
      setIds: CreditsUIContext.setIds,
      queryParams: CreditsUIContext.queryParams,
    };
  }, [CreditsUIContext]);

  // Credits Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Credits.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Credits we should close modal
  useEffect(() => {
    if (!CreditsUIProps.ids || CreditsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [CreditsUIProps.ids]);

  const deleteCredits = () => {
    // server request for deleting Credit by seleted ids
    dispatch(actions.deleteCredits(CreditsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchCredits(CreditsUIProps.queryParams)).then(() => {
        // clear selections list
        CreditsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Credits Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Credits?</span>
        )}
        {isLoading && <span>Credits are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteCredits}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
