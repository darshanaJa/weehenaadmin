import React from "react";
import { Route } from "react-router-dom";
import { CreditsLoadingDialog } from "./Credits-loading-dialog/CreditsLoadingDialog";
import { CreditDeleteDialog } from "./Credit-delete-dialog/CreditDeleteDialog";
import { CreditsDeleteDialog } from "./Credits-delete-dialog/CreditsDeleteDialog";
import { CreditsFetchDialog } from "./Credits-fetch-dialog/CreditsFetchDialog";
import { CreditsUpdateStatusDialog } from "./Credits-update-status-dialog/CreditsUpdateStatusDialog";
import { CreditsCard } from "./CreditsCard";
import { CreditsUIProvider } from "./CreditsUIContext";

export function CreditsPage({ history }) {
  const CreditsUIEvents = {
    newCreditButtonClick: () => {
      history.push("/sales/reatails/new");
    },
    openEditCreditPage: (id) => {
      history.push(`/sales/reatails/${id}/edit`);
    },
    openDeleteCreditDialog: (sales_Credit_id) => {
      history.push(`/sales/reatails/${sales_Credit_id}/delete`);
    },
    openDeleteCreditsDialog: () => {
      history.push(`/sales/reatails/deleteCredits`);
    },
    openFetchCreditsDialog: () => {
      history.push(`/sales/reatails/fetch`);
    },
    openUpdateCreditsStatusDialog: () => {
      history.push("/sales/reatails/updateStatus");
    },
  };

  return (
    <CreditsUIProvider CreditsUIEvents={CreditsUIEvents}>
      <CreditsLoadingDialog />
      <Route path="/sales/reatails/deleteCredits">
        {({ history, match }) => (
          <CreditsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/reatails");
            }}
          />
        )}
      </Route>
      <Route path="/sales/reatails/:sales_Credit_id/delete">
        {({ history, match }) => (
          <CreditDeleteDialog
            show={match != null}
            sales_Credit_id={match && match.params.sales_Credit_id}
            onHide={() => {
              history.push("/sales/reatails");
            }}
          />
        )}
      </Route>
      <Route path="/sales/reatails/fetch">
        {({ history, match }) => (
          <CreditsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/reatails");
            }}
          />
        )}
      </Route>
      <Route path="/sales/reatails/updateStatus">
        {({ history, match }) => (
          <CreditsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/reatails");
            }}
          />
        )}
      </Route>
      <CreditsCard />
    </CreditsUIProvider>
  );
}
