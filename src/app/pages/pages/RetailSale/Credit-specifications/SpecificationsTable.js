// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useState, useEffect } from "react";
import { getCreditById } from "../../../_redux/Credit/CreditsCrud";
import { Image_Url2 } from "../../../../config/config";

export function SpecificationsTable({id}) {

  const [pdf, setpdf] = useState()

  console.log(id)

  useEffect(() => {

  getCreditById(id)
  .then((res)=>{
    setpdf(res.data.file_url2)
  })

  }, [id])

  const file = Image_Url2 + pdf
  console.log(file);

  return (
    <>
      {/* <div className="form-group row"> */}
        <div className="col-lg-12">
          <iframe 
            width="550px"
            title="pdf2" height="650px" src={file} />
        {/* </div> */}
      </div>
    </>
  );
}
