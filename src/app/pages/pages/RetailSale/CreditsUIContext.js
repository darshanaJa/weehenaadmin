import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./CreditsUIHelpers";

const CreditsUIContext = createContext();

export function useCreditsUIContext() {
  return useContext(CreditsUIContext);
}

export const CreditsUIConsumer = CreditsUIContext.Consumer;

export function CreditsUIProvider({ CreditsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newCreditButtonClick: CreditsUIEvents.newCreditButtonClick,
    openEditCreditPage: CreditsUIEvents.openEditCreditPage,
    openDeleteCreditDialog: CreditsUIEvents.openDeleteCreditDialog,
    openDeleteCreditsDialog: CreditsUIEvents.openDeleteCreditsDialog,
    openFetchCreditsDialog: CreditsUIEvents.openFetchCreditsDialog,
    openUpdateCreditsStatusDialog: CreditsUIEvents.openUpdateCreditsStatusDialog,
  };

  return (
    <CreditsUIContext.Provider value={value}>
      {children}
    </CreditsUIContext.Provider>
  );
}
