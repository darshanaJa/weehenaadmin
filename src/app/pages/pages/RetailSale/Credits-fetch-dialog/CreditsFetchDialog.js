import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { CreditstatusCssClasses } from "../CreditsUIHelpers";
import { useCreditsUIContext } from "../CreditsUIContext";

const selectedCredits = (entities, ids) => {
  const _Credits = [];
  ids.forEach((id) => {
    const Credit = entities.find((el) => el.id === id);
    if (Credit) {
      _Credits.push(Credit);
    }
  });
  return _Credits;
};

export function CreditsFetchDialog({ show, onHide }) {
  // Credits UI Context
  const CreditsUIContext = useCreditsUIContext();
  const CreditsUIProps = useMemo(() => {
    return {
      ids: CreditsUIContext.ids,
      queryParams: CreditsUIContext.queryParams,
    };
  }, [CreditsUIContext]);

  // Credits Redux state
  const { Credits } = useSelector(
    (state) => ({
      Credits: selectedCredits(state.Credits.entities, CreditsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!CreditsUIProps.ids || CreditsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [CreditsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Credits.map((Credit) => (
              <div className="list-timeline-item mb-3" key={Credit.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      CreditstatusCssClasses[Credit.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Credit.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Credit.manufacture}, {Credit.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
