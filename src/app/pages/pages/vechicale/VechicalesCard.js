import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { VechicalesFilter } from "./Vechicales-filter/VechicalesFilter";
import { VechicalesTable } from "./Vechicales-table/VechicalesTable";
import { VechicalesGrouping } from "./Vechicales-grouping/VechicalesGrouping";
import { useVechicalesUIContext } from "./VechicalesUIContext";
import { notify } from "../../../config/Toastify";

export function VechicalesCard() {
  const VechicalesUIContext = useVechicalesUIContext();
  const VechicalesUIProps = useMemo(() => {
    return {
      ids: VechicalesUIContext.ids,
      queryParams: VechicalesUIContext.queryParams,
      setQueryParams: VechicalesUIContext.setQueryParams,
      newVechicaleButtonClick: VechicalesUIContext.newVechicaleButtonClick,
      openDeleteVechicalesDialog: VechicalesUIContext.openDeleteVechicalesDialog,
      openEditVechicalePage: VechicalesUIContext.openEditVechicalePage,
      openUpdateVechicalesStatusDialog:VechicalesUIContext.openUpdateVechicalesStatusDialog,
      openFetchVechicalesDialog: VechicalesUIContext.openFetchVechicalesDialog,
    };
  }, [VechicalesUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Vechicale list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={VechicalesUIProps.newVechicaleButtonClick}
          >
            New Vechicale
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <VechicalesFilter />
        {VechicalesUIProps.ids.length > 0 && (
          <>
            <VechicalesGrouping />
          </>
        )}
        <VechicalesTable />
      </CardBody>
    </Card>
  );
}
