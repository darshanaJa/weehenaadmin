/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Vechicale/VechicalesActions";
import { useVechicalesUIContext } from "../VechicalesUIContext";

export function VechicalesDeleteDialog({ show, onHide }) {
  // Vechicales UI Context
  const VechicalesUIContext = useVechicalesUIContext();
  const VechicalesUIProps = useMemo(() => {
    return {
      ids: VechicalesUIContext.ids,
      setIds: VechicalesUIContext.setIds,
      queryParams: VechicalesUIContext.queryParams,
    };
  }, [VechicalesUIContext]);

  // Vechicales Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Vechicales.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Vechicales we should close modal
  useEffect(() => {
    if (!VechicalesUIProps.ids || VechicalesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicalesUIProps.ids]);

  const deleteVechicales = () => {
    // server request for deleting Vechicale by seleted ids
    dispatch(actions.deleteVechicales(VechicalesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchVechicales(VechicalesUIProps.queryParams)).then(() => {
        // clear selections list
        VechicalesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Vechicales Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Vechicales?</span>
        )}
        {isLoading && <span>Vechicales are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteVechicales}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
