import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { VechicalestatusCssClasses } from "../VechicalesUIHelpers";
import * as actions from "../../../_redux/Vechicale/VechicalesActions";
import { useVechicalesUIContext } from "../VechicalesUIContext";

const selectedVechicales = (entities, ids) => {
  const _Vechicales = [];
  ids.forEach((id) => {
    const Vechicale = entities.find((el) => el.id === id);
    if (Vechicale) {
      _Vechicales.push(Vechicale);
    }
  });
  return _Vechicales;
};

export function VechicalesUpdateStatusDialog({ show, onHide }) {
  // Vechicales UI Context
  const VechicalesUIContext = useVechicalesUIContext();
  const VechicalesUIProps = useMemo(() => {
    return {
      ids: VechicalesUIContext.ids,
      setIds: VechicalesUIContext.setIds,
      queryParams: VechicalesUIContext.queryParams,
    };
  }, [VechicalesUIContext]);

  // Vechicales Redux state
  const { Vechicales, isLoading } = useSelector(
    (state) => ({
      Vechicales: selectedVechicales(state.Vechicales.entities, VechicalesUIProps.ids),
      isLoading: state.Vechicales.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Vechicales we should close modal
  useEffect(() => {
    if (VechicalesUIProps.ids || VechicalesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicalesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Vechicale by ids
    dispatch(actions.updateVechicalesStatus(VechicalesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchVechicales(VechicalesUIProps.queryParams)).then(
          () => {
            // clear selections list
            VechicalesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Vechicales
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Vechicales.map((Vechicale) => (
              <div className="list-timeline-item mb-3" key={Vechicale.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      VechicalestatusCssClasses[Vechicale.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Vechicale.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Vechicale.manufacture}, {Vechicale.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${VechicalestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
