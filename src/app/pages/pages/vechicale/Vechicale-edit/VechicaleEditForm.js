
import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { Image_Url } from "../../../../config/config";
import Admin from "../../../../config/Admin";


// Validation schema
const VechicaleEditSchema = Yup.object().shape({
  // id: Yup.string(),
  vehi_reg_number: Yup.string()
    .required("Register Number is required")
    .min(2, "Register Number must be at least 2 characters"),
    vehi_brand: Yup.string()
    .required("Brand is required")
    .min(2, "Brand must be at least 2 characters"),
    vehi_book: Yup.string()
    .required("Book is required")
    .min(2, "Book must be at least 2 characters"),
    vehi_reg_milage: Yup.string()
    .required("Milage is required")
    .min(2, "Milage must be at least 2 characters"),
    vehi_model: Yup.string()
    .required("Model is required")
    .min(2, "Model must be at least 2 characters"),
    vehi_manifac_year: Yup.number()
    .required("Year is required")
    .min(2, "Year must be at least 2 characters"),
    vehi_reg_year: Yup.number()
    .required("Register is required")
    .min(2, "Register must be at least 2 characters"),
    vehi_owner_contact: Yup.string()
    .required("Contact Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
});


export function VechicaleEditForm({
  Vechicale,
  btnRef,
  saveVechicale,
  image,
  adminId,
  createDate,
  updateDate
}) {

  const [store_image,set_Profile_pic]=useState('');

  console.log(store_image)
  const pic=image
  console.log(pic)
  const url = Image_Url+pic
  // console.log(url)

  const sumitImage=()=>{
    return(
      <div>
        <img  className="shopImg" alt="shop" src={url} />
      </div>
      
    );
  }

  const bodyFormData = new FormData();

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={Vechicale}
        validationSchema={VechicaleEditSchema}
        onSubmit={(values)  => {
          bodyFormData.append('vehi_reg_number',values.vehi_reg_number);
          bodyFormData.append('vehi_brand',values.vehi_brand);
          bodyFormData.append('vehi_model',values.vehi_model);
          bodyFormData.append('vehi_manifac_year',values.vehi_manifac_year);
          bodyFormData.append('vehi_reg_year',values.vehi_reg_year);
          bodyFormData.append('vehi_date_purchase',values.vehi_date_purchase);
          bodyFormData.append('vehi_book',values.vehi_book);
          bodyFormData.append('vehi_owner_contact',values.vehi_owner_contact);
          bodyFormData.append('vehi_reg_milage',values.vehi_reg_milage);
          bodyFormData.append('vehi_image',store_image);

          console.log(values);            
          saveVechicale(bodyFormData);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="vehi_reg_number"
                    component={Input}
                    placeholder="Register Number"
                    label="Register Number"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="vehi_brand"
                    component={Input}
                    placeholder="Brand"
                    label="Brand"
                  />
                </div>
                <div className="col-lg-4">
                    <Field
                        type="text"
                        name="vehi_model"
                        component={Input}
                        placeholder="Model"
                        label="Model"
                      />
                  </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="vehi_manifac_year"
                      component={Input}
                      placeholder="Manufacture Year"
                      label="Manufacture Year"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="vehi_reg_year"
                    component={Input}
                    placeholder="Register year"
                    label="Register year"
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="date"
                    name="vehi_date_purchase"
                    component={Input}
                    placeholder="Purschase date"
                    label="Purschase date"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="vehi_book"
                      component={Input}
                      placeholder="Book"
                      label="Book"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="vehi_owner_contact"
                    component={Input}
                    placeholder="Owner Contact"
                    label="Owner Contact"
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="vehi_reg_milage"
                    component={Input}
                    placeholder="Milage"
                    label="Milage"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                    <div className="form-group row">
                      <input className="agentImageBtn" type="file" name="store_image"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />

                    </div>
                  </div> 
                  <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2" >Save</button>
                 </div>  
              </div>
              <div className="form-group row">
              <div className="col-lg-4">
                  {sumitImage()}
                </div>
            </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={adminId}
                  createDate={createDate} 
                  updateDate={updateDate}
                />
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
