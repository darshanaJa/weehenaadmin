/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Vechicale/VechicalesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { VechicaleEditForm } from "./VechicaleEditForm";
import { VechicaleCreateForm } from "./VechicaleCreateForm";
// import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../Vechicale-specifications/Specifications";
import { SpecificationsUIProvider } from "../Vechicale-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Vechicale-remarks/RemarksUIContext";
import { Remarks } from "../Vechicale-remarks/Remarks";
import { Vechicales_GET_ID_URL } from "../../../_redux/Vechicale/VechicalesCrud";
import axios from "axios";
import moment from 'moment';

export function VechicaleEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();
  const[vehi_reg_number, setvehi_reg_number] = useState('');
  const[vehi_brand, setvehi_brand] = useState('');
  const[vehi_model, setvehi_model] = useState('');
  const[vehi_manifac_year, setvehi_manifac_year] = useState('');
  const[vehi_reg_year, setvehi_reg_year] = useState('');
  const[vehi_date_purchase, setvehi_date_purchase] = useState('');
  const[vehi_book, setvehi_book] = useState('');
  const[vehi_owner_contact, setvehi_owner_contact] = useState('');
  const[vehi_reg_milage, setvehi_reg_milage] = useState('');
  const[image, setImage] = useState('');
  const [adminId, setadminId] = useState('')
  const [createDate1, setcreateDate1] = useState('')
  const [updateDate, setupdateDate] = useState('')

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();

  const { actionsLoading, VechicaleForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Vechicales.actionsLoading,
      VechicaleForEdit: state.Vechicales.VechicaleForEdit,
    }),
    shallowEqual
  );

  useEffect(()=>{
    console.log("abcd")
    axios({
      method: 'get',
      baseURL: Vechicales_GET_ID_URL +`/${id}`
      })
      .then((res) => {
        console.log(res.data.vehi_image)
        setvehi_reg_number(res.data.vehi_reg_number)
        setvehi_brand(res.data.vehi_brand)
        setvehi_model(res.data.vehi_model)
        setvehi_manifac_year(moment(res.data.vehi_manifac_year).format("yyyy"))
        setvehi_reg_year(moment(res.data.vehi_reg_year).format("yyyy"))
        setvehi_date_purchase(moment(res.data.vehi_date_purchase).format("yyyy-MM-DD"))
        setvehi_book(res.data.vehi_book)
        setvehi_owner_contact(res.data.vehi_owner_contact)
        setvehi_reg_milage(res.data.vehi_reg_milage)
        setImage(res.data.vehi_image)

        setadminId(res.data.created_by.id);
        setcreateDate1(res.data.vehicle_created);
        setupdateDate(res.data.vehicle_updated);
        

      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[id])


  const initVechicale = {
  
    vehi_reg_number:vehi_reg_number,
    vehi_brand:vehi_brand,
    vehi_model:vehi_model,
    vehi_manifac_year:vehi_manifac_year,
    vehi_reg_year:vehi_reg_year,
    vehi_date_purchase:vehi_date_purchase,
    vehi_book:vehi_book,
    vehi_owner_contact:vehi_owner_contact,
    vehi_reg_milage:vehi_reg_milage 
    
    };

  useEffect(() => {
    dispatch(actions.fetchVechicale(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Vechicale";
    if (VechicaleForEdit && id) {
      // _title = `Edit Vechicale - ${VechicaleForEdit.buyback_name} ${VechicaleForEdit.buyback_contact} - ${VechicaleForEdit.sales_Vechicale_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicaleForEdit, id]);

  const saveVechicale = (values) => {
    if (!id) {
      dispatch(actions.createVechicale(values,id)).then(() => backToVechicalesList());
    } else {
      dispatch(actions.updateVechicale(values,id)).then(() => backToVechicalesList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateVechicalePassword(values,id)).then(() => backToVechicalesList());
  };

  const btnRef = useRef();

  const backToVechicalesList = () => {
    history.push(`/transport/vechicale`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToVechicalesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <VechicaleEditForm
                actionsLoading={actionsLoading}
                Vechicale={initVechicale}
                btnRef={btnRef}
                saveVechicale={saveVechicale}
                image={image}
                adminId={adminId}
                createDate={createDate1}
                updateDate={updateDate}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentVechicaleId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentVechicaleId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToVechicalesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            
            {`  `}
            
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Vechicale remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Vechicale specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <VechicaleCreateForm
                actionsLoading={actionsLoading}
                Vechicale={VechicaleForEdit || initVechicale}
                btnRef={btnRef}
                saveVechicale={saveVechicale}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentVechicaleId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentVechicaleId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
