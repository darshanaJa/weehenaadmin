// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Vechicale/VechicalesActions";
import * as uiHelpers from "../VechicalesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useVechicalesUIContext } from "../VechicalesUIContext";

export function VechicalesTable() {
  // Vechicales UI Context
  const VechicalesUIContext = useVechicalesUIContext();
  const VechicalesUIProps = useMemo(() => {
    return {
      ids: VechicalesUIContext.ids,
      setIds: VechicalesUIContext.setIds,
      queryParams: VechicalesUIContext.queryParams,
      setQueryParams: VechicalesUIContext.setQueryParams,
      openEditVechicalePage: VechicalesUIContext.openEditVechicalePage,
      openDeleteVechicaleDialog: VechicalesUIContext.openDeleteVechicaleDialog,
    };
  }, [VechicalesUIContext]);

  // Getting curret state of Vechicales list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Vechicales }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Vechicales Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    VechicalesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchVechicales(VechicalesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicalesUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Vechicale_id);

  const columns = [
    // {
    //   dataField: "sales_Vechicale_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "vehi_reg_number",
      text: "Register Number",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "vehi_brand",
      text: "Brand",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "vehi_model",
      text: "Model",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "vehi_manifac_year",
      text: "Manufature Year",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "vehi_reg_year",
      text: "Register Year",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "vehi_date_purchase",
      text: "Purchase Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "vehi_book",
      text: "Book",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "vehi_owner_contact",
      text: "Contact",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "vehi_reg_milage",
      text: "Milage",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditVechicalePage: VechicalesUIProps.openEditVechicalePage,
        openDeleteVechicaleDialog: VechicalesUIProps.openDeleteVechicaleDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: VechicalesUIProps.queryParams.pageSize,
    page: VechicalesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Vechicale_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  VechicalesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: VechicalesUIProps.ids,
                  setIds: VechicalesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
