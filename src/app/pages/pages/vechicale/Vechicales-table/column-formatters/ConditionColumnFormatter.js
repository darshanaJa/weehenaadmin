import React from "react";
import {
  VechicaleConditionCssClasses,
  VechicaleConditionTitles
} from "../../VechicalesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        VechicaleConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        VechicaleConditionCssClasses[row.condition]
      }`}
    >
      {VechicaleConditionTitles[row.condition]}
    </span>
  </>
);
