import React from "react";
import {
  VechicalestatusCssClasses,
  VechicalestatusTitles
} from "../../VechicalesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      VechicalestatusCssClasses[row.status]
    } label-inline`}
  >
    {VechicalestatusTitles[row.status]}
  </span>
);
