import React from "react";
import { Route } from "react-router-dom";
import { VechicalesLoadingDialog } from "./Vechicales-loading-dialog/VechicalesLoadingDialog";
import { VechicaleDeleteDialog } from "./Vechicale-delete-dialog/VechicaleDeleteDialog";
import { VechicalesDeleteDialog } from "./Vechicales-delete-dialog/VechicalesDeleteDialog";
import { VechicalesFetchDialog } from "./Vechicales-fetch-dialog/VechicalesFetchDialog";
import { VechicalesUpdateStatusDialog } from "./Vechicales-update-status-dialog/VechicalesUpdateStatusDialog";
import { VechicalesCard } from "./VechicalesCard";
import { VechicalesUIProvider } from "./VechicalesUIContext";

export function VechicalesPage({ history }) {
  const VechicalesUIEvents = {
    newVechicaleButtonClick: () => {
      history.push("/transport/vechicale/new");
    },
    openEditVechicalePage: (id) => {
      history.push(`/transport/vechicale/${id}/edit`);
    },
    openDeleteVechicaleDialog: (sales_Vechicale_id) => {
      history.push(`/transport/vechicale/${sales_Vechicale_id}/delete`);
    },
    openDeleteVechicalesDialog: () => {
      history.push(`/transport/vechicale/deleteVechicales`);
    },
    openFetchVechicalesDialog: () => {
      history.push(`/transport/vechicale/fetch`);
    },
    openUpdateVechicalesStatusDialog: () => {
      history.push("/transport/vechicale/updateStatus");
    },
  };

  return (
    <VechicalesUIProvider VechicalesUIEvents={VechicalesUIEvents}>
      <VechicalesLoadingDialog />
      <Route path="/transport/vechicale/deleteVechicales">
        {({ history, match }) => (
          <VechicalesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/vechicale");
            }}
          />
        )}
      </Route>
      <Route path="/transport/vechicale/:sales_Vechicale_id/delete">
        {({ history, match }) => (
          <VechicaleDeleteDialog
            show={match != null}
            sales_Vechicale_id={match && match.params.sales_Vechicale_id}
            onHide={() => {
              history.push("/transport/vechicale");
            }}
          />
        )}
      </Route>
      <Route path="/transport/vechicale/fetch">
        {({ history, match }) => (
          <VechicalesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/vechicale");
            }}
          />
        )}
      </Route>
      <Route path="/transport/vechicale/updateStatus">
        {({ history, match }) => (
          <VechicalesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/vechicale");
            }}
          />
        )}
      </Route>
      <VechicalesCard />
    </VechicalesUIProvider>
  );
}
