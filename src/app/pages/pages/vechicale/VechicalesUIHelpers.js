export const VechicalestatusCssClasses = ["success", "info", ""];
export const VechicalestatusTitles = ["Selling", "Sold"];
export const VechicaleConditionCssClasses = ["success", "danger", ""];
export const VechicaleConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_Vechicale_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    vehi_reg_number: ""
  },
  vehi_reg_number:null,
  sortOrder: "asc", // asc||desc
  sortField: "vehi_reg_number",
  pageNumber: 1,
  pageSize: 10
};
// export const AVAILABLE_COLORS = [
//   "Red",
//   "CadetBlue",
//   "Eagle",
//   "Gold",
//   "LightSlateGrey",
//   "RoyalBlue",
//   "Crimson",
//   "Blue",
//   "Sienna",
//   "Indigo",
//   "Green",
//   "Violet",
//   "GoldenRod",
//   "OrangeRed",
//   "Khaki",
//   "Teal",
//   "Purple",
//   "Orange",
//   "Pink",
//   "Black",
//   "DarkTurquoise"
// ];

// export const AVAILABLE_MANUFACTURES = [
//   "Pontiac",
//   "Kia",
//   "Lotus",
//   "Subaru",
//   "Jeep",
//   "Isuzu",
//   "Mitsubishi",
//   "Oldsmobile",
//   "Chevrolet",
//   "Chrysler",
//   "Audi",
//   "Suzuki",
//   "GMC",
//   "Cadillac",
//   "Infinity",
//   "Mercury",
//   "Dodge",
//   "Ram",
//   "Lexus",
//   "Lamborghini",
//   "Honda",
//   "Nissan",
//   "Ford",
//   "Hyundai",
//   "Saab",
//   "Toyota"
// ];
