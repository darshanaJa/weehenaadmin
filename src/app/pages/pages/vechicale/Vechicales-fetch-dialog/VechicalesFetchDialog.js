import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { VechicalestatusCssClasses } from "../VechicalesUIHelpers";
import { useVechicalesUIContext } from "../VechicalesUIContext";

const selectedVechicales = (entities, ids) => {
  const _Vechicales = [];
  ids.forEach((id) => {
    const Vechicale = entities.find((el) => el.id === id);
    if (Vechicale) {
      _Vechicales.push(Vechicale);
    }
  });
  return _Vechicales;
};

export function VechicalesFetchDialog({ show, onHide }) {
  // Vechicales UI Context
  const VechicalesUIContext = useVechicalesUIContext();
  const VechicalesUIProps = useMemo(() => {
    return {
      ids: VechicalesUIContext.ids,
      queryParams: VechicalesUIContext.queryParams,
    };
  }, [VechicalesUIContext]);

  // Vechicales Redux state
  const { Vechicales } = useSelector(
    (state) => ({
      Vechicales: selectedVechicales(state.Vechicales.entities, VechicalesUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!VechicalesUIProps.ids || VechicalesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicalesUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Vechicales.map((Vechicale) => (
              <div className="list-timeline-item mb-3" key={Vechicale.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      VechicalestatusCssClasses[Vechicale.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Vechicale.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Vechicale.manufacture}, {Vechicale.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
