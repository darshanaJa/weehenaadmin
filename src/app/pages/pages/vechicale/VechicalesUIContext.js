import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./VechicalesUIHelpers";

const VechicalesUIContext = createContext();

export function useVechicalesUIContext() {
  return useContext(VechicalesUIContext);
}

export const VechicalesUIConsumer = VechicalesUIContext.Consumer;

export function VechicalesUIProvider({ VechicalesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newVechicaleButtonClick: VechicalesUIEvents.newVechicaleButtonClick,
    openEditVechicalePage: VechicalesUIEvents.openEditVechicalePage,
    openDeleteVechicaleDialog: VechicalesUIEvents.openDeleteVechicaleDialog,
    openDeleteVechicalesDialog: VechicalesUIEvents.openDeleteVechicalesDialog,
    openFetchVechicalesDialog: VechicalesUIEvents.openFetchVechicalesDialog,
    openUpdateVechicalesStatusDialog: VechicalesUIEvents.openUpdateVechicalesStatusDialog,
  };

  return (
    <VechicalesUIContext.Provider value={value}>
      {children}
    </VechicalesUIContext.Provider>
  );
}
