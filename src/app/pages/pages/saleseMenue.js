import React, { Suspense } from "react";
import { Switch } from "react-router-dom";

import { AgentsPage } from "./agent/AgentsPage"; 
import {AgentEdit} from "./agent/agent-edit/AgentEdit";

import { SalesHistoryPage } from "./salesHistory/SalesHistoryPage"; 
import { SaleHistoryEdit } from "./salesHistory/saleHistory-edit/SaleHistoryEdit"

import { SalesTicketsPage } from "./salesTickets/SalesTicketsPage";
import { SalesTicketEdit } from "./salesTickets/salesTicket-edit/SalesTicketEdit";

import { ShopsPage } from "./shops/ShopsPage";
import { ShopEdit } from "./shops/shop-edit/ShopEdit";

import { RequestsPage } from "./Request/RequestsPage";
import { RequestEdit } from "./Request/Request-edit/RequestEdit";

import  {AgentsCard}  from "./track-agent/AgentsCard";

import { PaymentEdit } from "./Payment/Payment-edit/PaymentEdit";
import { PaymentsPage } from "./Payment/PaymentsPage";

import { CreditsPage } from "./RetailSale/CreditsPage";
import { CreditEdit } from "./RetailSale/Credit-edit/CreditEdit";


import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";

export default function saleseMenue() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        <ContentRoute path="/sales/agent/new" component={AgentEdit} />
        <ContentRoute path="/sales/agent/:id/edit" component={AgentEdit} /> 
        <ContentRoute path="/sales/agent" component={AgentsPage} />
        
        <ContentRoute path="/sales/tickets/new" component={SalesTicketEdit} />
        <ContentRoute path="/sales/tickets/:id/edit" component={SalesTicketEdit} />
        <ContentRoute path="/sales/tickets" component={SalesTicketsPage} />
        
        <ContentRoute path="/sales/history/:id/edit" component={SaleHistoryEdit} /> 
        <ContentRoute path="/sales/history/new" component={SaleHistoryEdit} />
        <ContentRoute path="/sales/history" component={SalesHistoryPage} />

        <ContentRoute path="/sales/shops/:id/edit" component={ShopEdit} /> 
        <ContentRoute path="/sales/shops/new" component={ShopEdit} />
        <ContentRoute path="/sales/shops" component={ShopsPage} />

        <ContentRoute path="/sales/requests/:id/edit" component={RequestEdit} /> 
        <ContentRoute path="/sales/requests/new" component={RequestEdit} />
        <ContentRoute path="/sales/requests" component={RequestsPage} />

        <ContentRoute path="/sales/payments/:id/edit" component={PaymentEdit} /> 
        <ContentRoute path="/sales/payments/new" component={PaymentEdit} />
        <ContentRoute path="/sales/payments" component={PaymentsPage} />

        <ContentRoute path="/sales/reatails/:id/edit" component={CreditEdit} /> 
        <ContentRoute path="/sales/reatails/new" component={CreditEdit} />
        <ContentRoute path="/sales/reatails" component={CreditsPage} />

        <ContentRoute path="/sales/track" component={AgentsCard} />

      </Switch>
    </Suspense>
  );
}
