import React, { Suspense } from "react";
import { Switch } from "react-router-dom";

import { VechicalesPage } from "./vechicale/VechicalesPage"; 
import {VechicaleEdit} from "./vechicale/Vechicale-edit/VechicaleEdit"



import { ServiceEdit } from "./service/Service-edit/ServiceEdit";
import { ServicesPage } from "./service/ServicesPage";

import { VechicleGatesPage } from "./VechicleGates/VechicleGatesPage"; 
import {VechicleGateEdit} from "./VechicleGates/VechicleGate-edit/VechicleGatesEdit";


import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";

export default function transportMenue() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        <ContentRoute path="/transport/vechicale/new" component={VechicaleEdit} />
        <ContentRoute path="/transport/vechicale/:id/edit" component={VechicaleEdit} /> 
        <ContentRoute path="/transport/vechicale" component={VechicalesPage} />

        <ContentRoute path="/transport/services/new" component={ServiceEdit} />
        <ContentRoute path="/transport/services/:id/edit" component={ServiceEdit} /> 
        <ContentRoute path="/transport/services" component={ServicesPage} />

        <ContentRoute path="/transport/vechicalegate/new" component={VechicleGateEdit} />
        <ContentRoute path="/transport/vechicalegate/:id/edit" component={VechicleGateEdit} /> 
        <ContentRoute path="/transport/vechicalegate" component={VechicleGatesPage} />


      </Switch>
    </Suspense>
  );
}
