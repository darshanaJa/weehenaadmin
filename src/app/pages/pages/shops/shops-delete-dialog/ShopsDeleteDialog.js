/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/shops/shopsActions";
import { useShopsUIContext } from "../ShopsUIContext";

export function ShopsDeleteDialog({ show, onHide }) {
  // Shops UI Context
  const shopsUIContext = useShopsUIContext();
  const ShopsUIProps = useMemo(() => {
    return {
      ids: shopsUIContext.ids,
      setIds: shopsUIContext.setIds,
      queryParams: shopsUIContext.queryParams,
    };
  }, [shopsUIContext]);

  // Shops Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Shops.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Shops we should close modal
  useEffect(() => {
    if (!ShopsUIProps.ids || ShopsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ShopsUIProps.ids]);

  const deleteShops = () => {
    // server request for deleting Shop by seleted ids
    dispatch(actions.deleteShops(ShopsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchShops(ShopsUIProps.queryParams)).then(() => {
        // clear selections list
        ShopsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Shops Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Shops?</span>
        )}
        {isLoading && <span>Shops are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteShops}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
