import React from "react";
import { Route } from "react-router-dom";
import { ShopsLoadingDialog } from "./shops-loading-dialog/ShopsLoadingDialog";
import { ShopDeleteDialog } from "./shop-delete-dialog/ShopDeleteDialog";
import { ShopsDeleteDialog } from "./shops-delete-dialog/ShopsDeleteDialog";
import { ShopsFetchDialog } from "./shops-fetch-dialog/ShopsFetchDialog";
import { ShopsUpdateStatusDialog } from "./shops-update-status-dialog/ShopsUpdateStatusDialog";
import { ShopsCard } from "./ShopsCard";
import { ShopsUIProvider } from "./ShopsUIContext";

export function ShopsPage({ history }) {
  const shopsUIEvents = {
    newShopButtonClick: () => {
      history.push("/sales/shops/new");
    },
    openEditShopPage: (id) => {
      history.push(`/sales/shops/${id}/edit`);
    },
    openDeleteShopDialog: (id) => {
      history.push(`/sales/shops/${id}/delete`);
    },
    openDeleteShopsDialog: () => {
      history.push(`/sales/shops/deleteShops`);
    },
    openFetchShopsDialog: () => {
      history.push(`/sales/shops/fetch`);
    },
    openUpdateShopsStatusDialog: () => {
      history.push("/sales/shops/updateStatus");
    },
  };

  return (
    <ShopsUIProvider shopsUIEvents={shopsUIEvents}>
      <ShopsLoadingDialog />
      <Route path="/sales/shops/deleteShops">
        {({ history, match }) => (
          <ShopsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/shops");
            }}
          />
        )}
      </Route>
      <Route path="/sales/shops/:id/delete">
        {({ history, match }) => (
          <ShopDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/sales/shops");
            }}
          />
        )}
      </Route>
      <Route path="/sales/shops/fetch">
        {({ history, match }) => (
          <ShopsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/shops");
            }}
          />
        )}
      </Route>
      <Route path="/sales/shops/updateStatus">
        {({ history, match }) => (
          <ShopsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/shops");
            }}
          />
        )}
      </Route>
      <ShopsCard />
    </ShopsUIProvider>
  );
}
