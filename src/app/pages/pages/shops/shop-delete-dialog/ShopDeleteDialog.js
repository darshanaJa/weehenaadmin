/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/shops/shopsActions";
import { useShopsUIContext } from "../ShopsUIContext";

export function ShopDeleteDialog({ id, show, onHide }) {
  // Shops UI Context
  const shopsUIContext = useShopsUIContext();
  const ShopsUIProps = useMemo(() => {
    return {
      setIds: shopsUIContext.setIds,
      queryParams: shopsUIContext.queryParams,
    };
  }, [shopsUIContext]);

  // Shops Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Shops.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteShop = () => {
    // server request for deleting Shop by id
    dispatch(actions.deleteShop(id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchShops(ShopsUIProps.queryParams));
      // clear selections list
      ShopsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Shop Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Shop?</span>
        )}
        {isLoading && <span>Shop is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteShop}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
