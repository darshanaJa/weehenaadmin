import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { ShopstatusCssClasses } from "../ShopsUIHelpers";
import { useShopsUIContext } from "../ShopsUIContext";

const selectedShops = (entities, ids) => {
  const _Shops = [];
  ids.forEach((id) => {
    const Shop = entities.find((el) => el.id === id);
    if (Shop) {
      _Shops.push(Shop);
    }
  });
  return _Shops;
};

export function ShopsFetchDialog({ show, onHide }) {
  // Shops UI Context
  const shopsUIContext = useShopsUIContext();
  const ShopsUIProps = useMemo(() => {
    return {
      ids: shopsUIContext.ids,
      queryParams: shopsUIContext.queryParams,
    };
  }, [shopsUIContext]);

  // Shops Redux state
  const { Shops } = useSelector(
    (state) => ({
      Shops: selectedShops(state.Shops.entities, ShopsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!ShopsUIProps.ids || ShopsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ShopsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Shops.map((Shop) => (
              <div className="list-timeline-item mb-3" key={Shop.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ShopstatusCssClasses[Shop.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Shop.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Shop.manufacture}, {Shop.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
