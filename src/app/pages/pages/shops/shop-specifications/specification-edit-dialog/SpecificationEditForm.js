// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  Select,
  Input,
} from "../../../../../../_metronic/_partials/controls";
// import { SPECIFICATIONS_DICTIONARY } from "../SpecificationsUIHelper";
import axios from "axios";
// import { Shops_URL_GET } from "../../../../_redux/shops/shopsCrud";
import { Payments_URL } from "../../../../_redux/Payment/PaymentsCrud";
// import { SalesTickets_URL_FIND } from "../../../../_redux/salesTickets/salesTicketsCrud";
// import moment from 'moment';

// Validation schema
const SpecificationEditSchema = Yup.object().shape({
  // mstock: Yup.string()
  //   .min(2, "Minimum 2 symbols")
  //   .max(50, "Maximum 50 symbols")
  //   .required("Stock name is required"),
  ammount: Yup.number().required("Amount is required"),
  paymentmethod_id: Yup.string().required("Paymnt type is required"),
});

export function SpecificationEditForm({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
  id,
  idedit
}) {

  const [shop,setShop]=useState([]);


  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Payments_URL,
      data:{
        "filter": [{"status": "1" }]
      }
      })
      .then((res) => {
        setShop(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  console.log(shop)
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={specification}
        validationSchema={SpecificationEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveSpecification(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <Form className="form form-label-right">
                <div className="form-group row">
                <div className="col-lg-12">
                  <Select name="paymentmethod_id" label="Payment Method">
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.id} >
                            {item.type}
                          </option>    
                      ))}
                  </Select>
                  </div>
                </div>

                <div className="form-group row">
                  <div className="col-lg-12">
                    <Field
                      name="ammount"
                      component={Input}
                      placeholder="Ammount"
                      label="Ammount"
                    />
                  </div>
                </div>  

                <div className="form-group row">
                  <div className="col-lg-12">
                    <Field
                      name="shopId"
                      component={Input}
                      placeholder="Shop Id"
                      label="Shop Id"
                      value={id}
                      readOny="readOnly"
                      // style={{ display: "none" }}
                    />
                  </div>
                </div>  

                {/* <div className="form-group row">
                  <div className="col-lg-12">
                    <Field
                      name="ticket_item_price"
                      component={Input}
                      placeholder="Item Price"
                      label="Item Price"
                    />
                  </div>
                </div>  
                <div className="form-group row">
                  <div className="col-lg-12">
                  <Field
                    name="saleticket"
                    component={Input}
                    placeholder="Sale Ticket ID"
                    label="Sale Ticket ID"
                    readonly="readonly"
                  />
                </div>
                </div> */}

                </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}
