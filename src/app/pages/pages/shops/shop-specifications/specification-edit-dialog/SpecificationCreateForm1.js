// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Modal } from "react-bootstrap";
import { getSpecificationById } from "../../../../_redux/specificationsCredit/specificationsCrud";
import { Image_Url2 } from "../../../../../config/config";


export function SpecificationCreateForm1({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
  id,
  idedit
}) {

  const [pdf, setpdf] = useState()

  useEffect(() => {
    getSpecificationById(id)
    .then((res)=>{
      setpdf(res.data.file_url2)
    })
    }, [id])

    const file = Image_Url2 + pdf

  return (
    <>
      <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
                <div className="form-group row">
                  <div className="col-lg-12">
                  <iframe 
                    width="450px"
                    height="650px"
                    title="pdf1" src={file} />
                  </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              {/* <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button> */}
            </Modal.Footer>
      
    </>
  );
}
