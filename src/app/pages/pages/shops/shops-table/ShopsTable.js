import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/shops/shopsActions";
import * as uiHelpers from "../ShopsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useShopsUIContext } from "../ShopsUIContext";

export function ShopsTable() {
  // Shops UI Context
  const shopsUIContext = useShopsUIContext();
  const ShopsUIProps = useMemo(() => {
    return {
      ids: shopsUIContext.ids,
      setIds: shopsUIContext.setIds,
      queryParams: shopsUIContext.queryParams,
      setQueryParams: shopsUIContext.setQueryParams,
      openEditShopPage: shopsUIContext.openEditShopPage,
      openDeleteShopDialog: shopsUIContext.openDeleteShopDialog,
    };
  }, [shopsUIContext]);

  // Getting curret state of Shops list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Shops }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Shops Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    ShopsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchShops(ShopsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ShopsUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: "store_name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "store_province",
      text: "Province",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "store_district",
      text: "District",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "store_city",
      text: "City",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "store_address",
      text: "Address",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "store_cid",
      text: "CID",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "store_email",
      text: "Email",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "store_phone_1",
      text: "Phone 01",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "store_phone_2",
      text: "Phone 02",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "store_owner_name",
      text: "Owner Name",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "store_owner_nic",
      text: "Owner NIC",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "store_owner_mobile_1",
      text: "Mobile 01",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "store_owner_mobile_2",
      text: "Mobile 02",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "store_owner_email",
      text: "Owner Email",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "store_br_no",
      text: "Branch",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditShopPage: ShopsUIProps.openEditShopPage,
        openDeleteShopDialog: ShopsUIProps.openDeleteShopDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ShopsUIProps.queryParams.pageSize,
    page: ShopsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ShopsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ShopsUIProps.ids,
                  setIds: ShopsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
