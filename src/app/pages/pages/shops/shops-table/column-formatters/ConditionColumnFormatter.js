import React from "react";
import {
  ShopConditionCssClasses,
  ShopConditionTitles
} from "../../ShopsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        ShopConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        ShopConditionCssClasses[row.condition]
      }`}
    >
      {ShopConditionTitles[row.condition]}
    </span>
  </>
);
