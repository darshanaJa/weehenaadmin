import React from "react";
import {
  ShopstatusCssClasses,
  ShopstatusTitles
} from "../../ShopsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      ShopstatusCssClasses[row.status]
    } label-inline`}
  >
    {ShopstatusTitles[row.status]}
  </span>
);
