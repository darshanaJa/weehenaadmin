/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";
import { useHistory } from "react-router-dom";

const ActionsColumnFormatter =({openEditSpecificationDialog, 
  openDeleteSpecificationDialog })=>{


  let history = useHistory();

  
  return (
    <>
      <OverlayTrigger
        overlay={<Tooltip id="specs-edit-tooltip">Go To Retail Sales Page</Tooltip>}
      >
        <a
          className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
          // onClick={alert("abcd")}
          // onClick={() => openEditSpecificationDialog(alert("abcd"))}
           onClick={() => {history.push(`/sales/reatails`)}}
        >
          <span className="svg-icon svg-icon-md svg-icon-primary">
            <SVG
              src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
            />
          </span>
        </a>
      </OverlayTrigger>

      <> </>
      {/* <OverlayTrigger
        overlay={
          <Tooltip id="spec-delete-tooltip">Delete specification</Tooltip>
        }
      >
        <a
          className="btn btn-icon btn-light btn-hover-danger btn-sm"
          onClick={() => openDeleteSpecificationDialog(row.sales_ticket_item_id)}
        >
          <span className="svg-icon svg-icon-md svg-icon-danger">
            <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
          </span>
        </a>
      </OverlayTrigger> */}
    </>
  );
}

export default ActionsColumnFormatter;
