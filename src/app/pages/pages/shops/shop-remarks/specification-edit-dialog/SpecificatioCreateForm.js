// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  // Select,
  Input,
} from "../../../../../../_metronic/_partials/controls";
// import { SPECIFICATIONS_DICTIONARY } from "../SpecificationsUIHelper";
import axios from "axios";
import { MainStocks_URL_GET } from "../../../../_redux/MainStock/MainStocksCrud";
// import { Shops_URL_GET } from "../../../../_redux/shops/shopsCrud";
import { Items_GETBYID_URL } from "../../../../_redux/specifications/specificationsCrud";
// import { Image_Url } from "../../../../../config/config";
// import { SalesTickets_URL_FIND } from "../../../../_redux/salesTickets/salesTicketsCrud";

// Validation schema
const SpecificationEditSchema = Yup.object().shape({
  mstock: Yup.string()
    .min(2, "Minimum 2 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Stock name is required"),
  ticket_item_opening_quantity: Yup.number().required("Quntity type is required"),
  ticket_item_price: Yup.number().required("Stock Price type is required"),
});

export function SpecificationCreateForm({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
  id,
  idedit
}) {

  //   const sumitImage=()=>{

  //     if(!specification.emp_image==null){
  //       console.log(specification.emp_image)
  //       // const pic=specification.emp_image
  //       // console.log(pic)
  //       // const url = Image_Url+pic
  //       // console.log(url)
        
  //       return(
  //         <div>
  //           {/* <img  className="shopImg" alt="shop" src={url} /> */}
  //           {/* {console.log(src)} */}
  //         </div>
        
  //       );
  //     }
  // }


  

  console.log(idedit)

  const [shop,setShop]=useState([]);
  // const [saleTicketID,setsaleTicketID]=useState([]);
  const [items,setItems]=useState();
  // const [retailSale,setRetailSale]=useState([]);

  console.log(shop)
  console.log(items)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Items_GETBYID_URL + `/${idedit}`
      })
      .then((res) => {
        setItems(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[idedit])
  

  // useEffect(()=>{
  //   axios({
  //     method: 'post',
  //     baseURL: SalesTickets_URL_FIND
  //     })
  //     .then((res) => {
  //       setRetailSale(res.data)
  //     })
  //     .catch(function (response) {
  //         // console.log(response);
  //     });
      
  // },[])

  // console.log(id)

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={specification}
        validationSchema={SpecificationEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveSpecification(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <Form className="form form-label-right">
                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="string"
                    name="emp_fname"
                    component={Input}
                    placeholder="First Name"
                    label="First Name"
                  />
                      </div>
                    </div>  

                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="string"
                    name="emp_lname"
                    component={Input}
                    placeholder="Last Name"
                    label="Last Name"
                  />
                      </div>
                    </div>  
                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="string"
                    name="emp_email"
                    component={Input}
                    placeholder="Email"
                    label="Email"
                  />
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-lg-12">
                    <Field
                    type="string"
                    name="emp_idcard_number"
                    component={Input}
                    placeholder="NIC"
                    label="NIC"
                  />
                      </div>
                    </div>
                  
                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="string"
                    name="emp_address_line1"
                    component={Input}
                    placeholder="Address Line 01"
                    label="Address Line 01"
                    // customFeedbackLabel="Please enter Price"
                  />
                      </div>
                    </div>  

                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    name="emp_address_line2"
                    component={Input}
                    placeholder="EmAddress Line 02"
                    label="EmAddress Line 02"
                  />
                      </div>
                    </div> 

                  <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="string"
                    name="emp_city"
                    component={Input}
                    placeholder="City"
                    label="City"
                  />
                      </div>
                    </div>  

                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="text"
                    name="emp_mobile1"
                    component={Input}
                    placeholder="Phone 01"
                    label="Phone 01"
                  />
                      </div>
                    </div>  
                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="text"
                    name="emp_mobile2"
                    component={Input}
                    placeholder="Phone 02"
                    label="Phone 02"
                  />
                    </div>
                  </div>

                  <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    type="text"
                    name="designation"
                    component={Input}
                    placeholder="Position"
                    label="Position"
                  />
                      </div>
                    </div>  
 
                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    name="passed_experiance"
                    component={Input}
                    placeholder="Passed Experiance"
                    label="Passed Experiance"
                  />
                    </div>
                  </div>

                  <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    name="updatedby"
                    component={Input}
                    placeholder="Update By"
                    label="Update By"
                  />
                    </div>
                  </div>

                  <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                    name="updated_date"
                    component={Input}
                    placeholder="Update Date"
                    label="Update Date"
                  />
                    </div>
                  </div>

                  <div className="form-group row">
              <div className="col-lg-4">
                  {/* {sumitImage()} */}
                </div>
            </div>

            </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              {/* <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button> */}
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}
