// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  Select,
  Input,
} from "../../../../../../_metronic/_partials/controls";
// import { SPECIFICATIONS_DICTIONARY } from "../SpecificationsUIHelper";
import axios from "axios";
import { MainStocks_URL_GET } from "../../../../_redux/MainStock/MainStocksCrud";
// import { Shops_URL_GET } from "../../../../_redux/shops/shopsCrud";
import { Items_GETBYID_URL } from "../../../../_redux/specifications/specificationsCrud";
// import { SalesTickets_URL_FIND } from "../../../../_redux/salesTickets/salesTicketsCrud";
// import moment from 'moment';

// Validation schema
const SpecificationEditSchema = Yup.object().shape({
  // mstock: Yup.string()
  //   .min(2, "Minimum 2 symbols")
  //   .max(50, "Maximum 50 symbols")
  //   .required("Stock name is required"),
  ticket_item_opening_quantity: Yup.number().required("Quntity type is required"),
  ticket_item_price: Yup.number().required("Stock Price type is required"),
});

export function SpecificationEditForm({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
  id,
  idedit
}) {

  // console.log(idedit)

  const [shop,setShop]=useState([]);
  // const [saleTicketID,setsaleTicketID]=useState([]);
  const [items,setItems]=useState();
  const [itemsName,setItemsName]=useState();
  // const [retailSale,setRetailSale]=useState([]);

  // console.log(items)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  // useEffect(()=>{
  //   axios({
  //     method: 'get',
  //     baseURL: MainStocks_URL_GETBYID + `/${idedit}`
  //     })
  //     .then((res) => {
  //       setItems(res.data)
  //     })
  //     .catch(function (response) {
  //         // console.log(response);
  //     });
      
  // },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Items_GETBYID_URL + `/${idedit}`
      })
      .then((res) => {
        setItems(res.data.mstock.item_id)
        setItemsName(res.data.mstock.item_name)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[idedit])
  

  // useEffect(()=>{
  //   axios({
  //     method: 'post',
  //     baseURL: SalesTickets_URL_FIND
  //     })
  //     .then((res) => {
  //       setRetailSale(res.data)
  //     })
  //     .catch(function (response) {
  //         // console.log(response);
  //     });
      
  // },[])

  // console.log(id)

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={specification}
        validationSchema={SpecificationEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveSpecification(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <Form className="form form-label-right">

                  <div className="form-group row">
                    <div className="col-lg-12">
                      <Select name="mstock" label="Stock Name">
                          <option value={items}>{itemsName}</option>
                          {shop.map((item) => (
                              <option value={item.item_id} >
                                {item.item_name}
                              </option>    
                          ))}
                      </Select>
                      </div>
                  </div>
                  
                    <div className="form-group row">
                      <div className="col-lg-12">
                        <Field
                          name="ticket_item_opening_quantity"
                          component={Input}
                          placeholder="Quntity"
                          label="Quntity"
                        />
                      </div>
                    </div>  

                    <div className="form-group row">
                      <div className="col-lg-12">
                        <Field
                          name="ticket_item_price"
                          component={Input}
                          placeholder="Item Price"
                          label="Item Price"
                        />
                      </div>
                    </div>  
                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                        name="saleticket"
                        component={Input}
                        placeholder="Sale Ticket ID"
                        label="Sale Ticket ID"
                        readonly="readonly"
                      />
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-lg-12">
                      <Select name="mstock" label="Stock Name">
                          <option value={items}>{itemsName}</option>
                          {shop.map((item) => (
                              <option value={item.item_id} >
                                {item.item_name}
                              </option>    
                          ))}
                      </Select>
                      </div>
                  </div>
                  
                    <div className="form-group row">
                      <div className="col-lg-12">
                        <Field
                          name="ticket_item_opening_quantity"
                          component={Input}
                          placeholder="Quntity"
                          label="Quntity"
                        />
                      </div>
                    </div>  

                    <div className="form-group row">
                      <div className="col-lg-12">
                        <Field
                          name="ticket_item_price"
                          component={Input}
                          placeholder="Item Price"
                          label="Item Price"
                        />
                      </div>
                    </div>  
                    <div className="form-group row">
                      <div className="col-lg-12">
                      <Field
                        name="saleticket"
                        component={Input}
                        placeholder="Sale Ticket ID"
                        label="Sale Ticket ID"
                        readonly="readonly"
                      />
                    </div>
                  </div>

            </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}
