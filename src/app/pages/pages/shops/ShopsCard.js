import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { ShopsFilter } from "./shops-filter/ShopsFilter";
import { ShopsTable } from "./shops-table/ShopsTable";
import { ShopsGrouping } from "./shops-grouping/ShopsGrouping";
import { useShopsUIContext } from "./ShopsUIContext";
import { notify } from "../../../config/Toastify";
// import { notify } from "../../_redux/shops/ShopToast";

export function ShopsCard() {
  const shopsUIContext = useShopsUIContext();
  const ShopsUIProps = useMemo(() => {
    return {
      ids: shopsUIContext.ids,
      queryParams: shopsUIContext.queryParams,
      setQueryParams: shopsUIContext.setQueryParams,
      newShopButtonClick: shopsUIContext.newShopButtonClick,
      openDeleteShopsDialog: shopsUIContext.openDeleteShopsDialog,
      openEditShopPage: shopsUIContext.openEditShopPage,
      openUpdateShopsStatusDialog:
        shopsUIContext.openUpdateShopsStatusDialog,
      openFetchShopsDialog: shopsUIContext.openFetchShopsDialog,
    };
  }, [shopsUIContext]);



  return (
    <Card>
      {notify()}
      <CardHeader title="Customer list">
      {/* {notify()} */}
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={ShopsUIProps.newShopButtonClick}
          >
            
            New Customer
          </button>
        </CardHeaderToolbar>
      </CardHeader> 
      <CardBody>
        <ShopsFilter />
        {ShopsUIProps.ids.length > 0 && (
          <>
            <ShopsGrouping />
          </>
        )}
        <ShopsTable />
      </CardBody>
    </Card>
  );
}
