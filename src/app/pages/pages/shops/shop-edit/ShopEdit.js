/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/shops/shopsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { ShopEditForm } from "./ShopEditForm";
import { ShopCreateForm } from "./ShopCreateForm";
import { Specifications } from "../shop-specifications/Specifications";
import { SpecificationsUIProvider } from "../shop-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
// import { RemarksUIProvider } from "../shop-remarks/RemarksUIContext";
// import { Remarks } from "../shop-remarks/Remarks";
import { SpecificationEditDialog } from "../shop-specifications/specification-edit-dialog/SpecificationEditDialog";
import { Specifications02 } from "../shop-remarks/Specifications";
import { SpecificationsUIProvider02 } from "../shop-remarks/SpecificationsUIContext02";
// import { notify } from "../../../_redux/shops/ShopToast";

const initShop = {
  retail_store_id: undefined,
  store_name: "",
  store_province: "",
  store_district: "",
  store_city: "",
  store_address: "",
  store_cid: "",
  store_email: "",
  store_phone_1: "",
  store_phone_2: "",
  store_owner_name: "",
  store_owner_nic: "",
  store_owner_mobile_1: "",
  store_owner_mobile_2: "",
  store_owner_email: "",
  store_image:"",
  store_br_no: "",
}



export function ShopEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, ShopForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Shops.actionsLoading,
      ShopForEdit: state.Shops.ShopForEdit,
    }),
    shallowEqual
  );
  useEffect(() => {
    dispatch(actions.fetchShop(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Customer";
    if (ShopForEdit && id) {
      // _title = `Edit Store '${ShopForEdit.store_name}  - ${ShopForEdit.store_phone_1}'`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ShopForEdit, id]);

  const saveShop = (values) => {
    if (!id) {
      dispatch(actions.createShop(values)).then(() => backToShopsList());
    }
     else {
      dispatch(actions.updateShop(values,id)).then(() => backToShopsList());
    }
  };

  const detail = {...ShopForEdit}

  console.log(detail)
  const due_amount = detail.due_amount

  console.log(due_amount)

  // notify();


  const btnRef = useRef();

  const backToShopsList = () => {
    history.push(`/sales/shops`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToShopsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
           
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
           {
               <li className="nav-item" onClick={() => setTab("basic")}>
               <a
                 className={`nav-link ${tab === "basic" && "active"}`}
                 data-toggle="tab"
                 role="tab"
                 aria-selected={(tab === "basic").toString()}
               >
                 Basic info
               </a>
             </li>
            }
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Customer Credites
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Retail Sales
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id}>
                <SpecificationEditDialog id={id} />
                <Specifications due_amount={due_amount} id={id} />
              </SpecificationsUIProvider>
            )}
            {tab === "basic" && (
              <ShopEditForm
                actionsLoading={actionsLoading}
                Shop={ ShopForEdit || initShop}
                btnRef={btnRef}
                saveShop={saveShop}
              />
            )}
           {tab === "remarks" && id && (
              <SpecificationsUIProvider02 idEmp={id} currentEmployeeId={id}>
                <Specifications02 />
              </SpecificationsUIProvider02>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToShopsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
           {
               <li className="nav-item" onClick={() => setTab("basic")}>
               <a
                 className={`nav-link ${tab === "basic" && "active"}`}
                 data-toggle="tab"
                 role="tab"
                 aria-selected={(tab === "basic").toString()}
               >
                 Basic info
               </a>
             </li>
            }
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Shop remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Shop specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {
  
            }
            {tab === "basic" && (
              <ShopCreateForm
                actionsLoading={actionsLoading}
                Shop={ ShopForEdit || initShop}
                btnRef={btnRef}
                saveShop={saveShop}
              />
            )}
            {/* {tab === "remarks" && id && (
              <RemarksUIProvider currentShopId={id}>
                <Remarks />
              </RemarksUIProvider>
            )} */}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentShopId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  

  
}
