import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const ShopEditSchema = Yup.object().shape({
  store_name: Yup.string()
    .required("Name is required")
    .min(2, "Name must be at least 2 characters"),
  // store_province: Yup.string()
  //   .required("Province is required"),
  // store_district: Yup.string()
  //   .required("District is required"),
  store_city: Yup.string()
    .required("City is required"),
  store_address: Yup.string()
    .required("Address is required"),
  store_cid: Yup.string()
    .required("CID is required"),
  store_email: Yup.string().email()
    .required("Email is required"),
  store_phone_1: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
  // store_phone_2: Yup.string()
  //   .required("Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  store_owner_name: Yup.string()
    .required("Owner Name is required")
    .min(2, "Name must be at least 2 characters"),
  store_owner_nic: Yup.string()
    .required("Owner NIC is required")
    .min(10, "NIC be at least 9 characters"),
  // store_owner_mobile_1: Yup.string()
  //   .required("Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  // store_owner_mobile_2: Yup.string()
  //   .required("Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  // store_owner_email: Yup.string().email()
  //   .required("Owner Email is required"),
  // store_br_no: Yup.string()
  //   .required("Branch is required"),
  // store_image: Yup.string()  
  //   .required(),

  
});

export function ShopCreateForm({
  Shop,
  btnRef,
  saveShop,
}) {

  console.log(Shop)

  const bodyFormData = new FormData();

  const [store_image,set_Profile_pic]=useState();


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Shop}
        validationSchema={ShopEditSchema}
        onSubmit={(values) => {
          console.log(values);
          bodyFormData.append('store_name',values.store_name);
          bodyFormData.append('store_province',values.store_province);
          bodyFormData.append('store_district',values.store_district);
          bodyFormData.append('store_city',values.store_city);
          bodyFormData.append('store_address',values.store_address);
          bodyFormData.append('store_cid',values.store_cid);
          bodyFormData.append('store_email',values.store_email);
          bodyFormData.append('store_phone_1',values.store_phone_1);
          bodyFormData.append('store_phone_2',values.store_phone_2);
          bodyFormData.append('store_owner_name',values.store_owner_name);
          bodyFormData.append('store_owner_nic',values.store_owner_nic);
          bodyFormData.append('store_owner_mobile_1',values.store_owner_mobile_1);
          bodyFormData.append('store_owner_mobile_2',values.store_owner_mobile_2);
          bodyFormData.append('store_owner_email',values.store_owner_email);
          bodyFormData.append('store_br_no',values.store_br_no);
          bodyFormData.append('store_image',store_image);

          console.log(bodyFormData);
          saveShop(bodyFormData);
          // saveShop(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="store_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_province"
                    component={Input}
                    placeholder="Province"
                    label="Province"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_district"
                    component={Input}
                    placeholder="District"
                    label="District"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_city"
                    component={Input}
                    placeholder="City"
                    label="City"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_address"
                    component={Input}
                    placeholder="Address"
                    label="Address"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_cid"
                    component={Input}
                    placeholder="CID"
                    label="CID"
                    // customFeedbackLabel="Please enter Price"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="store_email"
                    component={Input}
                    placeholder="Email"
                    label="Email"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_phone_1"
                    component={Input}
                    placeholder="Phone 01"
                    label="Phone 01"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_phone_2"
                    component={Input}
                    placeholder="Phone 02"
                    label="Phone 02"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="store_owner_name"
                    component={Input}
                    placeholder="Owner Name"
                    label="Owner Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_owner_email"
                    component={Input}
                    placeholder="Owner Email"
                    label="Owner Email"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_owner_nic"
                    component={Input}
                    placeholder="Owner NIC"
                    label="Owner NIC"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_owner_mobile_1"
                    component={Input}
                    placeholder="Mobile 01"
                    label="Mobile 01"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="store_owner_mobile_2"
                    component={Input}
                    placeholder="Mobile 02"
                    label="Mobile 02"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="store_br_no"
                    component={Input}
                    placeholder="Branch"
                    label="Branch"
                    customFeedbackLabel="Please enter Price"
                  />
                </div>  
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                    <div className="form-group row">
                      <input className="agentImageBtn" type="file" name="store_image"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />

                    </div>
                  </div> 
                  <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2" >Save</button>
                 </div>  
              </div>
            
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
