import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./ShopsUIHelpers";

const ShopsUIContext = createContext();

export function useShopsUIContext() {
  return useContext(ShopsUIContext);
}

export const ShopsUIConsumer = ShopsUIContext.Consumer;

export function ShopsUIProvider({ shopsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newShopButtonClick: shopsUIEvents.newShopButtonClick,
    openEditShopPage: shopsUIEvents.openEditShopPage,
    openDeleteShopDialog: shopsUIEvents.openDeleteShopDialog,
    openDeleteShopsDialog: shopsUIEvents.openDeleteShopsDialog,
    openFetchShopsDialog: shopsUIEvents.openFetchShopsDialog,
    openUpdateShopsStatusDialog:
    shopsUIEvents.openUpdateShopsStatusDialog,
  };

  return (
    <ShopsUIContext.Provider value={value}>
      {children}
    </ShopsUIContext.Provider>
  );
}
