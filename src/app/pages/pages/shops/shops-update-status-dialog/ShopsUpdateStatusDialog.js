import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ShopstatusCssClasses } from "../ShopsUIHelpers";
import * as actions from "../../../_redux/shops/shopsActions";
import { useShopsUIContext } from "../ShopsUIContext";

const selectedShops = (entities, ids) => {
  const _Shops = [];
  ids.forEach((id) => {
    const Shop = entities.find((el) => el.id === id);
    if (Shop) {
      _Shops.push(Shop);
    }
  });
  return _Shops;
};

export function ShopsUpdateStatusDialog({ show, onHide }) {
  // Shops UI Context
  const shopsUIContext = useShopsUIContext();
  const ShopsUIProps = useMemo(() => {
    return {
      ids: shopsUIContext.ids,
      setIds: shopsUIContext.setIds,
      queryParams: shopsUIContext.queryParams,
    };
  }, [shopsUIContext]);

  // Shops Redux state
  const { Shops, isLoading } = useSelector(
    (state) => ({
      Shops: selectedShops(state.Shops.entities, ShopsUIProps.ids),
      isLoading: state.Shops.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Shops we should close modal
  useEffect(() => {
    if (ShopsUIProps.ids || ShopsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ShopsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Shop by ids
    dispatch(actions.updateShopsStatus(ShopsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchShops(ShopsUIProps.queryParams)).then(
          () => {
            // clear selections list
            ShopsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Shops
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Shops.map((Shop) => (
              <div className="list-timeline-item mb-3" key={Shop.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ShopstatusCssClasses[Shop.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Shop.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Shop.manufacture}, {Shop.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${ShopstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
