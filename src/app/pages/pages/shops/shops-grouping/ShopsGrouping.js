import React, { useMemo } from "react";
import { useShopsUIContext } from "../ShopsUIContext";

export function ShopsGrouping() {
  // Shops UI Context
  const shopsUIContext = useShopsUIContext();
  const ShopsUIProps = useMemo(() => {
    return {
      ids: shopsUIContext.ids,
      setIds: shopsUIContext.setIds,
      openDeleteShopsDialog: shopsUIContext.openDeleteShopsDialog,
      openFetchShopsDialog: shopsUIContext.openFetchShopsDialog,
      openUpdateShopsStatusDialog:
        shopsUIContext.openUpdateShopsStatusDialog,
    };
  }, [shopsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{ShopsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={ShopsUIProps.openDeleteShopsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ShopsUIProps.openFetchShopsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ShopsUIProps.openUpdateShopsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
