import React from "react";
import { Route } from "react-router-dom";
import { DepartmentsLoadingDialog } from "./Departments-loading-dialog/DepartmentsLoadingDialog";
import { DepartmentDeleteDialog } from "./Department-delete-dialog/DepartmentDeleteDialog";
import { DepartmentsDeleteDialog } from "./Departments-delete-dialog/DepartmentsDeleteDialog";
import { DepartmentsFetchDialog } from "./Departments-fetch-dialog/DepartmentsFetchDialog";
import { DepartmentsUpdateStatusDialog } from "./Departments-update-status-dialog/DepartmentsUpdateStatusDialog";
import { DepartmentsCard } from "./DepartmentsCard";
import { DepartmentsUIProvider } from "./DepartmentsUIContext";

export function DepartmentsPage({ history }) {
  const DepartmentsUIEvents = {
    newDepartmentButtonClick: () => {
      history.push("/hr/departments/new");
    },
    openEditDepartmentPage: (id) => {
      history.push(`/hr/departments/${id}/edit`);
    },
    openDeleteDepartmentDialog: (id) => {
      history.push(`/hr/departments/${id}/delete`);
    },
    openDeleteDepartmentsDialog: () => {
      history.push(`/hr/departments/deleteDepartments`);
    },
    openFetchDepartmentsDialog: () => {
      history.push(`/hr/departments/fetch`);
    },
    openUpdateDepartmentsStatusDialog: () => {
      history.push("/hr/departments/updateStatus");
    },
  };

  return (
    <DepartmentsUIProvider DepartmentsUIEvents={DepartmentsUIEvents}>
      <DepartmentsLoadingDialog />
      <Route path="/hr/departments/deleteDepartments">
        {({ history, match }) => (
          <DepartmentsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/departments");
            }}
          />
        )}
      </Route>
      <Route path="/hr/departments/:id/delete">
        {({ history, match }) => (
          <DepartmentDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/hr/departments");
            }}
          />
        )}
      </Route>
      <Route path="/hr/departments/fetch">
        {({ history, match }) => (
          <DepartmentsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/departments");
            }}
          />
        )}
      </Route>
      <Route path="/hr/departments/updateStatus">
        {({ history, match }) => (
          <DepartmentsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/departments");
            }}
          />
        )}
      </Route>
      <DepartmentsCard />
    </DepartmentsUIProvider>
  );
}
