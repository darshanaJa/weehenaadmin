import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { DepartmentstatusCssClasses } from "../DepartmentsUIHelpers";
import * as actions from "../../../_redux/Department/DepartmentsActions";
import { useDepartmentsUIContext } from "../DepartmentsUIContext";

const selectedDepartments = (entities, ids) => {
  const _Departments = [];
  ids.forEach((id) => {
    const Department = entities.find((el) => el.id === id);
    if (Department) {
      _Departments.push(Department);
    }
  });
  return _Departments;
};

export function DepartmentsUpdateStatusDialog({ show, onHide }) {
  // Departments UI Context
  const DepartmentsUIContext = useDepartmentsUIContext();
  const DepartmentsUIProps = useMemo(() => {
    return {
      ids: DepartmentsUIContext.ids,
      setIds: DepartmentsUIContext.setIds,
      queryParams: DepartmentsUIContext.queryParams,
    };
  }, [DepartmentsUIContext]);

  // Departments Redux state
  const { Departments, isLoading } = useSelector(
    (state) => ({
      Departments: selectedDepartments(state.Departments.entities, DepartmentsUIProps.ids),
      isLoading: state.Departments.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Departments we should close modal
  useEffect(() => {
    if (DepartmentsUIProps.ids || DepartmentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [DepartmentsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Department by ids
    dispatch(actions.updateDepartmentsStatus(DepartmentsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchDepartments(DepartmentsUIProps.queryParams)).then(
          () => {
            // clear selections list
            DepartmentsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Departments
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Departments.map((Department) => (
              <div className="list-timeline-item mb-3" key={Department.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      DepartmentstatusCssClasses[Department.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Department.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Department.manufacture}, {Department.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${DepartmentstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
