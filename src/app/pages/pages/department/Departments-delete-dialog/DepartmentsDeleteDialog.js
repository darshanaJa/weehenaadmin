/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Department/DepartmentsActions";
import { useDepartmentsUIContext } from "../DepartmentsUIContext";

export function DepartmentsDeleteDialog({ show, onHide }) {
  // Departments UI Context
  const DepartmentsUIContext = useDepartmentsUIContext();
  const DepartmentsUIProps = useMemo(() => {
    return {
      ids: DepartmentsUIContext.ids,
      setIds: DepartmentsUIContext.setIds,
      queryParams: DepartmentsUIContext.queryParams,
    };
  }, [DepartmentsUIContext]);

  // Departments Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Departments.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Departments we should close modal
  useEffect(() => {
    if (!DepartmentsUIProps.ids || DepartmentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [DepartmentsUIProps.ids]);

  const deleteDepartments = () => {
    // server request for deleting Department by seleted ids
    dispatch(actions.deleteDepartments(DepartmentsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchDepartments(DepartmentsUIProps.queryParams)).then(() => {
        // clear selections list
        DepartmentsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Departments Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Departments?</span>
        )}
        {isLoading && <span>Departments are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteDepartments}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
