import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { DepartmentstatusCssClasses } from "../DepartmentsUIHelpers";
import { useDepartmentsUIContext } from "../DepartmentsUIContext";

const selectedDepartments = (entities, ids) => {
  const _Departments = [];
  ids.forEach((id) => {
    const Department = entities.find((el) => el.id === id);
    if (Department) {
      _Departments.push(Department);
    }
  });
  return _Departments;
};

export function DepartmentsFetchDialog({ show, onHide }) {
  // Departments UI Context
  const DepartmentsUIContext = useDepartmentsUIContext();
  const DepartmentsUIProps = useMemo(() => {
    return {
      ids: DepartmentsUIContext.ids,
      queryParams: DepartmentsUIContext.queryParams,
    };
  }, [DepartmentsUIContext]);

  // Departments Redux state
  const { Departments } = useSelector(
    (state) => ({
      Departments: selectedDepartments(state.Departments.entities, DepartmentsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!DepartmentsUIProps.ids || DepartmentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [DepartmentsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Departments.map((Department) => (
              <div className="list-timeline-item mb-3" key={Department.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      DepartmentstatusCssClasses[Department.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Department.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Department.manufacture}, {Department.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
