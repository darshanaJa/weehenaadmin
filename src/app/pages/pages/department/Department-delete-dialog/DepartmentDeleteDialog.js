/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Department/DepartmentsActions";
import { useDepartmentsUIContext } from "../DepartmentsUIContext";

export function DepartmentDeleteDialog({ id, show, onHide }) {
  // Departments UI Context
  const DepartmentsUIContext = useDepartmentsUIContext();
  const DepartmentsUIProps = useMemo(() => {
    return {
      setIds: DepartmentsUIContext.setIds,
      queryParams: DepartmentsUIContext.queryParams,
    };
  }, [DepartmentsUIContext]);

  // Departments Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Departments.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteDepartment = () => {
    // server request for deleting Department by id
    dispatch(actions.deleteDepartment(id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchDepartments(DepartmentsUIProps.queryParams));
      // clear selections list
      DepartmentsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Department Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Department?</span>
        )}
        {isLoading && <span>Department is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteDepartment}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
