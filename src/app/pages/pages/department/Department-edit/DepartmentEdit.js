/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Department/DepartmentsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { DepartmentEditForm } from "./DepartmentEditForm";
import { Specifications } from "../Department-specifications/Specifications";
import { SpecificationsUIProvider } from "../Department-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Department-remarks/RemarksUIContext";
import { Remarks } from "../Department-remarks/Remarks";
import { DepartmentCreateForm } from "./DepartmentCreateForm";
// import { notify } from "../../../../config/Toastify";

const initDepartment = {
  department_name:"",
  department_slug:"",
  department_description:"",
  department_head:""
};


export function DepartmentEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, DepartmentForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Departments.actionsLoading,
      DepartmentForEdit: state.Departments.DepartmentForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchDepartment(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Department";
    if (DepartmentForEdit && id) {
      // _title = `Edit Department - ${DepartmentForEdit.item_name} ${DepartmentForEdit.item_default_price} - ${DepartmentForEdit.item_name}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [DepartmentForEdit, id]);

  const saveDepartment = (values) => {
    if (!id) {
      dispatch(actions.createDepartment(values)).then(() => backToDepartmentsList());
      // {notify("create")}
    } else {
      dispatch(actions.updateDepartment(values,id)).then(() => backToDepartmentsList());
    }
  };

  const btnRef = useRef();  

  const backToDepartmentsList = () => {
    history.push(`/hr/departments`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToDepartmentsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <DepartmentEditForm
                actionsLoading={actionsLoading}
                Department={DepartmentForEdit || initDepartment}
                btnRef={btnRef}
                saveDepartment={saveDepartment}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentDepartmentId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentDepartmentId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToDepartmentsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Department remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Department specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <DepartmentCreateForm
                actionsLoading={actionsLoading}
                Department={DepartmentForEdit || initDepartment}
                btnRef={btnRef}
                saveDepartment={saveDepartment}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentDepartmentId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentDepartmentId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }
}
