// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
// import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { Image_Url } from "../../../../config/config";
import Admin from "../../../../config/Admin";
import { Departments_URL_GETBYID } from "../../../_redux/Department/DepartmentsCrud";
import axios from "axios";

// Validation schema
const DepartmentEditSchema = Yup.object().shape({
  department_name: Yup.string()
  .min(2, "Minimum 2 symbols")
  .max(50, "Maximum 50 symbols")
  .required("Name is required"),
  department_slug: Yup.string()
  .min(2, "Minimum 2 symbols")
  .max(50, "Maximum 50 symbols")
  .required("Is required"),
  department_description: Yup.string()
  .min(2, "Minimum 2 symbols")
  // .max(50, "Maximum 50 symbols")
  .required("Is required"),
  // .readOnly,
  department_head: Yup.string()
  .min(2, "Minimum 2 symbols")
  .max(50, "Maximum 50 symbols")
  .required("Is required"),
   
});

export function DepartmentEditForm({
  Department,
  btnRef,
  saveDepartment,
}) {

  const bodyFormData = new FormData();

  const [store_image,set_Profile_pic]=useState();

  const submitImage=()=>{
    return(
      <img className="agentImg" alt="agent" src={url} />
    );
}

console.log(Department)

const pic=Department.department_image
console.log(pic)
const url = Image_Url+pic
console.log(url)

const [idAdmin,setIdAdmin]=useState('');

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Departments_URL_GETBYID + `/${Department.id}`
      })
      .then((res) => {
        setIdAdmin(res.data.created_by.id)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[Department.id])

return (
  <>
    <Formik
      enableReinitialize={true}
      initialValues={Department}
      validationSchema={DepartmentEditSchema}
      onSubmit={(values) => {
        console.log(values);
        bodyFormData.append('department_name',values.department_name);
        bodyFormData.append('department_slug',values.department_slug);
        bodyFormData.append('department_head',values.department_head);
        // bodyFormData.append('department_head',values.department_head);
        bodyFormData.append('department_description',values.department_description);
        bodyFormData.append('department_image',store_image);

        console.log(bodyFormData);
        saveDepartment(bodyFormData);
      }}
    >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="department_name"
                    component={Input}
                    placeholder="Department Name"
                    label="Department Name"
                  />
              
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="department_slug"
                    component={Input}
                    placeholder="Department Slug"
                    label="Department Slug"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="department_head"
                      component={Input}
                      placeholder="Department Head"
                      label="Department Head"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-8">
                  {/* <label>Description</label> */}
                  <Field
                    type="text"
                    placeholder="Description"
                    component={Input}
                    label="Description"
                    name="department_description"
                  />
                </div>
                <div className="col-lg-4">
                    <div className="form-group row">
                    <p className="empFont">Add Department Image</p>
                      <input className="agentImageBtn2" type="file" name="file_title"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    </div>
                </div> 
                  <div className="col-lg-4">
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
              </div>
              <div className="form-group row">
                  {submitImage()}
                </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={idAdmin}
                  createDate={Department.dep_created_date} 
                  updateDate={Department.dep_updated_date} 
                />

            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
