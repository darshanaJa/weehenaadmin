import React, { useMemo } from "react";
import { useDepartmentsUIContext } from "../DepartmentsUIContext";

export function DepartmentsGrouping() {
  // Departments UI Context
  const DepartmentsUIContext = useDepartmentsUIContext();
  const DepartmentsUIProps = useMemo(() => {
    return {
      ids: DepartmentsUIContext.ids,
      setIds: DepartmentsUIContext.setIds,
      openDeleteDepartmentsDialog: DepartmentsUIContext.openDeleteDepartmentsDialog,
      openFetchDepartmentsDialog: DepartmentsUIContext.openFetchDepartmentsDialog,
      openUpdateDepartmentsStatusDialog:
        DepartmentsUIContext.openUpdateDepartmentsStatusDialog,
    };
  }, [DepartmentsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{DepartmentsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={DepartmentsUIProps.openDeleteDepartmentsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={DepartmentsUIProps.openFetchDepartmentsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={DepartmentsUIProps.openUpdateDepartmentsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
