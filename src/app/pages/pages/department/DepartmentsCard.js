import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { DepartmentsFilter } from "./Departments-filter/DepartmentsFilter";
import { DepartmentsTable } from "./Departments-table/DepartmentsTable";
// import { DepartmentsGrouping } from "./Departments-grouping/DepartmentsGrouping";
import { useDepartmentsUIContext } from "./DepartmentsUIContext";
import { notify } from "../../../config/Toastify";

export function DepartmentsCard() {

 

    // const [name,setName] = useState()
  
    // setName(msg)
    // console.log(msg)
    // notify(msg)
  
    // useEffect(()=>{
    //   console.log(msg)
    //   // notify("abcd")
    // },[])

  const DepartmentsUIContext = useDepartmentsUIContext();
  const DepartmentsUIProps = useMemo(() => {
    return {
      ids: DepartmentsUIContext.ids,
      queryParams: DepartmentsUIContext.queryParams,
      setQueryParams: DepartmentsUIContext.setQueryParams,
      newDepartmentButtonClick: DepartmentsUIContext.newDepartmentButtonClick,
      openDeleteDepartmentsDialog: DepartmentsUIContext.openDeleteDepartmentsDialog,
      openEditDepartmentPage: DepartmentsUIContext.openEditDepartmentPage,
      openUpdateDepartmentsStatusDialog:DepartmentsUIContext.openUpdateDepartmentsStatusDialog,
      openFetchDepartmentsDialog: DepartmentsUIContext.openFetchDepartmentsDialog,
    };
  }, [DepartmentsUIContext]);


  return (
    <Card>
      <CardHeader title="Department list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={DepartmentsUIProps.newDepartmentButtonClick}
          >
            New Department
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <DepartmentsFilter />
        {DepartmentsUIProps.ids.length > 0 && (
          <>
            {/* <DepartmentsGrouping /> */}
          </>
        )}
        <DepartmentsTable />
      </CardBody>
    </Card>
  );
}
