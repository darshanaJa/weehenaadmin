import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./DepartmentsUIHelpers";

const DepartmentsUIContext = createContext();

export function useDepartmentsUIContext() {
  return useContext(DepartmentsUIContext);
}

export const DepartmentsUIConsumer = DepartmentsUIContext.Consumer;

export function DepartmentsUIProvider({ DepartmentsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newDepartmentButtonClick: DepartmentsUIEvents.newDepartmentButtonClick,
    openEditDepartmentPage: DepartmentsUIEvents.openEditDepartmentPage,
    openDeleteDepartmentDialog: DepartmentsUIEvents.openDeleteDepartmentDialog,
    openDeleteDepartmentsDialog: DepartmentsUIEvents.openDeleteDepartmentsDialog,
    openFetchDepartmentsDialog: DepartmentsUIEvents.openFetchDepartmentsDialog,
    openUpdateDepartmentsStatusDialog: DepartmentsUIEvents.openUpdateDepartmentsStatusDialog,
  };

  return (
    <DepartmentsUIContext.Provider value={value}>
      {children}
    </DepartmentsUIContext.Provider>
  );
}
