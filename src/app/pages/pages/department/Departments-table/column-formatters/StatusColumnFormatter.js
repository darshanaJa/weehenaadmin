import React from "react";
import {
  DepartmentstatusCssClasses,
  DepartmentstatusTitles
} from "../../DepartmentsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      DepartmentstatusCssClasses[row.status]
    } label-inline`}
  >
    {DepartmentstatusTitles[row.status]}
  </span>
);
