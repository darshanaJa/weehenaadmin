import React from "react";
import {
  DepartmentConditionCssClasses,
  DepartmentConditionTitles
} from "../../DepartmentsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        DepartmentConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        DepartmentConditionCssClasses[row.condition]
      }`}
    >
      {DepartmentConditionTitles[row.condition]}
    </span>
  </>
);
