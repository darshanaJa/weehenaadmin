// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Department/DepartmentsActions";
import * as uiHelpers from "../DepartmentsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useDepartmentsUIContext } from "../DepartmentsUIContext";

export function DepartmentsTable() {
  // Departments UI Context
  const DepartmentsUIContext = useDepartmentsUIContext();
  const DepartmentsUIProps = useMemo(() => {
    return {
      ids: DepartmentsUIContext.ids,
      setIds: DepartmentsUIContext.setIds,
      queryParams: DepartmentsUIContext.queryParams,
      setQueryParams: DepartmentsUIContext.setQueryParams,
      openEditDepartmentPage: DepartmentsUIContext.openEditDepartmentPage,
      openDeleteDepartmentDialog: DepartmentsUIContext.openDeleteDepartmentDialog,
    };
  }, [DepartmentsUIContext]);

  // Getting curret state of Departments list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Departments }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Departments Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    DepartmentsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchDepartments(DepartmentsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [DepartmentsUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    // {
    //   dataField: "id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "department_name",
      text: "Department Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "department_slug",
      text: "Department Slug",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "department_description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "department_head",
      text: "Department Head",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    // {
    //   dataField: "mstock_description",
    //   text: "Description",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.PriceColumnFormatter,
    // },
    // {
    //   dataField: "item_registered_by",
    //   text: "Register By",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "item_registered_date",
    //   text: "Registerd Date",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "Department_updated_date",
    //   text: "Update Date",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditDepartmentPage: DepartmentsUIProps.openEditDepartmentPage,
        openDeleteDepartmentDialog: DepartmentsUIProps.openDeleteDepartmentDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: DepartmentsUIProps.queryParams.pageSize,
    page: DepartmentsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  DepartmentsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: DepartmentsUIProps.ids,
                  setIds: DepartmentsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
