import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModulestatusCssClasses } from "../ModulesUIHelpers";
import * as actions from "../../../_redux/Module/ModulesActions";
import { useModulesUIContext } from "../ModulesUIContext";

const selectedModules = (entities, ids) => {
  const _Modules = [];
  ids.forEach((id) => {
    const Module = entities.find((el) => el.id === id);
    if (Module) {
      _Modules.push(Module);
    }
  });
  return _Modules;
};

export function ModulesUpdateStatusDialog({ show, onHide }) {
  // Modules UI Context
  const ModulesUIContext = useModulesUIContext();
  const ModulesUIProps = useMemo(() => {
    return {
      ids: ModulesUIContext.ids,
      setIds: ModulesUIContext.setIds,
      queryParams: ModulesUIContext.queryParams,
    };
  }, [ModulesUIContext]);

  // Modules Redux state
  const { Modules, isLoading } = useSelector(
    (state) => ({
      Modules: selectedModules(state.Modules.entities, ModulesUIProps.ids),
      isLoading: state.Modules.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Modules we should close modal
  useEffect(() => {
    if (ModulesUIProps.ids || ModulesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ModulesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Module by ids
    dispatch(actions.updateModulesStatus(ModulesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchModules(ModulesUIProps.queryParams)).then(
          () => {
            // clear selections list
            ModulesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Modules
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Modules.map((Module) => (
              <div className="list-timeline-item mb-3" key={Module.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ModulestatusCssClasses[Module.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Module.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Module.manufacture}, {Module.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${ModulestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
