import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { ModulestatusCssClasses } from "../ModulesUIHelpers";
import { useModulesUIContext } from "../ModulesUIContext";

const selectedModules = (entities, ids) => {
  const _Modules = [];
  ids.forEach((id) => {
    const Module = entities.find((el) => el.id === id);
    if (Module) {
      _Modules.push(Module);
    }
  });
  return _Modules;
};

export function ModulesFetchDialog({ show, onHide }) {
  // Modules UI Context
  const ModulesUIContext = useModulesUIContext();
  const ModulesUIProps = useMemo(() => {
    return {
      ids: ModulesUIContext.ids,
      queryParams: ModulesUIContext.queryParams,
    };
  }, [ModulesUIContext]);

  // Modules Redux state
  const { Modules } = useSelector(
    (state) => ({
      Modules: selectedModules(state.Modules.entities, ModulesUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!ModulesUIProps.ids || ModulesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ModulesUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Modules.map((Module) => (
              <div className="list-timeline-item mb-3" key={Module.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ModulestatusCssClasses[Module.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Module.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Module.manufacture}, {Module.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
