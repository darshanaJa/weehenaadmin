import React from "react";
import {
  ModulestatusCssClasses,
  ModulestatusTitles
} from "../../ModulesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      ModulestatusCssClasses[row.status]
    } label-inline`}
  >
    {ModulestatusTitles[row.status]}
  </span>
);
