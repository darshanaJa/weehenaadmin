import React from "react";
import {
  ModuleConditionCssClasses,
  ModuleConditionTitles
} from "../../ModulesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        ModuleConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        ModuleConditionCssClasses[row.condition]
      }`}
    >
      {ModuleConditionTitles[row.condition]}
    </span>
  </>
);
