// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Module/ModulesActions";
import * as uiHelpers from "../ModulesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useModulesUIContext } from "../ModulesUIContext";

export function ModulesTable() {
  // Modules UI Context
  const ModulesUIContext = useModulesUIContext();
  const ModulesUIProps = useMemo(() => {
    return {
      ids: ModulesUIContext.ids,
      setIds: ModulesUIContext.setIds,
      queryParams: ModulesUIContext.queryParams,
      setQueryParams: ModulesUIContext.setQueryParams,
      openEditModulePage: ModulesUIContext.openEditModulePage,
      openDeleteModuleDialog: ModulesUIContext.openDeleteModuleDialog,
    };
  }, [ModulesUIContext]);

  // Getting curret state of Modules list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Modules }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Modules Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    ModulesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchModules(ModulesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ModulesUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Module_id);

  const columns = [
    // {
    //   dataField: "sales_Module_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "module_name",
      text: "Module Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "created_date",
      text: "Created Date",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "updated_date",
      text: "Updated Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    // {
    //   dataField: "buyback_passed_experiance",
    //   text: "Passed Experiance",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_Module_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditModulePage: ModulesUIProps.openEditModulePage,
        openDeleteModuleDialog: ModulesUIProps.openDeleteModuleDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ModulesUIProps.queryParams.pageSize,
    page: ModulesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Module_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ModulesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ModulesUIProps.ids,
                  setIds: ModulesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
