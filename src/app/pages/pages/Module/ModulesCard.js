import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { ModulesFilter } from "./Modules-filter/ModulesFilter";
import { ModulesTable } from "./Modules-table/ModulesTable";
// import { ModulesGrouping } from "./Modules-grouping/ModulesGrouping";
import { useModulesUIContext } from "./ModulesUIContext";
import { notify } from "../../../config/Toastify";

export function ModulesCard() {
  const ModulesUIContext = useModulesUIContext();
  const ModulesUIProps = useMemo(() => {
    return {
      ids: ModulesUIContext.ids,
      queryParams: ModulesUIContext.queryParams,
      setQueryParams: ModulesUIContext.setQueryParams,
      newModuleButtonClick: ModulesUIContext.newModuleButtonClick,
      openDeleteModulesDialog: ModulesUIContext.openDeleteModulesDialog,
      openEditModulePage: ModulesUIContext.openEditModulePage,
      openUpdateModulesStatusDialog:ModulesUIContext.openUpdateModulesStatusDialog,
      openFetchModulesDialog: ModulesUIContext.openFetchModulesDialog,
    };
  }, [ModulesUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Module list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={ModulesUIProps.newModuleButtonClick}
          >
            New Module
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ModulesFilter />
        {ModulesUIProps.ids.length > 0 && (
          <>
            {/* <ModulesGrouping /> */}
          </>
        )}
        <ModulesTable />
      </CardBody>
    </Card>
  );
}
