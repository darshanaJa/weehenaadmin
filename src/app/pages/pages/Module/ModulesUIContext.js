import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./ModulesUIHelpers";

const ModulesUIContext = createContext();

export function useModulesUIContext() {
  return useContext(ModulesUIContext);
}

export const ModulesUIConsumer = ModulesUIContext.Consumer;

export function ModulesUIProvider({ ModulesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newModuleButtonClick: ModulesUIEvents.newModuleButtonClick,
    openEditModulePage: ModulesUIEvents.openEditModulePage,
    openDeleteModuleDialog: ModulesUIEvents.openDeleteModuleDialog,
    openDeleteModulesDialog: ModulesUIEvents.openDeleteModulesDialog,
    openFetchModulesDialog: ModulesUIEvents.openFetchModulesDialog,
    openUpdateModulesStatusDialog: ModulesUIEvents.openUpdateModulesStatusDialog,
  };

  return (
    <ModulesUIContext.Provider value={value}>
      {children}
    </ModulesUIContext.Provider>
  );
}
