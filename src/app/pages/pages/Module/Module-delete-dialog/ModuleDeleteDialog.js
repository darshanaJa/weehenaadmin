/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Module/ModulesActions";
import { useModulesUIContext } from "../ModulesUIContext";

export function ModuleDeleteDialog({ sales_Module_id, show, onHide }) {
  // console.log(sales_Module_id)
  // Modules UI Context
  const ModulesUIContext = useModulesUIContext();
  const ModulesUIProps = useMemo(() => {
    return {
      setIds: ModulesUIContext.setIds,
      queryParams: ModulesUIContext.queryParams,
    };
  }, [ModulesUIContext]);

  // Modules Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Modules.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Module_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Module_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteModule = () => {
    // server request for deleting Module by id
    dispatch(actions.deleteModule(sales_Module_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchModules(ModulesUIProps.queryParams));
      // clear selections list
      ModulesUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Module Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Module?</span>
        )}
        {isLoading && <span>Module is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteModule}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
