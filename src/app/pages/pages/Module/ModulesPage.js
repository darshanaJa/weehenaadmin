import React from "react";
import { Route } from "react-router-dom";
import { ModulesLoadingDialog } from "./Modules-loading-dialog/ModulesLoadingDialog";
import { ModuleDeleteDialog } from "./Module-delete-dialog/ModuleDeleteDialog";
import { ModulesDeleteDialog } from "./Modules-delete-dialog/ModulesDeleteDialog";
import { ModulesFetchDialog } from "./Modules-fetch-dialog/ModulesFetchDialog";
import { ModulesUpdateStatusDialog } from "./Modules-update-status-dialog/ModulesUpdateStatusDialog";
import { ModulesCard } from "./ModulesCard";
import { ModulesUIProvider } from "./ModulesUIContext";

export function ModulesPage({ history }) {
  const ModulesUIEvents = {
    newModuleButtonClick: () => {
      history.push("/access/modules/new");
    },
    openEditModulePage: (id) => {
      history.push(`/access/modules/${id}/edit`);
    },
    openDeleteModuleDialog: (sales_Module_id) => {
      history.push(`/access/modules/${sales_Module_id}/delete`);
    },
    openDeleteModulesDialog: () => {
      history.push(`/access/modules/deleteModules`);
    },
    openFetchModulesDialog: () => {
      history.push(`/access/modules/fetch`);
    },
    openUpdateModulesStatusDialog: () => {
      history.push("/access/modules/updateStatus");
    },
  };

  return (
    <ModulesUIProvider ModulesUIEvents={ModulesUIEvents}>
      <ModulesLoadingDialog />
      <Route path="/access/modules/deleteModules">
        {({ history, match }) => (
          <ModulesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/access/modules");
            }}
          />
        )}
      </Route>
      <Route path="/access/modules/:sales_Module_id/delete">
        {({ history, match }) => (
          <ModuleDeleteDialog
            show={match != null}
            sales_Module_id={match && match.params.sales_Module_id}
            onHide={() => {
              history.push("/access/modules");
            }}
          />
        )}
      </Route>
      <Route path="/access/modules/fetch">
        {({ history, match }) => (
          <ModulesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/access/modules");
            }}
          />
        )}
      </Route>
      <Route path="/access/modules/updateStatus">
        {({ history, match }) => (
          <ModulesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/access/modules");
            }}
          />
        )}
      </Route>
      <ModulesCard />
    </ModulesUIProvider>
  );
}
