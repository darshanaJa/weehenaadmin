/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Module/ModulesActions";
import { useModulesUIContext } from "../ModulesUIContext";

export function ModulesDeleteDialog({ show, onHide }) {
  // Modules UI Context
  const ModulesUIContext = useModulesUIContext();
  const ModulesUIProps = useMemo(() => {
    return {
      ids: ModulesUIContext.ids,
      setIds: ModulesUIContext.setIds,
      queryParams: ModulesUIContext.queryParams,
    };
  }, [ModulesUIContext]);

  // Modules Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Modules.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Modules we should close modal
  useEffect(() => {
    if (!ModulesUIProps.ids || ModulesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ModulesUIProps.ids]);

  const deleteModules = () => {
    // server request for deleting Module by seleted ids
    dispatch(actions.deleteModules(ModulesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchModules(ModulesUIProps.queryParams)).then(() => {
        // clear selections list
        ModulesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Modules Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Modules?</span>
        )}
        {isLoading && <span>Modules are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteModules}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
