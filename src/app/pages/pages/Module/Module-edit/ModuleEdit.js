/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Module/ModulesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { ModuleEditForm } from "./ModuleEditForm";
import { ModuleCreateForm } from "./ModuleCreateForm";
import { Specifications } from "../Module-specifications/Specifications";
import { SpecificationsUIProvider } from "../Module-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Module-remarks/RemarksUIContext";
import { Remarks } from "../Module-remarks/Remarks";

const initModule = {

  module_name : "",
  // buyback_address : "",
  // buyback_Module_quantity : "",
  // buyback_email : "",
  // buyback_contact : "",
  // buyback_passed_experiance : ""

};

export function ModuleEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, ModuleForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Modules.actionsLoading,
      ModuleForEdit: state.Modules.ModuleForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchModule(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Module";
    if (ModuleForEdit && id) {
      // _title = `Edit Module - ${ModuleForEdit.buyback_name} ${ModuleForEdit.buyback_contact} - ${ModuleForEdit.sales_Module_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ModuleForEdit, id]);

  const saveModule = (values) => {
    if (!id) {
      dispatch(actions.createModule(values,id)).then(() => backToModulesList());
    } else {
      dispatch(actions.updateModule(values,id)).then(() => backToModulesList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateModulePassword(values,id)).then(() => backToModulesList());
  };

  const btnRef = useRef();

  const backToModulesList = () => {
    history.push(`/access/modules`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToModulesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ModuleEditForm
                actionsLoading={actionsLoading}
                Module={ModuleForEdit || initModule}
                btnRef={btnRef}
                saveModule={saveModule}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentModuleId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentModuleId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToModulesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Module remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Module specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ModuleCreateForm
                actionsLoading={actionsLoading}
                Module={ModuleForEdit || initModule}
                btnRef={btnRef}
                saveModule={saveModule}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentModuleId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentModuleId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
