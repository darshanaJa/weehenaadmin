/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/SpareStock/SpareStocksActions";
import { useSpareStocksUIContext } from "../SpareStocksUIContext";

export function SpareStocksDeleteDialog({ show, onHide }) {
  // SpareStocks UI Context
  const SpareStocksUIContext = useSpareStocksUIContext();
  const SpareStocksUIProps = useMemo(() => {
    return {
      ids: SpareStocksUIContext.ids,
      setIds: SpareStocksUIContext.setIds,
      queryParams: SpareStocksUIContext.queryParams,
    };
  }, [SpareStocksUIContext]);

  // SpareStocks Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SpareStocks.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected SpareStocks we should close modal
  useEffect(() => {
    if (!SpareStocksUIProps.ids || SpareStocksUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareStocksUIProps.ids]);

  const deleteSpareStocks = () => {
    // server request for deleting SpareStock by seleted ids
    dispatch(actions.deleteSpareStocks(SpareStocksUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSpareStocks(SpareStocksUIProps.queryParams)).then(() => {
        // clear selections list
        SpareStocksUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SpareStocks Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected SpareStocks?</span>
        )}
        {isLoading && <span>SpareStocks are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSpareStocks}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
