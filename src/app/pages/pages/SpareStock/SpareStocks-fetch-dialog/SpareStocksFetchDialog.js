import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { SpareStockstatusCssClasses } from "../SpareStocksUIHelpers";
import { useSpareStocksUIContext } from "../SpareStocksUIContext";

const selectedSpareStocks = (entities, ids) => {
  const _SpareStocks = [];
  ids.forEach((id) => {
    const SpareStock = entities.find((el) => el.id === id);
    if (SpareStock) {
      _SpareStocks.push(SpareStock);
    }
  });
  return _SpareStocks;
};

export function SpareStocksFetchDialog({ show, onHide }) {
  // SpareStocks UI Context
  const SpareStocksUIContext = useSpareStocksUIContext();
  const SpareStocksUIProps = useMemo(() => {
    return {
      ids: SpareStocksUIContext.ids,
      queryParams: SpareStocksUIContext.queryParams,
    };
  }, [SpareStocksUIContext]);

  // SpareStocks Redux state
  const { SpareStocks } = useSelector(
    (state) => ({
      SpareStocks: selectedSpareStocks(state.SpareStocks.entities, SpareStocksUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!SpareStocksUIProps.ids || SpareStocksUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareStocksUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SpareStocks.map((SpareStock) => (
              <div className="list-timeline-item mb-3" key={SpareStock.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SpareStockstatusCssClasses[SpareStock.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SpareStock.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SpareStock.manufacture}, {SpareStock.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
