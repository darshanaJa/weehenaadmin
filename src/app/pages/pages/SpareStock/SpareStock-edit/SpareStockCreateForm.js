import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const SpareStockEditSchema = Yup.object().shape({
  // id: Yup.string(),
  spare_part_name: Yup.string()
    .required("Name is required")
    .min(2, "Name must be at least 2 characters"),
    spare_part_quantity: Yup.number()
    .required("Quntity is required")
    .min(2, "Quntity must be at least 2 characters"),
    spare_part_madein: Yup.string()
    .required("Made In is required")
    .min(2, "Made In must be at least 2 characters"),
    buying_price: Yup.number()
    .required("price is required")
   .min(1, "price must be at least 1 characters"),
    reorder_level: Yup.string()
    .required("Record level is required"),
    spare_part_type: Yup.string()
    .required("Type is required"),
});


export function SpareStockCreateForm({
  SpareStock,
  btnRef,
  saveSpareStock,
}) {

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={SpareStock}
        validationSchema={SpareStockEditSchema}
        onSubmit={(values)  => {

          console.log(values);
          saveSpareStock(values);

        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="spare_part_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="spare_part_quantity"
                    component={Input}
                    placeholder="Quntity"
                    label="Quntity"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="spare_part_madein"
                    component={Input}
                    placeholder="Made In"
                    label="Made In"
                  />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="buying_price"
                      component={Input}
                      placeholder="Price"
                      label="Price"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="reorder_level"
                    component={Input}
                    placeholder="Level"
                    label="Level"
                    // customFeedbackLabel="Please enter "
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="spare_part_type"
                    component={Input}
                    placeholder="Type"
                    label="Type"
                  />
                </div>
              </div>

            <div className="form-group row">
                  <div className="col-lg-4">
                     <button type="submit" className="btn btn-primary ml-2"> Save</button>
                  </div>
                  
                </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>

            <div>
               
            </div>
          </>
        )}
      </Formik>
    </>
  );
}
