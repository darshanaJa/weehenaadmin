/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/SpareStock/SpareStocksActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { SpareStockEditForm } from "./SpareStockEditForm";
import { SpareStockCreateForm } from "./SpareStockCreateForm";
import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../SpareStock-specifications/Specifications";
import { SpecificationsUIProvider } from "../SpareStock-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../SpareStock-remarks/RemarksUIContext";
import { Remarks } from "../SpareStock-remarks/Remarks";
// import { SpecificationsTable } from "../SpareStock-specifications/SpecificationsTable";

const initSpareStock = {

  spare_part_name : "",
  spare_part_quantity : "",
  spare_part_madein : "",
  buying_price : "",
  reorder_level : "",
  spare_part_type : "",

};

const initSpareStockPassword = {
  curent_password : "",
  new_password : "",
  confirm_password : ""

};

export function SpareStockEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, SpareStockForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.SpareStocks.actionsLoading,
      SpareStockForEdit: state.SpareStocks.SpareStockForEdit,
    }),
    shallowEqual
  );

  

  useEffect(() => {
    dispatch(actions.fetchSpareStock(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New SpareStock";
    if (SpareStockForEdit && id) {
      // _title = `Edit SpareStock - ${SpareStockForEdit.sales_SpareStock_fname} ${SpareStockForEdit.sales_SpareStock_lname} - ${SpareStockForEdit.sales_SpareStock_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareStockForEdit, id]);

  const saveSpareStock = (values) => {
    if (!id) {
      dispatch(actions.createSpareStock(values,id)).then(() => backToSpareStocksList());
    } else {
      dispatch(actions.updateSpareStock(values,id)).then(() => backToSpareStocksList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateSpareStockPassword(values,id)).then(() => backToSpareStocksList());
  };

  const btnRef = useRef(); 

  const backToSpareStocksList = () => {
    // history.push(`/sales/SpareStock`);
    history.push(`/stocks/sparepartstock`);
    // window.location.reload(`/sales/SpareStock`)
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSpareStocksList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SpareStockEditForm
                actionsLoading={actionsLoading}
                SpareStock={SpareStockForEdit || initSpareStock}
                btnRef={btnRef}
                saveSpareStock={saveSpareStock}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSpareStockId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSpareStockId={id}>
                <PasswordReset
                  actionsLoading={actionsLoading}
                  SpareStock={initSpareStockPassword}
                  btnRef={btnRef}
                  savePassword={savePassword}
    />
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSpareStocksList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    SpareStock remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    SpareStock specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SpareStockCreateForm
                actionsLoading={actionsLoading}
                SpareStock={SpareStockForEdit || initSpareStock}
                btnRef={btnRef}
                saveSpareStock={saveSpareStock}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSpareStockId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSpareStockId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
