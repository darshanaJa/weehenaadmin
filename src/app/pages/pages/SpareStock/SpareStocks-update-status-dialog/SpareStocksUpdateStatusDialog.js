import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { SpareStockstatusCssClasses } from "../SpareStocksUIHelpers";
import * as actions from "../../../_redux/SpareStock/SpareStocksActions";
import { useSpareStocksUIContext } from "../SpareStocksUIContext";

const selectedSpareStocks = (entities, ids) => {
  const _SpareStocks = [];
  ids.forEach((id) => {
    const SpareStock = entities.find((el) => el.id === id);
    if (SpareStock) {
      _SpareStocks.push(SpareStock);
    }
  });
  return _SpareStocks;
};

export function SpareStocksUpdateStatusDialog({ show, onHide }) {
  // SpareStocks UI Context
  const SpareStocksUIContext = useSpareStocksUIContext();
  const SpareStocksUIProps = useMemo(() => {
    return {
      ids: SpareStocksUIContext.ids,
      setIds: SpareStocksUIContext.setIds,
      queryParams: SpareStocksUIContext.queryParams,
    };
  }, [SpareStocksUIContext]);

  // SpareStocks Redux state
  const { SpareStocks, isLoading } = useSelector(
    (state) => ({
      SpareStocks: selectedSpareStocks(state.SpareStocks.entities, SpareStocksUIProps.ids),
      isLoading: state.SpareStocks.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected SpareStocks we should close modal
  useEffect(() => {
    if (SpareStocksUIProps.ids || SpareStocksUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareStocksUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing SpareStock by ids
    dispatch(actions.updateSpareStocksStatus(SpareStocksUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchSpareStocks(SpareStocksUIProps.queryParams)).then(
          () => {
            // clear selections list
            SpareStocksUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected SpareStocks
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SpareStocks.map((SpareStock) => (
              <div className="list-timeline-item mb-3" key={SpareStock.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SpareStockstatusCssClasses[SpareStock.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SpareStock.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SpareStock.manufacture}, {SpareStock.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${SpareStockstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
