import React, { useMemo } from "react";
import { useSpareStocksUIContext } from "../SpareStocksUIContext";

export function SpareStocksGrouping() {
  // SpareStocks UI Context
  const SpareStocksUIContext = useSpareStocksUIContext();
  const SpareStocksUIProps = useMemo(() => {
    return {
      ids: SpareStocksUIContext.ids,
      setIds: SpareStocksUIContext.setIds,
      openDeleteSpareStocksDialog: SpareStocksUIContext.openDeleteSpareStocksDialog,
      openFetchSpareStocksDialog: SpareStocksUIContext.openFetchSpareStocksDialog,
      openUpdateSpareStocksStatusDialog:
        SpareStocksUIContext.openUpdateSpareStocksStatusDialog,
    };
  }, [SpareStocksUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{SpareStocksUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={SpareStocksUIProps.openDeleteSpareStocksDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SpareStocksUIProps.openFetchSpareStocksDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SpareStocksUIProps.openUpdateSpareStocksStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
