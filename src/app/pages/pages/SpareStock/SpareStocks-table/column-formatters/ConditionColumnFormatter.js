import React from "react";
import {
  SpareStockConditionCssClasses,
  SpareStockConditionTitles
} from "../../SpareStocksUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        SpareStockConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        SpareStockConditionCssClasses[row.condition]
      }`}
    >
      {SpareStockConditionTitles[row.condition]}
    </span>
  </>
);
