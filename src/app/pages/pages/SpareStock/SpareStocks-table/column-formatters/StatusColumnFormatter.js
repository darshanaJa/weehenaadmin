import React from "react";
import {
  SpareStockstatusCssClasses,
  SpareStockstatusTitles
} from "../../SpareStocksUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      SpareStockstatusCssClasses[row.status]
    } label-inline`}
  >
    {SpareStockstatusTitles[row.status]}
  </span>
);
