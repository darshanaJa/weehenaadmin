// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/SpareStock/SpareStocksActions";
import * as uiHelpers from "../SpareStocksUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useSpareStocksUIContext } from "../SpareStocksUIContext";

export function SpareStocksTable() {
  // SpareStocks UI Context
  const SpareStocksUIContext = useSpareStocksUIContext();
  const SpareStocksUIProps = useMemo(() => {
    return {
      ids: SpareStocksUIContext.ids,
      setIds: SpareStocksUIContext.setIds,
      queryParams: SpareStocksUIContext.queryParams,
      setQueryParams: SpareStocksUIContext.setQueryParams,
      openEditSpareStockPage: SpareStocksUIContext.openEditSpareStockPage,
      openDeleteSpareStockDialog: SpareStocksUIContext.openDeleteSpareStockDialog,
    };
  }, [SpareStocksUIContext]);

  // Getting curret state of SpareStocks list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.SpareStocks }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // SpareStocks Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    SpareStocksUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchSpareStocks(SpareStocksUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareStocksUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_SpareStock_id);

  const columns = [
    // {
    //   dataField: "sales_SpareStock_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "spare_part_name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "spare_part_quantity",
      text: "Quntity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "spare_part_madein",
      text: "Made In",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "buying_price",
      text: "Price",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "reorder_level",
      text: "Level",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "spare_part_type",
      text: "Type",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "reg_date",
      text: "Register Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "updated_date",
      text: "Update Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditSpareStockPage: SpareStocksUIProps.openEditSpareStockPage,
        openDeleteSpareStockDialog: SpareStocksUIProps.openDeleteSpareStockDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: SpareStocksUIProps.queryParams.pageSize,
    page: SpareStocksUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_SpareStock_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  SpareStocksUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: SpareStocksUIProps.ids,
                  setIds: SpareStocksUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
