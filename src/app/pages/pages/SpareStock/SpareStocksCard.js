import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { SpareStocksFilter } from "./SpareStocks-filter/SpareStocksFilter";
import { SpareStocksTable } from "./SpareStocks-table/SpareStocksTable";
import { SpareStocksGrouping } from "./SpareStocks-grouping/SpareStocksGrouping";
import { useSpareStocksUIContext } from "./SpareStocksUIContext";
import { notify } from "../../../config/Toastify";

export function SpareStocksCard(msg){

  // const [name,setName] = useState()

  // setName(msg)
  // console.log(msg)

  // useEffect(()=>{
  //   console.log(msg)
  //   // notify("abcd")
  // },[])

  const SpareStocksUIContext = useSpareStocksUIContext();
  const SpareStocksUIProps = useMemo(() => {
    return {
      ids: SpareStocksUIContext.ids,
      queryParams: SpareStocksUIContext.queryParams,
      setQueryParams: SpareStocksUIContext.setQueryParams,
      newSpareStockButtonClick: SpareStocksUIContext.newSpareStockButtonClick,
      openDeleteSpareStocksDialog: SpareStocksUIContext.openDeleteSpareStocksDialog,
      openEditSpareStockPage: SpareStocksUIContext.openEditSpareStockPage,
      openUpdateSpareStocksStatusDialog:SpareStocksUIContext.openUpdateSpareStocksStatusDialog,
      openFetchSpareStocksDialog: SpareStocksUIContext.openFetchSpareStocksDialog,
    };
  }, [SpareStocksUIContext]);

  return (
    <>
    <Card>
      {notify()}
      <CardHeader title="Spare Part Stock list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={SpareStocksUIProps.newSpareStockButtonClick}
          >
            New Spare Part
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <SpareStocksFilter />
        {SpareStocksUIProps.ids.length > 0 && (
          <>
            <SpareStocksGrouping />
          </>
        )}
        <SpareStocksTable />
      </CardBody>
    </Card>
    </>
  );
}
