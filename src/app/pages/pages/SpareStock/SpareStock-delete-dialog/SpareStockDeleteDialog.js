/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/SpareStock/SpareStocksActions";
import { useSpareStocksUIContext } from "../SpareStocksUIContext";

export function SpareStockDeleteDialog({ sales_SpareStock_id, show, onHide }) {
  // console.log(sales_SpareStock_id)
  // SpareStocks UI Context
  const SpareStocksUIContext = useSpareStocksUIContext();
  const SpareStocksUIProps = useMemo(() => {
    return {
      setIds: SpareStocksUIContext.setIds,
      queryParams: SpareStocksUIContext.queryParams,
    };
  }, [SpareStocksUIContext]);

  // SpareStocks Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SpareStocks.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_SpareStock_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_SpareStock_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteSpareStock = () => {
    // server request for deleting SpareStock by id
    dispatch(actions.deleteSpareStock(sales_SpareStock_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSpareStocks(SpareStocksUIProps.queryParams));
      // clear selections list
      SpareStocksUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SpareStock Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this SpareStock?</span>
        )}
        {isLoading && <span>SpareStock is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSpareStock}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
