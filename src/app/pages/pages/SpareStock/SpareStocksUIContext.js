import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./SpareStocksUIHelpers";

const SpareStocksUIContext = createContext();

export function useSpareStocksUIContext() {
  return useContext(SpareStocksUIContext);
}

export const SpareStocksUIConsumer = SpareStocksUIContext.Consumer;

export function SpareStocksUIProvider({ SpareStocksUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newSpareStockButtonClick: SpareStocksUIEvents.newSpareStockButtonClick,
    openEditSpareStockPage: SpareStocksUIEvents.openEditSpareStockPage,
    openDeleteSpareStockDialog: SpareStocksUIEvents.openDeleteSpareStockDialog,
    openDeleteSpareStocksDialog: SpareStocksUIEvents.openDeleteSpareStocksDialog,
    openFetchSpareStocksDialog: SpareStocksUIEvents.openFetchSpareStocksDialog,
    openUpdateSpareStocksStatusDialog: SpareStocksUIEvents.openUpdateSpareStocksStatusDialog,
  };

  return (
    <SpareStocksUIContext.Provider value={value}>
      {children}
    </SpareStocksUIContext.Provider>
  );
}
