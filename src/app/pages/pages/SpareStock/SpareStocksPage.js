import React from "react";
import { Route } from "react-router-dom";
import { SpareStocksLoadingDialog } from "./SpareStocks-loading-dialog/SpareStocksLoadingDialog";
import { SpareStockDeleteDialog } from "./SpareStock-delete-dialog/SpareStockDeleteDialog";
import { SpareStocksDeleteDialog } from "./SpareStocks-delete-dialog/SpareStocksDeleteDialog";
import { SpareStocksFetchDialog } from "./SpareStocks-fetch-dialog/SpareStocksFetchDialog";
import { SpareStocksUpdateStatusDialog } from "./SpareStocks-update-status-dialog/SpareStocksUpdateStatusDialog";
import { SpareStocksCard } from "./SpareStocksCard";
import { SpareStocksUIProvider } from "./SpareStocksUIContext";
import { notify } from "../../../config/Toastify";

// window.location.reload()

export function SpareStocksPage({ history }) {
  const SpareStocksUIEvents = {
    newSpareStockButtonClick: () => {
      history.push("/stocks/sparepartstock/new");
    },
    openEditSpareStockPage: (id) => {
      history.push(`/stocks/sparepartstock/${id}/edit`);
    },
    openDeleteSpareStockDialog: (sales_SpareStock_id) => {
      history.push(`/stocks/sparepartstock/${sales_SpareStock_id}/delete`);
    },
    openDeleteSpareStocksDialog: () => {
      history.push(`/stocks/sparepartstock/deleteSpareStocks`);
    },
    openFetchSpareStocksDialog: () => {
      history.push(`/stocks/sparepartstock/fetch`);
    },
    openUpdateSpareStocksStatusDialog: () => {
      history.push("/stocks/sparepartstock/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <SpareStocksUIProvider SpareStocksUIEvents={SpareStocksUIEvents}>
      <SpareStocksLoadingDialog />
      <Route path="/stocks/sparepartstock/deleteSpareStocks">
        {({ history, match }) => (
          <SpareStocksDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/sparepartstock");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/sparepartstock/:sales_SpareStock_id/delete">
        {({ history, match }) => (
          <SpareStockDeleteDialog
            show={match != null}
            sales_SpareStock_id={match && match.params.sales_SpareStock_id}
            onHide={() => {
              history.push("/stocks/sparepartstock");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/sparepartstock/fetch">
        {({ history, match }) => (
          <SpareStocksFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/sparepartstock");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/sparepartstock/updateStatus">
        {({ history, match }) => (
          <SpareStocksUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/sparepartstock");
            }}
          />
        )}
      </Route>
      <SpareStocksCard />
    </SpareStocksUIProvider>
    </>
  );
}
