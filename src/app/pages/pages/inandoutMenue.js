import React, { Suspense } from "react";
import { Switch } from "react-router-dom";
import { VechicleGatesPage } from "./VechicleGates/VechicleGatesPage"; 
import {VechicleGateEdit} from "./VechicleGates/VechicleGate-edit/VechicleGatesEdit";
import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";


export default function inandoutMenue() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        <ContentRoute path="/inandout/vechicalegate/new" component={VechicleGateEdit} />
        <ContentRoute path="/inandout/vechicalegate/:id/edit" component={VechicleGateEdit} /> 
        <ContentRoute path="/inandout/vechicalegate" component={VechicleGatesPage} />

      </Switch>
    </Suspense>
  );
}
