import React, { Suspense } from "react";
import { Switch } from "react-router-dom";

import { DepartmentsPage } from "./department/DepartmentsPage";
import { DepartmentEdit } from "./department/Department-edit/DepartmentEdit";

import { EmployeesPage } from "./employee/EmployeesPage";
import { EmployeeEdit } from "./employee/Employee-edit/EmployeeEdit";

import { PositionsPage } from "./position/PositionsPage"; 
import {PositionEdit} from "./position/Position-edit/PositionEdit";

import { GateKeepersPage } from "./geateKeeper/GateKeepersPage"; 
import {GateKeeperEdit} from "./geateKeeper/GateKeeper-edit/GateKeeperEdit";


import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";

export default function hrMenue() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>

        <ContentRoute path="/hr/departments/new" component={DepartmentEdit} />
        <ContentRoute path="/hr/departments/:id/edit" component={DepartmentEdit} /> 
        <ContentRoute path="/hr/departments" component={DepartmentsPage} />

        <ContentRoute path="/hr/employee/new" component={EmployeeEdit} />
        <ContentRoute path="/hr/employee/:id/edit" component={EmployeeEdit} /> 
        <ContentRoute path="/hr/employee" component={EmployeesPage} />

        <ContentRoute path="/hr/position/new" component={PositionEdit} />
        <ContentRoute path="/hr/position/:id/edit" component={PositionEdit} /> 
        <ContentRoute path="/hr/position" component={PositionsPage} />

        <ContentRoute path="/hr/gatekeeper/new" component={GateKeeperEdit} />
        <ContentRoute path="/hr/gatekeeper/:id/edit" component={GateKeeperEdit} /> 
        <ContentRoute path="/hr/gatekeeper" component={GateKeepersPage} />

        

      </Switch>
    </Suspense>
  );
}
