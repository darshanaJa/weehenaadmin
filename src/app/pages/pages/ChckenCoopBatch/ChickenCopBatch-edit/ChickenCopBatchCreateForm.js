import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select, Input1 } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { notifyWarning } from "../../../../config/Toastify";
import { Api_Login } from "../../../../config/config";
import axios from "axios";

// Validation schema
const ChickenCopBatchEditSchema = Yup.object().shape({
  chick_batch_quantity: Yup.number()
    .required("Quantity is required")
    .min(1, "Quantity must be at least 1 characters"),
  chick_coop_batch_description: Yup.string()
    .required("Description is required")
    .min(2, "Description must be at least 2 characters"),
});

export function ChickenCopBatchCreateForm({
  ChickenCopBatch,
  btnRef,
  saveChickenCopBatch,
  farm,
  setFarmValue,
  farmValue,
  coopName,
  entity,
  setCoopValue,
  coopValue,
  batch,
  setCoopId,
  coopId,
  coopQu,
  coopQuValue,
  entity2,
  batchID,
  stBatchID,
  total2
}) {

  const [togle, settogle] = useState(false)

  // const url = Api_Login + '/api/chick-batch/update';
  const url2 = Api_Login + '/api/chickencoop/update';

  console.log(coopId)
  console.log(farmValue)
  console.log(entity2)
  

  const quntityHandler =(e)=> {
    if(parseInt(e.target.value) > parseInt(coopQu)){
      settogle(true)
      return notifyWarning("Sorry Maximum Can Add - " +  coopQu)
    }
    if(parseInt(e.target.value) > parseInt(total2)){
      settogle(true)
      return notifyWarning("Sorry Remaining Batch Quantity - " +  total2)
    }
    settogle(false)
  }


  return (
    <div>
      {notifyWarning()}
      <Formik
        enableReinitialize={true}
        initialValues={ChickenCopBatch}
        validationSchema={ChickenCopBatchEditSchema}
        onSubmit={(values)  => {

          // console.log(values.mstockItemId)

          // if(values.batch_name==='Choose One' || values.batch_name===undefined){
          //   return notifyWarning("Please Select Batch Name")
          // }
          if(farmValue==='Choose One'){
            return notifyWarning("Please Select Farm Name")
          }
          if(values.coop_name==='Choose One'|| values.coop_name===undefined){
            return notifyWarning("Please Select Coop Name")
          }
          else{

            if(parseInt(values.chick_batch_quantity) === parseInt(total2)){
              // axios.put(url + `/${batchID}`, {"active":"off"});
              console.log(values.chick_batch_quantity,total2)

              console.log(values);
              saveChickenCopBatch(values);
              axios.put(url2 + `/${values.coop_name}`, {"active":"off"});
            }
            else{
              console.log(values.chick_batch_quantity,total2)

              console.log(values);
              saveChickenCopBatch(values);
              // axios.put(url + `/${batchID}`, {status:0});
              axios.put(url2 + `/${values.coop_name}`, {"active":"off"});
            }

            console.log(parseInt(values.chick_batch_quantity), parseInt(total2))

            
          }
          
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
            <div className="form-group row">
            <div className="col-lg-4">
            <Select name="batch_name" label="Batch Name" 
              onChange={(e)=>stBatchID(e.target.value)}
              value={batchID}
              
            >
                      <option>Choose One</option>
                      {entity2.map((item) => (
                          <option value={item.chick_batch_id} >
                            {item.batchname}
                          </option>    
                      ))}
                  </Select>
                </div>
              <div className="col-lg-4">
                <Select name="buyback_farm_name" label="Farm Name" 
                onChange={(e)=>{setFarmValue(e.target.value)}}
                value={farmValue}
                >
                      <option>Choose One</option>
                      {farm.map((item) => (
                          <option value={item.id} >
                            {item.buyback_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                  <Select name="coop_name" label="Coop Name"
                    onChange={(e)=>{setCoopId(e.target.value)}}
                    value={coopId}
                  >
                      <option >Choose One</option>
                      {entity.map((item) => (
                          <option value={item.chickcoop_id} >
                            {item.chickcoop_name}
                          </option>    
                      ))}
                  </Select>
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chick_batch_quantity"
                    component={Input1}
                    placeholder={"Max Coop Quantity - " + coopQu}
                    label="Quantity"
                    onKeyUp={quntityHandler}
                  />
                  <p>Remaining Batch Quantity - <b>{total2}</b> </p>
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chick_coop_batch_description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
                <div className="col-lg-4">
                    <button type="submit" hidden={togle===true} className="btn btn-primary ml-2 + downbtn"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
              </div>
              
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
