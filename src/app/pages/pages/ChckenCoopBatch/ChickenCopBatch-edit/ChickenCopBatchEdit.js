/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/ChickenCopBatch/ChickenCopBatchesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { ChickenCopBatchEditForm } from "./ChickenCopBatchEditForm";
import { ChickenCopBatchCreateForm } from "./ChickenCopBatchCreateForm";
import { Specifications } from "../ChickenCopBatch-specifications/Specifications";
import { SpecificationsUIProvider } from "../ChickenCopBatch-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../ChickenCopBatch-remarks/RemarksUIContext";
import { Remarks } from "../ChickenCopBatch-remarks/Remarks";
import { 
  // Farms_GET_ID_URL, 
  Farms_URL } from "../../../_redux/Farm/FarmsCrud";
import axios from "axios";
import { ChickenBatches_GET_ID_URL, ChickenBatches_URL } from "../../../_redux/ChickenBatch/ChickenBatchesCrud";
import { Chickens_GET_ID_URL } from "../../../_redux/Chicken/ChickensCrud";
import { Api_Login } from "../../../../config/config";
import { ChickenCopBatches_GET_ID_URL } from "../../../_redux/ChickenCopBatch/ChickenCopBatchesCrud";

export function ChickenCopBatchEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const [farm,setFarm]= useState([]);
  const [batch,setBatch]= useState([]);
  const [farmValue, setFarmValue] = useState('Choose One');
  const [fa, setFa] = useState('');
  const [coopValue, setCoopValue] = useState('Choose One');
  const [coopName, setCoopName] = useState([])
  const [coopId, setCoopId] = useState('')
  const [coopQu, setCoopQu] = useState('')
  const [coopQuValue, setCoopQuValue] = useState('')
  // const [enti, setenti] = useState([])
  // const [enti2, setenti2] = useState([])
  const [batchID, stBatchID] = useState('')
  const [backbatchID, stbackBatchID] = useState('')
  const [backcoopId, setbackCoopId] = useState('')
  const [qu, stqu] = useState('')
  const [de, setde] = useState('')
  const dispatch = useDispatch();
  const [day, setday] = useState('');
  const [createDate, setcreateDate] = useState('')
  const [chickbatchQu, setchickbatchQu] = useState()
  const [total2, settotal2] = useState()
  const [total4, settotal4] = useState()
  const [act, setact] = useState("")
  const [seCoopId, setseCoopId] = useState('')
  const [seCooopNam, setseCooopNam] = useState('')
  const [adminId, setadminId] = useState('')
  const [createDate1, setcreateDate1] = useState('')
  const [updateDate, setupdateDate] = useState('')
  // const [total11, settotal11] = useState()

  console.log(backbatchID,backcoopId)

  // const url = Api_Login + '/api/chick-batch/update';
  // const url2 = Api_Login + '/api/chickencoop/update';
  // const url4 = Api_Login + '/api/chickencoop/update';
  
 
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, ChickenCopBatchForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.ChickenCopBatches.actionsLoading,
      ChickenCopBatchForEdit: state.ChickenCopBatches.ChickenCopBatchForEdit,
    }),
    shallowEqual
  );

  
  useEffect(()=>{
    axios({
      method: 'Post',
      baseURL: Farms_URL,
      data:{
        "filter": [{"status": "1" }],
      }
      })
      .then((res) => {
        setFarm(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'Post',
      baseURL: ChickenBatches_URL,
      data:{
        "filter": [{"active": "Active"}],
      }
      })
      .then((res) => {
        setBatch(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  useEffect(()=>{
    let total1 = 0
    axios({
      method: 'Post',
      baseURL: Api_Login + '/api/chickcoop-batch/search',
      data:{
        "filter": [{"batch_name": batchID}],
      }
      })
      .then((res) => {
        (res.data.data.results).forEach((item) => {
             total1 = total1 + item.chick_batch_quantity
        })
        settotal2(chickbatchQu - total1)
        settotal4(chickbatchQu - total1 + qu)
      })
  },[chickbatchQu, batchID, qu])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Farms_URL,
      data:{"filter": [{"id": farmValue},],
          "relation_filters": [{"buyback.active": "Active"}],
      }
      })
      .then((res) => {
        console.log(res.data.data.results[0].buyback)
        setCoopName(res.data.data.results[0].buyback)
        // setCoopName(res.data.buyback)
      })
      .catch(function (response) {
      });
      
  },[farmValue])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: ChickenBatches_GET_ID_URL + `/${batchID}`
      })
      .then((res) => {
        setchickbatchQu(res.data.chick_batch_quantity)
        
      })
      .catch(function (response) {
      });
      
  },[batchID])

  // console.log(coopName)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Chickens_GET_ID_URL + `/${coopId}`
      })
      .then((res) => {
        setCoopQu(res.data.chickcoop_quantity)
        setCoopQuValue(res.data.chickcoop_id)
        console.log(res.data.active.active)
        console.log("###############################")
        setact(res.data.active)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[coopId])

  

  const initChickenCopBatch = {

    chick_batch_quantity : qu,
    chick_coop_batch_description : de,
    buyback_farm_name:farmValue,
    coop_name : coopId,
    chickBatchId : batchID,
    batch_name:batchID,
    chickcoop:coopId,
    buyback:farmValue
  
  };

  useEffect(() => {

    axios({
      method: 'get',
      baseURL: ChickenCopBatches_GET_ID_URL + `/${id}`
      })
      .then((res) => {
        // axios.put(url4+`/${res.data.chickcoop.chickcoop_id}`, {"active":"Active"});
        setFarmValue(res.data.buyback.id)
        setFa(res.data.buyback.id)
        setCoopId(res.data.chickcoop.chickcoop_id)
        stBatchID(res.data.batch_name)
        stqu(res.data.chick_batch_quantity)
        setde(res.data.chick_coop_batch_description)
        setday(res.data.coopbatch.chicken_days_from_birth)
        setcreateDate(res.data.coopbatch.chick_batch_registered_date)

        setadminId(res.data.created_by);
        setcreateDate1(res.data.chick_coop_batch_registered_date);
        setupdateDate(res.data.chick__coop_batch_updated_date);

        setseCoopId(res.data.chickcoop.chickcoop_id)
        setseCooopNam(res.data.chickcoop.chickcoop_name)

        setbackCoopId(res.data.coop_name)
        stbackBatchID(res.data.batch_name)
        
        // axios.put(url + `/${res.data.batch_name}`, {status:1});
        // axios.put(url2 + `/${res.data.coop_name}`, {status:1});
        // entity2.push(res.data.batch_name)
        
      })
      .catch(function (response) {
      });
  }, [id])


  
  // console.log(coopName)
  // console.log(batch)
  // console.log(enti)

  useEffect(() => {
    dispatch(actions.fetchChickenCopBatch(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Chicken Coop Batch";
    if (ChickenCopBatchForEdit && id) {
      // _title = `Edit ChickenCopBatch - ${ChickenCopBatchForEdit.buyback_name} ${ChickenCopBatchForEdit.buyback_contact} - ${ChickenCopBatchForEdit.sales_ChickenCopBatch_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenCopBatchForEdit, id]);

  const saveChickenCopBatch = (values) => {
    if (!id) {
      dispatch(actions.createChickenCopBatch(values,id)).then(() => backToChickenCopBatchesList());
    } else {
      dispatch(actions.updateChickenCopBatch(values,id)).then(() => backToChickenCopBatchesList());
    }
  };

  const savePassword = (values) => {
    // console.log(values)
      dispatch(actions.updateChickenCopBatchPassword(values,id)).then(() => backToChickenCopBatchesList());
  };

  const btnRef = useRef();

  const backToChickenCopBatchesList = (values) => {
    history.push(`/buyback/chcoopbatch`);
    // if(id && values===null){
    //   axios.put(url + `/${backbatchID}`, {status:0});
    //   axios.put(url2 + `/${backcoopId}`, {status:0});
    // }
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickenCopBatchesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickenCopBatchEditForm
                actionsLoading={actionsLoading}
                ChickenCopBatch={initChickenCopBatch}
                btnRef={btnRef}
                saveChickenCopBatch={saveChickenCopBatch}
                farm={farm}
                setFarmValue={setFarmValue}
                farmValue={farmValue}
                fa={fa}
                coopName={coopName}
                entity={coopName}
                setCoopValue={setCoopValue}
                coopValue={coopValue}
                entity2={batch}
                coopId={coopId}
                setCoopId={setCoopId}
                coopQu={coopQu}
                coopQuValue={coopQuValue}
                stBatchID={stBatchID}
                batchID={batchID}
                day={day}
                createDate={createDate}
                total2={total2}
                act={act}
                total4={total4}
                seCoopId={seCoopId}
                seCooopName={seCooopNam}
                ChickenCopBatch2={ChickenCopBatchForEdit}
                adminId={adminId}
                createDate1={createDate1}
                updateDate={updateDate}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentChickenCopBatchId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentChickenCopBatchId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickenCopBatchesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    ChickenCopBatch remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    ChickenCopBatch specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickenCopBatchCreateForm
                actionsLoading={actionsLoading}
                ChickenCopBatch={ChickenCopBatchForEdit || initChickenCopBatch}
                btnRef={btnRef}
                saveChickenCopBatch={saveChickenCopBatch}
                savePassword={savePassword}
                farm={farm}
                setFarmValue={setFarmValue}
                farmValue={farmValue}
                coopName={coopName}
                entity={coopName}
                setCoopValue={setCoopValue}
                coopValue={coopValue}
                batch={batch}
                coopId={coopId}
                setCoopId={setCoopId}
                coopQu={coopQu}
                coopQuValue={coopQuValue}
                entity2={batch}
                stBatchID={stBatchID}
                batchID={batchID}
                total2={total2}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentChickenCopBatchId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentChickenCopBatchId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
