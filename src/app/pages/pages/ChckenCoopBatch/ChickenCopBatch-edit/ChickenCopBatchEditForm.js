import React, {useState, useEffect} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select, Input1 } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { notify, notifyWarning } from "../../../../config/Toastify";
import axios from "axios";
import { Api_Login } from "../../../../config/config";
import Pr from "./p1"
import Switch from '@material-ui/core/Switch';
import Admin from "../../../../config/Admin";
// import { truncate } from "lodash";

// Validation schema
const ChickenCopBatchEditSchema = Yup.object().shape({
  chick_batch_quantity: Yup.number()
    .required("Quantity is required")
    .min(1, "Quantity must be at least 1 characters"),
  chick_coop_batch_description: Yup.string()
    .required("Description is required")
    .min(2, "Description must be at least 2 characters"),
});

export function ChickenCopBatchEditForm({
  ChickenCopBatch,
  btnRef,
  saveChickenCopBatch,
  farm,
  setFarmValue,
  farmValue,
  coopName,
  entity,
  batch,
  entity2,
  stBatchID,
  batchID,
  coopId,
  coopQu,
  setCoopId,
  day,
  createDate,
  total2,
  act,
  total4,
  seCoopId,
  seCooopName,
  fa,
  adminId,
  createDate1,
  updateDate
}) {

  const [togle, settogle] = useState(false)
  const [togle2, settogle2] = useState(true)
  const [chView, setView] = useState(false)

  const url = Api_Login + '/api/chickencoop/update';
  // const url2 = Api_Login + '/api/chickencoop/soft-delete';

  console.log(coopQu)
  console.log(farmValue)
  console.log(entity2)

  useEffect(() => {
    if(act==="off"){
    setView(true);
  }
  console.log(act)
  }, [act])

  // console.log(ChickenCopBatch2.batch_name)


  
  const handleChangeView =(event)=>{
    if(event.target.checked===true)
    {
      axios.put(url+`/${coopId}`, {"active":"off"});
      setView(event.target.checked)
      notify("Sucessfully Active Coop....")
    }
    else{
      axios.put(url+`/${coopId}`, {"active":"Active"});
      setView(event.target.checked)
      notify("Sucessfully Closed Coop....")
    }
    console.log(event.target.checked)
  }
  

  const quntityHandler =(e)=> {
    if(parseInt(e.target.value) > parseInt(coopQu)){
      settogle(true)
      return notifyWarning("Sorry Maximum Can Add - " +  coopQu)
    }
    if(parseInt(e.target.value) > parseInt(total4)){
      settogle(true)
      return notifyWarning("Sorry You Can Maximum Add - " +  total4)
    }
    settogle(false)
  }

  const CoopHandler =(e)=>{
    setFarmValue(e.target.value)
    if(e.target.value===fa)
    {
      settogle2(true)
    }
    else{
      settogle2(false)
    }
  }

  return (
    <div>
      {notifyWarning()}
      <Formik
        enableReinitialize={true}
        initialValues={ChickenCopBatch}
        validationSchema={ChickenCopBatchEditSchema}
        onSubmit={(values)  => {

          // console.log(values.mstockItemId)

          // if(values.batch_name==='Choose One' || values.batch_name===undefined){
          if(values.batch_name==='Choose One'){
            return notifyWarning("Please Select Batch Name")
          }
          if(farmValue==='Choose One'){
            return notifyWarning("Please Select Farm Name")
          }
          if(values.coop_name==='Choose One'|| values.coop_name===undefined){
            return notifyWarning("Please Select Coop Name")
          }
          else{
            axios.put(url+`/${seCoopId}`, {"active":"Active"});
            console.log(values);
            saveChickenCopBatch(values);
            axios.put(url+`/${coopId}`, {"active":"off"});
          }
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
            <div className="form-group row">
            <div className="col-lg-4">
              <Select name="batch_name" label="Batch Name" 
                onChange={(e)=>stBatchID(e.target.value)}
                value={batchID}
              >
                    <option >Choose one</option>
                    {entity2.map((item) => (
                        <option value={item.chick_batch_id} >
                          {item.batchname}
                        </option>    
                    ))}
                </Select>
              </div>
            <div className="col-lg-4">
                <Select name="buyback_farm_name" label="Farm Name" 
                  onChange={CoopHandler}
                  value={farmValue}
                >
                      <option>Choose One</option>
                      {farm.map((item) => (
                          <option value={item.id} >
                            {item.buyback_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                {togle2===true && <div className="col-lg-4">
                  <Select name="chickcoop" label="Coop Name"
                    onChange={(e) => {setCoopId(e.target.value)}}
                    value={coopId}
                  >
                      <option value={seCoopId}>{seCooopName}</option>
                      {entity.map((item) => (
                          <option value={item.chickcoop_id} >
                            {item.chickcoop_name}
                          </option>    
                      ))}
                  </Select>
                </div>}
                {togle2===false && <div className="col-lg-4">
                  <Select name="chickcoop" label="Coop Name"
                    onChange={(e) => {setCoopId(e.target.value)}}
                    value={coopId}
                  >
                      <option >Choose One</option>
                      {entity.map((item) => (
                          <option value={item.chickcoop_id} >
                            {item.chickcoop_name}
                          </option>    
                      ))}
                  </Select>
                </div>}
              </div>

              <div className="form-group row">
              <div className="col-lg-4">
                <Field
                    type="text"
                    name="chick_batch_quantity"
                    component={Input1}
                    placeholder={"Quantity - " + coopQu}
                    label="Quantity"
                    onKeyUp={quntityHandler}
                  />
                  <p>Remaining Batch Quantity - <b>{total2}</b> </p>
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chick_coop_batch_description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
                <div className="col-lg-2">
                    <button type="submit" hidden={togle===true} className="btn btn-primary ml-2 + downbtn"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                </div>
                <div className="col-lg-2">
                <p>Coop Status</p>
                      <Switch
                       checked={chView}
                       value={chView}
                       onChange={handleChangeView}
                       color="primary"
                       inputProps={{ 'aria-label': 'secondary checkbox' }}
                  />
                  {chView===true && <p><b>Started Coop</b></p>}
                  {chView===false && <p><b>Cloesd Coop</b></p>}
                  </div>
              </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

              {/* <div className="form-group row">
                <div className="col-lg-6 + downbtn">
                  <Pr 
                    day1={day} 
                    createDate={createDate}
                  />
                </div>
                <div className="col-lg-2">
                <p>Coop Status</p>
                </div>
                <div className="col-lg-2">
                      <Switch
                       checked={chView}
                       value={chView}
                       onChange={handleChangeView}
                       color="primary"
                       inputProps={{ 'aria-label': 'secondary checkbox' }}
                  />
                  </div>
                  <div className="col-lg-2">
                  {chView===true && <p><b>Started Coop</b></p>}
                  {chView===false && <p><b>Cloesd Coop</b></p>}
                  </div> */}
                    {/* <button type="submit" hidden={togle===true} className="btn btn-primary ml-2"> Closed </button> */}
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                {/* </div> */}
                {/* <div className="col-lg-2 + downbtn">
                  {chView===true && <p><b>Started Coop</b></p>}
                  {chView===false && <p><b>Cloesd Coop</b></p>}
                </div> */}
              {/* </div> */}

              <div className="form-group row">
                <div className="col-lg-12 + downbtn">
                  <Pr 
                    day1={day} 
                    createDate={createDate}
                  />
                </div>
              </div>

              <Admin 
                  adminId={adminId}
                  createDate={createDate1} 
                  updateDate={updateDate}
                />
              
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
