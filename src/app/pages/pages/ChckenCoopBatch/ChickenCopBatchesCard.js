import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { ChickenCopBatchesFilter } from "./ChickenCopBatches-filter/ChickenCopBatchesFilter";
import { ChickenCopBatchesTable } from "./ChickenCopBatches-table/ChickenCopBatchesTable";
// import { ChickenCopBatchesGrouping } from "./ChickenCopBatches-grouping/ChickenCopBatchesGrouping";
import { useChickenCopBatchesUIContext } from "./ChickenCopBatchesUIContext";
import { notify } from "../../../config/Toastify";

export function ChickenCopBatchesCard() {
  const ChickenCopBatchesUIContext = useChickenCopBatchesUIContext();
  const ChickenCopBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenCopBatchesUIContext.ids,
      queryParams: ChickenCopBatchesUIContext.queryParams,
      setQueryParams: ChickenCopBatchesUIContext.setQueryParams,
      newChickenCopBatchButtonClick: ChickenCopBatchesUIContext.newChickenCopBatchButtonClick,
      openDeleteChickenCopBatchesDialog: ChickenCopBatchesUIContext.openDeleteChickenCopBatchesDialog,
      openEditChickenCopBatchPage: ChickenCopBatchesUIContext.openEditChickenCopBatchPage,
      openUpdateChickenCopBatchesStatusDialog:ChickenCopBatchesUIContext.openUpdateChickenCopBatchesStatusDialog,
      openFetchChickenCopBatchesDialog: ChickenCopBatchesUIContext.openFetchChickenCopBatchesDialog,
    };
  }, [ChickenCopBatchesUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Chicken Coop Batch list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={ChickenCopBatchesUIProps.newChickenCopBatchButtonClick}
          >
            New Chicken Coop Batch
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ChickenCopBatchesFilter />
        {ChickenCopBatchesUIProps.ids.length > 0 && (
          <>
            {/* <ChickenCopBatchesGrouping /> */}
          </>
        )}
        <ChickenCopBatchesTable />
      </CardBody>
    </Card>
  );
}
