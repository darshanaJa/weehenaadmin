import React, { useMemo } from "react";
import { useChickenCopBatchesUIContext } from "../ChickenCopBatchesUIContext";

export function ChickenCopBatchesGrouping() {
  // ChickenCopBatches UI Context
  const ChickenCopBatchesUIContext = useChickenCopBatchesUIContext();
  const ChickenCopBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenCopBatchesUIContext.ids,
      setIds: ChickenCopBatchesUIContext.setIds,
      openDeleteChickenCopBatchesDialog: ChickenCopBatchesUIContext.openDeleteChickenCopBatchesDialog,
      openFetchChickenCopBatchesDialog: ChickenCopBatchesUIContext.openFetchChickenCopBatchesDialog,
      openUpdateChickenCopBatchesStatusDialog:
        ChickenCopBatchesUIContext.openUpdateChickenCopBatchesStatusDialog,
    };
  }, [ChickenCopBatchesUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{ChickenCopBatchesUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={ChickenCopBatchesUIProps.openDeleteChickenCopBatchesDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ChickenCopBatchesUIProps.openFetchChickenCopBatchesDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ChickenCopBatchesUIProps.openUpdateChickenCopBatchesStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
