import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { ChickenCopBatchestatusCssClasses } from "../ChickenCopBatchesUIHelpers";
import { useChickenCopBatchesUIContext } from "../ChickenCopBatchesUIContext";

const selectedChickenCopBatches = (entities, ids) => {
  const _ChickenCopBatches = [];
  ids.forEach((id) => {
    const ChickenCopBatch = entities.find((el) => el.id === id);
    if (ChickenCopBatch) {
      _ChickenCopBatches.push(ChickenCopBatch);
    }
  });
  return _ChickenCopBatches;
};

export function ChickenCopBatchesFetchDialog({ show, onHide }) {
  // ChickenCopBatches UI Context
  const ChickenCopBatchesUIContext = useChickenCopBatchesUIContext();
  const ChickenCopBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenCopBatchesUIContext.ids,
      queryParams: ChickenCopBatchesUIContext.queryParams,
    };
  }, [ChickenCopBatchesUIContext]);

  // ChickenCopBatches Redux state
  const { ChickenCopBatches } = useSelector(
    (state) => ({
      ChickenCopBatches: selectedChickenCopBatches(state.ChickenCopBatches.entities, ChickenCopBatchesUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!ChickenCopBatchesUIProps.ids || ChickenCopBatchesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenCopBatchesUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {ChickenCopBatches.map((ChickenCopBatch) => (
              <div className="list-timeline-item mb-3" key={ChickenCopBatch.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ChickenCopBatchestatusCssClasses[ChickenCopBatch.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {ChickenCopBatch.id}
                  </span>{" "}
                  <span className="ml-5">
                    {ChickenCopBatch.manufacture}, {ChickenCopBatch.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
