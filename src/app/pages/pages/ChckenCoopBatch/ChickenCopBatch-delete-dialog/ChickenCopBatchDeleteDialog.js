/* eslint-disable no-restricted-imports */
import React, { useState, useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/ChickenCopBatch/ChickenCopBatchesActions";
import { useChickenCopBatchesUIContext } from "../ChickenCopBatchesUIContext";
import { Api_Login } from "../../../../config/config";
import axios from "axios";
import { ChickenCopBatches_GET_ID_URL } from "../../../_redux/ChickenCopBatch/ChickenCopBatchesCrud";

export function ChickenCopBatchDeleteDialog({ sales_ChickenCopBatch_id, show, onHide }) {
  // console.log(sales_ChickenCopBatch_id)
  // ChickenCopBatches UI Context
  const ChickenCopBatchesUIContext = useChickenCopBatchesUIContext();
  const ChickenCopBatchesUIProps = useMemo(() => {
    return {
      setIds: ChickenCopBatchesUIContext.setIds,
      queryParams: ChickenCopBatchesUIContext.queryParams,
    };
  }, [ChickenCopBatchesUIContext]);

  const url1 = Api_Login + '/api/chickencoop/update';
  const url2 = Api_Login + '/api/chick-batch/update';

  const [coopId, setcoopId] = useState('')
  const [BatchId, setBatchId] = useState('')

  // ChickenCopBatches Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.ChickenCopBatches.actionsLoading }),
    shallowEqual
  );

  console.log(sales_ChickenCopBatch_id)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: ChickenCopBatches_GET_ID_URL + `/${sales_ChickenCopBatch_id}`
      })
      .then((res) => {
        setcoopId(res.data.coop_name)
        setBatchId(res.data.batch_name)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[sales_ChickenCopBatch_id])

  console.log(coopId)



  // if !id we should close modal
  useEffect(() => {
    if (!sales_ChickenCopBatch_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_ChickenCopBatch_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteChickenCopBatch = () => {
    // server request for deleting ChickenCopBatch by id
    dispatch(actions.deleteChickenCopBatch(sales_ChickenCopBatch_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickenCopBatches(ChickenCopBatchesUIProps.queryParams));
      // clear selections list
      ChickenCopBatchesUIProps.setIds([]);
      // closing delete modal
      onHide();

      // axios({
      //   method: 'put',
      //   baseURL: url +`/${coopId}`,
      //   data:{status:1}
      //   })

      //   axios({
      //     method: 'put',
      //     baseURL: url2 +`/${BatchId}`,
      //     data:{status:1}
      //     })

      axios.put(url1 + `/${coopId}`, {"active":"Active"})
      axios.put(url2 + `/${BatchId}`, {status:1})

      // axios({method: 'put',baseURL: Api_Login + '/api/chickencoop/update'+`/${coopId}`}, data: {status:1})
      // axios({method: 'put',baseURL: ChickenCopBatches_GET_ID_URL + `/${sales_ChickenCopBatch_id}`})

    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          ChickenCopBatch Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this ChickenCopBatch?</span>
        )}
        {isLoading && <span>ChickenCopBatch is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChickenCopBatch}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
