import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./ChickenCopBatchesUIHelpers";

const ChickenCopBatchesUIContext = createContext();

export function useChickenCopBatchesUIContext() {
  return useContext(ChickenCopBatchesUIContext);
}

export const ChickenCopBatchesUIConsumer = ChickenCopBatchesUIContext.Consumer;

export function ChickenCopBatchesUIProvider({ ChickenCopBatchesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newChickenCopBatchButtonClick: ChickenCopBatchesUIEvents.newChickenCopBatchButtonClick,
    openEditChickenCopBatchPage: ChickenCopBatchesUIEvents.openEditChickenCopBatchPage,
    openDeleteChickenCopBatchDialog: ChickenCopBatchesUIEvents.openDeleteChickenCopBatchDialog,
    openDeleteChickenCopBatchesDialog: ChickenCopBatchesUIEvents.openDeleteChickenCopBatchesDialog,
    openFetchChickenCopBatchesDialog: ChickenCopBatchesUIEvents.openFetchChickenCopBatchesDialog,
    openUpdateChickenCopBatchesStatusDialog: ChickenCopBatchesUIEvents.openUpdateChickenCopBatchesStatusDialog,
  };

  return (
    <ChickenCopBatchesUIContext.Provider value={value}>
      {children}
    </ChickenCopBatchesUIContext.Provider>
  );
}
