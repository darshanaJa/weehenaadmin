import React from "react";
import {
  ChickenCopBatchestatusCssClasses,
  ChickenCopBatchestatusTitles
} from "../../ChickenCopBatchesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      ChickenCopBatchestatusCssClasses[row.status]
    } label-inline`}
  >
    {ChickenCopBatchestatusTitles[row.status]}
  </span>
);
