import React from "react";
import {
  ChickenCopBatchConditionCssClasses,
  ChickenCopBatchConditionTitles
} from "../../ChickenCopBatchesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        ChickenCopBatchConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        ChickenCopBatchConditionCssClasses[row.condition]
      }`}
    >
      {ChickenCopBatchConditionTitles[row.condition]}
    </span>
  </>
);
