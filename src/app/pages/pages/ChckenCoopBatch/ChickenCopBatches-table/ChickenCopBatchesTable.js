// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/ChickenCopBatch/ChickenCopBatchesActions";
import * as uiHelpers from "../ChickenCopBatchesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useChickenCopBatchesUIContext } from "../ChickenCopBatchesUIContext";

export function ChickenCopBatchesTable() {
  // ChickenCopBatches UI Context
  const ChickenCopBatchesUIContext = useChickenCopBatchesUIContext();
  const ChickenCopBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenCopBatchesUIContext.ids,
      setIds: ChickenCopBatchesUIContext.setIds,
      queryParams: ChickenCopBatchesUIContext.queryParams,
      setQueryParams: ChickenCopBatchesUIContext.setQueryParams,
      openEditChickenCopBatchPage: ChickenCopBatchesUIContext.openEditChickenCopBatchPage,
      openDeleteChickenCopBatchDialog: ChickenCopBatchesUIContext.openDeleteChickenCopBatchDialog,
    };
  }, [ChickenCopBatchesUIContext]);

  // Getting curret state of ChickenCopBatches list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.ChickenCopBatches }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // ChickenCopBatches Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    ChickenCopBatchesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchChickenCopBatches(ChickenCopBatchesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenCopBatchesUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_ChickenCopBatch_id);

  const columns = [
    // {
    //   dataField: "sales_ChickenCopBatch_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    // {
    //   dataField: "chick_coop_batch_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "buyback.buyback_name",
      text: "Farm Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "coopbatch.batchname",
      text: "Batch Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chickcoop.chickcoop_name",
      text: "Coop Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chick_batch_quantity",
      text: "Quanity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chick_coop_batch_description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
    },
    // {
    //   dataField: "buyback_ChickenCopBatch_quantity",
    //   text: "Quantity",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_passed_experiance",
    //   text: "Passed Experiance",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_ChickenCopBatch_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditChickenCopBatchPage: ChickenCopBatchesUIProps.openEditChickenCopBatchPage,
        openDeleteChickenCopBatchDialog: ChickenCopBatchesUIProps.openDeleteChickenCopBatchDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ChickenCopBatchesUIProps.queryParams.pageSize,
    page: ChickenCopBatchesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_ChickenCopBatch_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ChickenCopBatchesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ChickenCopBatchesUIProps.ids,
                  setIds: ChickenCopBatchesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
