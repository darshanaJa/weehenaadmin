export const ChickenCopBatchestatusCssClasses = ["success", "info", ""];
export const ChickenCopBatchestatusTitles = ["Selling", "Sold"];
export const ChickenCopBatchConditionCssClasses = ["success", "danger", ""];
export const ChickenCopBatchConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_ChickenCopBatch_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    buyback_name: ""
  },
  buyback_name:null,
  sortOrder: "asc", // asc||desc
  sortField: "buyback_name",
  pageNumber: 1,
  pageSize: 10
};
