import React from "react";
import { Route } from "react-router-dom";
import { ChickenCopBatchesLoadingDialog } from "./ChickenCopBatches-loading-dialog/ChickenCopBatchesLoadingDialog";
import { ChickenCopBatchDeleteDialog } from "./ChickenCopBatch-delete-dialog/ChickenCopBatchDeleteDialog";
import { ChickenCopBatchesDeleteDialog } from "./ChickenCopBatches-delete-dialog/ChickenCopBatchesDeleteDialog";
import { ChickenCopBatchesFetchDialog } from "./ChickenCopBatches-fetch-dialog/ChickenCopBatchesFetchDialog";
import { ChickenCopBatchesUpdateStatusDialog } from "./ChickenCopBatches-update-status-dialog/ChickenCopBatchesUpdateStatusDialog";
import { ChickenCopBatchesCard } from "./ChickenCopBatchesCard";
import { ChickenCopBatchesUIProvider } from "./ChickenCopBatchesUIContext";

export function ChickenCopBatchesPage({ history }) {
  const ChickenCopBatchesUIEvents = {
    newChickenCopBatchButtonClick: () => {
      history.push("/buyback/chcoopbatch/new");
    },
    openEditChickenCopBatchPage: (id) => {
      history.push(`/buyback/chcoopbatch/${id}/edit`);
    },
    openDeleteChickenCopBatchDialog: (sales_ChickenCopBatch_id) => {
      history.push(`/buyback/chcoopbatch/${sales_ChickenCopBatch_id}/delete`);
    },
    openDeleteChickenCopBatchesDialog: () => {
      history.push(`/buyback/chcoopbatch/deleteChickenCopBatches`);
    },
    openFetchChickenCopBatchesDialog: () => {
      history.push(`/buyback/chcoopbatch/fetch`);
    },
    openUpdateChickenCopBatchesStatusDialog: () => {
      history.push("/buyback/chcoopbatch/updateStatus");
    },
  };

  return (
    <ChickenCopBatchesUIProvider ChickenCopBatchesUIEvents={ChickenCopBatchesUIEvents}>
      <ChickenCopBatchesLoadingDialog />
      <Route path="/buyback/chcoopbatch/deleteChickenCopBatches">
        {({ history, match }) => (
          <ChickenCopBatchesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/chcoopbatch");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/chcoopbatch/:sales_ChickenCopBatch_id/delete">
        {({ history, match }) => (
          <ChickenCopBatchDeleteDialog
            show={match != null}
            sales_ChickenCopBatch_id={match && match.params.sales_ChickenCopBatch_id}
            onHide={() => {
              history.push("/buyback/chcoopbatch");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/chcoopbatch/fetch">
        {({ history, match }) => (
          <ChickenCopBatchesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/chcoopbatch");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/chcoopbatch/updateStatus">
        {({ history, match }) => (
          <ChickenCopBatchesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/chcoopbatch");
            }}
          />
        )}
      </Route>
      <ChickenCopBatchesCard />
    </ChickenCopBatchesUIProvider>
  );
}
