/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/ChickenCopBatch/ChickenCopBatchesActions";
import { useChickenCopBatchesUIContext } from "../ChickenCopBatchesUIContext";

export function ChickenCopBatchesDeleteDialog({ show, onHide }) {
  // ChickenCopBatches UI Context
  const ChickenCopBatchesUIContext = useChickenCopBatchesUIContext();
  const ChickenCopBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenCopBatchesUIContext.ids,
      setIds: ChickenCopBatchesUIContext.setIds,
      queryParams: ChickenCopBatchesUIContext.queryParams,
    };
  }, [ChickenCopBatchesUIContext]);

  // ChickenCopBatches Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.ChickenCopBatches.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected ChickenCopBatches we should close modal
  useEffect(() => {
    if (!ChickenCopBatchesUIProps.ids || ChickenCopBatchesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenCopBatchesUIProps.ids]);

  const deleteChickenCopBatches = () => {
    // server request for deleting ChickenCopBatch by seleted ids
    dispatch(actions.deleteChickenCopBatches(ChickenCopBatchesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickenCopBatches(ChickenCopBatchesUIProps.queryParams)).then(() => {
        // clear selections list
        ChickenCopBatchesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          ChickenCopBatches Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected ChickenCopBatches?</span>
        )}
        {isLoading && <span>ChickenCopBatches are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChickenCopBatches}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
