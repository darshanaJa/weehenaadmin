import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ChickenCopBatchestatusCssClasses } from "../ChickenCopBatchesUIHelpers";
import * as actions from "../../../_redux/ChickenCopBatch/ChickenCopBatchesActions";
import { useChickenCopBatchesUIContext } from "../ChickenCopBatchesUIContext";

const selectedChickenCopBatches = (entities, ids) => {
  const _ChickenCopBatches = [];
  ids.forEach((id) => {
    const ChickenCopBatch = entities.find((el) => el.id === id);
    if (ChickenCopBatch) {
      _ChickenCopBatches.push(ChickenCopBatch);
    }
  });
  return _ChickenCopBatches;
};

export function ChickenCopBatchesUpdateStatusDialog({ show, onHide }) {
  // ChickenCopBatches UI Context
  const ChickenCopBatchesUIContext = useChickenCopBatchesUIContext();
  const ChickenCopBatchesUIProps = useMemo(() => {
    return {
      ids: ChickenCopBatchesUIContext.ids,
      setIds: ChickenCopBatchesUIContext.setIds,
      queryParams: ChickenCopBatchesUIContext.queryParams,
    };
  }, [ChickenCopBatchesUIContext]);

  // ChickenCopBatches Redux state
  const { ChickenCopBatches, isLoading } = useSelector(
    (state) => ({
      ChickenCopBatches: selectedChickenCopBatches(state.ChickenCopBatches.entities, ChickenCopBatchesUIProps.ids),
      isLoading: state.ChickenCopBatches.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected ChickenCopBatches we should close modal
  useEffect(() => {
    if (ChickenCopBatchesUIProps.ids || ChickenCopBatchesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenCopBatchesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing ChickenCopBatch by ids
    dispatch(actions.updateChickenCopBatchesStatus(ChickenCopBatchesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchChickenCopBatches(ChickenCopBatchesUIProps.queryParams)).then(
          () => {
            // clear selections list
            ChickenCopBatchesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected ChickenCopBatches
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {ChickenCopBatches.map((ChickenCopBatch) => (
              <div className="list-timeline-item mb-3" key={ChickenCopBatch.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ChickenCopBatchestatusCssClasses[ChickenCopBatch.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {ChickenCopBatch.id}
                  </span>{" "}
                  <span className="ml-5">
                    {ChickenCopBatch.manufacture}, {ChickenCopBatch.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${ChickenCopBatchestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
