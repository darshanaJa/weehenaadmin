import React from "react";
import {
  FarmstatusCssClasses,
  FarmstatusTitles
} from "../../FarmsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      FarmstatusCssClasses[row.status]
    } label-inline`}
  >
    {FarmstatusTitles[row.status]}
  </span>
);
