import React from "react";
import {
  FarmConditionCssClasses,
  FarmConditionTitles
} from "../../FarmsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        FarmConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        FarmConditionCssClasses[row.condition]
      }`}
    >
      {FarmConditionTitles[row.condition]}
    </span>
  </>
);
