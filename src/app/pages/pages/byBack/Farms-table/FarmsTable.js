// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Farm/FarmsActions";
import * as uiHelpers from "../FarmsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useFarmsUIContext } from "../FarmsUIContext";

export function FarmsTable() {
  // Farms UI Context
  const FarmsUIContext = useFarmsUIContext();
  const FarmsUIProps = useMemo(() => {
    return {
      ids: FarmsUIContext.ids,
      setIds: FarmsUIContext.setIds,
      queryParams: FarmsUIContext.queryParams,
      setQueryParams: FarmsUIContext.setQueryParams,
      openEditFarmPage: FarmsUIContext.openEditFarmPage,
      openDeleteFarmDialog: FarmsUIContext.openDeleteFarmDialog,
    };
  }, [FarmsUIContext]);

  // Getting curret state of Farms list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Farms }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Farms Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    FarmsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchFarms(FarmsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [FarmsUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Farm_id);

  const columns = [
    // {
    //   dataField: "sales_Farm_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "buyback_name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "buyback_contact",
      text: "Contact",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "buyback_address",
      text: "Address",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "buyback_farm_quantity",
      text: "Quantity",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "buyback_passed_experiance",
      text: "Passed Experiance",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "buyback_email",
      text: "Email",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    // {
    //   dataField: "sales_Farm_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditFarmPage: FarmsUIProps.openEditFarmPage,
        openDeleteFarmDialog: FarmsUIProps.openDeleteFarmDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: FarmsUIProps.queryParams.pageSize,
    page: FarmsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Farm_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  FarmsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: FarmsUIProps.ids,
                  setIds: FarmsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
