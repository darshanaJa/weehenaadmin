import React from "react";
import { Route } from "react-router-dom";
import { FarmsLoadingDialog } from "./Farms-loading-dialog/FarmsLoadingDialog";
import { FarmDeleteDialog } from "./Farm-delete-dialog/FarmDeleteDialog";
import { FarmsDeleteDialog } from "./Farms-delete-dialog/FarmsDeleteDialog";
import { FarmsFetchDialog } from "./Farms-fetch-dialog/FarmsFetchDialog";
import { FarmsUpdateStatusDialog } from "./Farms-update-status-dialog/FarmsUpdateStatusDialog";
import { FarmsCard } from "./FarmsCard";
import { FarmsUIProvider } from "./FarmsUIContext";

export function FarmsPage({ history }) {
  const FarmsUIEvents = {
    newFarmButtonClick: () => {
      history.push("/buyback/farm/new");
    },
    openEditFarmPage: (id) => {
      history.push(`/buyback/farm/${id}/edit`);
    },
    openDeleteFarmDialog: (sales_Farm_id) => {
      history.push(`/buyback/farm/${sales_Farm_id}/delete`);
    },
    openDeleteFarmsDialog: () => {
      history.push(`/buyback/farm/deleteFarms`);
    },
    openFetchFarmsDialog: () => {
      history.push(`/buyback/farm/fetch`);
    },
    openUpdateFarmsStatusDialog: () => {
      history.push("/buyback/farm/updateStatus");
    },
  };

  return (
    <FarmsUIProvider FarmsUIEvents={FarmsUIEvents}>
      <FarmsLoadingDialog />
      <Route path="/buyback/farm/deleteFarms">
        {({ history, match }) => (
          <FarmsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/farm");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/farm/:sales_Farm_id/delete">
        {({ history, match }) => (
          <FarmDeleteDialog
            show={match != null}
            sales_Farm_id={match && match.params.sales_Farm_id}
            onHide={() => {
              history.push("/buyback/farm");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/farm/fetch">
        {({ history, match }) => (
          <FarmsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/farm");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/farm/updateStatus">
        {({ history, match }) => (
          <FarmsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/farm");
            }}
          />
        )}
      </Route>
      <FarmsCard />
    </FarmsUIProvider>
  );
}
