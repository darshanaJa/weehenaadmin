import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./FarmsUIHelpers";

const FarmsUIContext = createContext();

export function useFarmsUIContext() {
  return useContext(FarmsUIContext);
}

export const FarmsUIConsumer = FarmsUIContext.Consumer;

export function FarmsUIProvider({ FarmsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newFarmButtonClick: FarmsUIEvents.newFarmButtonClick,
    openEditFarmPage: FarmsUIEvents.openEditFarmPage,
    openDeleteFarmDialog: FarmsUIEvents.openDeleteFarmDialog,
    openDeleteFarmsDialog: FarmsUIEvents.openDeleteFarmsDialog,
    openFetchFarmsDialog: FarmsUIEvents.openFetchFarmsDialog,
    openUpdateFarmsStatusDialog: FarmsUIEvents.openUpdateFarmsStatusDialog,
  };

  return (
    <FarmsUIContext.Provider value={value}>
      {children}
    </FarmsUIContext.Provider>
  );
}
