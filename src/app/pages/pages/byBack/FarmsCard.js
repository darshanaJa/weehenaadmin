import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { FarmsFilter } from "./Farms-filter/FarmsFilter";
import { FarmsTable } from "./Farms-table/FarmsTable";
// import { FarmsGrouping } from "./Farms-grouping/FarmsGrouping";
import { useFarmsUIContext } from "./FarmsUIContext";
import { notify } from "../../../config/Toastify";

export function FarmsCard() {
  const FarmsUIContext = useFarmsUIContext();
  const FarmsUIProps = useMemo(() => {
    return {
      ids: FarmsUIContext.ids,
      queryParams: FarmsUIContext.queryParams,
      setQueryParams: FarmsUIContext.setQueryParams,
      newFarmButtonClick: FarmsUIContext.newFarmButtonClick,
      openDeleteFarmsDialog: FarmsUIContext.openDeleteFarmsDialog,
      openEditFarmPage: FarmsUIContext.openEditFarmPage,
      openUpdateFarmsStatusDialog:FarmsUIContext.openUpdateFarmsStatusDialog,
      openFetchFarmsDialog: FarmsUIContext.openFetchFarmsDialog,
    };
  }, [FarmsUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Farm list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={FarmsUIProps.newFarmButtonClick}
          >
            New Farm
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <FarmsFilter />
        {FarmsUIProps.ids.length > 0 && (
          <>
            {/* <FarmsGrouping /> */}
          </>
        )}
        <FarmsTable />
      </CardBody>
    </Card>
  );
}
