import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const FarmEditSchema = Yup.object().shape({
  buyback_name: Yup.string()
    .required("Name is required")
    .min(2, "Name must be at least 2 characters"),
    buyback_address: Yup.string()
    .required("Address is required")
    .min(2, "Address must be at least 2 characters"),
  buyback_farm_quantity: Yup.string()
    .required("Quntity is required"),
    buyback_email: Yup.string().email()
    .required("Email is required"),
    buyback_contact: Yup.string()
    .required("Contact Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
    buyback_passed_experiance: Yup.string()
    .required("Passed Experiance Required")
});


export function FarmCreateForm({
  Farm,
  btnRef,
  saveFarm,
}) {


  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={Farm}
        validationSchema={FarmEditSchema}
        onSubmit={(values)  => {
          console.log(values);            
          saveFarm(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="buyback_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="buyback_farm_quantity"
                    component={Input}
                    placeholder="Quntity"
                    label="Quntity"
                  />
                </div>
                <div className="col-lg-4">
                    <Field
                        type="email"
                        name="buyback_email"
                        component={Input}
                        placeholder="Email"
                        label="Email"
                      />
                  </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="buyback_contact"
                      component={Input}
                      placeholder="Contact"
                      label="Contact"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="buyback_passed_experiance"
                    component={Input}
                    placeholder="Passed Experiance"
                    label="Passed Experiance"
                    // customFeedbackLabel="Please enter "
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="buyback_address"
                    component={Input}
                    placeholder="address"
                    label="Address"
                  />
                </div>
              </div>

              <div className="form-group row">
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
                </div>  
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
