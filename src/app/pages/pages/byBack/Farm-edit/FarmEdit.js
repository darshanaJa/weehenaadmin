/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Farm/FarmsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { FarmEditForm } from "./FarmEditForm";
import { FarmCreateForm } from "./FarmCreateForm";
import { Specifications } from "../Farm-specifications/Specifications";
import { SpecificationsUIProvider } from "../Farm-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Farm-remarks/RemarksUIContext";
import { Remarks } from "../Farm-remarks/Remarks";

const initFarm = {

  buyback_name : "",
  buyback_address : "",
  buyback_farm_quantity : "",
  buyback_email : "",
  buyback_contact : "",
  buyback_passed_experiance : ""

};

export function FarmEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, FarmForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Farms.actionsLoading,
      FarmForEdit: state.Farms.FarmForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchFarm(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Farm";
    if (FarmForEdit && id) {
      // _title = `Edit Farm - ${FarmForEdit.buyback_name} ${FarmForEdit.buyback_contact} - ${FarmForEdit.sales_Farm_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [FarmForEdit, id]);

  const saveFarm = (values) => {
    if (!id) {
      dispatch(actions.createFarm(values,id)).then(() => backToFarmsList());
    } else {
      dispatch(actions.updateFarm(values,id)).then(() => backToFarmsList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateFarmPassword(values,id)).then(() => backToFarmsList());
  };

  const btnRef = useRef();

  const backToFarmsList = () => {
    history.push(`/buyback/farm`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToFarmsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Chicken Coop List
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <FarmEditForm
                actionsLoading={actionsLoading}
                Farm={FarmForEdit || initFarm}
                btnRef={btnRef}
                saveFarm={saveFarm}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentFarmId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id} currentFarmId={id}>
                <Specifications id={id} />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToFarmsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Farm remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Farm specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <FarmCreateForm
                actionsLoading={actionsLoading}
                Farm={FarmForEdit || initFarm}
                btnRef={btnRef}
                saveFarm={saveFarm}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentFarmId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentFarmId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
