/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Farm/FarmsActions";
import { useFarmsUIContext } from "../FarmsUIContext";

export function FarmsDeleteDialog({ show, onHide }) {
  // Farms UI Context
  const FarmsUIContext = useFarmsUIContext();
  const FarmsUIProps = useMemo(() => {
    return {
      ids: FarmsUIContext.ids,
      setIds: FarmsUIContext.setIds,
      queryParams: FarmsUIContext.queryParams,
    };
  }, [FarmsUIContext]);

  // Farms Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Farms.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Farms we should close modal
  useEffect(() => {
    if (!FarmsUIProps.ids || FarmsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [FarmsUIProps.ids]);

  const deleteFarms = () => {
    // server request for deleting Farm by seleted ids
    dispatch(actions.deleteFarms(FarmsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchFarms(FarmsUIProps.queryParams)).then(() => {
        // clear selections list
        FarmsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Farms Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Farms?</span>
        )}
        {isLoading && <span>Farms are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteFarms}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
