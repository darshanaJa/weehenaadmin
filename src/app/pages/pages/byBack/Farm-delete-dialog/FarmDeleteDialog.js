/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Farm/FarmsActions";
import { useFarmsUIContext } from "../FarmsUIContext";

export function FarmDeleteDialog({ sales_Farm_id, show, onHide }) {
  // console.log(sales_Farm_id)
  // Farms UI Context
  const FarmsUIContext = useFarmsUIContext();
  const FarmsUIProps = useMemo(() => {
    return {
      setIds: FarmsUIContext.setIds,
      queryParams: FarmsUIContext.queryParams,
    };
  }, [FarmsUIContext]);

  // Farms Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Farms.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Farm_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Farm_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteFarm = () => {
    // server request for deleting Farm by id
    dispatch(actions.deleteFarm(sales_Farm_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchFarms(FarmsUIProps.queryParams));
      // clear selections list
      FarmsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Farm Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Farm?</span>
        )}
        {isLoading && <span>Farm is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteFarm}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
