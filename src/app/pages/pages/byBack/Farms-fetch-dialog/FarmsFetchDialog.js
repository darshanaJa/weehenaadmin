import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { FarmstatusCssClasses } from "../FarmsUIHelpers";
import { useFarmsUIContext } from "../FarmsUIContext";

const selectedFarms = (entities, ids) => {
  const _Farms = [];
  ids.forEach((id) => {
    const Farm = entities.find((el) => el.id === id);
    if (Farm) {
      _Farms.push(Farm);
    }
  });
  return _Farms;
};

export function FarmsFetchDialog({ show, onHide }) {
  // Farms UI Context
  const FarmsUIContext = useFarmsUIContext();
  const FarmsUIProps = useMemo(() => {
    return {
      ids: FarmsUIContext.ids,
      queryParams: FarmsUIContext.queryParams,
    };
  }, [FarmsUIContext]);

  // Farms Redux state
  const { Farms } = useSelector(
    (state) => ({
      Farms: selectedFarms(state.Farms.entities, FarmsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!FarmsUIProps.ids || FarmsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [FarmsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Farms.map((Farm) => (
              <div className="list-timeline-item mb-3" key={Farm.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      FarmstatusCssClasses[Farm.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Farm.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Farm.manufacture}, {Farm.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
