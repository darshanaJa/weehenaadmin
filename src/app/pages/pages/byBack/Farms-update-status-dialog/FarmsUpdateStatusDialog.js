import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { FarmstatusCssClasses } from "../FarmsUIHelpers";
import * as actions from "../../../_redux/Farm/FarmsActions";
import { useFarmsUIContext } from "../FarmsUIContext";

const selectedFarms = (entities, ids) => {
  const _Farms = [];
  ids.forEach((id) => {
    const Farm = entities.find((el) => el.id === id);
    if (Farm) {
      _Farms.push(Farm);
    }
  });
  return _Farms;
};

export function FarmsUpdateStatusDialog({ show, onHide }) {
  // Farms UI Context
  const FarmsUIContext = useFarmsUIContext();
  const FarmsUIProps = useMemo(() => {
    return {
      ids: FarmsUIContext.ids,
      setIds: FarmsUIContext.setIds,
      queryParams: FarmsUIContext.queryParams,
    };
  }, [FarmsUIContext]);

  // Farms Redux state
  const { Farms, isLoading } = useSelector(
    (state) => ({
      Farms: selectedFarms(state.Farms.entities, FarmsUIProps.ids),
      isLoading: state.Farms.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Farms we should close modal
  useEffect(() => {
    if (FarmsUIProps.ids || FarmsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [FarmsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Farm by ids
    dispatch(actions.updateFarmsStatus(FarmsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchFarms(FarmsUIProps.queryParams)).then(
          () => {
            // clear selections list
            FarmsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Farms
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Farms.map((Farm) => (
              <div className="list-timeline-item mb-3" key={Farm.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      FarmstatusCssClasses[Farm.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Farm.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Farm.manufacture}, {Farm.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${FarmstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
