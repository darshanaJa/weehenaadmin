import React, { Suspense } from "react";
import { Switch } from "react-router-dom";
import { AccessPage } from "./access/AccessPage"; 
import {AccesEdit} from "./access/Acces-edit/AccesEdit"
import { ModulesPage } from "./Module/ModulesPage";
import { ModuleEdit } from "./Module/Module-edit/ModuleEdit";
import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";


export default function accessMenue() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        <ContentRoute path="/access/permisions/new" component={AccesEdit} />
        <ContentRoute path="/access/permisions/:id/edit" component={AccesEdit} /> 
        <ContentRoute path="/access/permisions" component={AccessPage} />

        <ContentRoute path="/access/modules/new" component={ModuleEdit} />
        <ContentRoute path="/access/modules/:id/edit" component={ModuleEdit} /> 
        <ContentRoute path="/access/modules" component={ModulesPage} />

        {/* <ContentRoute path="/transport/services/new" component={ServiceEdit} />
        <ContentRoute path="/transport/services/:id/edit" component={ServiceEdit} /> 
        <ContentRoute path="/transport/services" component={ServicesPage} /> */}
      </Switch>
    </Suspense>
  );
}
