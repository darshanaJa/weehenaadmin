import React, { Suspense } from "react";
import { Switch } from "react-router-dom";

import { FarmsPage } from "./byBack/FarmsPage"; 
import {FarmEdit} from "./byBack/Farm-edit/FarmEdit";

import { ChickensPage } from "./Chicken/ChickensPage";
import { ChickenEdit } from "./Chicken/Chicken-edit/ChickenEdit";


import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";
import { ChickenBatchesPage } from "./ChickenBatch/ChickenBatchesPage";
import { ChickenBatchEdit } from "./ChickenBatch/ChickenBatch-edit/ChickenBatchEdit";
import { ChickenCopBatchesPage } from "./ChckenCoopBatch/ChickenCopBatchesPage";
import { ChickenCopBatchEdit } from "./ChckenCoopBatch/ChickenCopBatch-edit/ChickenCopBatchEdit";
import { ChickensAgesPage } from "./ChickensAges/ChickensAgesPage";
import { ChickensAgeEdit } from "./ChickensAges/ChickensAge-edit/ChickensAgeEdit";


export default function buyBackMenue() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        
        <ContentRoute path="/buyback/farm/new" component={FarmEdit} />
        <ContentRoute path="/buyback/farm/:id/edit" component={FarmEdit} /> 
        <ContentRoute path="/buyback/farm" component={FarmsPage} />

        <ContentRoute path="/buyback/coop/new" component={ChickenEdit} />
        <ContentRoute path="/buyback/coop/:id/edit" component={ChickenEdit} /> 
        <ContentRoute path="/buyback/coop" component={ChickensPage} />

        <ContentRoute path="/buyback/batch/new" component={ChickenBatchEdit} />
        <ContentRoute path="/buyback/batch/:id/edit" component={ChickenBatchEdit} /> 
        <ContentRoute path="/buyback/batch" component={ChickenBatchesPage} />

        <ContentRoute path="/buyback/chcoopbatch/new" component={ChickenCopBatchEdit} />
        <ContentRoute path="/buyback/chcoopbatch/:id/edit" component={ChickenCopBatchEdit} /> 
        <ContentRoute path="/buyback/chcoopbatch" component={ChickenCopBatchesPage} />

        <ContentRoute path="/buyback/chicken/new" component={ChickensAgeEdit} />
        <ContentRoute path="/buyback/chicken/:id/edit" component={ChickensAgeEdit} /> 
        <ContentRoute path="/buyback/chicken"  exact component={ChickensAgesPage} />

      </Switch>
    </Suspense>
  );
}
