import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { VechicleGatestatusCssClasses } from "../VechicleGatesUIHelpers";
import * as actions from "../../../_redux/VechicleGate/VechicleGatesActions";
import { useVechicleGatesUIContext } from "../VechicleGatesUIContext";

const selectedVechicleGates = (entities, ids) => {
  const _VechicleGates = [];
  ids.forEach((id) => {
    const VechicleGate = entities.find((el) => el.id === id);
    if (VechicleGate) {
      _VechicleGates.push(VechicleGate);
    }
  });
  return _VechicleGates;
};

export function VechicleGatesUpdateStatusDialog({ show, onHide }) {
  // VechicleGates UI Context
  const VechicleGatesUIContext = useVechicleGatesUIContext();
  const VechicleGatesUIProps = useMemo(() => {
    return {
      ids: VechicleGatesUIContext.ids,
      setIds: VechicleGatesUIContext.setIds,
      queryParams: VechicleGatesUIContext.queryParams,
    };
  }, [VechicleGatesUIContext]);

  // VechicleGates Redux state
  const { VechicleGates, isLoading } = useSelector(
    (state) => ({
      VechicleGates: selectedVechicleGates(state.VechicleGates.entities, VechicleGatesUIProps.ids),
      isLoading: state.VechicleGates.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected VechicleGates we should close modal
  useEffect(() => {
    if (VechicleGatesUIProps.ids || VechicleGatesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicleGatesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing VechicleGate by ids
    dispatch(actions.updateVechicleGatesStatus(VechicleGatesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchVechicleGates(VechicleGatesUIProps.queryParams)).then(
          () => {
            // clear selections list
            VechicleGatesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected VechicleGates
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {VechicleGates.map((VechicleGate) => (
              <div className="list-timeline-item mb-3" key={VechicleGate.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      VechicleGatestatusCssClasses[VechicleGate.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {VechicleGate.id}
                  </span>{" "}
                  <span className="ml-5">
                    {VechicleGate.manufacture}, {VechicleGate.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${VechicleGatestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
