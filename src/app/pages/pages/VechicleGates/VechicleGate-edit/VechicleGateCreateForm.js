import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const VechicleGateEditSchema = Yup.object().shape({
  vehicle_number: Yup.string()
    .required("Vehicale Number is required")
    .min(2, "Vehicale Number must be at least 2 characters"),
  type: Yup.string()
    .required("Type is required")
    .min(1, "Type must be at least 1 characters"),
  vehicle_weight_before: Yup.number()
    .required("Before weight is required"),
  vehicle_weigt_after: Yup.number()
    .required("After Weight is required"),
  driver_name: Yup.string()
    .required("Driver Name Required"),
  helper_name: Yup.string()
    .required("Helper Name is Required"),
  reason: Yup.string()
    .required("Reason is Required")
});


export function VechicleGateCreateForm({
  VechicleGate,
  btnRef,
  saveVechicleGate,
  vechicaleNumber,
  driver,
  Helper
}) {


  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={VechicleGate}
        validationSchema={VechicleGateEditSchema}
        onSubmit={(values)  => {
          console.log(values);            
          saveVechicleGate(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Select name="vehicle_number" label="Vehicale Number">
                      <option>Choose One</option>
                      {vechicaleNumber.map((item) => (
                          <option value={item.vehi_reg_number} >
                            {item.vehi_reg_number}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="type"
                    component={Input}
                    placeholder="Type"
                    label="Type"
                  />
                </div>
                <div className="col-lg-4">
                    <Field
                        type="text"
                        name="vehicle_weight_before"
                        component={Input}
                        placeholder="Before Wight"
                        label="Before Wight"
                      />
                  </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="vehicle_weigt_after"
                      component={Input}
                      placeholder="After Weight"
                      label="After Weight"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                <div className="col-lg-4">
                  {/* <Field
                    type="text"
                    name="driver_name"
                    component={Input}
                    placeholder="Driver Name"
                    label="Driver Name"
                    // customFeedbackLabel="Please enter "
                  /> */}
                  <Select name="driver_name"  label="Driver Name" 
                  // onKeyUp={(e)=>setVeId(e.target.value)}
                  >
                    <option>Choose One</option>
                    {driver.map((item) => (
                      <option value={item.name}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                {/* <Field
                    type="text"
                    name="helper_name"
                    component={Input}
                    placeholder="Helper Name"
                    label="Helper Name"
                  /> */}
                  <Select name="helper_name" label="Helper Name" 
                  // onKeyUp={(e)=>setVeId(e.target.value)}
                  >
                    <option>Choose One</option>
                    {Helper.map((item) => (
                      <option value={item.name}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
              </div>

              <div className="form-group row">
              <div className="col-lg-4">
                <Field
                    type="text"
                    name="reason"
                    component={Input}
                    placeholder="Reason"
                    label="Reason"
                  />
                </div>
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
                </div>  
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
