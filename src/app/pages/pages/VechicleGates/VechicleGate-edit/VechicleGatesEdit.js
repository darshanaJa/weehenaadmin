/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/VechicleGate/VechicleGatesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { VechicleGateEditForm } from "./VechicleGateEditForm";
import { VechicleGateCreateForm } from "./VechicleGateCreateForm";
import { Specifications } from "../VechicleGate-specifications/Specifications";
import { SpecificationsUIProvider } from "../VechicleGate-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../VechicleGate-remarks/RemarksUIContext";
import { Remarks } from "../VechicleGate-remarks/Remarks";
import { Vechicales_URL } from "../../../_redux/Vechicale/VechicalesCrud";
import axios from 'axios';
import { Employees_URL } from "../../../_redux/Employee/EmployeesCrud";

const initVechicleGate = {

  vehicle_number : "",
  type : "",
  vehicle_weight_before : "",
  vehicle_weigt_after : "",
  driver_name : "",
  helper_name : "",
  reason : ""

};

export function VechicleGateEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();
  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const [shop,setShop]=useState([]);
  const [designation2,setDesignation]=useState([])
  const dispatch = useDispatch();

  const driver = [];
  const Helper=[]

  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, VechicleGateForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.VechicleGates.actionsLoading,
      VechicleGateForEdit: state.VechicleGates.VechicleGateForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchVechicleGate(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New VechicleGate";
    if (VechicleGateForEdit && id) {
      // _title = `Edit VechicleGate - ${VechicleGateForEdit.buyback_name} ${VechicleGateForEdit.buyback_contact} - ${VechicleGateForEdit.sales_VechicleGate_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicleGateForEdit, id]);

  const saveVechicleGate = (values) => {
    if (!id) {
      dispatch(actions.createVechicleGate(values,id)).then(() => backToVechicleGatesList());
    } else {
      dispatch(actions.updateVechicleGate(values,id)).then(() => backToVechicleGatesList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateVechicleGatePassword(values,id)).then(() => backToVechicleGatesList());
  };

  const btnRef = useRef();

  const backToVechicleGatesList = () => {
    history.push(`/transport/vechicalegate`);
  };

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Vechicales_URL,
      data:{
        "filter": [{"status": "1" }]
      }
      })
      .then((res) => {
        setShop(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  console.log(shop)

  useEffect(()=>{
    console.log("abcd")
    axios({
      method: 'post',
      baseURL: Employees_URL,
      data: { 
        "filter": [{"status": "1" }],
      } 
    })
    .then((res) => {
      setDesignation(res.data.data.results)
      console.log(res.data.data.results)
      
    })
    .catch(function (response) {
    });
    
    
  },[])

  if(!id){
    designation2.map(item => {
      if(item.positi.position_name==='Driver'){
        // console.log(item.id,item.emp_fname)
        driver.push({id:item.id, name:item.emp_fname}) 
      }

      if(item.positi.position_name==='Helper'){
        // console.log(item.id,item.emp_fname)
        Helper.push({id:item.id, name:item.emp_fname}) 
      }

      return({}
        // console.log(designation2)
      );
      
    })
    }
    else{
      designation2.map(item => {
        if(item.positi.position_name==='Driver'){
          // console.log(item.id,item.emp_fname)
          driver.push({id:item.id, name:item.emp_fname}) 
        }
  
        if(item.positi.position_name==='Helper'){
          // console.log(item.id,item.emp_fname)
          Helper.push({id:item.id, name:item.emp_fname}) 
        }
  
        return({}
          // console.log(designation2)
        )
      })
    }

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToVechicleGatesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <VechicleGateEditForm
                actionsLoading={actionsLoading}
                VechicleGate={VechicleGateForEdit || initVechicleGate}
                btnRef={btnRef}
                saveVechicleGate={saveVechicleGate}
                vechicaleNumber={shop}
                driver={driver}
                Helper={Helper}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentVechicleGateId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentVechicleGateId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToVechicleGatesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    VechicleGate remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    VechicleGate specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <VechicleGateCreateForm
                actionsLoading={actionsLoading}
                VechicleGate={VechicleGateForEdit || initVechicleGate}
                btnRef={btnRef}
                saveVechicleGate={saveVechicleGate}
                savePassword={savePassword}
                vechicaleNumber={shop}
                driver={driver}
                Helper={Helper}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentVechicleGateId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentVechicleGateId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
