import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { VechicleGatesFilter } from "./VechicleGates-filter/VechicleGatesFilter";
import { VechicleGatesTable } from "./VechicleGates-table/VechicleGatesTable";
import { VechicleGatesGrouping } from "./VechicleGates-grouping/VechicleGatesGrouping";
import { useVechicleGatesUIContext } from "./VechicleGatesUIContext";
import { notify } from "../../../config/Toastify";

export function VechicleGatesCard() {
  const VechicleGatesUIContext = useVechicleGatesUIContext();
  const VechicleGatesUIProps = useMemo(() => {
    return {
      ids: VechicleGatesUIContext.ids,
      queryParams: VechicleGatesUIContext.queryParams,
      setQueryParams: VechicleGatesUIContext.setQueryParams,
      newVechicleGateButtonClick: VechicleGatesUIContext.newVechicleGateButtonClick,
      openDeleteVechicleGatesDialog: VechicleGatesUIContext.openDeleteVechicleGatesDialog,
      openEditVechicleGatePage: VechicleGatesUIContext.openEditVechicleGatePage,
      openUpdateVechicleGatesStatusDialog:VechicleGatesUIContext.openUpdateVechicleGatesStatusDialog,
      openFetchVechicleGatesDialog: VechicleGatesUIContext.openFetchVechicleGatesDialog,
    };
  }, [VechicleGatesUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="VechicleGate list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={VechicleGatesUIProps.newVechicleGateButtonClick}
          >
            New VechicleGate
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <VechicleGatesFilter />
        {VechicleGatesUIProps.ids.length > 0 && (
          <>
            <VechicleGatesGrouping />
          </>
        )}
        <VechicleGatesTable />
      </CardBody>
    </Card>
  );
}
