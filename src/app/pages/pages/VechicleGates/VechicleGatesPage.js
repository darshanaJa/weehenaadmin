import React from "react";
import { Route } from "react-router-dom";
import { VechicleGatesLoadingDialog } from "./VechicleGates-loading-dialog/VechicleGatesLoadingDialog";
import { VechicleGateDeleteDialog } from "./VechicleGate-delete-dialog/VechicleGateDeleteDialog";
import { VechicleGatesDeleteDialog } from "./VechicleGates-delete-dialog/VechicleGatesDeleteDialog";
import { VechicleGatesFetchDialog } from "./VechicleGates-fetch-dialog/VechicleGatesFetchDialog";
import { VechicleGatesUpdateStatusDialog } from "./VechicleGates-update-status-dialog/VechicleGatesUpdateStatusDialog";
import { VechicleGatesCard } from "./VechicleGatesCard";
import { VechicleGatesUIProvider } from "./VechicleGatesUIContext";

export function VechicleGatesPage({ history }) {
  const VechicleGatesUIEvents = {
    newVechicleGateButtonClick: () => {
      history.push("/transport/vechicalegate/new");
    },
    openEditVechicleGatePage: (id) => {
      history.push(`/transport/vechicalegate/${id}/edit`);
    },
    openDeleteVechicleGateDialog: (sales_VechicleGate_id) => {
      history.push(`/transport/vechicalegate/${sales_VechicleGate_id}/delete`);
    },
    openDeleteVechicleGatesDialog: () => {
      history.push(`/transport/vechicalegate/deleteVechicleGates`);
    },
    openFetchVechicleGatesDialog: () => {
      history.push(`/transport/vechicalegate/fetch`);
    },
    openUpdateVechicleGatesStatusDialog: () => {
      history.push("/transport/vechicalegate/updateStatus");
    },
  };

  return (
    <VechicleGatesUIProvider VechicleGatesUIEvents={VechicleGatesUIEvents}>
      <VechicleGatesLoadingDialog />
      <Route path="/transport/vechicalegate/deleteVechicleGates">
        {({ history, match }) => (
          <VechicleGatesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/vechicalegate");
            }}
          />
        )}
      </Route>
      <Route path="/transport/vechicalegate/:sales_VechicleGate_id/delete">
        {({ history, match }) => (
          <VechicleGateDeleteDialog
            show={match != null}
            sales_VechicleGate_id={match && match.params.sales_VechicleGate_id}
            onHide={() => {
              history.push("/transport/vechicalegate");
            }}
          />
        )}
      </Route>
      <Route path="/transport/vechicalegate/fetch">
        {({ history, match }) => (
          <VechicleGatesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/vechicalegate");
            }}
          />
        )}
      </Route>
      <Route path="/transport/vechicalegate/updateStatus">
        {({ history, match }) => (
          <VechicleGatesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/vechicalegate");
            }}
          />
        )}
      </Route>
      <VechicleGatesCard />
    </VechicleGatesUIProvider>
  );
}
