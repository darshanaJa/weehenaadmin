/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/VechicleGate/VechicleGatesActions";
import { useVechicleGatesUIContext } from "../VechicleGatesUIContext";

export function VechicleGatesDeleteDialog({ show, onHide }) {
  // VechicleGates UI Context
  const VechicleGatesUIContext = useVechicleGatesUIContext();
  const VechicleGatesUIProps = useMemo(() => {
    return {
      ids: VechicleGatesUIContext.ids,
      setIds: VechicleGatesUIContext.setIds,
      queryParams: VechicleGatesUIContext.queryParams,
    };
  }, [VechicleGatesUIContext]);

  // VechicleGates Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.VechicleGates.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected VechicleGates we should close modal
  useEffect(() => {
    if (!VechicleGatesUIProps.ids || VechicleGatesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicleGatesUIProps.ids]);

  const deleteVechicleGates = () => {
    // server request for deleting VechicleGate by seleted ids
    dispatch(actions.deleteVechicleGates(VechicleGatesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchVechicleGates(VechicleGatesUIProps.queryParams)).then(() => {
        // clear selections list
        VechicleGatesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          VechicleGates Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected VechicleGates?</span>
        )}
        {isLoading && <span>VechicleGates are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteVechicleGates}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
