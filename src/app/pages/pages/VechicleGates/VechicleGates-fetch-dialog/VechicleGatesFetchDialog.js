import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { VechicleGatestatusCssClasses } from "../VechicleGatesUIHelpers";
import { useVechicleGatesUIContext } from "../VechicleGatesUIContext";

const selectedVechicleGates = (entities, ids) => {
  const _VechicleGates = [];
  ids.forEach((id) => {
    const VechicleGate = entities.find((el) => el.id === id);
    if (VechicleGate) {
      _VechicleGates.push(VechicleGate);
    }
  });
  return _VechicleGates;
};

export function VechicleGatesFetchDialog({ show, onHide }) {
  // VechicleGates UI Context
  const VechicleGatesUIContext = useVechicleGatesUIContext();
  const VechicleGatesUIProps = useMemo(() => {
    return {
      ids: VechicleGatesUIContext.ids,
      queryParams: VechicleGatesUIContext.queryParams,
    };
  }, [VechicleGatesUIContext]);

  // VechicleGates Redux state
  const { VechicleGates } = useSelector(
    (state) => ({
      VechicleGates: selectedVechicleGates(state.VechicleGates.entities, VechicleGatesUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!VechicleGatesUIProps.ids || VechicleGatesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicleGatesUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {VechicleGates.map((VechicleGate) => (
              <div className="list-timeline-item mb-3" key={VechicleGate.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      VechicleGatestatusCssClasses[VechicleGate.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {VechicleGate.id}
                  </span>{" "}
                  <span className="ml-5">
                    {VechicleGate.manufacture}, {VechicleGate.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
