import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./VechicleGatesUIHelpers";

const VechicleGatesUIContext = createContext();

export function useVechicleGatesUIContext() {
  return useContext(VechicleGatesUIContext);
}

export const VechicleGatesUIConsumer = VechicleGatesUIContext.Consumer;

export function VechicleGatesUIProvider({ VechicleGatesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newVechicleGateButtonClick: VechicleGatesUIEvents.newVechicleGateButtonClick,
    openEditVechicleGatePage: VechicleGatesUIEvents.openEditVechicleGatePage,
    openDeleteVechicleGateDialog: VechicleGatesUIEvents.openDeleteVechicleGateDialog,
    openDeleteVechicleGatesDialog: VechicleGatesUIEvents.openDeleteVechicleGatesDialog,
    openFetchVechicleGatesDialog: VechicleGatesUIEvents.openFetchVechicleGatesDialog,
    openUpdateVechicleGatesStatusDialog: VechicleGatesUIEvents.openUpdateVechicleGatesStatusDialog,
  };

  return (
    <VechicleGatesUIContext.Provider value={value}>
      {children}
    </VechicleGatesUIContext.Provider>
  );
}
