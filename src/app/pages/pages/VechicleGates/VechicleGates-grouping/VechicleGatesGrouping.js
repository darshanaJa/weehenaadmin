import React, { useMemo } from "react";
import { useVechicleGatesUIContext } from "../VechicleGatesUIContext";

export function VechicleGatesGrouping() {
  // VechicleGates UI Context
  const VechicleGatesUIContext = useVechicleGatesUIContext();
  const VechicleGatesUIProps = useMemo(() => {
    return {
      ids: VechicleGatesUIContext.ids,
      setIds: VechicleGatesUIContext.setIds,
      openDeleteVechicleGatesDialog: VechicleGatesUIContext.openDeleteVechicleGatesDialog,
      openFetchVechicleGatesDialog: VechicleGatesUIContext.openFetchVechicleGatesDialog,
      openUpdateVechicleGatesStatusDialog:
        VechicleGatesUIContext.openUpdateVechicleGatesStatusDialog,
    };
  }, [VechicleGatesUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{VechicleGatesUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={VechicleGatesUIProps.openDeleteVechicleGatesDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={VechicleGatesUIProps.openFetchVechicleGatesDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={VechicleGatesUIProps.openUpdateVechicleGatesStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
