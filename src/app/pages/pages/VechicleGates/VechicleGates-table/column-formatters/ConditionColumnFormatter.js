import React from "react";
import {
  VechicleGateConditionCssClasses,
  VechicleGateConditionTitles
} from "../../VechicleGatesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        VechicleGateConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        VechicleGateConditionCssClasses[row.condition]
      }`}
    >
      {VechicleGateConditionTitles[row.condition]}
    </span>
  </>
);
