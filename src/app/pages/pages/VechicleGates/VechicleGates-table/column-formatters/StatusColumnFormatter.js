import React from "react";
import {
  VechicleGatestatusCssClasses,
  VechicleGatestatusTitles
} from "../../VechicleGatesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      VechicleGatestatusCssClasses[row.status]
    } label-inline`}
  >
    {VechicleGatestatusTitles[row.status]}
  </span>
);
