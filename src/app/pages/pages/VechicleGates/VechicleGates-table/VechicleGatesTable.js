// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/VechicleGate/VechicleGatesActions";
import * as uiHelpers from "../VechicleGatesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useVechicleGatesUIContext } from "../VechicleGatesUIContext";

export function VechicleGatesTable() {
  // VechicleGates UI Context
  const VechicleGatesUIContext = useVechicleGatesUIContext();
  const VechicleGatesUIProps = useMemo(() => {
    return {
      ids: VechicleGatesUIContext.ids,
      setIds: VechicleGatesUIContext.setIds,
      queryParams: VechicleGatesUIContext.queryParams,
      setQueryParams: VechicleGatesUIContext.setQueryParams,
      openEditVechicleGatePage: VechicleGatesUIContext.openEditVechicleGatePage,
      openDeleteVechicleGateDialog: VechicleGatesUIContext.openDeleteVechicleGateDialog,
    };
  }, [VechicleGatesUIContext]);

  // Getting curret state of VechicleGates list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.VechicleGates }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // VechicleGates Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    VechicleGatesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchVechicleGates(VechicleGatesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [VechicleGatesUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_VechicleGate_id);

  const columns = [
    // {
    //   dataField: "sales_VechicleGate_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "type",
      text: "Type",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "vehicle_number",
      text: "Vehicale Number",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "vehicle_weight_before",
      text: "Before Weight",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "vehicle_weigt_after",
      text: "Afer Weight",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "driver_name",
      text: "Drver Name",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "reason",
      text: "Reason",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditVechicleGatePage: VechicleGatesUIProps.openEditVechicleGatePage,
        openDeleteVechicleGateDialog: VechicleGatesUIProps.openDeleteVechicleGateDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: VechicleGatesUIProps.queryParams.pageSize,
    page: VechicleGatesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_VechicleGate_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  VechicleGatesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: VechicleGatesUIProps.ids,
                  setIds: VechicleGatesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
