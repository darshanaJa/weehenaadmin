/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/VechicleGate/VechicleGatesActions";
import { useVechicleGatesUIContext } from "../VechicleGatesUIContext";

export function VechicleGateDeleteDialog({ sales_VechicleGate_id, show, onHide }) {
  // console.log(sales_VechicleGate_id)
  // VechicleGates UI Context
  const VechicleGatesUIContext = useVechicleGatesUIContext();
  const VechicleGatesUIProps = useMemo(() => {
    return {
      setIds: VechicleGatesUIContext.setIds,
      queryParams: VechicleGatesUIContext.queryParams,
    };
  }, [VechicleGatesUIContext]);

  // VechicleGates Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.VechicleGates.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_VechicleGate_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_VechicleGate_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteVechicleGate = () => {
    // server request for deleting VechicleGate by id
    dispatch(actions.deleteVechicleGate(sales_VechicleGate_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchVechicleGates(VechicleGatesUIProps.queryParams));
      // clear selections list
      VechicleGatesUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          VechicleGate Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this VechicleGate?</span>
        )}
        {isLoading && <span>VechicleGate is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteVechicleGate}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
