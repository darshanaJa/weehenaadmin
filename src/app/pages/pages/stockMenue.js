import React, { Suspense } from "react";
import { Switch } from "react-router-dom";

import { MainStocksPage } from "./mainStock/MainStocksPage"; 
import { MainStocksPage02 } from "./mainStock/MainStocksPage02"; 
import { MainStockEdit } from "./mainStock/mainStock-edit/MainStockEdit";

import { PricesHistoryPage } from "./salesPriceHistory/PricesHistoryPage";
import {PriceHistoryEdit} from "./salesPriceHistory/PriceHistory-edit/PriceHistoryEdit";

import { StockBatchsPage } from "./stockBatch/StockBatchsPage"; 
import {StockBatchEdit} from "./stockBatch/StockBatch-edit/StockBatchEdit";

import { SpareStocksPage } from "./SpareStock/SpareStocksPage";
import { SpareStockEdit } from "./SpareStock/SpareStock-edit/SpareStockEdit";

import { SparegrnsPage } from "./sparegrn/SparegrnsPage";
import { SparegrnEdit } from "./sparegrn/Sparegrn-edit/SparegrnEdit";

import { LayoutSplashScreen, ContentRoute } from "../../../_metronic/layout";






export default function stockMenue() {

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from eCommerce root URL to /customers */
          // <Redirect
          //   exact={true}
          //   from="/sales"
          //   to="/sales/customers"
          // />
        }
        {/* <ContentRoute path="/sales/customers" component={CustomersPage} /> */}

        
        <ContentRoute path="/stocks/mainstocks/report" component={MainStocksPage02} />
        <ContentRoute path="/stocks/mainstocks/new" component={MainStockEdit} />
        <ContentRoute path="/stocks/mainstocks/:id/edit" component={MainStockEdit} /> 
        <ContentRoute path="/stocks/mainstocks" component={MainStocksPage} />
        

        <ContentRoute path="/stocks/pricehistory/new" component={PriceHistoryEdit} />
        <ContentRoute path="/stocks/pricehistory/:id/edit" component={PriceHistoryEdit} /> 
        <ContentRoute path="/stocks/pricehistory" component={PricesHistoryPage} />

        <ContentRoute path="/stocks/stockbatch/new" component={StockBatchEdit} />
        <ContentRoute path="/stocks/stockbatch/:id/edit" component={StockBatchEdit} /> 
        <ContentRoute path="/stocks/stockbatch" component={StockBatchsPage} />

        <ContentRoute path="/stocks/sparepartstock/new" component={SpareStockEdit} />
        <ContentRoute path="/stocks/sparepartstock/:id/edit" component={SpareStockEdit} /> 
        <ContentRoute path="/stocks/sparepartstock" component={SpareStocksPage} />

        <ContentRoute path="/stocks/sparepartgrn/new" component={SparegrnEdit} />
        <ContentRoute path="/stocks/sparepartgrn/:id/edit" component={SparegrnEdit} /> 
        <ContentRoute path="/stocks/sparepartgrn" component={SparegrnsPage} />

        

      </Switch>
    </Suspense>
  );
}
