import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { PaymentsFilter } from "./Payments-filter/PaymentsFilter";
import { PaymentsTable } from "./Payments-table/PaymentsTable";
// import { PaymentsGrouping } from "./Payments-grouping/PaymentsGrouping";
import { usePaymentsUIContext } from "./PaymentsUIContext";
import { notify } from "../../../config/Toastify";

export function PaymentsCard() {
  const PaymentsUIContext = usePaymentsUIContext();
  const PaymentsUIProps = useMemo(() => {
    return {
      ids: PaymentsUIContext.ids,
      queryParams: PaymentsUIContext.queryParams,
      setQueryParams: PaymentsUIContext.setQueryParams,
      newPaymentButtonClick: PaymentsUIContext.newPaymentButtonClick,
      openDeletePaymentsDialog: PaymentsUIContext.openDeletePaymentsDialog,
      openEditPaymentPage: PaymentsUIContext.openEditPaymentPage,
      openUpdatePaymentsStatusDialog:PaymentsUIContext.openUpdatePaymentsStatusDialog,
      openFetchPaymentsDialog: PaymentsUIContext.openFetchPaymentsDialog,
    };
  }, [PaymentsUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="Payment list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={PaymentsUIProps.newPaymentButtonClick}
          >
            New Payment
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <PaymentsFilter />
        {PaymentsUIProps.ids.length > 0 && (
          <>
            {/* <PaymentsGrouping /> */}
          </>
        )}
        <PaymentsTable />
      </CardBody>
    </Card>
  );
}
