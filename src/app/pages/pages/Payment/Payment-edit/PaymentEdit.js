/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Payment/PaymentsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { PaymentEditForm } from "./PaymentEditForm";
import { PaymentCreateForm } from "./PaymentCreateForm";
import { Specifications } from "../Payment-specifications/Specifications";
import { SpecificationsUIProvider } from "../Payment-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Payment-remarks/RemarksUIContext";
import { Remarks } from "../Payment-remarks/Remarks";

const initPayment = {

  buyback_name : "",
  buyback_address : "",
  buyback_Payment_quantity : "",
  buyback_email : "",
  buyback_contact : "",
  buyback_passed_experiance : ""

};

export function PaymentEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, PaymentForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Payments.actionsLoading,
      PaymentForEdit: state.Payments.PaymentForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchPayment(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Payment";
    if (PaymentForEdit && id) {
      // _title = `Edit Payment - ${PaymentForEdit.buyback_name} ${PaymentForEdit.buyback_contact} - ${PaymentForEdit.sales_Payment_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PaymentForEdit, id]);

  const savePayment = (values) => {
    if (!id) {
      dispatch(actions.createPayment(values,id)).then(() => backToPaymentsList());
    } else {
      dispatch(actions.updatePayment(values,id)).then(() => backToPaymentsList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updatePaymentPassword(values,id)).then(() => backToPaymentsList());
  };

  const btnRef = useRef();

  const backToPaymentsList = () => {
    history.push(`/sales/payments`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToPaymentsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Chicken Coop List
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <PaymentEditForm
                actionsLoading={actionsLoading}
                Payment={PaymentForEdit || initPayment}
                btnRef={btnRef}
                savePayment={savePayment}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentPaymentId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id} currentPaymentId={id}>
                <Specifications id={id} />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToPaymentsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Payment remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Payment specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <PaymentCreateForm
                actionsLoading={actionsLoading}
                Payment={PaymentForEdit || initPayment}
                btnRef={btnRef}
                savePayment={savePayment}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentPaymentId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentPaymentId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
