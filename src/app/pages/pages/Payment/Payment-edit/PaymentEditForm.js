import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import Admin from "../../../../config/Admin";

// Validation schema
const PaymentEditSchema = Yup.object().shape({
  // id: Yup.string(),
  type: Yup.string()
    .required("Type is required")
    .min(2, "Type must be at least 2 characters"),
  //   buyback_address: Yup.string()
  //   .required("Address is required")
  //   .min(2, "Address must be at least 2 characters"),
  // buyback_Payment_quantity: Yup.string()
  //   .required("Quntity is required"),
  //   buyback_email: Yup.string().email()
  //   .required("Email is required"),
  //   buyback_contact: Yup.string()
  //   .required("Contact Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  //   buyback_passed_experiance: Yup.string()
  //   .required("Passed Experiance Required")
});

export function PaymentEditForm({
  Payment,
  btnRef,
  savePayment,
}) {

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={Payment}
        validationSchema={PaymentEditSchema}
        onSubmit={(values)  => {

          console.log(values);
          savePayment(values);
        }}
      >
        {({ handleSubmit,values,setFieldValue }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="type"
                    component={Input}
                    placeholder="Type"
                    label="Type"
                    // onKeyUp={e => {
                    //   setFieldValue("buyback_name", e.target.value);
                    // }}
                  />
                </div>
                {/* <div className="col-lg-4">
                  <Field
                    type="text"
                    name="buyback_Payment_quantity"
                    component={Input}
                    placeholder="Quntity"
                    label="Quntity"
                  />
                </div>
                <div className="col-lg-4">
                    <Field
                        type="email"
                        name="buyback_email"
                        component={Input}
                        placeholder="Email"
                        label="Email"
                      />
                  </div> */}
              </div>
              {/* <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="buyback_contact"
                      component={Input}
                      placeholder="Contact"
                      label="Contact"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="buyback_passed_experiance"
                    component={Input}
                    placeholder="Passed Experiance"
                    label="Passed Experiance"
                    // customFeedbackLabel="Please enter "
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="buyback_address"
                    component={Input}
                    placeholder="address"
                    label="Address"
                  />
                </div>
              </div> */}

                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <div className="form-group row">
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
                </div>  


                {/* <div className="form-group row">
                  <div className="col-lg-12">
                    <hr class="new3" />
                  </div>  
                </div> */}
                
                <Admin 
                  adminId={Payment.created_by}
                  createDate={Payment.buyback_Payment_created} 
                  updateDate={Payment.buyback_Payment_updated} 
                />

                {/* <div className="form-group row"> */}
                  {/* <p>Created by - <b>{Payment.created_by}</b></p>
                  <p>Creaed Date - <b>{(moment(Payment.buyback_Payment_created).format("yyyy-MMM-DD hh:mm:ss"))}</b></p>
                  <p>Updated Date - <b>{(moment(Payment.buyback_Payment_updated).format("yyyy-MMM-DD hh:mm:ss"))}</b></p> */}
                {/* </div> */}
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
