/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Payment/PaymentsActions";
import { usePaymentsUIContext } from "../PaymentsUIContext";

export function PaymentsDeleteDialog({ show, onHide }) {
  // Payments UI Context
  const PaymentsUIContext = usePaymentsUIContext();
  const PaymentsUIProps = useMemo(() => {
    return {
      ids: PaymentsUIContext.ids,
      setIds: PaymentsUIContext.setIds,
      queryParams: PaymentsUIContext.queryParams,
    };
  }, [PaymentsUIContext]);

  // Payments Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Payments.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Payments we should close modal
  useEffect(() => {
    if (!PaymentsUIProps.ids || PaymentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PaymentsUIProps.ids]);

  const deletePayments = () => {
    // server request for deleting Payment by seleted ids
    dispatch(actions.deletePayments(PaymentsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchPayments(PaymentsUIProps.queryParams)).then(() => {
        // clear selections list
        PaymentsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Payments Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Payments?</span>
        )}
        {isLoading && <span>Payments are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deletePayments}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
