/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Payment/PaymentsActions";
import { usePaymentsUIContext } from "../PaymentsUIContext";

export function PaymentDeleteDialog({ sales_Payment_id, show, onHide }) {
  // console.log(sales_Payment_id)
  // Payments UI Context
  const PaymentsUIContext = usePaymentsUIContext();
  const PaymentsUIProps = useMemo(() => {
    return {
      setIds: PaymentsUIContext.setIds,
      queryParams: PaymentsUIContext.queryParams,
    };
  }, [PaymentsUIContext]);

  // Payments Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Payments.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Payment_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Payment_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deletePayment = () => {
    // server request for deleting Payment by id
    dispatch(actions.deletePayment(sales_Payment_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchPayments(PaymentsUIProps.queryParams));
      // clear selections list
      PaymentsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Payment Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Payment?</span>
        )}
        {isLoading && <span>Payment is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deletePayment}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
