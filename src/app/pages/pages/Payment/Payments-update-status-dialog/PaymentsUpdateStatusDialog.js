import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { PaymentstatusCssClasses } from "../PaymentsUIHelpers";
import * as actions from "../../../_redux/Payment/PaymentsActions";
import { usePaymentsUIContext } from "../PaymentsUIContext";

const selectedPayments = (entities, ids) => {
  const _Payments = [];
  ids.forEach((id) => {
    const Payment = entities.find((el) => el.id === id);
    if (Payment) {
      _Payments.push(Payment);
    }
  });
  return _Payments;
};

export function PaymentsUpdateStatusDialog({ show, onHide }) {
  // Payments UI Context
  const PaymentsUIContext = usePaymentsUIContext();
  const PaymentsUIProps = useMemo(() => {
    return {
      ids: PaymentsUIContext.ids,
      setIds: PaymentsUIContext.setIds,
      queryParams: PaymentsUIContext.queryParams,
    };
  }, [PaymentsUIContext]);

  // Payments Redux state
  const { Payments, isLoading } = useSelector(
    (state) => ({
      Payments: selectedPayments(state.Payments.entities, PaymentsUIProps.ids),
      isLoading: state.Payments.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Payments we should close modal
  useEffect(() => {
    if (PaymentsUIProps.ids || PaymentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PaymentsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Payment by ids
    dispatch(actions.updatePaymentsStatus(PaymentsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchPayments(PaymentsUIProps.queryParams)).then(
          () => {
            // clear selections list
            PaymentsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Payments
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Payments.map((Payment) => (
              <div className="list-timeline-item mb-3" key={Payment.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      PaymentstatusCssClasses[Payment.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Payment.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Payment.manufacture}, {Payment.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${PaymentstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
