import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./PaymentsUIHelpers";

const PaymentsUIContext = createContext();

export function usePaymentsUIContext() {
  return useContext(PaymentsUIContext);
}

export const PaymentsUIConsumer = PaymentsUIContext.Consumer;

export function PaymentsUIProvider({ PaymentsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newPaymentButtonClick: PaymentsUIEvents.newPaymentButtonClick,
    openEditPaymentPage: PaymentsUIEvents.openEditPaymentPage,
    openDeletePaymentDialog: PaymentsUIEvents.openDeletePaymentDialog,
    openDeletePaymentsDialog: PaymentsUIEvents.openDeletePaymentsDialog,
    openFetchPaymentsDialog: PaymentsUIEvents.openFetchPaymentsDialog,
    openUpdatePaymentsStatusDialog: PaymentsUIEvents.openUpdatePaymentsStatusDialog,
  };

  return (
    <PaymentsUIContext.Provider value={value}>
      {children}
    </PaymentsUIContext.Provider>
  );
}
