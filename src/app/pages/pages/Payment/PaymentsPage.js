import React from "react";
import { Route } from "react-router-dom";
import { PaymentsLoadingDialog } from "./Payments-loading-dialog/PaymentsLoadingDialog";
import { PaymentDeleteDialog } from "./Payment-delete-dialog/PaymentDeleteDialog";
import { PaymentsDeleteDialog } from "./Payments-delete-dialog/PaymentsDeleteDialog";
import { PaymentsFetchDialog } from "./Payments-fetch-dialog/PaymentsFetchDialog";
import { PaymentsUpdateStatusDialog } from "./Payments-update-status-dialog/PaymentsUpdateStatusDialog";
import { PaymentsCard } from "./PaymentsCard";
import { PaymentsUIProvider } from "./PaymentsUIContext";

export function PaymentsPage({ history }) {
  const PaymentsUIEvents = {
    newPaymentButtonClick: () => {
      history.push("/sales/payments/new");
    },
    openEditPaymentPage: (id) => {
      history.push(`/sales/payments/${id}/edit`);
    },
    openDeletePaymentDialog: (sales_Payment_id) => {
      history.push(`/sales/payments/${sales_Payment_id}/delete`);
    },
    openDeletePaymentsDialog: () => {
      history.push(`/sales/payments/deletePayments`);
    },
    openFetchPaymentsDialog: () => {
      history.push(`/sales/payments/fetch`);
    },
    openUpdatePaymentsStatusDialog: () => {
      history.push("/sales/payments/updateStatus");
    },
  };

  return (
    <PaymentsUIProvider PaymentsUIEvents={PaymentsUIEvents}>
      <PaymentsLoadingDialog />
      <Route path="/sales/payments/deletePayments">
        {({ history, match }) => (
          <PaymentsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <Route path="/sales/payments/:sales_Payment_id/delete">
        {({ history, match }) => (
          <PaymentDeleteDialog
            show={match != null}
            sales_Payment_id={match && match.params.sales_Payment_id}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <Route path="/sales/payments/fetch">
        {({ history, match }) => (
          <PaymentsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <Route path="/sales/payments/updateStatus">
        {({ history, match }) => (
          <PaymentsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/payments");
            }}
          />
        )}
      </Route>
      <PaymentsCard />
    </PaymentsUIProvider>
  );
}
