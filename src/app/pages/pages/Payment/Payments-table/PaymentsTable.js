// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Payment/PaymentsActions";
import * as uiHelpers from "../PaymentsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { usePaymentsUIContext } from "../PaymentsUIContext";

export function PaymentsTable() {
  // Payments UI Context
  const PaymentsUIContext = usePaymentsUIContext();
  const PaymentsUIProps = useMemo(() => {
    return {
      ids: PaymentsUIContext.ids,
      setIds: PaymentsUIContext.setIds,
      queryParams: PaymentsUIContext.queryParams,
      setQueryParams: PaymentsUIContext.setQueryParams,
      openEditPaymentPage: PaymentsUIContext.openEditPaymentPage,
      openDeletePaymentDialog: PaymentsUIContext.openDeletePaymentDialog,
    };
  }, [PaymentsUIContext]);

  // Getting curret state of Payments list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Payments }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Payments Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    PaymentsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchPayments(PaymentsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PaymentsUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Payment_id);

  const columns = [
    {
      dataField: "id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "type",
      text: "Type",
      sort: true,
      sortCaret: sortCaret,
    },
    // {
    //   dataField: "buyback_contact",
    //   text: "Contact",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    // {
    //   dataField: "buyback_address",
    //   text: "Address",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    // {
    //   dataField: "buyback_Payment_quantity",
    //   text: "Quantity",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_passed_experiance",
    //   text: "Passed Experiance",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_Payment_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditPaymentPage: PaymentsUIProps.openEditPaymentPage,
        openDeletePaymentDialog: PaymentsUIProps.openDeletePaymentDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: PaymentsUIProps.queryParams.pageSize,
    page: PaymentsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Payment_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  PaymentsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: PaymentsUIProps.ids,
                  setIds: PaymentsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
