import React from "react";
import {
  PaymentConditionCssClasses,
  PaymentConditionTitles
} from "../../PaymentsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        PaymentConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        PaymentConditionCssClasses[row.condition]
      }`}
    >
      {PaymentConditionTitles[row.condition]}
    </span>
  </>
);
