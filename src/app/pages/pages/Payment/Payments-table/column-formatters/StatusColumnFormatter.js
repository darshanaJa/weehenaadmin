import React from "react";
import {
  PaymentstatusCssClasses,
  PaymentstatusTitles
} from "../../PaymentsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      PaymentstatusCssClasses[row.status]
    } label-inline`}
  >
    {PaymentstatusTitles[row.status]}
  </span>
);
