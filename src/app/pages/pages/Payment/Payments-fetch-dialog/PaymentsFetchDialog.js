import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { PaymentstatusCssClasses } from "../PaymentsUIHelpers";
import { usePaymentsUIContext } from "../PaymentsUIContext";

const selectedPayments = (entities, ids) => {
  const _Payments = [];
  ids.forEach((id) => {
    const Payment = entities.find((el) => el.id === id);
    if (Payment) {
      _Payments.push(Payment);
    }
  });
  return _Payments;
};

export function PaymentsFetchDialog({ show, onHide }) {
  // Payments UI Context
  const PaymentsUIContext = usePaymentsUIContext();
  const PaymentsUIProps = useMemo(() => {
    return {
      ids: PaymentsUIContext.ids,
      queryParams: PaymentsUIContext.queryParams,
    };
  }, [PaymentsUIContext]);

  // Payments Redux state
  const { Payments } = useSelector(
    (state) => ({
      Payments: selectedPayments(state.Payments.entities, PaymentsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!PaymentsUIProps.ids || PaymentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PaymentsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Payments.map((Payment) => (
              <div className="list-timeline-item mb-3" key={Payment.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      PaymentstatusCssClasses[Payment.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Payment.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Payment.manufacture}, {Payment.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
