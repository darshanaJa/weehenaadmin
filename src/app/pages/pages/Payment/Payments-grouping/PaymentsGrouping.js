import React, { useMemo } from "react";
import { usePaymentsUIContext } from "../PaymentsUIContext";

export function PaymentsGrouping() {
  // Payments UI Context
  const PaymentsUIContext = usePaymentsUIContext();
  const PaymentsUIProps = useMemo(() => {
    return {
      ids: PaymentsUIContext.ids,
      setIds: PaymentsUIContext.setIds,
      openDeletePaymentsDialog: PaymentsUIContext.openDeletePaymentsDialog,
      openFetchPaymentsDialog: PaymentsUIContext.openFetchPaymentsDialog,
      openUpdatePaymentsStatusDialog:
        PaymentsUIContext.openUpdatePaymentsStatusDialog,
    };
  }, [PaymentsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{PaymentsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={PaymentsUIProps.openDeletePaymentsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={PaymentsUIProps.openFetchPaymentsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={PaymentsUIProps.openUpdatePaymentsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
