export const SuplierstatusCssClasses = ["success", "info", ""];
export const SuplierstatusTitles = ["Selling", "Sold"];
export const SuplierConditionCssClasses = ["success", "danger", ""];
export const SuplierConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_Suplier_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    supplier_name: ""
  },
  supplier_name: null,
  sortOrder: "asc", // asc||desc
  sortField: "supplier_name",
  pageNumber: 1,
  pageSize: 10
};
