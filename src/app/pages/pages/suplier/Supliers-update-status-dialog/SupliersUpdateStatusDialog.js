import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { SuplierstatusCssClasses } from "../SupliersUIHelpers";
import * as actions from "../../../_redux/Suplier/SupliersActions";
import { useSupliersUIContext } from "../SupliersUIContext";

const selectedSupliers = (entities, ids) => {
  const _Supliers = [];
  ids.forEach((id) => {
    const Suplier = entities.find((el) => el.id === id);
    if (Suplier) {
      _Supliers.push(Suplier);
    }
  });
  return _Supliers;
};

export function SupliersUpdateStatusDialog({ show, onHide }) {
  // Supliers UI Context
  const SupliersUIContext = useSupliersUIContext();
  const SupliersUIProps = useMemo(() => {
    return {
      ids: SupliersUIContext.ids,
      setIds: SupliersUIContext.setIds,
      queryParams: SupliersUIContext.queryParams,
    };
  }, [SupliersUIContext]);

  // Supliers Redux state
  const { Supliers, isLoading } = useSelector(
    (state) => ({
      Supliers: selectedSupliers(state.Supliers.entities, SupliersUIProps.ids),
      isLoading: state.Supliers.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Supliers we should close modal
  useEffect(() => {
    if (SupliersUIProps.ids || SupliersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SupliersUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Suplier by ids
    dispatch(actions.updateSupliersStatus(SupliersUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchSupliers(SupliersUIProps.queryParams)).then(
          () => {
            // clear selections list
            SupliersUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Supliers
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Supliers.map((Suplier) => (
              <div className="list-timeline-item mb-3" key={Suplier.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SuplierstatusCssClasses[Suplier.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Suplier.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Suplier.manufacture}, {Suplier.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${SuplierstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
