import React from "react";
import { Route } from "react-router-dom";
import { SupliersLoadingDialog } from "./Supliers-loading-dialog/SupliersLoadingDialog";
import { SuplierDeleteDialog } from "./Suplier-delete-dialog/SuplierDeleteDialog";
import { SupliersDeleteDialog } from "./Supliers-delete-dialog/SupliersDeleteDialog";
import { SupliersFetchDialog } from "./Supliers-fetch-dialog/SupliersFetchDialog";
import { SupliersUpdateStatusDialog } from "./Supliers-update-status-dialog/SupliersUpdateStatusDialog";
import { SupliersCard } from "./SupliersCard";
import { SupliersUIProvider } from "./SupliersUIContext";

export function SupliersPage({ history }) {
  const SupliersUIEvents = {
    newSuplierButtonClick: () => {
      history.push("/suplier/Suplier/new");
    },
    openEditSuplierPage: (id) => {
      history.push(`/suplier/Suplier/${id}/edit`);
    },
    openDeleteSuplierDialog: (sales_Suplier_id) => {
      history.push(`/suplier/Suplier/${sales_Suplier_id}/delete`);
    },
    openDeleteSupliersDialog: () => {
      history.push(`/suplier/Suplier/deleteSupliers`);
    },
    openFetchSupliersDialog: () => {
      history.push(`/suplier/Suplier/fetch`);
    },
    openUpdateSupliersStatusDialog: () => {
      history.push("/suplier/Suplier/updateStatus");
    },
  };

  return (
    <SupliersUIProvider SupliersUIEvents={SupliersUIEvents}>
      <SupliersLoadingDialog />
      <Route path="/suplier/suplier/deleteSupliers">
        {({ history, match }) => (
          <SupliersDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/suplier/suplier");
            }}
          />
        )}
      </Route>
      <Route path="/suplier/suplier/:sales_Suplier_id/delete">
        {({ history, match }) => (
          <SuplierDeleteDialog
            show={match != null}
            sales_Suplier_id={match && match.params.sales_Suplier_id}
            onHide={() => {
              history.push("/suplier/suplier");
            }}
          />
        )}
      </Route>
      <Route path="/suplier/suplier/fetch">
        {({ history, match }) => (
          <SupliersFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/suplier/suplier");
            }}
          />
        )}
      </Route>
      <Route path="/suplier/suplier/updateStatus">
        {({ history, match }) => (
          <SupliersUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/suplier/suplier");
            }}
          />
        )}
      </Route>
      <SupliersCard />
    </SupliersUIProvider>
  );
}
