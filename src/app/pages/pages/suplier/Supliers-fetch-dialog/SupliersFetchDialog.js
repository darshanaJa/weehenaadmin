import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { SuplierstatusCssClasses } from "../SupliersUIHelpers";
import { useSupliersUIContext } from "../SupliersUIContext";

const selectedSupliers = (entities, ids) => {
  const _Supliers = [];
  ids.forEach((id) => {
    const Suplier = entities.find((el) => el.id === id);
    if (Suplier) {
      _Supliers.push(Suplier);
    }
  });
  return _Supliers;
};

export function SupliersFetchDialog({ show, onHide }) {
  // Supliers UI Context
  const SupliersUIContext = useSupliersUIContext();
  const SupliersUIProps = useMemo(() => {
    return {
      ids: SupliersUIContext.ids,
      queryParams: SupliersUIContext.queryParams,
    };
  }, [SupliersUIContext]);

  // Supliers Redux state
  const { Supliers } = useSelector(
    (state) => ({
      Supliers: selectedSupliers(state.Supliers.entities, SupliersUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!SupliersUIProps.ids || SupliersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SupliersUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Supliers.map((Suplier) => (
              <div className="list-timeline-item mb-3" key={Suplier.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SuplierstatusCssClasses[Suplier.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Suplier.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Suplier.manufacture}, {Suplier.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
