import React, { useMemo } from "react";
import { useSupliersUIContext } from "../SupliersUIContext";

export function SupliersGrouping() {
  // Supliers UI Context
  const SupliersUIContext = useSupliersUIContext();
  const SupliersUIProps = useMemo(() => {
    return {
      ids: SupliersUIContext.ids,
      setIds: SupliersUIContext.setIds,
      openDeleteSupliersDialog: SupliersUIContext.openDeleteSupliersDialog,
      openFetchSupliersDialog: SupliersUIContext.openFetchSupliersDialog,
      openUpdateSupliersStatusDialog:
        SupliersUIContext.openUpdateSupliersStatusDialog,
    };
  }, [SupliersUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{SupliersUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={SupliersUIProps.openDeleteSupliersDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SupliersUIProps.openFetchSupliersDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SupliersUIProps.openUpdateSupliersStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
