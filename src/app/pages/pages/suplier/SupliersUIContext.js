import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./SupliersUIHelpers";

const SupliersUIContext = createContext();

export function useSupliersUIContext() {
  return useContext(SupliersUIContext);
}

export const SupliersUIConsumer = SupliersUIContext.Consumer;

export function SupliersUIProvider({ SupliersUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newSuplierButtonClick: SupliersUIEvents.newSuplierButtonClick,
    openEditSuplierPage: SupliersUIEvents.openEditSuplierPage,
    openDeleteSuplierDialog: SupliersUIEvents.openDeleteSuplierDialog,
    openDeleteSupliersDialog: SupliersUIEvents.openDeleteSupliersDialog,
    openFetchSupliersDialog: SupliersUIEvents.openFetchSupliersDialog,
    openUpdateSupliersStatusDialog: SupliersUIEvents.openUpdateSupliersStatusDialog,
  };

  return (
    <SupliersUIContext.Provider value={value}>
      {children}
    </SupliersUIContext.Provider>
  );
}
