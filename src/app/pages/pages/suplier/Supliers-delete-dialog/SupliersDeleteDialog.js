/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Suplier/SupliersActions";
import { useSupliersUIContext } from "../SupliersUIContext";

export function SupliersDeleteDialog({ show, onHide }) {
  // Supliers UI Context
  const SupliersUIContext = useSupliersUIContext();
  const SupliersUIProps = useMemo(() => {
    return {
      ids: SupliersUIContext.ids,
      setIds: SupliersUIContext.setIds,
      queryParams: SupliersUIContext.queryParams,
    };
  }, [SupliersUIContext]);

  // Supliers Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Supliers.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Supliers we should close modal
  useEffect(() => {
    if (!SupliersUIProps.ids || SupliersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SupliersUIProps.ids]);

  const deleteSupliers = () => {
    // server request for deleting Suplier by seleted ids
    dispatch(actions.deleteSupliers(SupliersUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSupliers(SupliersUIProps.queryParams)).then(() => {
        // clear selections list
        SupliersUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Supliers Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Supliers?</span>
        )}
        {isLoading && <span>Supliers are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSupliers}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
