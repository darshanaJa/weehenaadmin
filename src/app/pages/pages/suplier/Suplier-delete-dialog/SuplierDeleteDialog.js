/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Suplier/SupliersActions";
import { useSupliersUIContext } from "../SupliersUIContext";

export function SuplierDeleteDialog({ sales_Suplier_id, show, onHide }) {
  const SupliersUIContext = useSupliersUIContext();
  const SupliersUIProps = useMemo(() => {
    return {
      setIds: SupliersUIContext.setIds,
      queryParams: SupliersUIContext.queryParams,
    };
  }, [SupliersUIContext]);

  // Supliers Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Supliers.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Suplier_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Suplier_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteSuplier = () => {
    // server request for deleting Suplier by id
    dispatch(actions.deleteSuplier(sales_Suplier_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSupliers(SupliersUIProps.queryParams));
      // clear selections list
      SupliersUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Suplier Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Suplier?</span>
        )}
        {isLoading && <span>Suplier is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSuplier}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
