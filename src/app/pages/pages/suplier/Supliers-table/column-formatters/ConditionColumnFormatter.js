import React from "react";
import {
  SuplierConditionCssClasses,
  SuplierConditionTitles
} from "../../SupliersUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        SuplierConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        SuplierConditionCssClasses[row.condition]
      }`}
    >
      {SuplierConditionTitles[row.condition]}
    </span>
  </>
);
