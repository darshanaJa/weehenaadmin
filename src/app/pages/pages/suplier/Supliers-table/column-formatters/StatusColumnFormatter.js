import React from "react";
import {
  SuplierstatusCssClasses,
  SuplierstatusTitles
} from "../../SupliersUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      SuplierstatusCssClasses[row.status]
    } label-inline`}
  >
    {SuplierstatusTitles[row.status]}
  </span>
);
