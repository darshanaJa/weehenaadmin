// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Suplier/SupliersActions";
import * as uiHelpers from "../SupliersUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useSupliersUIContext } from "../SupliersUIContext";

export function SupliersTable() {
  // Supliers UI Context
  const SupliersUIContext = useSupliersUIContext();
  const SupliersUIProps = useMemo(() => {
    return {
      ids: SupliersUIContext.ids,
      setIds: SupliersUIContext.setIds,
      queryParams: SupliersUIContext.queryParams,
      setQueryParams: SupliersUIContext.setQueryParams,
      openEditSuplierPage: SupliersUIContext.openEditSuplierPage,
      openDeleteSuplierDialog: SupliersUIContext.openDeleteSuplierDialog,
    };
  }, [SupliersUIContext]);

  // Getting curret state of Supliers list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Supliers }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Supliers Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    SupliersUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchSupliers(SupliersUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SupliersUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Suplier_id);

  const columns = [
    {
      dataField: "supplier_name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "supplier_company",
      text: "Company",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "supplier_address",
      text: "Address",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "supplier_contact",
      text: "Contact",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "supplier_prod_details",
      text: "Details",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "supplier_email",
      text: "Email",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditSuplierPage: SupliersUIProps.openEditSuplierPage,
        openDeleteSuplierDialog: SupliersUIProps.openDeleteSuplierDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: SupliersUIProps.queryParams.pageSize,
    page: SupliersUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Suplier_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  SupliersUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: SupliersUIProps.ids,
                  setIds: SupliersUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
