import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { SupliersFilter } from "./Supliers-filter/SupliersFilter";
import { SupliersTable } from "./Supliers-table/SupliersTable";
import { SupliersGrouping } from "./Supliers-grouping/SupliersGrouping";
import { useSupliersUIContext } from "./SupliersUIContext";
import { notify } from "../../../config/Toastify";

export function SupliersCard() {
  const SupliersUIContext = useSupliersUIContext();
  const SupliersUIProps = useMemo(() => {
    return {
      ids: SupliersUIContext.ids,
      queryParams: SupliersUIContext.queryParams,
      setQueryParams: SupliersUIContext.setQueryParams,
      newSuplierButtonClick: SupliersUIContext.newSuplierButtonClick,
      openDeleteSupliersDialog: SupliersUIContext.openDeleteSupliersDialog,
      openEditSuplierPage: SupliersUIContext.openEditSuplierPage,
      openUpdateSupliersStatusDialog:SupliersUIContext.openUpdateSupliersStatusDialog,
      openFetchSupliersDialog: SupliersUIContext.openFetchSupliersDialog,
    };
  }, [SupliersUIContext]);

  return (
    <Card>
      <CardHeader title="Supplier list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={SupliersUIProps.newSuplierButtonClick}
          >
            New Supplier
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <SupliersFilter />
        {SupliersUIProps.ids.length > 0 && (
          <>
            <SupliersGrouping />
          </>
        )}
        <SupliersTable />
      </CardBody>
    </Card>
  );
}
