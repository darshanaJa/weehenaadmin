import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const SuplierEditSchema = Yup.object().shape({
  // id: Yup.string(),
  supplier_name: Yup.string()
    .required("Name is required")
    .min(2, "Name must be at least 2 characters"),
    supplier_company: Yup.string()
    .required("Company is required")
    .min(2, "Company must be at least 2 characters"),
    supplier_address: Yup.string()
    .required("Address is required")
    .min(2, "Address must be at least 2 characters"),
  supplier_prod_details: Yup.string()
    .required("Suplier details is required")
    .min(2, "Suplier details must be at least 2 characters"),
    supplier_email: Yup.string().email()
    .required("Email is required")
    .min(2, "Email must be at least 2 characters"),
    supplier_contact: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
  
});


export function SuplierCreateForm({
  Suplier,
  btnRef,
  saveSuplier,
}) {

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={Suplier}
        validationSchema={SuplierEditSchema}
        onSubmit={(values)  => {
          console.log(values);
          saveSuplier(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="supplier_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="supplier_company"
                    component={Input}
                    placeholder="Company"
                    label="Company"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="supplier_prod_details"
                    component={Input}
                    placeholder="Details"
                    label="Details"
                  />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="supplier_contact"
                      component={Input}
                      placeholder="Contact"
                      label="Contact"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="supplier_address"
                    component={Input}
                    placeholder="Address"
                    label="Address"
                  />
                </div>
              </div>

              <div className="form-group row">
                  <div className="col-lg-4">
                    <Field
                        type="email"
                        name="supplier_email"
                        component={Input}
                        placeholder="Email"
                        label="Email"
                      />
                  </div>
                  
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtnedit"> Save</button>
                     </div>
                </div>  
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
