/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Suplier/SupliersActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { SuplierEditForm } from "./SuplierEditForm";
import { SuplierCreateForm } from "./SuplierCreateForm";
import { Specifications } from "../Suplier-specifications/Specifications";
import { SpecificationsUIProvider } from "../Suplier-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Suplier-remarks/RemarksUIContext";
import { Remarks } from "../Suplier-remarks/Remarks";

const initSuplier = {
  supplier_name : "",
  supplier_company : "",
  supplier_address : "",
  supplier_prod_details : "",
  supplier_email : "",
  supplier_contact : ""
  // profile_pic : []

};

export function SuplierEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, SuplierForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Supliers.actionsLoading,
      SuplierForEdit: state.Supliers.SuplierForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchSuplier(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Supplier";
    if (SuplierForEdit && id) {
      // _title = `Edit Supplier - ${SuplierForEdit.supplier_name}  - ${SuplierForEdit.supplier_company}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SuplierForEdit, id]);

  const saveSuplier = (values) => {
    if (!id) {
      dispatch(actions.createSuplier(values,id)).then(() => backToSupliersList());
    } else {
      dispatch(actions.updateSuplier(values,id)).then(() => backToSupliersList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateSuplierPassword(values,id)).then(() => backToSupliersList());
  };

  const btnRef = useRef();
  const backToSupliersList = () => {
    history.push(`/suplier/suplier`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSupliersList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SuplierEditForm
                actionsLoading={actionsLoading}
                Suplier={SuplierForEdit || initSuplier}
                btnRef={btnRef}
                saveSuplier={saveSuplier}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSuplierId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSuplierId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSupliersList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Suplier remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Suplier specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SuplierCreateForm
                actionsLoading={actionsLoading}
                Suplier={SuplierForEdit || initSuplier}
                btnRef={btnRef}
                saveSuplier={saveSuplier}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSuplierId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSuplierId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
