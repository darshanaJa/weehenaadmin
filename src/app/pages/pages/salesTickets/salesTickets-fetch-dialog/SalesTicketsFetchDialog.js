import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { SalesTicketstatusCssClasses } from "../SalesTicketsUIHelpers";
import { useSalesTicketsUIContext } from "../SalesTicketsUIContext";

const selectedSalesTickets = (entities, ids) => {
  const _SalesTickets = [];
  ids.forEach((id) => {
    const SalesTicket = entities.find((el) => el.id === id);
    if (SalesTicket) {
      _SalesTickets.push(SalesTicket);
    }
  });
  return _SalesTickets;
};

export function SalesTicketsFetchDialog({ show, onHide }) {
  // SalesTickets UI Context
  const saleTicketsUIContext = useSalesTicketsUIContext();
  const salesTicketsUIProps = useMemo(() => {
    return {
      ids: saleTicketsUIContext.ids,
      queryParams: saleTicketsUIContext.queryParams,
    };
  }, [saleTicketsUIContext]);

  // SalesTickets Redux state
  const { SalesTickets } = useSelector(
    (state) => ({
      SalesTickets: selectedSalesTickets(state.SalesTickets.entities, salesTicketsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!salesTicketsUIProps.ids || salesTicketsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [salesTicketsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SalesTickets.map((SalesTicket) => (
              <div className="list-timeline-item mb-3" key={SalesTicket.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SalesTicketstatusCssClasses[SalesTicket.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SalesTicket.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SalesTicket.manufacture}, {SalesTicket.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
