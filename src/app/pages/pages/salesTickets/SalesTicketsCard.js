import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { SalesTicketsFilter } from "./salesTickets-filter/SalesTicketsFilter";
import { SalesTicketsTable } from "./salesTickets-table/SalesTicketsTable";
// import { SalesTicketsGrouping } from "./salesTickets-grouping/SalesTicketsGrouping";
import { useSalesTicketsUIContext } from "./SalesTicketsUIContext";
import { notify } from "../../../config/Toastify";

export function SalesTicketsCard() {
  const saleTicketsUIContext = useSalesTicketsUIContext();
  const salesTicketsUIProps = useMemo(() => {
   
    return {
      ids: saleTicketsUIContext.ids,
      queryParams: saleTicketsUIContext.queryParams,
      setQueryParams: saleTicketsUIContext.setQueryParams,
      newSalesTicketButtonClick: saleTicketsUIContext.newSalesTicketButtonClick,
      openDeleteSalesTicketsDialog: saleTicketsUIContext.openDeleteSalesTicketsDialog,
      openEditSalesTicketPage: saleTicketsUIContext.openEditSalesTicketPage,
      openUpdateSalesTicketsStatusDialog:
        saleTicketsUIContext.openUpdateSalesTicketsStatusDialog,
      openFetchSalesTicketsDialog: saleTicketsUIContext.openFetchSalesTicketsDialog,
    };
  }, [saleTicketsUIContext]);

  return (
    <Card>
      <CardHeader title="Ticket list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={salesTicketsUIProps.newSalesTicketButtonClick}
          >
            New Ticket
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <SalesTicketsFilter />
        {salesTicketsUIProps.ids.length > 0 && (
          <>
            {/* <SalesTicketsGrouping /> */}
          </>
        )}
        <SalesTicketsTable />
      </CardBody>
    </Card>
  );
}
