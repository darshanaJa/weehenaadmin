// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/salesTickets/salesTicketsActions";
import * as uiHelpers from "../SalesTicketsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  // sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useSalesTicketsUIContext } from "../SalesTicketsUIContext";
// import moment from 'moment';

export function SalesTicketsTable() {
  // SalesTickets UI Context
  const saleTicketsUIContext = useSalesTicketsUIContext();
  const salesTicketsUIProps = useMemo(() => {
    return {
      ids: saleTicketsUIContext.ids,
      setIds: saleTicketsUIContext.setIds,
      queryParams: saleTicketsUIContext.queryParams,
      setQueryParams: saleTicketsUIContext.setQueryParams,
      openEditSalesTicketPage: saleTicketsUIContext.openEditSalesTicketPage,
      openDeleteSalesTicketDialog: saleTicketsUIContext.openDeleteSalesTicketDialog,
    };
  }, [saleTicketsUIContext]);

  // Getting curret state of SalesTickets list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.SalesTickets }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // SalesTickets Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    salesTicketsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchSalesTickets(salesTicketsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [salesTicketsUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: "sales_ticket_id",
      text: "ID",
      sort: true,
      // sortCaret: sortCaret,
    },
    {
      // dataField:moment("sales_ticket_start_date").format("yyyy-MM-DD"),
      dataField: "sales_ticket_start_date",
      text: "Start Date",
      sort: true,
      // sortCaret: sortCaret,
    },
    {
      dataField: "sales_ticket_closed_date",
      text: "Closed Date",
      sort: true,
      // sortCaret: sortCaret,
    },
    {
      dataField: "sales_ticket_tot_qnty",
      text: "Quntity",
      sort: true,
      // sortCaret: sortCaret,
    },
    {
      dataField: "slaes_expected_profit",
      text: "Profit",
      sort: true,
      // sortCaret: sortCaret,
    },
    // {
    //   dataField: "price",
    //   text: "Price",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   formatter: columnFormatters.PriceColumnFormatter,
    // },
    // {
    //   dataField: "status",
    //   text: "Status",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "condition",
    //   text: "Condition",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditSalesTicketPage: salesTicketsUIProps.openEditSalesTicketPage,
        openDeleteSalesTicketDialog: salesTicketsUIProps.openDeleteSalesTicketDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: salesTicketsUIProps.queryParams.pageSize,
    page: salesTicketsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  salesTicketsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: salesTicketsUIProps.ids,
                  setIds: salesTicketsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
