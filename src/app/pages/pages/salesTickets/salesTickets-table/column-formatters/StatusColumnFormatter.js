import React from "react";
import {
  SalesTicketstatusCssClasses,
  SalesTicketstatusTitles
} from "../../SalesTicketsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      SalesTicketstatusCssClasses[row.status]
    } label-inline`}
  >
    {SalesTicketstatusTitles[row.status]}
  </span>
);
