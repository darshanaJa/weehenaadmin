import React from "react";
import {
  SalesTicketConditionCssClasses,
  SalesTicketConditionTitles
} from "../../SalesTicketsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        SalesTicketConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        SalesTicketConditionCssClasses[row.condition]
      }`}
    >
      {SalesTicketConditionTitles[row.condition]}
    </span>
  </>
);
