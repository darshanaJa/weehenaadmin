/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/salesTickets/salesTicketsActions";
import { useSalesTicketsUIContext } from "../SalesTicketsUIContext";

export function SalesTicketDeleteDialog({ id, show, onHide }) {
  // SalesTickets UI Context
  const saleTicketsUIContext = useSalesTicketsUIContext();
  const salesTicketsUIProps = useMemo(() => {
    return {
      setIds: saleTicketsUIContext.setIds,
      queryParams: saleTicketsUIContext.queryParams,
    };
  }, [saleTicketsUIContext]);

  // SalesTickets Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SalesTickets.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteSalesTicket = () => {
    // server request for deleting SalesTicket by id
    dispatch(actions.deleteSalesTicket(id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSalesTickets(salesTicketsUIProps.queryParams));
      // clear selections list
      salesTicketsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SalesTicket Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this SalesTicket?</span>
        )}
        {isLoading && <span>SalesTicket is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSalesTicket}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
