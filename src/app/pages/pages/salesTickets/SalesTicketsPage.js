import React from "react";
import { Route } from "react-router-dom";
import { SalesTicketsLoadingDialog } from "./salesTickets-loading-dialog/SalesTicketsLoadingDialog";
import { SalesTicketDeleteDialog } from "./salesTicket-delete-dialog/SalesTicketDeleteDialog";
import { SalesTicketsDeleteDialog } from "./salesTickets-delete-dialog/SalesTicketsDeleteDialog";
import { SalesTicketsFetchDialog } from "./salesTickets-fetch-dialog/SalesTicketsFetchDialog";
import { SalesTicketsUpdateStatusDialog } from "./salesTickets-update-status-dialog/SalesTicketsUpdateStatusDialog";
import { SalesTicketsCard } from "./SalesTicketsCard";
import { SaleTicketsUIProvider } from "./SalesTicketsUIContext";

export function SalesTicketsPage({ history }) {
  const salesTicketsUIEvents = {
    newSalesTicketButtonClick: () => {
      history.push("/sales/tickets/new");
    },
    openEditSalesTicketPage: (id) => {
      history.push(`/sales/tickets/${id}/edit`);
    },
    openDeleteSalesTicketDialog: (id) => {
      history.push(`/sales/tickets/${id}/delete`);
    },
    openDeleteSalesTicketsDialog: () => {
      history.push(`/sales/tickets/deleteSalesTickets`);
    },
    openFetchSalesTicketsDialog: () => {
      history.push(`/sales/tickets/fetch`);
    },
    openUpdateSalesTicketsStatusDialog: () => {
      history.push("/sales/tickets/updateStatus");
    },
  };

  return (
    <SaleTicketsUIProvider salesTicketsUIEvents={salesTicketsUIEvents}>
      <SalesTicketsLoadingDialog />
      <Route path="/sales/tickets/deleteSalesTickets">
        {({ history, match }) => (
          <SalesTicketsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/tickets");
            }}
          />
        )}
      </Route>
      <Route path="/sales/tickets/:id/delete">
        {({ history, match }) => (
          <SalesTicketDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/sales/tickets");
            }}
          />
        )}
      </Route>
      <Route path="/sales/tickets/fetch">
        {({ history, match }) => (
          <SalesTicketsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/tickets");
            }}
          />
        )}
      </Route>
      <Route path="/sales/tickets/updateStatus">
        {({ history, match }) => (
          <SalesTicketsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/tickets");
            }}
          />
        )}
      </Route>
      <SalesTicketsCard />
    </SaleTicketsUIProvider>
  );
}
