/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/salesTickets/salesTicketsActions";
import { useSalesTicketsUIContext } from "../SalesTicketsUIContext";

export function SalesTicketsDeleteDialog({ show, onHide }) {
  // SalesTickets UI Context
  const saleTicketsUIContext = useSalesTicketsUIContext();
  const salesTicketsUIProps = useMemo(() => {
    return {
      ids: saleTicketsUIContext.ids,
      setIds: saleTicketsUIContext.setIds,
      queryParams: saleTicketsUIContext.queryParams,
    };
  }, [saleTicketsUIContext]);

  // SalesTickets Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SalesTickets.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected SalesTickets we should close modal
  useEffect(() => {
    if (!salesTicketsUIProps.ids || salesTicketsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [salesTicketsUIProps.ids]);

  const deleteSalesTickets = () => {
    // server request for deleting SalesTicket by seleted ids
    dispatch(actions.deleteSalesTickets(salesTicketsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSalesTickets(salesTicketsUIProps.queryParams)).then(() => {
        // clear selections list
        salesTicketsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SalesTickets Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected SalesTickets?</span>
        )}
        {isLoading && <span>SalesTickets are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSalesTickets}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
