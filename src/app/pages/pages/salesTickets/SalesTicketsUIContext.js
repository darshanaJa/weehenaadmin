import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./SalesTicketsUIHelpers";

const SalesTicketsUIContext = createContext();

export function useSalesTicketsUIContext() {
  return useContext(SalesTicketsUIContext);
}

export const SaleTicketsUIConsumer = SalesTicketsUIContext.Consumer;

export function SaleTicketsUIProvider({ salesTicketsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newSalesTicketButtonClick: salesTicketsUIEvents.newSalesTicketButtonClick,
    openEditSalesTicketPage: salesTicketsUIEvents.openEditSalesTicketPage,
    openDeleteSalesTicketDialog: salesTicketsUIEvents.openDeleteSalesTicketDialog,
    openDeleteSalesTicketsDialog: salesTicketsUIEvents.openDeleteSalesTicketsDialog,
    openFetchSalesTicketsDialog: salesTicketsUIEvents.openFetchSalesTicketsDialog,
    openUpdateSalesTicketsStatusDialog:
      salesTicketsUIEvents.openUpdateSalesTicketsStatusDialog,
  };

  return (
    <SalesTicketsUIContext.Provider value={value}>
      {children}
    </SalesTicketsUIContext.Provider>
  );
}
