// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input,Input1, Select } from "../../../../../_metronic/_partials/controls";
import { SalesTicketForm } from './SalesTickeForm';
import { SaleTicketsComplete } from './SaleTicketsComplete';
// import { SalesList } from './SalesList';
import { salesTicketsSlice } from "../../../_redux/salesTickets/salesTicketsSlice";
// import moment from 'moment';
import { AGENTS_GET_URL } from "../../../_redux/agent/agentsCrud";
import { useDispatch,useSelector } from "react-redux";
import axios from 'axios'
// import { notifyWarning } from "../../../../config/Toastify";

// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   SalesTicketstatusTitles,
//   SalesTicketConditionTitles,
// } from "../SalesTicketsUIHelpers";

// Validation schema
const SalesTicketEditSchema = Yup.object().shape({
  stat: Yup.string()
    .required("Stat is required"),
  // // sales_ticket_tot_qnty: Yup.number()
  // // .required("Total Quantity is required")
  // // .min(2, "Minimum 2 symbols"),
  // slaes_expected_profit: Yup.number()
  // .required("Profit is required")
  // .min(2, "Minimum 2 symbols"),
  sales_ticket_start_date:Yup.date()
  .required("date is required"),
  sales_ticket_closed_date:Yup.date()
  .required("date is required"),
  // // agntSalesAgentIdYup:Yup.string()
  // // .required("Agnet Name is required"),
  quantity: Yup.number()
  .required("Quantity is required")
  .min(2, "Minimum 2 symbols"),
  // vehicId: Yup.number()
  // .required("Vechicale Id is required")
  // .min(2, "Minimum 2 symbols"),
  // vehic_before_weight: Yup.number()
  // .required("Vechical before weight is required")
  // .min(2, "Minimum 2 symbols"),
  // vehic_return_weight: Yup.number()
  // .required("Vechical return weight is required")
  // .min(2, "Minimum 2 symbols"),
  // vehic_after_weight: Yup.number()
  // .required("Vechical After weight is required")
  // .min(2, "Minimum 2 symbols"),
  driverid: Yup.number()
  .required("Driver Id is required")
  .min(2, "Minimum 2 symbols"),
  helperId: Yup.number()
  .required("Helper Id is required")
  .min(2, "Minimum 2 symbols"),
  dispatchempid: Yup.number()
  .required("Dispathcment Id is required")
  .min(2, "Minimum 2 symbols"),
  returncheckempid: Yup.string()
  .required("Return Check Id is required")
  .min(2, "Minimum 2 symbols")

});

export function SalesTicketCreateForm({
  SalesTicket,
  btnRef,
  saveSalesTicket,
  // todos,
  // setTodos,
  // setInputStat,
  // inputStat,
  inputText,
  setInputText,
  // designation,
  driver,
  Helper,
  Dispatch,
  Return,
  setInputCloesdDate,
  setInputStatslaes_expected_profit,
  setInputStatquantity,
  setInputStat2,
  setToViewDate2,
  setInputagntSalesAgentId
}) {

  const {actions} = salesTicketsSlice;
  


  // const[inputText, setInputText] = useState('');
  // const[todos,setTodos] = useState([]);

  const[toViewDate, setToViewDate] = useState(Yup.date);
  const[agent, setAgent] = useState([]);

  const[veId, setVeId] = useState();

  // const[sale2, setSale] = useState(sale);

  const sale = useSelector(state => state.SalesTickets.totalQuaniy)

  const dispatch = useDispatch();
  // setSale(setToViewDate);

  console.log(veId)
  console.log(setToViewDate)

  // console.log(setToViewDate)
  // const openDateHandler = (e) =>{
  //   setToViewDate(moment(e.target.value).format("yyyy-MM-DD"))
  // }

  console.log(toViewDate)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: AGENTS_GET_URL
      })
      .then((res) => {
        setAgent(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
  
      
  },[])

  console.log(agent)

  // const inputStatHandler =(e)=>{
  //   console.log(e.target.value)
  //   setInputStat(e.target.value)
  // }
  // console.log(toViewDate)

  // useEffect(()=>{
    
   
      
      
  // },[])

  

  console.log(driver)

  return (
    <>
      {/* {notifyWarning()} */}
      <Formik
        enableReinitialize={true}
        initialValues={SalesTicket}
        // validationSchema={SalesTicketEditSchema}
        
        onSubmit={(values) => {
          saveSalesTicket(values);
          console.log(values)

          dispatch(actions.EmptySalesTickeet())

        }}
      >
        {({ handleSubmit,setFieldValue }) => (
          <>
          {/* {notify} */}
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="date"
                    name="sales_ticket_start_date"
                    // value={toViewDate}
                    // onChange={openDateHandler}
                    component={Input}
                    placeholder="Start Date"
                    label="Start Date"
                    onChange={(e) => {setToViewDate2(e.target.value)}}
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="number"
                    name="quantity"
                    component={Input}
                    placeholder="Quantity"
                    label="Quantity"
                    onChange={(e) => {setInputStatquantity(e.target.value)}}
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="slaes_expected_profit"
                    component={Input}
                    placeholder="Expected Profit"
                    label="Expected Profit"
                    onChange={(e) => {setInputStatslaes_expected_profit(e.target.value)}}
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-3">
                    <Select name="stat" label="Stat"
                    onChange={(e) => {setInputStat2(e.target.value)}}> 
                        <option>Choose One</option>
                        <option value="1">draft</option>
                        <option value="2">pending</option>
                        <option value="3">started</option>
                        <option value="4">closed</option>
                      </Select>
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="date"
                      name="sales_ticket_closed_date"
                      component={Input}
                      placeholder="Closed Date"
                      label="Closed Date"
                      onChange={(e) => {setInputCloesdDate(e.target.value)}}
                    />
                  </div>
                  <div className="col-lg-3">
                  <Select name="agntSalesAgentId" label="Agents Name"
                  onChange={(e) => {setInputagntSalesAgentId(e.target.value)}}>
                    <option>Choose One</option>
                      {agent.map((item) => (
                        <option value={item.sales_agent_id} >
                          {item.sales_agent_fname}
                        </option>    
                      ))}
                  </Select>  
                  </div>
                  <div className="col-lg-3">
                    {/* <p>Total Quanit</p> */}
                    <Field
                      type="number"
                      name="sales_ticket_tot_qnty"
                      component={Input1}
                      placeholder="Total Quantity"
                      value={sale}
                      label="Total Quantity"
                      readonly="readonly"
                    />
                  </div>
              </div>
              <br />
              <p><b>Add Stock List</b></p>
                {/* <SalesList setTodos={setTodos} todos={todos} /> */}
                <SaleTicketsComplete />
                <SalesTicketForm inputText={inputText} setInputText={setInputText}/>
              <br />  
              <p><b>Add Employee Details</b></p>  
              <div className="form-group row">
              <div className="col-lg-3">
                <Select name="driverid" label="Driver ID" onKeyUp={(e)=>setVeId(e.target.value)}>
                    <option>Choose One</option>
                    {driver.map((item) => (
                      <option value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
                <div className="col-lg-3">
                <Select name="helperId" label="Helper ID" onKeyUp={(e)=>setVeId(e.target.value)}>
                    <option>Choose One</option>
                    {Helper.map((item) => (
                      <option value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
                <div className="col-lg-3">
                <Select name="dispatchempid" label="Dispatch ID" onKeyUp={(e)=>setVeId(e.target.value)}>
                    <option>Choose One</option>
                    {Dispatch.map((item) => (
                      <option value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
                <div className="col-lg-3">
                <Select name="returncheckempid" label="Return Chek ID" onKeyUp={(e)=>setVeId(e.target.value)}>
                    <option>Choose One</option>
                    {Return.map((item) => (
                      <option key={item.id} value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
              </div>  
              <div className="form-group row">
              {/* <div className="col-lg-3 + dwnbField" >
                  <Field
                    type="text"
                    name="vehicId"
                    component={Input}
                    placeholder="Vechicale ID"
                    label="Vechicale ID"
                  />
                </div> */}
              <div className="col-lg-4">
                  <Field
                    type="number"
                    name="vehic_before_weight"
                    component={Input}
                    placeholder="Vechicle Before Weight"
                    label="Vechicle Before Weight"
                  />
                </div>
                
                <div className="col-lg-4">
                  <Field
                    type="number"
                    name="vehic_after_weight"
                    component={Input}
                    placeholder="Vechicle After Weight"
                    label="Vechicle After Weight"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="number"
                    name="vehic_return_weight"
                    component={Input}
                    placeholder="Vechicle Return Weight"
                    label="Vechicle Return Weight"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2"> Save</button>
                </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
