// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select, Input1 } from "../../../../../_metronic/_partials/controls";
import axios from "axios";
import {SalesTickets_URL_GETBYID } from "../../../_redux/salesTickets/salesTicketsCrud";
// import { SalesTicketForm } from './SalesTickeForm';
// import { SalesList } from './SalesList';
import moment from 'moment';
import { AGENTS_GET_URL } from "../../../_redux/agent/agentsCrud";
import Admin from "../../../../config/Admin";
// import { Dropdown } from 'semantic-ui-react'

// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   SalesTicketstatusTitles,
//   SalesTicketConditionTitles,
// } from "../SalesTicketsUIHelpers";

// Validation schema
const SalesTicketEditSchema = Yup.object().shape({
  stat: Yup.string()
    .required("Stat is required"),
  sales_ticket_tot_qnty: Yup.number()
  .required("Total Quantity is required")
  .min(2, "Minimum 2 symbols"),
  // slaes_expected_profit: Yup.number()
  // .required("Profit is required")
  // .min(2, "Minimum 2 symbols"),
  sales_ticket_start_date:Yup.date()
  .required("date is required"),
  sales_ticket_closed_date:Yup.date()
  .required("date is required"),
  // agntSalesAgentIdYup:Yup.string()
  // .required("Agnet Name is required"),
  quantity: Yup.number()
  .required("Quantity is required")
  .min(2, "Minimum 2 symbols"),
  // vehicId: Yup.number()
  // .required("Vechicale Id is required")
  // .min(2, "Minimum 2 symbols"),
  // vehic_before_weight: Yup.number()
  // .required("Vechical before weight is required")
  // .min(2, "Minimum 2 symbols"),
  // vehic_return_weight: Yup.number()
  // .required("Vechical return weight is required")
  // .min(2, "Minimum 2 symbols"),
  // vehic_after_weight: Yup.number()
  // .required("Vechical After weight is required")
  // .min(2, "Minimum 2 symbols"),
  driverid: Yup.number()
  .required("Driver Id is required")
  .min(2, "Minimum 2 symbols"),
  helperId: Yup.number()
  .required("Helper Id is required")
  .min(2, "Minimum 2 symbols"),
  dispatchempid: Yup.number()
  .required("Dispathcment Id is required")
  .min(2, "Minimum 2 symbols"),
  returncheckempid: Yup.number()
  .required("Return Check Id is required")
  .min(2, "Minimum 2 symbols")

});

export function SalesTicketEditForm({
  SalesTicket,
  btnRef,
  saveSalesTicket,
  todos,
  setTodos,
  id,
  setInputvehicId,
  driver,
  Helper,
  Dispatch,
  Return,
  adminId,
  createDate,
  updateDate
}) {

  // const[toView, setToView] = useState([]);
  const[sales_ticket_start_date, setToViewDate] = useState();
  const[agent, setAgent] = useState([]);
  const[agentName, setAgentName] = useState();
  const[agentId, setAgentId] = useState();

  // const[veId, setVeId] = useState();

  console.log(sales_ticket_start_date)
  console.log(agentName)
  console.log(agentId)
  // console.log(veId)

  // const openDateHandler = (event) =>{
  //   setToViewDate(event.target.value)
  // }

  // console.log(agent)

  // const countryOptions = (

  //   agent.map((item) => ({
  //      value: item.sales_agent_fname,  key: item.sales_agent_fname, text: item.sales_agent_fname ,
  //       // console.log({value: item.sales_agent_fname,text:item.sales_agent_fname})
  //   }))
  

  //     // console.log({ value: 'ax', text: 'Aland Islands' }),
  //     // { value: 'al', text: 'Albania' },
  //     // { value: 'dz', text: 'Algeria' },       
  //     //         [item.sales_agent_fname,item.sales_agent_fname]
  //     //                       )),
  // )
  //   // shop.map((item) => (
  //   //   [item.SalesTicketEntity_stat,item.sales_ticket_stats_count]
  //   // )),

  //   // { key: 'af', value: 'af', flag: 'af', text: 'Afghanistan' },
  //   // { key: 'ax', value: 'ax', flag: 'ax', text: 'Aland Islands' },
  //   // { key: 'al', value: 'al', flag: 'al', text: 'Albania' },


  // const DropdownExampleSearchSelection = () => (
  //   <Dropdown 
  //   // className="col-lg-3"
  //   // component={Input}
  //   // value="2021-03-18"
  //   // onChange={openDateHandler}
  //   placeholder="Start Date"
  //   // label="Start Date"
  //     fluid
  //     search
  //     selection
  //     options={countryOptions}
  //     // value={value}
  //     // name="abcd"
  //     // onKeyUp={abcd}
  //     onChange={(e, { value }) => {
  //       console.log(e.target.textContent);
  //     }}
  //     // defaultValue={this.state.selected}
  //   />
  // );

  // const abcd =(e)=>{
  //   console.log(e.target.value)
  // }
  
  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: SalesTickets_URL_GETBYID +`/${id}`
      })
      .then((res) => {
        setAgentName(res.data.slsagnt.sales_agent_fname)
        setAgentId(res.data.slsagnt.sales_agent_id)
        // setToView(moment(res.data.sales_ticket_closed_date).format("yyyy-MM-DD"))


        setToViewDate(moment(res.data.sales_ticket_start_date).format("yyyy-MM-DD"))
        console.log("#####################")
        // setToViewDate(res.data.sales_ticket_start_date)
      })
      .catch(function (response) {
          // console.log(response);
      });
  
      
  },[id])

  // useEffect(()=>{
  //   axios({
  //     method: 'get',
  //     baseURL: AGENTS_GET_ID_URL
  //     })
  //     .then((res) => {
  //       setAgentName(res.data.data)
  //     })
  //     .catch(function (response) {
  //         // console.log(response);
  //     });
  
      
  // },[])

  // console.log(agentName)
  // console.log(agentId)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: AGENTS_GET_URL
      })
      .then((res) => {
        setAgent(res.data)
        console.log(res.data);
      })
      .catch(function (response) {
          // console.log(response);
      });
  
      
  },[])

  // const[inputText, setInputText] = useState('');
  // const[todos,setTodos] = useState([]);

  // console.log(sales_ticket_start_date)
 

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={SalesTicket}
        validationSchema={SalesTicketEditSchema}
        onSubmit={(values) => {
          saveSalesTicket(values);
          console.log(values)
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="date"
                    name="sales_ticket_start_date"
                    component={Input}
                    // value="2021-03-18"
                    // onChange={openDateHandler}
                    placeholder="Start Date"
                    label="Start Date"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="number"
                    name="quantity"
                    component={Input}
                    placeholder="Quantity"
                    label="Quantity"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="slaes_expected_profit"
                    component={Input}
                    placeholder="Expected Profit"
                    label="Expected Profit"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-3">
                    <Select name="stat" label="Stat"> 
                        <option>Choose One</option>
                        <option value="1">draft</option>
                        <option value="2">pending</option>
                        <option value="3">started</option>
                        <option value="4">closed</option>
                      </Select>
                  </div>
                <div className="col-lg-3">
                    <Field
                      type="date"
                      name="sales_ticket_closed_date"
                      // value="2021-03-18"
                      component={Input}
                      // value={sales_ticket_start_date}
                      placeholder="Closed Date"
                      label="Closed Date"
                    />
                  </div>
                  <div className="col-lg-3">
                  <Select name="agntSalesAgentId" label="Agents Name">
                    {/* <option >Choose One</option> */}
                      {agent.map((item) => (
                        <option Key={item.sales_agent_id} value={item.sales_agent_id} >
                          {item.sales_agent_fname}
                        </option> 
                      )
                      )}
                  </Select>  
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="number"
                      name="sales_ticket_tot_qnty"
                      component={Input1}
                      placeholder="Total Quantity"
                      label="Total Quantity"
                      readOnly="readOnly"
                    />
                  </div>
              </div>
              <br />
              {/* <p><b>Edit Stock List</b></p>
              {toView.map((item) => (
                     <div className="form-group row">
                     <div className="col-lg-4">
                         <Field
                           type="text"
                           name="driverid"
                           value={item.sales_ticket_item_id}
                           component={Input}
                           placeholder="Stock ID"
                           label="Stock ID"
                         />
                       </div>
                       
                       <div className="col-lg-4">
                        <Field
                          type="text"
                          name="sticketitem.ticket_item_opening_quantity"
                          // value={item.ticket_item_opening_quantity}
                          component={Input}
                          placeholder="Opening Quntity"
                          label="Opening Quntity"
                          // onChange={quntityHandler}
                        />
                      </div>
                      <div className="col-lg-4">
                        <Field
                          type="text"
                          name=""
                          value={item.ticket_item_price}
                          component={Input}
                          placeholder="Item Price"
                          label="Item Price"
                          // onChange={priceHandler}
                        />
                      </div>
                     </div>
              ))}
              <br /> */}
              <p><b>Add Employee Details</b></p>  
              <div className="form-group row">
              <div className="col-lg-3">
                <Select name="driverid" label="Driver ID" 
                // onKeyUp={(e)=>setVeId(e.target.value)}
                >
                    <option>Choose One</option>
                    {driver.map((item) => (
                      <option value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
                <div className="col-lg-3">
                <Select name="helperId" label="Helper ID" 
                // onKeyUp={(e)=>setVeId(e.target.value)}
                >
                    {/* <option>Choose One</option> */}
                    {Helper.map((item) => (
                      <option value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
                <div className="col-lg-3">
                <Select name="dispatchempid" label="Dispatch ID" 
                // onKeyUp={(e)=>setVeId(e.target.value)}
                >
                    {/* <option>Choose One</option> */}
                    {Dispatch.map((item) => (
                      <option value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
                <div className="col-lg-3">
                <Select name="returncheckempid" label="Return Chek ID" 
                // onKeyUp={(e)=>setVeId(e.target.value)}
                >
                    {/* <option>Choose One</option> */}
                    {Return.map((item) => (
                      <option value={item.id}>{item.name}</option>
                      
                    ))}
                  </Select>
                </div>
              </div>  
              <div className="form-group row">
              {/* <div className="col-lg-3 + dwnbField" >
                  <Field
                    type="text"
                    name="vehicId"
                    component={Input}
                    placeholder="Vechicale ID"
                    label="Vechicale ID"
                  />
                </div> */}
              <div className="col-lg-4">
                  <Field
                    type="number"
                    name="vehic_before_weight"
                    component={Input}
                    placeholder="Vechicle Before Weight"
                    label="Vechicle Before Weight"
                  />
                </div>
                
                <div className="col-lg-4">
                  <Field
                    type="number"
                    name="vehic_after_weight"
                    component={Input}
                    placeholder="Vechicle After Weight"
                    label="Vechicle After Weight"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="number"
                    name="vehic_return_weight"
                    component={Input}
                    placeholder="Vechicle Return Weight"
                    label="Vechicle Return Weight"
                  />
                </div>
              </div>
                {/* <Dropdown
                  placeholder="Select Country"
                  fluid
                  search
                  selection
                  options={countryOptions}
              /> */}

            {/* {DropdownExampleSearchSelection()} */}

                {/* <SelectSearch options={options} value="sv" name="language" placeholder="Choose your language" /> */}
              {/* </div> */}
                {/* <SalesTicketForm inputText={inputText} setInputText={setInputText} todos={todos} setTodos={setTodos}/>
                <SalesList setTodos={setTodos} todos={todos} /> */}
{/* <label htmlFor="country">Country</label>
<Dropdown
    name="country"
    placeholder="Select Country"
    fluid
    search
    selection
    options={countryOptions}
    onChange={(e, { value }) => {
      setInputvehicId(e.target.textContent);
    }}
  /> */}

              <div className="form-group row">
                <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2"> Save</button>
                </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={adminId}
                  createDate={createDate} 
                  updateDate={updateDate}
                />

            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
