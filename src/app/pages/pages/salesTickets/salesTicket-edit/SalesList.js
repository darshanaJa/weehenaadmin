// // Form is based on Formik
// // Data validation is based on Yup
// // Please, be familiar with article first:
// // https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
// import React from 'react';
// import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
// import { Input, Select } from "../../../../../_metronic/_partials/controls";
// // import { SalesTicketForm } from './SalesTickeForm';
// // import { SalesList } from './SalesList';

// // import {
// //   AVAILABLE_COLORS,
// //   AVAILABLE_MANUFACTURES,
// //   SalesTicketstatusTitles,
// //   SalesTicketConditionTitles,
// // } from "../SalesTicketsUIHelpers";

// // Validation schema
// const SalesTicketEditSchema = Yup.object().shape({
//   // sales_ticket_closed_date: Yup.date()
//   //   .required("Closed Date is required"),
//   //   sales_ticket_tot_qnty: Yup.number()
//   //   .required("Quantity is required"),
//   //   slaes_expected_profit: Yup.number()
//   //   .required("Profit is required"),
//   // mileage: Yup.number()
//   //   .min(0, "0 is minimum")
//   //   .max(1000000, "1000000 is maximum")
//   //   .required("Mileage is required"),
//   // color: Yup.string().required("Color is required"),
//   // price: Yup.number()
//   //   .min(1, "$1 is minimum")
//   //   .max(1000000, "$1000000 is maximum")
//   //   .required("Price is required"),
//   // VINCode: Yup.string().required("VINCode is required"),
// });

// export function SalesList({
//   SalesTicket,
//   btnRef,
//   saveSalesTicket,
//   todos,
//   setTodos
// }) {

//   // const[inputText, setInputText] = useState('');
//   // const[todos,setTodos] = useState([]);

//   console.log(todos)
 

//   return (
//     <>
//       <Formik
//         enableReinitialize={true}
//         initialValues={SalesTicket}
//         validationSchema={SalesTicketEditSchema}
//         onSubmit={(values) => {
//           saveSalesTicket(values);
//           console.log(values)
//         }}
//       >
//         {({ handleSubmit }) => (
//           <>
//             <Form className="form form-label-right">
//               <div className="form-group row">
//                 <div className="col-lg-3">
//                   <Field
//                     type="date"
//                     name="sales_ticket_closed_date"
//                     component={Input}
//                     placeholder="Closed Date"
//                     label="Closed Date"
//                   />
//                 </div>
                
//                 <div className="col-lg-3">
//                   <Field
//                     type="number"
//                     name="sales_ticket_tot_qnty"
//                     component={Input}
//                     placeholder="Quantity"
//                     label="Quantity"
//                   />
//                 </div>
//                 <div className="col-lg-3">
//                   <Field
//                     type="text"
//                     name="slaes_expected_profit"
//                     component={Input}
//                     placeholder="Profit"
//                     label="Profit"
//                   />
//                 </div>
//                 <div className="col-lg-3">
//                     <Select name="stat" label="Stat">
//                       <option>Choose One</option>
//                       <option>1</option>
//                       <option>2</option>
//                       <option>3</option>
//                     </Select>
//                   </div>
//               </div>
//                 {/* <SalesTicketForm inputText={inputText} setInputText={setInputText} todos={todos} setTodos={setTodos}/>
//                 <SalesList setTodos={setTodos} todos={todos} /> */}
//               <div className="form-group row">
//                 <div className="col-lg-4">
//                   <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
//                 </div>
//               </div>
//               <button
//                 type="submit"
//                 style={{ display: "none" }}
//                 ref={btnRef}
//                 onSubmit={() => handleSubmit()}
//               ></button>
//             </Form>
//           </>
//         )}
//       </Formik>
//     </>
//   );
// }














// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { SaleTicketsComplete } from './SaleTicketsComplete';


// Validation schema
// const SalesTicketEditSchema = Yup.object().shape({
//   sales_ticket_closed_date: Yup.string()
//     .required("Closed Date is required"),
//     sales_ticket_tot_qnty: Yup.number()
//     .required("Quantity is required"),
//     slaes_expected_profit: Yup.number()
//     .required("Profit is required"),
//   mileage: Yup.number()
//     .min(0, "0 is minimum")
//     .max(1000000, "1000000 is maximum")
//     .required("Mileage is required"),
//   color: Yup.string().required("Color is required"),
//   price: Yup.number()
//     .min(1, "$1 is minimum")
//     .max(1000000, "$1000000 is maximum")
//     .required("Price is required"),
//   VINCode: Yup.string().required("VINCode is required"),
// });

export function SalesList({todos,setTodos}) {

  return (
    <div>
      {
        todos.map(todo => (
          <SaleTicketsComplete todos={todos} setTodos={setTodos}  todo={todo} text={todo.mstock} quntity={todo.ticket_item_opening_quantity} price={todo.ticket_item_price} />
          // setTotal(todo.ticket_item_opening_quantity)
        ))
      }
    </div>
  );
}
