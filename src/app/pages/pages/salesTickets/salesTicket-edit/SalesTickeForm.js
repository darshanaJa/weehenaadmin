import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../_metronic/_partials/controls";
import axios from "axios";
import { MainStocks_URL_GET,MainStocks_URL_GETBYID } from "../../../_redux/MainStock/MainStocksCrud";
import { notifyWarning } from "../../../../config/Toastify";
import { salesTicketsSlice } from "../../../_redux/salesTickets/salesTicketsSlice";
import { useDispatch,useSelector } from "react-redux";

// Validation schema
const SalesTicketEditSchema = Yup.object().shape({
});

export function SalesTicketForm({
  SalesTicket,
  btnRef,
  saveSalesTicket,
  todos,
  setInputText,
  setTodos,
  inputText,
}) {

  const {actions} = salesTicketsSlice;
  const dispatch = useDispatch();

  console.log(todos)

  const[idQuntity, idSetQuntity] = useState();
  const[quntity, setQuntity] = useState(0);
  const[price, setPrice] = useState('');

  const sale = useSelector(state => state.SalesTickets.salesItems)
  console.log(sale);

  const inputTextHandler =(e)=>{
      console.log(e.target.id)
      setInputText(e.target.value)
      

    }

const quntityHandler =(e)=>{
  idSetQuntity(e.target.value)
  if(e.target.value>quntity){
    notifyWarning("Not enough stock")
    idSetQuntity(quntity)
  }
}

const priceHandler =(e)=>{
  setPrice(e.target.value)
}


const submitTodoHandler = (e) =>{

    if(inputText==='' || inputText==='Choose One'){
      notifyWarning("please choose the Item Name")
      e.preventDefault();
    }
    else{
      if(sale !== null){
        const newItem = sale.find(entity => entity.mstock===inputText)
        // return (notifyWarning("Already Taken Email"));
        if(newItem){
          return (notifyWarning("Already added item"));
        }
      else{
        console.log(inputText)

        e.preventDefault();

        dispatch(actions.SalesTicketItemCrated({
          mstock:inputText,
          ticket_item_opening_quantity:parseInt(idQuntity),
          ticket_item_price:parseInt(price)
        }))
      }}

      setInputText('');
      idSetQuntity('');
      setPrice('');
      console.log(todos)
    }

}

const [shop,setShop]=useState([]);


useEffect(()=>{
  axios({
    method: 'get',
    baseURL: MainStocks_URL_GET
    })
    .then((res) => {
      setShop(res.data.data)
    })
    .catch(function (response) {
        // console.log(response);
    });

    
},[])

useEffect(()=>{
  axios({
    method: 'get',
    baseURL: `${MainStocks_URL_GETBYID}/${inputText}`
    })
    .then((res) => {
      console.log("############")
      idSetQuntity(res.data.item_current_quantity)
      setQuntity(res.data.item_current_quantity)
      setPrice(res.data.item_default_price)
    })
    .catch(function (response) {
        // console.log(response);
    });
    
},[inputText])
 

  return (
    <>
    {notifyWarning()}
      <Formik
        enableReinitialize={true}
        initialValues={SalesTicket}
        validationSchema={SalesTicketEditSchema}
        onSubmit={(values) => {
          saveSalesTicket(values);
          console.log(values)
        }}
      >
        {({ handleSubmit }) => (
          <>
          <Form onSubmit={submitTodoHandler} className="form form-label-right">
          <div className="form-group row">
          <div className="col-lg-3">
            <Select name="mstockItemId" label="StockName" value={inputText} onChange={inputTextHandler}>
              <option>Choose One</option>
                {shop.map((item) => (
                  <option id={item.item_name} value={item.item_id} >
                    {item.item_name}
                  </option>    
              ))}
            </Select>
                </div>
                <div className="col-lg-3">
                  <Field
                    type="number"
                    name="sales_ticket_closed_date"
                    value={idQuntity}
                    component={Input}
                    placeholder="Opening Quntity"
                    label="Opening Quntity"
                    onChange={quntityHandler}
                  />
                </div>
                <div className="col-lg-3">
                  <Field
                    type="number"
                    name="sales_ticket_closed_date"
                    value={price}
                    component={Input}
                    placeholder="Item Price"
                    label="Item Price"
                    onChange={priceHandler}
                  />
                </div>
                <div className="col-lg-3">
                  <button type="button" hidden={!(idQuntity && price)} onClick={submitTodoHandler} className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                </div>
              </div>
            </Form>
          </>
        )}
      </Formik>
    </>
  );

}