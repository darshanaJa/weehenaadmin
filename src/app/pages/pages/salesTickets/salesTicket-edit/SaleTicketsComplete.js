// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
// import { Input, Select } from "../../../../../_metronic/_partials/controls";
import { Input1} from "../../../../../_metronic/_partials/controls";
import { MainStocks_URL_GETBYID } from "../../../_redux/MainStock/MainStocksCrud";
import axios from "axios";
import { useDispatch,useSelector } from "react-redux";
import { salesTicketsSlice } from "../../../_redux/salesTickets/salesTicketsSlice";
// import { SalesTicketForm } from './SalesTickeForm';
// import { SalesList } from './SalesList';

// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   SalesTicketstatusTitles,
//   SalesTicketConditionTitles,
// } from "../SalesTicketsUIHelpers";

// Validation schema
const SalesTicketEditSchema = Yup.object().shape({
  // sales_ticket_closed_date: Yup.date()
  //   .required("Closed Date is required"),
  //   sales_ticket_tot_qnty: Yup.number()
  //   .required("Quantity is required"),
  //   slaes_expected_profit: Yup.number()
  //   .required("Profit is required"),
  // mileage: Yup.number()
  //   .min(0, "0 is minimum")
  //   .max(1000000, "1000000 is maximum")
  //   .required("Mileage is required"),
  // color: Yup.string().required("Color is required"),
  // price: Yup.number()
  //   .min(1, "$1 is minimum")
  //   .max(1000000, "$1000000 is maximum")
  //   .required("Price is required"),
  // VINCode: Yup.string().required("VINCode is required"),
});


export function SaleTicketsComplete({
  SalesTicket,
  btnRef,
  saveSalesTicket,
  todos,
  text,
  quntity,
  price,
  todo,
  setTodos
}) {

  const {actions} = salesTicketsSlice;

  const sale = useSelector(state => state.SalesTickets.salesItems)
  console.log(sale);

  const dispatch = useDispatch();

  // const [total,setTotal] =useState(0)
  // const [Ttotal,TsetTotal] =useState()

  // setTotal(total+quntity)
  // setTotal(total+1)
  // console.log(total)

  // console.log(Ttotal)

  const [items,setItems]=useState();

  // console.log(
  //   [1, 2, 3, 4].reduce((a, b) => a + b, 0)
  // )

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GETBYID +`/${text}`
      })
      .then((res) => {
        setItems(res.data.item_name)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[text])

  // const[inputText, setInputText] = useState('');
  // const[todos,setTodos] = useState([]);

  // console.log(todos)

  // const deleteHandler =(id)=>{
  //   dispatch(actions.RemvesalesTickeItem(id))
  //   // setTodos(todos.filter((el) => el.mstock !== text))
  //   // console.log(text)

  // }

 

  console.log(items)
 

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={SalesTicket}
        validationSchema={SalesTicketEditSchema}
        onSubmit={(values) => {
          saveSalesTicket(values);
          console.log(values)
        }}
      >
        {({ handleSubmit }) => (
          <>
          <Form className="form form-label-right">

          {sale.map((item) => (
            <div className="form-group row">
            <div className="col-lg-3">
              <Field
                type="text"
                // name="sales_ticket_closed_date"
                value={item.mstock}
                component={Input1}
                placeholder="Stock Name"
                // label="Closed Date"
                // onChange={inputTextHandler}
              />
            </div>
            <div className="col-lg-3">
              <Field
                type="text"
                value={item.ticket_item_opening_quantity}
                // name="sales_ticket_closed_date"
                // value={inputText}
                component={Input1}
                placeholder="Opening Quntity"
                // label="Closed Date"
                // onChange={inputTextHandler}
              />
            </div>
            <div className="col-lg-3">
              <Field
                type="text"
                value={item.ticket_item_price}
                // name="sales_ticket_closed_date"
                // value={inputText}
                component={Input1}
                placeholder=" Item Price"
                // label="Closed Date"
                // onChange={inputTextHandler}
              />
            </div>
            <div className="col-lg-3">
                <button type="button" onClick={()=> {dispatch(actions.RemvesalesTickeItem(item.mstock))}} className="btnform" ><i class="fa fa-trash"></i></button> 
            </div>
          </div>

          ))}
          
        </Form>
          </>
        )}
      </Formik>
    </>
  );
}


















// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
// import React from 'react';
// import { Form, Field } from "formik";
// import * as Yup from "yup";
// import { Input1} from "../../../../../_metronic/_partials/controls";
// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   SalesTicketstatusTitles,
//   SalesTicketConditionTitles,
// } from "../SalesTicketsUIHelpers";

// Validation schema
// const SalesTicketEditSchema = Yup.object().shape({
  // sales_ticket_closed_date: Yup.string()
  //   .required("Closed Date is required"),
  //   sales_ticket_tot_qnty: Yup.number()
  //   .required("Quantity is required"),
  //   slaes_expected_profit: Yup.number()
  //   .required("Profit is required"),
  // mileage: Yup.number()
  //   .min(0, "0 is minimum")
  //   .max(1000000, "1000000 is maximum")
  //   .required("Mileage is required"),
  // color: Yup.string().required("Color is required"),
  // price: Yup.number()
  //   .min(1, "$1 is minimum")
  //   .max(1000000, "$1000000 is maximum")
  //   .required("Price is required"),
  // VINCode: Yup.string().required("VINCode is required"),
// });

// export function SaleTicketsComplete({
// //   setInputText,
// //   todos,
// //   setTodos,
// //   inputText
//      text,
//      quntity,
//      price,
//      todo,
//      todos,
//      setTodos
// }) {

//   const deleteHandler =()=>{
//     setTodos(todos.filter((el) => el.mstock !== text))
//     console.log(text)

//   }

//   return (
//     <>
//       <Form className="form form-label-right">
//         <div className="form-group row">
//           <div className="col-lg-3">
//             <Field
//               type="text"
//               // name="sales_ticket_closed_date"
//               value={text}
//               component={Input1}
//               placeholder="Item Name"
//               // label="Closed Date"
//               // onChange={inputTextHandler}
//             />
//           </div>
//           <div className="col-lg-3">
//             <Field
//               type="text"
//               value={quntity}
//               // name="sales_ticket_closed_date"
//               // value={inputText}
//               component={Input1}
//               placeholder="Opening Quntity"
//               // label="Closed Date"
//               // onChange={inputTextHandler}
//             />
//           </div>
//           <div className="col-lg-3">
//             <Field
//               type="text"
//               value={price}
//               // name="sales_ticket_closed_date"
//               // value={inputText}
//               component={Input1}
//               placeholder=" Item Price"
//               // label="Closed Date"
//               // onChange={inputTextHandler}
//             />
//           </div>
//           <div className="col-lg-3">
//               <button type="button" onClick={deleteHandler} className="btnform" ><i class="fa fa-trash"></i></button> 
//           </div>
//         </div>
//     </Form>

      
//     </>
//   );
// }
