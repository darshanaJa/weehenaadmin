/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/salesTickets/salesTicketsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { SalesTicketEditForm } from "./SalesTicketEditForm";
import { SalesTicketCreateForm } from "./SalesTicketCreateForm";
// import { SalesTicketForm } from "./SalesTickeForm";
import { Specifications } from "../salesTicket-specifications/Specifications";
import { SpecificationsUIProvider } from "../salesTicket-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { ItemsUIProvider } from "../salesTicket-Items/ItemsUIContext";
import { Items } from "../salesTicket-Items/Items";
// import { SalesTicketForm } from "./SalesTickeForm";
// import { SalesList } from "./SalesList";
// import { SaleTicketsComplete } from "./SaleTicketsComplete";
import { SpecificationEditDialog } from "../salesTicket-specifications/specification-edit-dialog/SpecificationEditDialog";
import { notify } from "../../../../config/Toastify";
import moment from 'moment';
import {SalesTickets_URL_GETBYID } from "../../../_redux/salesTickets/salesTicketsCrud";
import axios from "axios";
import { Employees_URL } from "../../../_redux/Employee/EmployeesCrud";


export function SalesTicketEdit({
  history,
  match: {
    params: { id },
  },
}) {

  useEffect(() => {
    // alert("abcd")
    notify()
   });

  //  const value = 0;

 
  // Subheader
  const suhbeader = useSubheader();
  const[todos,setTodos] = useState([]);
  const[inputText, setInputText] = useState('');
  const[inputCloesdDate, setInputCloesdDate] = useState('');
  const[inputStatsales_ticket_tot_qnty, setInputStatsales_ticket_tot_qnty] = useState('');
  const[inputStatslaes_expected_profit, setInputStatslaes_expected_profit] = useState('');
  const[inputStatdriverid, setInputStatdriverid] = useState('');
  const[inputStathelperId, setInputStathelperId] = useState('');
  const[inputStatquantity, setInputStatquantity] = useState('');
  const[inputStatvehic_after_weight, setInputStatvehic_after_weight] = useState('');
  const[inputStatvehic_before_weight, setInputStatvehic_before_weight] = useState('');
  const[inputStatvehic_return_weight, setInputStatvehic_return_weight] = useState('');
  const[inputStat, setInputStat] = useState('');

  const[inputvehicId, setInputvehicId] = useState('');
  const[inputagntSalesAgentId, setInputagntSalesAgentId] = useState('');
  const[inputdispatchempid, setInputdispatchempid] = useState('');
  const[inputreturncheckempid, setInputreturncheckempid] = useState('');
  const[inputreturndate, setInputreturndate] = useState('');
  const [adminId, setadminId] = useState('')
  const [createDate1, setcreateDate1] = useState('')
  const [updateDate, setupdateDate] = useState('')
  // const[inputStat, setInputStat] = useState('');
  // const[inputStat, setInputStat] = useState('');

  const[toViewDate, setToViewDate] = useState("");

  const [designation2,setDesignation]=useState([])
  // const [designation2,setDesignation]=useState([])
  // const [designation12,setdesignation12]=useState([])
  const driver = [];
  // const [Helper4,setHelper]=useState([])
  // const [Dispatch4,setDispath]=useState([])
  // const [Return4,setReturn]=useState([])

  // const driver2=[]
  // const Helper2=[]
  // const Dispatch2=[]
  // const Return2=[]

  // let driver=[]s
  const Helper=[]
  const Dispatch=[]
  const Return=[]

  const sale = useSelector(state => state.SalesTickets.totalQuaniy)
  const sale2 = useSelector(state => state.SalesTickets.salesItems)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: SalesTickets_URL_GETBYID +`/${id}`
      })
      .then((res) => {
        console.log(res.data.driverid)
        // alert("abcd")
        setToViewDate(moment(res.data.sales_ticket_start_date).format("yyyy-MM-DD"))
        setInputCloesdDate(moment(res.data.sales_ticket_closed_date).format("yyyy-MM-DD"))
        setInputStatsales_ticket_tot_qnty(res.data.sales_ticket_tot_qnty)
        setInputStatslaes_expected_profit(res.data.slaes_expected_profit)
        setInputStatdriverid(res.data.driverid)
        setInputStathelperId(res.data.helperId)
        setInputStatquantity(res.data.quantity)
        setInputStatvehic_after_weight(res.data.vehic_after_weight)
        setInputStatvehic_before_weight(res.data.vehic_before_weight)
        setInputStatvehic_return_weight(res.data.vehic_return_weight)
        setInputStat(res.data.stat)
        setInputvehicId(res.data.vehicId)
        setInputagntSalesAgentId(res.data.slsagnt.sales_agent_id)
        setInputdispatchempid(res.data.dispatchempid)
        setInputreturncheckempid(res.data.returncheckempid)
        setInputreturndate(res.data.returndate)

        setadminId(res.data.created_by.id);
        setcreateDate1(res.data.vehicle_created);
        setupdateDate(res.data.vehicle_updated);

      })
      .catch(function (response) {
          // console.log(response);
      });
  },[id])

  console.log(inputagntSalesAgentId)
  // console.log(toViewDate)

  // console.log(todos)

  const initSalesTicket = {
    // sales_ticket_closed_date:"2021-03-25",
    sales_ticket_closed_date:inputCloesdDate,
    sales_ticket_tot_qnty: inputStatsales_ticket_tot_qnty,
    slaes_expected_profit:inputStatslaes_expected_profit ,
    stat: inputStat,
    sales_ticket_start_date:toViewDate,
    agntSalesAgentId:inputagntSalesAgentId,
    quantity:inputStatquantity,
    vehicId:inputvehicId,
    vehic_before_weight: inputStatvehic_before_weight,
    vehic_after_weight:inputStatvehic_after_weight ,
    vehic_return_weight: inputStatvehic_return_weight,
    driverid: inputStatdriverid,
    helperId: inputStathelperId,
    dispatchempid: inputdispatchempid ,
    returncheckempid: inputreturncheckempid,
    returndate:inputreturndate,
    saleticketitem:[...todos]
  };

  const initSalesTicket02 = {
    // sales_ticket_closed_date:"2021-03-25",
    sales_ticket_closed_date:inputCloesdDate,
    // sales_ticket_tot_qnty: inputStatsales_ticket_tot_qnty,
    slaes_expected_profit:inputStatslaes_expected_profit ,
    stat: inputStat,
    sales_ticket_start_date:toViewDate ,
    agntSalesAgentId:inputagntSalesAgentId,
    quantity:inputStatquantity,

    sales_ticket_tot_qnty:sale ,
    vehic_before_weight: "",
    vehic_after_weight:"",
    vehic_return_weight: "",
    driverid:"" ,
    helperId: "",
    dispatchempid:"" ,
    returncheckempid:"",
    // returndate:"",
    saleticketitem:[...sale2]
  };

  // console.log(inputagntSalesAgentId)

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, SalesTicketForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.SalesTickets.actionsLoading,
      SalesTicketForEdit: state.SalesTickets.SalesTicketForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchSalesTicket(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Tickets";
    if (SalesTicketForEdit && id) {
      // _title = `Edit Tickets Quntity - ${SalesTicketForEdit.sales_ticket_tot_qnty}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SalesTicketForEdit, id]);

  const saveSalesTicket = (values) => {
    if (!id) {
      dispatch(actions.createSalesTicket(values)).then(() => backToSalesTicketsList());
    } else {
      dispatch(actions.updateSalesTicket(values,id)).then(() => backToSalesTicketsList());
    }
  };

  useEffect(()=>{
    console.log("abcd")
    axios({
      method: 'post',
      baseURL: Employees_URL,
      data: { 
        "filter": [{"status": "1" }],
        // "sort": "DESC",
        // "limit": "",
        // "skip":""
      } 
    })
    .then((res) => {
      setDesignation(res.data.data.results)
      console.log(res.data.data.results)
      
    })
    .catch(function (response) {
    });
    
    
  },[])

    if(!id){
    designation2.map(item => {
      if(item.positi.position_name==='Driver'){
        // console.log(item.id,item.emp_fname)
        driver.push({id:item.id, name:item.emp_fname}) 
      }

      if(item.positi.position_name==='Helper'){
        // console.log(item.id,item.emp_fname)
        Helper.push({id:item.id, name:item.emp_fname}) 
      }

      if(item.positi.position_name==='Dispatch'){
        // console.log(item.id,item.emp_fname)
        Dispatch.push({id:item.id, name:item.emp_fname}) 
      }

      if(item.positi.position_name==='Return Check'){
        // console.log(item.id,item.emp_fname)
        Return.push({id:item.id, name:item.emp_fname}) 
      }

      return({}
        // console.log(designation2)
      );
      
    })
    }
    else{
      designation2.map(item => {
        if(item.positi.position_name==='Driver'){
          // console.log(item.id,item.emp_fname)
          driver.push({id:item.id, name:item.emp_fname}) 
        }
  
        if(item.positi.position_name==='Helper'){
          // console.log(item.id,item.emp_fname)
          Helper.push({id:item.id, name:item.emp_fname}) 
        }
  
        if(item.positi.position_name==='Dispatch'){
          // console.log(item.id,item.emp_fname)
          Dispatch.push({id:item.id, name:item.emp_fname}) 
        }
  
        if(item.positi.position_name==='Return Check'){
          // console.log(item.id,item.emp_fname)
          Return.push({id:item.id, name:item.emp_fname}) 
        }
  
        return({}
          // console.log(designation2)
        )
      })
    }



 

  const btnRef = useRef();

  const backToSalesTicketsList = () => {
    history.push(`/sales/tickets`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSalesTicketsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Sales Ticket Items
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SalesTicketEditForm
                actionsLoading={actionsLoading}
                // SalesTicket={SalesTicketForEdit || initSalesTicket}
                SalesTicket={initSalesTicket}
                btnRef={btnRef}
                saveSalesTicket={saveSalesTicket}
                todos={todos}
                setTodos={setTodos}
                id={id}
                setInputvehicId={setInputvehicId}
                driver={driver}
                Helper={Helper}
                Dispatch={Dispatch}
                Return={Return}
                adminId={adminId}
                createDate={createDate1}
                updateDate={updateDate}
              />
            )}
            
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id}>
                <SpecificationEditDialog id={id} />
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSalesTicketsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("Items")}>
                  <a
                    className={`nav-link ${tab === "Items" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "Items").toString()}
                  >
                    SalesTicket Items
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Sales Ticket Items
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
            <SalesTicketCreateForm
            actionsLoading={actionsLoading}
            SalesTicket={SalesTicketForEdit || initSalesTicket02}
            btnRef={btnRef}
            saveSalesTicket={saveSalesTicket}
            todos={todos}
            setTodos={setTodos}
            // setInputStat={setInputStat}
            inputText={inputText} 
            setInputText={setInputText}
            designation={designation2}
            driver={driver}
            Helper={Helper}
            Dispatch={Dispatch}
            Return={Return}
            setInputCloesdDate={setInputCloesdDate}
            setInputStatslaes_expected_profit={setInputStatslaes_expected_profit}
            setInputStatquantity={setInputStatquantity}
            setInputStat2={setInputStat}
            setToViewDate2={setToViewDate}
            setInputagntSalesAgentId={setInputagntSalesAgentId}
          />
            )}
           
            {tab === "Items" && id && (
              <ItemsUIProvider id={id} currentSalesTicketId={id}>
                <Items />
              </ItemsUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id} currentSalesTicketId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }
}
