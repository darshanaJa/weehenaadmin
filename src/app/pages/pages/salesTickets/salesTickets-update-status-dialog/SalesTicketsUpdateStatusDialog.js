import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { SalesTicketstatusCssClasses } from "../SalesTicketsUIHelpers";
import * as actions from "../../../_redux/salesTickets/salesTicketsActions";
import { useSalesTicketsUIContext } from "../SalesTicketsUIContext";

const selectedSalesTickets = (entities, ids) => {
  const _SalesTickets = [];
  ids.forEach((id) => {
    const SalesTicket = entities.find((el) => el.id === id);
    if (SalesTicket) {
      _SalesTickets.push(SalesTicket);
    }
  });
  return _SalesTickets;
};

export function SalesTicketsUpdateStatusDialog({ show, onHide }) {
  // SalesTickets UI Context
  const saleTicketsUIContext = useSalesTicketsUIContext();
  const salesTicketsUIProps = useMemo(() => {
    return {
      ids: saleTicketsUIContext.ids,
      setIds: saleTicketsUIContext.setIds,
      queryParams: saleTicketsUIContext.queryParams,
    };
  }, [saleTicketsUIContext]);

  // SalesTickets Redux state
  const { SalesTickets, isLoading } = useSelector(
    (state) => ({
      SalesTickets: selectedSalesTickets(state.SalesTickets.entities, salesTicketsUIProps.ids),
      isLoading: state.SalesTickets.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected SalesTickets we should close modal
  useEffect(() => {
    if (salesTicketsUIProps.ids || salesTicketsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [salesTicketsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing SalesTicket by ids
    dispatch(actions.updateSalesTicketsStatus(salesTicketsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchSalesTickets(salesTicketsUIProps.queryParams)).then(
          () => {
            // clear selections list
            salesTicketsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected SalesTickets
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SalesTickets.map((SalesTicket) => (
              <div className="list-timeline-item mb-3" key={SalesTicket.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SalesTicketstatusCssClasses[SalesTicket.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SalesTicket.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SalesTicket.manufacture}, {SalesTicket.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${SalesTicketstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
