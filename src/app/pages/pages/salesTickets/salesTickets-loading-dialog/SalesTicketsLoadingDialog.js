import React, {useEffect} from "react";
import {shallowEqual, useSelector} from "react-redux";
import {LoadingDialog} from "../../../../../_metronic/_partials/controls";

export function SalesTicketsLoadingDialog() {
  const { isLoading } = useSelector(
    state => ({ isLoading: state.SalesTickets.listLoading }),
    shallowEqual
  );
  useEffect(() => {}, [isLoading]);
  return <LoadingDialog isLoading={isLoading} text="Loading ..." />;
}
