// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import * as actions from "../../../_redux/Items/ItemsActions";
import { ActionsColumnFormatter } from "./column-formatters/ActionsColumnFormatter";
import * as uiHelpers from "./ItemsUIHelper";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import { useItemsUIContext } from "./ItemsUIContext";

export function ItemsTable() {
  // Items UI Context
  const ItemsUIContext = useItemsUIContext();
  const ItemsUIProps = useMemo(() => {
    return {
      ids: ItemsUIContext.ids,
      setIds: ItemsUIContext.setIds,
      queryParams: ItemsUIContext.queryParams,
      setQueryParams: ItemsUIContext.setQueryParams,
      productId: ItemsUIContext.productId,
      openEditItemDialog: ItemsUIContext.openEditItemDialog,
      openDeleteItemDialog: ItemsUIContext.openDeleteItemDialog,
    };
  }, [ItemsUIContext]);

  // Getting curret state of SalesTickets list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Items }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  const dispatch = useDispatch();
  useEffect(() => {
    ItemsUIProps.setIds([]);
    dispatch(
      actions.fetchItems(ItemsUIProps.queryParams, ItemsUIProps.productId)
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ItemsUIProps.queryParams, dispatch, ItemsUIProps.productId]);
  const columns = [
    {
      dataField: "sales_ticket_item_id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "ticket_item_opening_quantity",
      text: "Quntity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "ticket_item_price",
      text: "Price",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: ActionsColumnFormatter,
      formatExtraData: {
        openEditItemDialog: ItemsUIProps.openEditItemDialog,
        openDeleteItemDialog: ItemsUIProps.openDeleteItemDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];

  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ItemsUIProps.queryParams.pageSize,
    page: ItemsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bordered={false}
                bootstrap4
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ItemsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ItemsUIProps.ids,
                  setIds: ItemsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
