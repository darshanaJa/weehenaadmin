/* eslint-disable no-unused-vars */
import React, { useEffect, useContext, createContext, useState, useCallback } from "react";
import {isEqual, isFunction} from "lodash";
import {initialFilter} from "./ItemsUIHelper";

const ItemsUIContext = createContext();

export function useItemsUIContext() {
  return useContext(ItemsUIContext);
}

export const ItemsUIConsumer = ItemsUIContext.Consumer;

export function ItemsUIProvider({ currentItemId, children }) {
  const [ItemId, setItemId] = useState(currentItemId);
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback(nextQueryParams => {
    setQueryParamsBase(prevQueryParams => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);
  const [selectedId, setSelectedId] = useState(null);
  const [showEditItemDialog, setShowEditItemDialog] = useState(false);
  const initItem = {
    // id: undefined,
    name: "",
    rsaleSaleId: "f0f48684-5554-4294-a4b0-828d01010024",
    mstockItemId:"",
    saleitemRetailSaleId:"",
    saleticketSalesTicketId:""
  };
  useEffect(()=> {
    initItem.ItemId = currentItemId;
    initItem.carId = currentItemId;
    setItemId(currentItemId);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentItemId]);
  const openNewItemDialog = () => {
    setSelectedId(undefined);
    setShowEditItemDialog(true);
  };
  const openEditItemDialog = id => {
    setSelectedId(id);
    setShowEditItemDialog(true);
  };
  const closeEditItemDialog = () => {
    setSelectedId(undefined);
    setShowEditItemDialog(false);
  };
  const [showDeleteItemDialog, setShowDeleteItemDialog] = useState(false);
  const openDeleteItemDialog = id => {
    setSelectedId(id);
    setShowDeleteItemDialog(true);
  };
  const closeDeleteItemDialog = () => {
    setSelectedId(undefined);
    setShowDeleteItemDialog(false);
  };

  const [showDeleteItemsDialog, setShowDeleteItemsDialog] = useState(false);
  const openDeleteItemsDialog = () => {
    setShowDeleteItemsDialog(true);
  };
  const closeDeleteItemsDialog = () => {
    setShowDeleteItemsDialog(false);
  };

  const [showFetchItemsDialog, setShowFetchItemsDialog] = useState(false);
  const openFetchItemsDialog = () => {
    setShowFetchItemsDialog(true);
  };
  const closeFetchItemsDialog = () => {
    setShowFetchItemsDialog(false);
  };

  const value = {
    ids,
    setIds,
    ItemId,
    setItemId,
    queryParams,
    setQueryParams,
    initItem,
    selectedId,
    showEditItemDialog,
    openNewItemDialog,    
    openEditItemDialog,
    closeEditItemDialog,
    showDeleteItemDialog,
    openDeleteItemDialog,
    closeDeleteItemDialog,
    showDeleteItemsDialog,
    openDeleteItemsDialog,
    closeDeleteItemsDialog,
    openFetchItemsDialog,
    closeFetchItemsDialog,
    showFetchItemsDialog
  };
  
  return (
    <ItemsUIContext.Provider value={value}>
      {children}
    </ItemsUIContext.Provider>
  );
}
