import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { useItemsUIContext } from "./ItemsUIContext";

const selectedItems = (entities, ids) => {
  const _Items = [];
  ids.forEach((id) => {
    const Item = entities.find((el) => el.id === id);
    if (Item) {
      _Items.push(Item);
    }
  });
  return _Items;
};

export function ItemsFetchDialog() {
  // Items UI Context
  const ItemsUIContext = useItemsUIContext();
  const ItemsUIProps = useMemo(() => {
    return {
      ids: ItemsUIContext.ids,
      queryParams: ItemsUIContext.queryParams,
      showFetchItemsDialog: ItemsUIContext.showFetchItemsDialog,
      closeFetchItemsDialog: ItemsUIContext.closeFetchItemsDialog,
    };
  }, [ItemsUIContext]);

  const { Items } = useSelector(
    (state) => ({
      Items: selectedItems(state.Items.entities, ItemsUIProps.ids),
    }),
    shallowEqual
  );

  useEffect(() => {
    if (!ItemsUIProps.ids || ItemsUIProps.ids.length === 0) {
      ItemsUIProps.closeFetchItemsDialog();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ItemsUIProps.ids]);

  return (
    <Modal
      show={ItemsUIProps.showFetchItemsDialog}
      onHide={ItemsUIProps.closeFetchItemsDialog}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Items.map((Item) => (
              <div className="list-timeline-item mb-3" key={Item.id}>
                <span className="list-timeline-text">
                  <span
                    className="label label-lg label-light-success label-inline"
                    style={{ width: "60px" }}
                  >
                    ID: {Item.id}
                  </span>{" "}
                  <span className="ml-5">{Item.text} </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={ItemsUIProps.closeFetchItemsDialog}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={ItemsUIProps.closeFetchItemsDialog}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
