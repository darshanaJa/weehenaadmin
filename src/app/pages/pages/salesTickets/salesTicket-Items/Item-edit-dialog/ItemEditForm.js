// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
import {
  Input,
  Select,
  // DatePickerField,
} from "../../../../../../_metronic/_partials/controls";
import axios from "axios";
import { MainStocks_URL_GET } from "../../../../_redux/MainStock/MainStocksCrud";
import { Shops_URL_GET } from "../../../../_redux/shops/shopsCrud";
// import { SalesTickets_URL_GET } from "../../../../_redux/salesTickets/salesTicketsCrud";

// Validation schema
// const ItemEditSchema = Yup.object().shape({
  // name: Yup.string()
  //   .min(2, "Minimum 2 symbols")
  //   .max(50, "Maximum 50 symbols")
  //   .required("Text is required"),
  
// });

export function ItemEditForm({ saveItem, Item, actionsLoading, onHide }) {


  const [shop,setShop]=useState([]);
  // const [saleTicketID,setsaleTicketID]=useState([]);
  const [retailshop,setRetailshop]=useState([]);
  // const [retailSale,setRetailSale]=useState([]);

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Shops_URL_GET
      })
      .then((res) => {
        setRetailshop(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  // useEffect(()=>{
  //   axios({
  //     method: 'get',
  //     baseURL: SalesTickets_URL_GET
  //     })
  //     .then((res) => {
  //       setRetailSale(res.data)
  //     })
  //     .catch(function (response) {
  //         // console.log(response);
  //     });
      
  // },[])

  console.log(retailshop)

// const handleClose = (event?: React.SyntheticEvent, reason?:'') => {
//     if (reason === 'clickaway') {
//     return;
//     }
//     setOpen(false);
// };
   // showSnakBar("success", "Tips Loaded");
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Item}
        // validationSchema={ItemEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveItem(values)}}
      >
        {({ handleSubmit }) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <Form className="form form-label-right">

              <div className="form-group row">
                  <div className="col-lg-12">
                  <Select name="mstockItemId" label="Item Name">
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.item_id} >
                            {item.item_name}
                          </option>    
                      ))}
                  </Select>
                  </div>
                </div>

                <div className="form-group row">
                  <div className="col-lg-12">
                  <Select name="saleitemRetailSaleId" label="Retail Shop">
                      <option>Choose One</option>
                      {retailshop.map((item) => (
                          <option value={item.retail_store_id} >
                            {item.store_name}
                          </option>    
                      ))}
                  </Select>
                  </div>
                </div>


                {/* <div className="form-group row">
                  <div className="col-lg-12">
                  <Select name="saleticketSalesTicketId" label="Retail Shop">
                      <option>Choose One</option>
                      {retailshop.map((item) => (
                          <option value={item.sales_ticket_id} >
                            {item.sales_ticket_id}
                          </option>    
                      ))}
                  </Select>
                  </div>
                </div> */}


                <div className="form-group row">
                  <div className="col-lg-12">
                    <Field
                      name="rsaleSaleId"
                      component={Input}
                      placeholder="Retail Sale ID"
                      label="Retail Sale ID"
                    />
                  </div>
                </div>

                


                <div className="form-group row">
                  <div className="col-lg-12">
                    <Field
                      name="name"
                      component={Input}
                      placeholder="Name"
                      label="Name"
                    />
                  </div>
                </div>
              
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}