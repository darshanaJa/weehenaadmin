/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { Modal } from "react-bootstrap";
import { ModalProgressBar } from "../../../../../../_metronic/_partials/controls";

export function ItemEditDialogHeader({ id }) {
  const [title, setTitle] = useState("");
  // Items Redux state
  const { ItemForEdit, actionsLoading } = useSelector(
    (state) => ({
      ItemForEdit: state.Items.ItemForEdit,
      actionsLoading: state.Items.actionsLoading,
    }),
    shallowEqual
  );

  useEffect(() => {
    let _title = id ? "" : "New Item";
    if (ItemForEdit && id) {
      _title = "Edit Item";
    }

    setTitle(_title);
    // eslint-disable-next-line
  }, [ItemForEdit, actionsLoading]);

  return (
    <>
      {actionsLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">{title}</Modal.Title>
      </Modal.Header>
    </>
  );
}
