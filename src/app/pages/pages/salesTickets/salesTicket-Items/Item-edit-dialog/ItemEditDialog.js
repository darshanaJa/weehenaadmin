import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../../_redux/Items/ItemsActions";
import { ItemEditDialogHeader } from "./ItemEditDialogHeader";
import { ItemEditForm } from "./ItemEditForm";
import { useItemsUIContext } from "../ItemsUIContext";

function getFormattedDate(date) {
  if (typeof date === "string") {
    return date;
  }

  var year = date.getFullYear();

  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : "0" + month;

  var day = date.getDate().toString();
  day = day.length > 1 ? day : "0" + day;

  return month + "/" + day + "/" + year;
}

export function ItemEditDialog() {
  // Items UI Context
  const ItemsUIContext = useItemsUIContext();
  const ItemsUIProps = useMemo(() => {
    return {
      id: ItemsUIContext.selectedId,
      setIds: ItemsUIContext.setIds,
      ItemId: ItemsUIContext.ItemId,
      queryParams: ItemsUIContext.queryParams,
      showEditItemDialog: ItemsUIContext.showEditItemDialog,
      closeEditItemDialog: ItemsUIContext.closeEditItemDialog,
      initItem: ItemsUIContext.initItem,
    };
  }, [ItemsUIContext]);

  // Items Redux state
  const dispatch = useDispatch();
  const { actionsLoading, ItemForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Items.actionsLoading,
      ItemForEdit: state.Items.ItemForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    // server request for getting Item by seleted id
    dispatch(actions.fetchItem(ItemsUIProps.id));
  }, [ItemsUIProps.id, dispatch]);

  const saveItem = (Item) => {
    Item.dueDate = getFormattedDate(Item.dueDate);
    if (ItemsUIProps.id) {
      // server request for creating Items
      dispatch(actions.createItem(Item)).then(() => {
        // refresh list after deletion
        dispatch(
          actions.fetchItems(
            ItemsUIProps.queryParams,
            ItemsUIProps.ItemId
          )
        ).then(() => {
          // clear selections list
          ItemsUIProps.setIds([]);
          // closing edit modal
          ItemsUIProps.closeEditItemDialog();
        });
      });
    } else {
      // server request for updating Items
      dispatch(actions.updateItem(Item)).then(() => {
        // refresh list after deletion
        dispatch(
          // refresh list after deletion
          actions.fetchItems(
            ItemsUIProps.queryParams,
            ItemsUIProps.ItemId
          )
        ).then(() => {
          // clear selections list
          ItemsUIProps.setIds([]);
          // closing edit modal
          ItemsUIProps.closeEditItemDialog();
        });
      });
    }
  };

  return (
    <Modal
      show={ItemsUIProps.showEditItemDialog}
      onHide={ItemsUIProps.closeEditItemDialog}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <ItemEditDialogHeader id={ItemsUIProps.id} />
      <ItemEditForm
        saveItem={saveItem}
        actionsLoading={actionsLoading}
        Item={ItemForEdit || ItemsUIProps.initItem}
        onHide={ItemsUIProps.closeEditItemDialog}
      />
    </Modal>
  );
}
