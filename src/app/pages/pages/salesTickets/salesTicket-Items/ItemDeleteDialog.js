/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Items/ItemsActions";
import { useItemsUIContext } from "./ItemsUIContext";

export function ItemDeleteDialog() {
  // Items UI Context
  const ItemsUIContext = useItemsUIContext();
  const ItemsUIProps = useMemo(() => {
    return {
      id: ItemsUIContext.selectedId,
      setIds: ItemsUIContext.setIds,
      ItemId: ItemsUIContext.ItemId,
      queryParams: ItemsUIContext.queryParams,
      showDeleteItemDialog: ItemsUIContext.showDeleteItemDialog,
      closeDeleteItemDialog: ItemsUIContext.closeDeleteItemDialog,
    };
  }, [ItemsUIContext]);

  // Items Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Items.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!ItemsUIProps.id) {
      ItemsUIProps.closeDeleteItemDialog();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ItemsUIProps.id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteItem = () => {
    // server request for deleting Item by id
    dispatch(actions.deleteItem(ItemsUIProps.id)).then(() => {
      // refresh list after deletion
      dispatch(
        actions.fetchItems(
          ItemsUIProps.queryParams,
          ItemsUIProps.ItemId
        )
      );
      // clear selections list
      ItemsUIProps.setIds([]);
      // closing delete modal
      ItemsUIProps.closeDeleteItemDialog();
    });
  };

  return (
    <Modal
      show={ItemsUIProps.showDeleteItemDialog}
      onHide={ItemsUIProps.closeDeleteItemDialog}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Item Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Item?</span>
        )}
        {isLoading && <span>Item is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={ItemsUIProps.closeDeleteItemDialog}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteItem}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
