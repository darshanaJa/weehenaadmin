import React, { useMemo } from "react";
import { ItemsFilter } from "./ItemsFilter";
import { ItemsTable } from "./ItemsTable";
import { ItemsLoadingDialog } from "./ItemsLoadingDialog";
import { ItemsDeleteDialog } from "./ItemsDeleteDialog";
import { ItemDeleteDialog } from "./ItemDeleteDialog";
import { ItemsFetchDialog } from "./ItemsFetchDialog";
import { ItemsGrouping } from "./ItemsGrouping";
import { ItemEditDialog } from "./Item-edit-dialog/ItemEditDialog";
import { useItemsUIContext } from "./ItemsUIContext";

export function Items() {
  // Items UI Context
  const ItemsUIContext = useItemsUIContext();
  const ItemsUIProps = useMemo(() => {
    return { ids: ItemsUIContext.ids };
  }, [ItemsUIContext]);

  return (
    <>
      <ItemsLoadingDialog />
      <ItemEditDialog />
      <ItemDeleteDialog />
      <ItemsDeleteDialog />
      <ItemsFetchDialog />
      <div className="form margin-b-30">
        <ItemsFilter />
        {ItemsUIProps.ids.length > 0 && <ItemsGrouping />}
      </div>
      <ItemsTable />
    </>
  );
}
