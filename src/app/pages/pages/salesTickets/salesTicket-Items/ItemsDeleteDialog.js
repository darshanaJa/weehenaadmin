/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Items/ItemsActions";
import { useItemsUIContext } from "./ItemsUIContext";

export function ItemsDeleteDialog() {
  // Items UI Context
  const ItemsUIContext = useItemsUIContext();
  const ItemsUIProps = useMemo(() => {
    return {
      ids: ItemsUIContext.ids,
      setIds: ItemsUIContext.setIds,
      ItemId: ItemsUIContext.ItemId,
      queryParams: ItemsUIContext.queryParams,
      showDeleteItemsDialog: ItemsUIContext.showDeleteItemsDialog,
      closeDeleteItemsDialog: ItemsUIContext.closeDeleteItemsDialog,
    };
  }, [ItemsUIContext]);

  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Items.actionsLoading }),
    shallowEqual
  );

  useEffect(() => {}, [isLoading, dispatch]);
  useEffect(() => {
    if (!ItemsUIProps.ids || ItemsUIProps.ids.length === 0) {
      ItemsUIProps.closeDeleteItemsDialog();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ItemsUIProps.ids]);

  const deleteItems = () => {
    dispatch(actions.deleteItems(ItemsUIProps.ids)).then(() => {
      dispatch(
        actions.fetchItems(
          ItemsUIProps.queryParams,
          ItemsUIProps.ItemId
        )
      ).then(() => {
        ItemsUIProps.setIds([]);
        ItemsUIProps.closeDeleteItemsDialog();
      });
    });
  };

  return (
    <Modal
      show={ItemsUIProps.showDeleteItemsDialog}
      onHide={ItemsUIProps.closeDeleteItemsDialog}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Items Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Items?</span>
        )}
        {isLoading && <span>Items are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={ItemsUIProps.closeDeleteItemsDialog}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteItems}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
