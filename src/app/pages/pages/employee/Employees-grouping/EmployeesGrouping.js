import React, { useMemo } from "react";
import { useEmployeesUIContext } from "../EmployeesUIContext";

export function EmployeesGrouping() {
  // Employees UI Context
  const EmployeesUIContext = useEmployeesUIContext();
  const EmployeesUIProps = useMemo(() => {
    return {
      ids: EmployeesUIContext.ids,
      setIds: EmployeesUIContext.setIds,
      openDeleteEmployeesDialog: EmployeesUIContext.openDeleteEmployeesDialog,
      openFetchEmployeesDialog: EmployeesUIContext.openFetchEmployeesDialog,
      openUpdateEmployeesStatusDialog:
        EmployeesUIContext.openUpdateEmployeesStatusDialog,
    };
  }, [EmployeesUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{EmployeesUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={EmployeesUIProps.openDeleteEmployeesDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={EmployeesUIProps.openFetchEmployeesDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={EmployeesUIProps.openUpdateEmployeesStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
