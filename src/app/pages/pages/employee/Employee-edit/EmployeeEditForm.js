import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input,Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { Image_Url } from "../../../../config/config";
import Admin from "../../../../config/Admin";

// Validation schema
const EmployeeEditSchema = Yup.object().shape({
  emp_name_initials: Yup.string()
  .min(2, "Minimum 2 symbols")
  .max(50, "Maximum 50 symbols")
  .required("Inital Name is required"),
  emp_fname: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  emp_lname: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  emp_address_line1: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  emp_address_line2: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  emp_city: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  emp_bank: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  emp_bank_branch: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  emp_bank_acc_no: Yup.string()
  .min(2, "Minimum 2 symbols")
  .required("Is required"),
  departmentid: Yup.string()
  .required("Is required"),
  positionId: Yup.string()
  .required("Is required"),
  emp_email: Yup.string().email()
  .required("Email is required"),
  emp_mobile1: Yup.string()
  .required("Required")
  .matches(/^[0-9]+$/, "Must be only digits")
  .min(10, "Contact number be at least 10 numbers")
  .max(10, "Contact number be at least 10 numbers"),
  emp_mobile2: Yup.string()
  .required("Required")
  .matches(/^[0-9]+$/, "Must be only digits")
  .min(10, "Contact number be at least 10 numbers")
  .max(10, "Contact number be at least 10 numbers"),
  land_number: Yup.string()
  .required("Required")
  .matches(/^[0-9]+$/, "Must be only digits")
  .min(10, "Contact number be at least 10 numbers")
  .max(10, "Contact number be at least 10 numbers"),
  emp_idcard_number: Yup.string()
  .required("Agent NIC is required")
  .min(10, "NIC be at least 9 characters"),

});

export function EmployeeEditForm({
  Employee,
  btnRef,
  saveEmployee,
  pposition,
  department
}) {

  const submitImage=()=>{
    return(
      <img className="shopImg" alt="agent" src={url} />
    );
}

console.log(Employee)

const pic=Employee.emp_image
console.log(pic)
const url = Image_Url+pic
console.log(url)

  const [store_image,set_Profile_pic]=useState();

  console.log(set_Profile_pic)
  console.log(store_image)

  const bodyFormData = new FormData();


 
  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Employee}
        validationSchema={EmployeeEditSchema}
        onSubmit={(values) => {
          console.log(values)

          bodyFormData.append('emp_name_initials',values.emp_name_initials);
          bodyFormData.append('emp_fname',values.emp_fname);
          bodyFormData.append('emp_lname',values.emp_lname);
          bodyFormData.append('emp_mobile1',values.emp_mobile1);
          bodyFormData.append('emp_mobile2',values.emp_mobile2);
          bodyFormData.append('emp_email',values.emp_email);
          bodyFormData.append('emp_idcard_number',values.emp_idcard_number);
          bodyFormData.append('emp_address_line1',values.emp_address_line1);
          bodyFormData.append('emp_address_line2',values.emp_address_line2);
          bodyFormData.append('emp_city',values.emp_city);
          bodyFormData.append('land_number',values.land_number);
          bodyFormData.append('district',values.district);
          bodyFormData.append('emp_bank',values.emp_bank);
          bodyFormData.append('emp_bank_branch',values.emp_bank_branch);
          bodyFormData.append('emp_bank_acc_no',values.emp_bank_acc_no);
          bodyFormData.append('emp_image',store_image);
          bodyFormData.append('departmentid',values.departmentid);
          bodyFormData.append('positionId',values.positionId);

          console.log(bodyFormData);

          saveEmployee(bodyFormData);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="emp_name_initials"
                    component={Input}
                    placeholder="Initial Name"
                    label="Initial Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_fname"
                    component={Input}
                    placeholder="First Name"
                    label="First Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_lname"
                    component={Input}
                    placeholder="Last Name"
                    label="Last Name"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_email"
                    component={Input}
                    placeholder="Email"
                    label="Email"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_idcard_number"
                    component={Input}
                    placeholder="NIC"
                    label="NIC"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_address_line1"
                    component={Input}
                    placeholder="Address Line 01"
                    label="Address Line 01"
                    // customFeedbackLabel="Please enter Price"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="emp_address_line2"
                    component={Input}
                    placeholder="EmAddress Line 02"
                    label="EmAddress Line 02"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    name="land_number"
                    component={Input}
                    placeholder="Land Number"
                    label="Land Number"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_city"
                    component={Input}
                    placeholder="City"
                    label="City"
                  />
                </div>
              </div>  
                <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="emp_mobile1"
                    component={Input}
                    placeholder="Phone 01"
                    label="Phone 01"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="emp_mobile2"
                    component={Input}
                    placeholder="Phone 02"
                    label="Phone 02"
                  />
                </div>
                <div className="col-lg-4">
                  <Select name="departmentid" label="Dapartment Name">
                      <option>Choose One</option>
                      {department.map((item) => (
                          <option value={item.id} >
                            {item.department_name}
                          </option>    
                      ))}
                  </Select>
                </div>
              </div>
              <div className="form-group row">
              <div className="col-lg-4">
                  <Select name="positionId" label="Position Name">
                      <option>Choose One</option>
                      {pposition.map((item) => (
                          <option value={item.id} >
                            {item.position_name}
                          </option>    
                      ))}
                  </Select>    
                </div>
                <div className="col-lg-4">
                  <Field
                    name="district"
                    component={Input}
                    placeholder="District"
                    label="District"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_bank"
                    component={Input}
                    placeholder="Bank"
                    label="Bank"
                  />
                </div>
              </div>
              <div className="form-group row">
              <div className="col-lg-4">
                  <Field
                    type="string"
                    name="emp_bank_branch"
                    component={Input}
                    placeholder="Branch"
                    label="Branch"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="emp_bank_acc_no"
                    component={Input}
                    placeholder="Account Number"
                    label="Account Number"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                    <div className="form-group row">
                    <p className="empFont">Add Employee Image</p>
                      <input className="agentImageBtn2" type="file" name="file_title"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    </div>
                </div> 
                <div className="col-lg-4">
                  <button type="submit" className="btn btn-primary ml-2 + downbtn" >Save</button>
                 </div> 
              </div>
              <div className="form-group row"> 
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
              <div className="form-group row">
                  {submitImage()}
                </div>

                <Admin 
                  adminId={Employee.created_by}
                  createDate={Employee.emp_created_date} 
                  updateDate={Employee.emp_updated_date} 
                />


            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
