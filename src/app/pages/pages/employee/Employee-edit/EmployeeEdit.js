/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Employee/EmployeesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { EmployeeEditForm } from "./EmployeeEditForm";
import { Specifications } from "../Employee-specifications/Specifications";
import { SpecificationsUIProvider } from "../Employee-specifications/SpecificationsUIContext";
import { Specifications02 } from "../history/Specifications";
import { SpecificationsUIProvider02 } from "../history/SpecificationsUIContext02";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Employee-remarks/RemarksUIContext";
import { Remarks } from "../Employee-remarks/Remarks";
import { EmployeeCreateForm } from "./EmployeeCreateForm";
import { notify } from "../../../../config/Toastify";
import {Positions_URL } from "../../../_redux/Position/PositionsCrud";
import axios from "axios";
import { Departments_URL } from "../../../_redux/Department/DepartmentsCrud";




export function EmployeeEdit({
  history,
  match: {
    params: { id },
  },
}) {

  
  // Subheader
  const suhbeader = useSubheader();
  const[department, setDepartment] = useState([]);

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const [pposition, setPosition] = useState([]);
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, EmployeeForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Employees.actionsLoading,
      EmployeeForEdit: state.Employees.EmployeeForEdit,
    }),
    shallowEqual
  );

  const em = [EmployeeForEdit]
  console.log(em[0])

  const initEmployee = {
    emp_name_initials:"ABCD",
    emp_fname:"Mevan",
    emp_lname:"Supeshala",
    emp_mobile1:"0812345789",
    emp_mobile2:"0114578963",
    emp_email:"aaafff@Gmail.com",
    emp_idcard_number:"217846398V",
    emp_address_line1:"159/D Rathnawali road",
    emp_address_line2:"Kalubowila",
    emp_city:"Nugegoda",
    land_number:"0812547896",
    district:"Kandy",
    emp_bank:"Sampath Bank",
    emp_bank_branch:"Peradeniya",
    emp_bank_acc_no:"457896547852",
    file_title:"cdfdf",
    departmentid:"8",
    positionId:"7",
    designation:"gregr",
    };

  useEffect(() => {
    dispatch(actions.fetchEmployee(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Employee";
    if (EmployeeForEdit && id) {
      // _title = `Edit Employee - ${EmployeeForEdit.item_name} ${EmployeeForEdit.item_default_price} - ${EmployeeForEdit.item_name}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [EmployeeForEdit, id]);

  const saveEmployee = (values) => {
    if (!id) {
      dispatch(actions.createEmployee(values)).then(() => backToEmployeesList());
      // {notify("create")}
    } else {
      dispatch(actions.updateEmployee(values,id)).then(() => backToEmployeesList());
    }
  };

  useEffect(()=>{
    axios({
          method: 'post',
          baseURL: Positions_URL,
          data:{ 
            "filter": [{"status": "1" }],
            "sort": "DESC",
            "limit": "", 
            "skip":""
        }
          })
    .then((res) => {
      setPosition(res.data.data.results);
      console.log(res.data.data);
    })
  },[])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Departments_URL,
      data: {
        "filter": [{"status": "1"}],
        "limit": 10,
        "skip": 0,
        "sort":"DESC"
      }
      })
      .then((res) => {
        console.log(res.data.data.results)
        setDepartment(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  console.log(pposition)

  const btnRef = useRef();

const backToEmployeesList = () => {
  history.push(`/hr/employee`);
};

notify()
  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToEmployeesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
           
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Employee History
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Employee Documents
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <EmployeeEditForm
                actionsLoading={actionsLoading}
                Employee={EmployeeForEdit || initEmployee}
                btnRef={btnRef}
                saveEmployee={saveEmployee}
                pposition={pposition}
                department={department}
              />
            )}
            {tab === "remarks" && id && (
              <SpecificationsUIProvider02 idEmp={id} currentEmployeeId={id}>
                <Specifications02 />
              </SpecificationsUIProvider02>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider idEmp={id} currentEmployeeId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToEmployeesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Employee remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Employee specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <EmployeeCreateForm
                actionsLoading={actionsLoading}
                Employee={EmployeeForEdit || initEmployee}
                btnRef={btnRef}
                saveEmployee={saveEmployee}
                pposition={pposition}
                department={department}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentEmployeeId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentEmployeeId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }
}
