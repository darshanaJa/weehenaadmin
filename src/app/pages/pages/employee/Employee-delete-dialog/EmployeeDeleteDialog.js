/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Employee/EmployeesActions";
import { useEmployeesUIContext } from "../EmployeesUIContext";

export function EmployeeDeleteDialog({ id, show, onHide }) {
  // Employees UI Context
  const EmployeesUIContext = useEmployeesUIContext();
  const EmployeesUIProps = useMemo(() => {
    return {
      setIds: EmployeesUIContext.setIds,
      queryParams: EmployeesUIContext.queryParams,
    };
  }, [EmployeesUIContext]);

  // Employees Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Employees.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteEmployee = () => {
    // server request for deleting Employee by id
    dispatch(actions.deleteEmployee(id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchEmployees(EmployeesUIProps.queryParams));
      // clear selections list
      EmployeesUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Employee Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Employee?</span>
        )}
        {isLoading && <span>Employee is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteEmployee}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
