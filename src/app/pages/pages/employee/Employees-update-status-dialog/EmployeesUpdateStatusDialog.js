import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { EmployeestatusCssClasses } from "../EmployeesUIHelpers";
import * as actions from "../../../_redux/Employee/EmployeesActions";
import { useEmployeesUIContext } from "../EmployeesUIContext";

const selectedEmployees = (entities, ids) => {
  const _Employees = [];
  ids.forEach((id) => {
    const Employee = entities.find((el) => el.id === id);
    if (Employee) {
      _Employees.push(Employee);
    }
  });
  return _Employees;
};

export function EmployeesUpdateStatusDialog({ show, onHide }) {
  // Employees UI Context
  const EmployeesUIContext = useEmployeesUIContext();
  const EmployeesUIProps = useMemo(() => {
    return {
      ids: EmployeesUIContext.ids,
      setIds: EmployeesUIContext.setIds,
      queryParams: EmployeesUIContext.queryParams,
    };
  }, [EmployeesUIContext]);

  // Employees Redux state
  const { Employees, isLoading } = useSelector(
    (state) => ({
      Employees: selectedEmployees(state.Employees.entities, EmployeesUIProps.ids),
      isLoading: state.Employees.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Employees we should close modal
  useEffect(() => {
    if (EmployeesUIProps.ids || EmployeesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [EmployeesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Employee by ids
    dispatch(actions.updateEmployeesStatus(EmployeesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchEmployees(EmployeesUIProps.queryParams)).then(
          () => {
            // clear selections list
            EmployeesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Employees
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Employees.map((Employee) => (
              <div className="list-timeline-item mb-3" key={Employee.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      EmployeestatusCssClasses[Employee.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Employee.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Employee.manufacture}, {Employee.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${EmployeestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
