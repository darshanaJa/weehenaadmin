import React from "react";
import { Route } from "react-router-dom";
import { EmployeesLoadingDialog } from "./Employees-loading-dialog/EmployeesLoadingDialog";
import { EmployeeDeleteDialog } from "./Employee-delete-dialog/EmployeeDeleteDialog";
import { EmployeesDeleteDialog } from "./Employees-delete-dialog/EmployeesDeleteDialog";
import { EmployeesFetchDialog } from "./Employees-fetch-dialog/EmployeesFetchDialog";
import { EmployeesUpdateStatusDialog } from "./Employees-update-status-dialog/EmployeesUpdateStatusDialog";
import { EmployeesCard } from "./EmployeesCard";
import { EmployeesUIProvider } from "./EmployeesUIContext";

export function EmployeesPage({ history }) {
  const EmployeesUIEvents = {
    newEmployeeButtonClick: () => {
      history.push("/hr/employee/new");
    },
    openEditEmployeePage: (id) => {
      history.push(`/hr/employee/${id}/edit`);
    },
    openDeleteEmployeeDialog: (id) => {
      history.push(`/hr/employee/${id}/delete`);
    },
    openDeleteEmployeesDialog: () => {
      history.push(`/hr/employee/deleteEmployees`);
    },
    openFetchEmployeesDialog: () => {
      history.push(`/hr/employee/fetch`);
    },
    openUpdateEmployeesStatusDialog: () => {
      history.push("/hr/employee/updateStatus");
    },
  };

  return (
    <EmployeesUIProvider EmployeesUIEvents={EmployeesUIEvents}>
      <EmployeesLoadingDialog />
      <Route path="/hr/employee/deleteEmployees">
        {({ history, match }) => (
          <EmployeesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/employee");
            }}
          />
        )}
      </Route>
      <Route path="/hr/employee/:id/delete">
        {({ history, match }) => (
          <EmployeeDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/hr/employee");
            }}
          />
        )}
      </Route>
      <Route path="/hr/employee/fetch">
        {({ history, match }) => (
          <EmployeesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/employee");
            }}
          />
        )}
      </Route>
      <Route path="/hr/employee/updateStatus">
        {({ history, match }) => (
          <EmployeesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/employee");
            }}
          />
        )}
      </Route>
      <EmployeesCard />
    </EmployeesUIProvider>
  );
}
