/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Employee/EmployeesActions";
import { useEmployeesUIContext } from "../EmployeesUIContext";

export function EmployeesDeleteDialog({ show, onHide }) {
  // Employees UI Context
  const EmployeesUIContext = useEmployeesUIContext();
  const EmployeesUIProps = useMemo(() => {
    return {
      ids: EmployeesUIContext.ids,
      setIds: EmployeesUIContext.setIds,
      queryParams: EmployeesUIContext.queryParams,
    };
  }, [EmployeesUIContext]);

  // Employees Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Employees.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Employees we should close modal
  useEffect(() => {
    if (!EmployeesUIProps.ids || EmployeesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [EmployeesUIProps.ids]);

  const deleteEmployees = () => {
    // server request for deleting Employee by seleted ids
    dispatch(actions.deleteEmployees(EmployeesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchEmployees(EmployeesUIProps.queryParams)).then(() => {
        // clear selections list
        EmployeesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Employees Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Employees?</span>
        )}
        {isLoading && <span>Employees are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteEmployees}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
