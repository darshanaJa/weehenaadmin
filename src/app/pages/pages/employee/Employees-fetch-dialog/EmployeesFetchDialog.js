import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { EmployeestatusCssClasses } from "../EmployeesUIHelpers";
import { useEmployeesUIContext } from "../EmployeesUIContext";

const selectedEmployees = (entities, ids) => {
  const _Employees = [];
  ids.forEach((id) => {
    const Employee = entities.find((el) => el.id === id);
    if (Employee) {
      _Employees.push(Employee);
    }
  });
  return _Employees;
};

export function EmployeesFetchDialog({ show, onHide }) {
  // Employees UI Context
  const EmployeesUIContext = useEmployeesUIContext();
  const EmployeesUIProps = useMemo(() => {
    return {
      ids: EmployeesUIContext.ids,
      queryParams: EmployeesUIContext.queryParams,
    };
  }, [EmployeesUIContext]);

  // Employees Redux state
  const { Employees } = useSelector(
    (state) => ({
      Employees: selectedEmployees(state.Employees.entities, EmployeesUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!EmployeesUIProps.ids || EmployeesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [EmployeesUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Employees.map((Employee) => (
              <div className="list-timeline-item mb-3" key={Employee.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      EmployeestatusCssClasses[Employee.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Employee.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Employee.manufacture}, {Employee.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
