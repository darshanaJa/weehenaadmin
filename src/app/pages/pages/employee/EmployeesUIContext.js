import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./EmployeesUIHelpers";

const EmployeesUIContext = createContext();

export function useEmployeesUIContext() {
  return useContext(EmployeesUIContext);
}

export const EmployeesUIConsumer = EmployeesUIContext.Consumer;

export function EmployeesUIProvider({ EmployeesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newEmployeeButtonClick: EmployeesUIEvents.newEmployeeButtonClick,
    openEditEmployeePage: EmployeesUIEvents.openEditEmployeePage,
    openDeleteEmployeeDialog: EmployeesUIEvents.openDeleteEmployeeDialog,
    openDeleteEmployeesDialog: EmployeesUIEvents.openDeleteEmployeesDialog,
    openFetchEmployeesDialog: EmployeesUIEvents.openFetchEmployeesDialog,
    openUpdateEmployeesStatusDialog: EmployeesUIEvents.openUpdateEmployeesStatusDialog,
  };

  return (
    <EmployeesUIContext.Provider value={value}>
      {children}
    </EmployeesUIContext.Provider>
  );
}
