// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Employee/EmployeesActions";
import * as uiHelpers from "../EmployeesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useEmployeesUIContext } from "../EmployeesUIContext";

export function EmployeesTable() {
  // Employees UI Context
  const EmployeesUIContext = useEmployeesUIContext();
  const EmployeesUIProps = useMemo(() => {
    return {
      ids: EmployeesUIContext.ids,
      setIds: EmployeesUIContext.setIds,
      queryParams: EmployeesUIContext.queryParams,
      setQueryParams: EmployeesUIContext.setQueryParams,
      openEditEmployeePage: EmployeesUIContext.openEditEmployeePage,
      openDeleteEmployeeDialog: EmployeesUIContext.openDeleteEmployeeDialog,
    };
  }, [EmployeesUIContext]);

  // Getting curret state of Employees list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Employees }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Employees Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    EmployeesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchEmployees(EmployeesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [EmployeesUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    // {
    //   dataField: "id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "emp_name_initials",
      text: "Initial Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "emp_fname",
      text: "First Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "emp_lname",
      text: "Last Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "emp_mobile1",
      text: "Mobile 01",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "emp_mobile2",
      text: "Mobile 01-2",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "land_number",
      text: "Land Number",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "emp_email",
      text: "Email",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "emp_idcard_number",
      text: "NIC",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "emp_address_line1",
      text: "Address",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "emp_address_line2",
      text: "address 02",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "emp_city",
      text: "City",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "emp_bank",
      text: "Bank",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "emp_bank_branch",
      text: "Branch",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "emp_bank_acc_no",
      text: "Account Number",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "positi.position_name",
      text: "Position",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "passed_experiance",
      text: "Passed Experiance",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "emp_code",
      text: "Emp Code",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditEmployeePage: EmployeesUIProps.openEditEmployeePage,
        openDeleteEmployeeDialog: EmployeesUIProps.openDeleteEmployeeDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: EmployeesUIProps.queryParams.pageSize,
    page: EmployeesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  EmployeesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: EmployeesUIProps.ids,
                  setIds: EmployeesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
