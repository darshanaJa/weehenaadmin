import React from "react";
import {
  EmployeeConditionCssClasses,
  EmployeeConditionTitles
} from "../../EmployeesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        EmployeeConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        EmployeeConditionCssClasses[row.condition]
      }`}
    >
      {EmployeeConditionTitles[row.condition]}
    </span>
  </>
);
