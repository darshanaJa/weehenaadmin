import React from "react";
import {
  EmployeestatusCssClasses,
  EmployeestatusTitles
} from "../../EmployeesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      EmployeestatusCssClasses[row.status]
    } label-inline`}
  >
    {EmployeestatusTitles[row.status]}
  </span>
);
