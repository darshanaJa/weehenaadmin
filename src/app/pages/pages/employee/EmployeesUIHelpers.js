export const EmployeestatusCssClasses = ["success", "info", ""];
export const EmployeestatusTitles = ["Selling", "Sold"];
export const EmployeeConditionCssClasses = ["success", "danger", ""];
export const EmployeeConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    emp_fname: "",
  },
  emp_fname: null,
  sortOrder: "asc", // asc||desc
  sortField: "emp_fname",
  pageNumber: 1,
  pageSize: 10
};
