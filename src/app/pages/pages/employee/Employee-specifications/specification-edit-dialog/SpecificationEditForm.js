// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React,{useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import {
  Select,
  Input,
} from "../../../../../../_metronic/_partials/controls";
import { SPECIFICATIONS_DICTIONARY } from "../SpecificationsUIHelper";
import { Image_Url } from "../../../../../config/config";

// Validation schema
const SpecificationEditSchema = Yup.object().shape({
  value: Yup.string()
    .min(2, "Minimum 2 symbols")
    .max(50, "Maximum 50 symbols")
    .required("Value is required"),
  specId: Yup.number().required("Specification type is required"),
});

export function SpecificationEditForm({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
  id
}) {

  console.log(onHide)

  const file=Image_Url + specification.fille_path

  console.log(specification.id)

  const [image,setImage]=useState([]);

  const bodyFormData = new FormData();


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={specification}
        validationSchema={SpecificationEditSchema}

        onSubmit={(values) => {

          // if(fille_path==="underfind" || fille_path===null){
          //   // alert("abcd")
          //   notifyWarning("Please add Document...")
          // }
          // else{
          //   console.log(fille_path)

          //   bodyFormData.append('employeeid',values.employeeid);
          //   bodyFormData.append('file_title',values.file_title);
          //   bodyFormData.append('fille_path',fille_path);
          

            console.log(bodyFormData);

            saveSpecification(bodyFormData);
          // }

          
        }}
      >
        {({ handleSubmit}) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
            
              
              
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );


  return (
        <>
              <>
                {/* <Modal.Body className="overlay overlay-block cursor-default">
                  {actionsLoading && (
                    <div className="overlay-layer bg-transparent">
                      <div className="spinner sspinner-lg spinner-success" />
                    </div>
                  )}
                  <iframe width="450px" height="650px" src={file} />
                </Modal.Body> */}
                <Modal.Footer>
              <button
                type="button"
                className="btn btn-light btn-elevate"
                >
                Cancel
              </button>
              <> </>
              <button
                type="button"
                onClick={onHide}
                // onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
              </>
        </>
      );
}




// // Form is based on Formik
// // Data validation is based on Yup
// // Please, be familiar with article first:
// // https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
// import React, {useEffect,useState} from "react";
// import { Modal } from "react-bootstrap";
// import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
// import pdf02 from "./Doc1.docx";
// import {
//   Select,
//   Input,
// } from "../../../../../../_metronic/_partials/controls";
// import { SPECIFICATIONS_DICTIONARY } from "../SpecificationsUIHelper";
// import { Api_Login, Image_Url } from "../../../../../config/config";
// import { getSpecificationById } from "../../../../_redux/specificationsEmp/specificationsCrud";
// import axios from "axios";

// // Validation schema
// const SpecificationEditSchema = Yup.object().shape({
//   // value: Yup.string()
//   //   .min(2, "Minimum 2 symbols")
//   //   .max(50, "Maximum 50 symbols")
//   //   .required("Value is required"),
//   // specId: Yup.number().required("Specification type is required"),
//   // fille_path: yup
//   // .mixed()
//   // .required("A file is required")
// });

// export function SpecificationEditForm({
//   saveSpecification,
//   specification,
//   actionsLoading,
//   onHide,
// }) {

  

//   // const file=Image_Url + specification.fille_path

//   // console.log(specification.id)

//   // const [image,setImage]=useState([]);

//   // const AbcduseEffect=()=>{
//     // axios({
//     //   method: 'get',
//     //   baseURL: Api_Login +'/api/employee-documet' + `/4`,
//     //   responseType:'blob'
//     //   })
//     //   .then((res) => {

//     //     const li = Api_Login + '/' +res.data.fille_path
//     //     console.log(li)

//     //     const url = window.URL.createObjectURL(new Blob([li]))
//     //     const link = document.createElement('a')
//     //     link.href=url

//     //     link.setAttribute("download","abcd.pdf")

//     //     document.body.appendChild(link)
//     //     link.click();


//     //     setImage(file)
//     //     console.log(res.data.fille_path)
//     //   })
//     //   .catch(function (response) {
//     //       // console.log(response);
//     //       console.log("error")
//     //   });
//   //   var element = document.createElement("a");
//   //   var file02 = new Blob(
//   //     [
//   //       file
//   //     ],
//   //     { type: "image/*" }
//   //   );
//   //   element.href = URL.createObjectURL(file02);
//   //   element.download = "image.pdf";
//   //   element.click();

//   // }

//   // console.log(image)


//   return (
//     <>
//           <>
//             <Modal.Body className="overlay overlay-block cursor-default">
//               {actionsLoading && (
//                 <div className="overlay-layer bg-transparent">
//                   <div className="spinner spinner-lg spinner-success" />
//                 </div>
//               )}
//               {/* <iframe width="450px" height="650px" src={file} /> */}
//             </Modal.Body>
//             <Modal.Footer>
//               <button
//                 type="button"
//                 onClick={onHide}
//                 className="btn btn-light btn-elevate"
//               >
//                 Cancel
//               </button>
//               <> </>
//               <button
//                 className="btn btn-primary btn-elevate"
//               >
//                 Save
//               </button>
//             </Modal.Footer>
//           </>
//     </>
//   );
// }
