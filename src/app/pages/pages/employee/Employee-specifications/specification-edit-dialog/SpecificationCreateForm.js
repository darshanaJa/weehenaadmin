// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useState} from "react";
import { Modal } from "react-bootstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";
// import pdf02 from "./OOP.pdf";
import {
  // Select,
  Input,
  // FieldFeedbackLabel,
  // CustomImageInput
} from "../../../../../../_metronic/_partials/controls";
// import { SPECIFICATIONS_DICTIONARY } from "../SpecificationsUIHelper";
import { notifyWarning } from "../../../../../config/Toastify";

// Validation schema
const SpecificationEditSchema = Yup.object().shape({
  file_title: Yup.string()
    .min(2, "Minimum 2 symbols")
    .max(50, "Maximum 50 symbols")
    .required("File Title is required"),
  // specId: Yup.number().required("Specification type is required"),
  // file: Yup.string()
  // .required("A file is required")
  
});

export function SpecificationCreateForm({
  saveSpecification,
  specification,
  actionsLoading,
  onHide,
}) {

  const bodyFormData = new FormData();

  console.log(onHide)

  // const [store_image,set_Profile_pic]=useState();
  const [fille_path,set_file]=useState(null);

  console.log(fille_path)

  // const inputFileHandler =(e)=>{
  //   set_file(e.currentTarget.files[0])
  // }


  // const abcd = "https://storage.googleapis.com/weehena-storage-bucket1/employee_doc/6b19653d-a33c-4c01-809c-9458b1413292.pdf"

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={specification}
        validationSchema={SpecificationEditSchema}

        onSubmit={(values) => {

          if(fille_path==="underfind" || fille_path===null){
            // alert("abcd")
            notifyWarning("Please add Document...")
          }
          else{
            console.log(fille_path)

            bodyFormData.append('employeeid',values.employeeid);
            bodyFormData.append('file_title',values.file_title);
            bodyFormData.append('fille_path',fille_path);
          

            console.log(bodyFormData);

            saveSpecification(bodyFormData);
          }

          
        }}
      >
        {({ handleSubmit}) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <div className="form-group row"> 
                <div className="col-lg-12">
                  <Field
                    name="employeeid"
                    component={Input}
                    placeholder="Employee Id"
                    label="Employee Id"
                    readonly="readonly"
                  />
                </div>
              </div> 
              <div className="form-group row"> 
                <div className="col-lg-12">
                  <Field
                    name="file_title"
                    component={Input}
                    placeholder="File Title"
                    label="File Title"
                  />
                </div>
              </div> 
              <div className="form-group row"> 
                <div className="col-lg-12">
                  <Field
                   name="fille_path"
                   label="Document"
                   component={Input}
                   type="file"
                   onChange={(event) => {set_file(event.currentTarget.files[0])}}
                  // onChange={inputFileHandler}
                  />
                </div>
              </div>
                {/* <div className="form-group row">
                <div className="col-lg-12">
                      <Field
                        label="File Name"
                        component={Input}
                        className="agentImageBtn2" 
                        type="file" 
                        name="fille_path"
                        onChange={(event) => {set_file(event.currentTarget.files[0]); }}
                        // errorMessage={errors["file"] ? errors["file"] : undefined}
                       />
                </div>   
              </div> */}
              {/* <pre>
                {values.file
                  ? JSON.stringify(
                      {
                        fileName: values.file.name,
                        type: values.file.type,
                        size: `${values.file.size} bytes`
                      },
                      null,
                      2
                    )
                  : null}
              </pre>
              <pre>{values.text ? values.text : null} </pre> */}
              
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Cancel
              </button>
              <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Save
              </button>
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}
