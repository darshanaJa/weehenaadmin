/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo, useState } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
// import * as actions from "../../../_redux/specificationsEmp/specificationsActions";
import { useSpecificationsUIContext } from "./SpecificationsUIContext";
// import file from "./specification-edit-dialog/OOP.pdf";
import { getSpecificationById } from "../../../_redux/specificationsEmp/specificationsCrud";
import { Image_Url } from "../../../../config/config";

export function SpecificationDeleteDialog() {
  // Specifications UI Context
  const specsUIContext = useSpecificationsUIContext();
  const specsUIProps = useMemo(() => {
    return {
      id: specsUIContext.selectedId,
      productId: specsUIContext.productId,
      show: specsUIContext.showDeleteSpecificationDialog,
      onHide: specsUIContext.closeDeleteSpecificationDialog,
      queryParams: specsUIContext.queryParams,
      setIds: specsUIContext.setIds,
    };
  }, [specsUIContext]);

  // Specs Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.specifications.actionsLoading }),
    shallowEqual
  );

  const [filePath,setFilePath] = useState()
  const [fileTitle,setFileTitle] = useState()


  // if !id we should close modal
  useEffect(() => {
    if (!specsUIProps.id) {
      specsUIProps.onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [specsUIProps.id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // const deleteSpecification = () => {
  //   // server request for deleting spec by id
  //   dispatch(actions.deleteSpecification(specsUIProps.id)).then(() => {
  //     // refresh list after deletion
  //     dispatch(
  //       actions.fetchSpecifications(
  //         specsUIProps.queryParams,
  //         specsUIProps.productId
  //       )
  //     );
  //     specsUIProps.setIds([]);
  //     specsUIProps.onHide();
  //   });
  // };

  console.log(specsUIProps.id)

  useEffect(()=>{
    getSpecificationById(specsUIProps.id)
    .then((res) => {
      setFilePath(res.data.fille_path);
      setFileTitle(res.data.file_title);
      console.log(res.data);
    })
  },[specsUIProps.id])

  const filePath02 = Image_Url +filePath


  return (
    <Modal
      show={specsUIProps.show}
      onHide={specsUIProps.onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          {fileTitle}
        </Modal.Title>
      </Modal.Header>
      
      <Modal.Body className="overlay overlay-block cursor-default">
                     {/* {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )} */}
              <iframe width="450px" title={filePath02} height="650px" src={filePath02} />
            </Modal.Body>
  
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={specsUIProps.onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          
            <a 
              // target="_blank"
             download href  = {filePath02}>
                <button
                type="button"
                className="btn btn-primary btn-elevate"
              >
                Download
                </button>
            </a>   
        </div>
      </Modal.Footer>
    </Modal>
  );
}
