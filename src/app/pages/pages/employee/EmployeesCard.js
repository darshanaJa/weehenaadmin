import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { EmployeesFilter } from "./Employees-filter/EmployeesFilter";
import { EmployeesTable } from "./Employees-table/EmployeesTable";
// import { EmployeesGrouping } from "./Employees-grouping/EmployeesGrouping";
import { useEmployeesUIContext } from "./EmployeesUIContext";
import { notify } from "../../../config/Toastify";

export function EmployeesCard() {
    // },[])

  const EmployeesUIContext = useEmployeesUIContext();
  const EmployeesUIProps = useMemo(() => {
    return {
      ids: EmployeesUIContext.ids,
      queryParams: EmployeesUIContext.queryParams,
      setQueryParams: EmployeesUIContext.setQueryParams,
      newEmployeeButtonClick: EmployeesUIContext.newEmployeeButtonClick,
      openDeleteEmployeesDialog: EmployeesUIContext.openDeleteEmployeesDialog,
      openEditEmployeePage: EmployeesUIContext.openEditEmployeePage,
      openUpdateEmployeesStatusDialog:EmployeesUIContext.openUpdateEmployeesStatusDialog,
      openFetchEmployeesDialog: EmployeesUIContext.openFetchEmployeesDialog,
    };
  }, [EmployeesUIContext]);


  return (
    <Card>
      <CardHeader title="Employee list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={EmployeesUIProps.newEmployeeButtonClick}
          >
            New Employee
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <EmployeesFilter />
        {EmployeesUIProps.ids.length > 0 && (
          <>
            {/* <EmployeesGrouping /> */}
          </>
        )}
        <EmployeesTable />
      </CardBody>
    </Card>
  );
}
