import React, { useMemo } from "react";
import { useSalestHistoryUIContext } from "../SalesHistoryUIContext";

export function SalesHistoryGrouping() {
  // SalesHistory UI Context
  const salesHistoryUIContext = useSalestHistoryUIContext();
  const SalesHistoryUIProps = useMemo(() => {
    return {
      ids: salesHistoryUIContext.ids,
      setIds: salesHistoryUIContext.setIds,
      openDeleteSalesHistoryDialog: salesHistoryUIContext.openDeleteSalesHistoryDialog,
      openFetchSalesHistoryDialog: salesHistoryUIContext.openFetchSalesHistoryDialog,
      openUpdateSalesHistoryStatusDialog:
        salesHistoryUIContext.openUpdateSalesHistoryStatusDialog,
    };
  }, [salesHistoryUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{SalesHistoryUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={SalesHistoryUIProps.openDeleteSalesHistoryDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SalesHistoryUIProps.openFetchSalesHistoryDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SalesHistoryUIProps.openUpdateSalesHistoryStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
