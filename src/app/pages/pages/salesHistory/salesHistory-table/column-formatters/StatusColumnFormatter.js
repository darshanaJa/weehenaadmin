import React from "react";
import {
  SalesHistorytatusCssClasses,
  SalesHistorytatusTitles
} from "../../SalesHistoryUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      SalesHistorytatusCssClasses[row.status]
    } label-inline`}
  >
    {SalesHistorytatusTitles[row.status]}
  </span>
);
