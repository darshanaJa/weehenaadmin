import React from "react";
import {
  SaleHistoryConditionCssClasses,
  SaleHistoryConditionTitles
} from "../../SalesHistoryUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        SaleHistoryConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        SaleHistoryConditionCssClasses[row.condition]
      }`}
    >
      {SaleHistoryConditionTitles[row.condition]}
    </span>
  </>
);
