import React from "react";
import { Route } from "react-router-dom";
import { SalesHistoryLoadingDialog } from "./salesHistory-loading-dialog/SalesHistoryLoadingDialog";
import { SaleHistoryDeleteDialog } from "./saleHistory-delete-dialog/SaleHistoryDeleteDialog"
import { SalesHistoryDeleteDialog } from "./salesHistory-delete-dialog/SalesHistoryDeleteDialog";
import { SalesHistoryFetchDialog } from "./salesHistory-fetch-dialog/SalesHistoryFetchDialog";
import { SalesHistoryUpdateStatusDialog } from "./salesHistory-update-status-dialog/SalesHistoryUpdateStatusDialog";
import { SalesHistoryCard } from "./SalesHistoryCard";
import { SalesHistoryUIProvider } from "./SalesHistoryUIContext";

export function SalesHistoryPage({ history }) {
  const salesHistoryUIEvents = {
    newSaleHistoryButtonClick: () => {
      history.push("/sales/history/new");
    },
    openEditSaleHistoryPage: (id) => {
      history.push(`/sales/history/${id}/edit`);
    },
    openDeleteSaleHistoryDialog: (id) => {
      history.push(`/sales/history/${id}/delete`);
    },
    openDeleteSalesHistoryDialog: () => {
      history.push(`/sales/history/deleteSalesHistory`);
    },
    openFetchSalesHistoryDialog: () => {
      history.push(`/sales/history/fetch`);
    },
    openUpdateSalesHistoryStatusDialog: () => {
      history.push("/sales/history/updateStatus");
    },
  };

  return (
    <SalesHistoryUIProvider salesHistoryUIEvents={salesHistoryUIEvents}>
      <SalesHistoryLoadingDialog />
      <Route path="/sales/history/deleteSalesHistory">
        {({ history, match }) => (
          <SalesHistoryDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <Route path="/sales/history/:id/delete">
        {({ history, match }) => (
          <SaleHistoryDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <Route path="/sales/history/fetch">
        {({ history, match }) => (
          <SalesHistoryFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <Route path="/sales/history/updateStatus">
        {({ history, match }) => (
          <SalesHistoryUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/history");
            }}
          />
        )}
      </Route>
      <SalesHistoryCard />
    </SalesHistoryUIProvider>
  );
}
