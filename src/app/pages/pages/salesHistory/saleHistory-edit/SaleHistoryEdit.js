/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/salesHistory/salesHistoryActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { SaleHistoryEditForm } from "./SaleHistoryEditForm";
import { Specifications } from "../saleHistory-specifications/Specifications";
import { SpecificationsUIProvider } from "../saleHistory-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../saleHistory-remarks/RemarksUIContext";
import { Remarks } from "../saleHistory-remarks/Remarks";

const initSaleHistory = {
  id: undefined,
  model: "",
  manufacture: "Pontiac",
  modelYear: 2020,
  mileage: 0,
  description: "",
  color: "Red",
  price: 10000,
  condition: 1,
  status: 0,
  VINCode: "",
};

export function SaleHistoryEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, SaleHistoryForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.SalesHistory.actionsLoading,
      SaleHistoryForEdit: state.SalesHistory.SaleHistoryForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchSaleHistory(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New History";
    if (SaleHistoryForEdit && id) {
      // _title = `Edit History '${SaleHistoryForEdit.manufacture} ${SaleHistoryForEdit.model} - ${SaleHistoryForEdit.modelYear}'`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SaleHistoryForEdit, id]);

  const saveSaleHistory = (values) => {
    if (!id) {
      dispatch(actions.createSaleHistory(values)).then(() => backToSalesHistoryList());
    } else {
      dispatch(actions.updateSaleHistory(values)).then(() => backToSalesHistoryList());
    }
  };

  const btnRef = useRef();  
  const saveSaleHistoryClick = () => {
    if (btnRef && btnRef.current) {
      btnRef.current.click();
    }
  };

  const backToSalesHistoryList = () => {
    history.push(`/sales/history`);
  };

  return (
    <Card>
      {actionsLoading && <ModalProgressBar />}
      <CardHeader title={title}>
        <CardHeaderToolbar>
          <button
            type="button"
            onClick={backToSalesHistoryList}
            className="btn btn-light"
          >
            <i className="fa fa-arrow-left"></i>
            Back
          </button>
          {`  `}
          {/* <button className="btn btn-light ml-2">
            <i className="fa fa-redo"></i>
            Reset
          </button> */}
          {`  `}
          <button
            type="submit"
            className="btn btn-primary ml-2"
            onClick={saveSaleHistoryClick}
          >
            Save
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ul className="nav nav-tabs nav-tabs-line " role="tablist">
          <li className="nav-item" onClick={() => setTab("basic")}>
            <a
              className={`nav-link ${tab === "basic" && "active"}`}
              data-toggle="tab"
              role="tab"
              aria-selected={(tab === "basic").toString()}
            >
              Basic info
            </a>
          </li>
          {id && (
            <>
              {" "}
              <li className="nav-item" onClick={() => setTab("remarks")}>
                <a
                  className={`nav-link ${tab === "remarks" && "active"}`}
                  data-toggle="tab"
                  role="button"
                  aria-selected={(tab === "remarks").toString()}
                >
                  SaleHistory remarks
                </a>
              </li>
              <li className="nav-item" onClick={() => setTab("specs")}>
                <a
                  className={`nav-link ${tab === "specs" && "active"}`}
                  data-toggle="tab"
                  role="tab"
                  aria-selected={(tab === "specs").toString()}
                >
                  SaleHistory specifications
                </a>
              </li>
            </>
          )}
        </ul>
        <div className="mt-5">
          {tab === "basic" && (
            <SaleHistoryEditForm
              actionsLoading={actionsLoading}
              SaleHistory={SaleHistoryForEdit || initSaleHistory}
              btnRef={btnRef}
              saveSaleHistory={saveSaleHistory}
            />
          )}
          {tab === "remarks" && id && (
            <RemarksUIProvider currentSaleHistoryId={id}>
              <Remarks />
            </RemarksUIProvider>
          )}
          {tab === "specs" && id && (
            <SpecificationsUIProvider currentSaleHistoryId={id}>
              <Specifications />
            </SpecificationsUIProvider>
          )}
        </div>
      </CardBody>
    </Card>
  );
}
