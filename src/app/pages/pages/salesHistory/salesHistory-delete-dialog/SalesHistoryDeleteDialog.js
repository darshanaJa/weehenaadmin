/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/salesHistory/salesHistoryActions";
import { useSalestHistoryUIContext } from "../SalesHistoryUIContext";

export function SalesHistoryDeleteDialog({ show, onHide }) {
  // SalesHistory UI Context
  const salesHistoryUIContext = useSalestHistoryUIContext();
  const SalesHistoryUIProps = useMemo(() => {
    return {
      ids: salesHistoryUIContext.ids,
      setIds: salesHistoryUIContext.setIds,
      queryParams: salesHistoryUIContext.queryParams,
    };
  }, [salesHistoryUIContext]);

  // SalesHistory Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SalesHistory.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected SalesHistory we should close modal
  useEffect(() => {
    if (!SalesHistoryUIProps.ids || SalesHistoryUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SalesHistoryUIProps.ids]);

  const deleteSalesHistory = () => {
    // server request for deleting SaleHistory by seleted ids
    dispatch(actions.deleteSalesHistory(SalesHistoryUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSalesHistory(SalesHistoryUIProps.queryParams)).then(() => {
        // clear selections list
        SalesHistoryUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SalesHistory Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected SalesHistory?</span>
        )}
        {isLoading && <span>SalesHistory are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSalesHistory}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
