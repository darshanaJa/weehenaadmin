/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/salesHistory/salesHistoryActions";
import { useSalestHistoryUIContext } from "../SalesHistoryUIContext";

export function SaleHistoryDeleteDialog({ id, show, onHide }) {
  // SalesHistory UI Context
  const salesHistoryUIContext = useSalestHistoryUIContext();
  const SalesHistoryUIProps = useMemo(() => {
    return {
      setIds: salesHistoryUIContext.setIds,
      queryParams: salesHistoryUIContext.queryParams,
    };
  }, [salesHistoryUIContext]);

  // SalesHistory Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SalesHistory.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteSaleHistory = () => {
    // server request for deleting SaleHistory by id
    dispatch(actions.deleteSaleHistory(id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSalesHistory(SalesHistoryUIProps.queryParams));
      // clear selections list
      SalesHistoryUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SaleHistory Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this SaleHistory?</span>
        )}
        {isLoading && <span>SaleHistory is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSaleHistory}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
