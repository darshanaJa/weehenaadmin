import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { SalesHistorytatusCssClasses } from "../SalesHistoryUIHelpers";
import { useSalestHistoryUIContext } from "../SalesHistoryUIContext";

const selectedSalesHistory = (entities, ids) => {
  const _SalesHistory = [];
  ids.forEach((id) => {
    const SaleHistory = entities.find((el) => el.id === id);
    if (SaleHistory) {
      _SalesHistory.push(SaleHistory);
    }
  });
  return _SalesHistory;
};

export function SalesHistoryFetchDialog({ show, onHide }) {
  // SalesHistory UI Context
  const salesHistoryUIContext = useSalestHistoryUIContext();
  const SalesHistoryUIProps = useMemo(() => {
    return {
      ids: salesHistoryUIContext.ids,
      queryParams: salesHistoryUIContext.queryParams,
    };
  }, [salesHistoryUIContext]);

  // SalesHistory Redux state
  const { SalesHistory } = useSelector(
    (state) => ({
      SalesHistory: selectedSalesHistory(state.SalesHistory.entities, SalesHistoryUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!SalesHistoryUIProps.ids || SalesHistoryUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SalesHistoryUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SalesHistory.map((SaleHistory) => (
              <div className="list-timeline-item mb-3" key={SaleHistory.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SalesHistorytatusCssClasses[SaleHistory.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SaleHistory.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SaleHistory.manufacture}, {SaleHistory.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
