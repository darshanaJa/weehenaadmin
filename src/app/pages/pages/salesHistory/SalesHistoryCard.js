import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { SalesHistoryFilter } from "./salesHistory-filter/SalesHistoryFilter";
import { SalesHistoryTable } from "./salesHistory-table/SalesHistoryTable";
// import { SalesHistoryGrouping } from "./salesHistory-grouping/SalesHistoryGrouping";
import { useSalestHistoryUIContext } from "./SalesHistoryUIContext";
import { notify } from "../../../config/Toastify";

export function SalesHistoryCard() {
  const salesHistoryUIContext = useSalestHistoryUIContext();
  const SalesHistoryUIProps = useMemo(() => {
    return {
      ids: salesHistoryUIContext.ids,
      queryParams: salesHistoryUIContext.queryParams,
      setQueryParams: salesHistoryUIContext.setQueryParams,
      newSaleHistoryButtonClick: salesHistoryUIContext.newSaleHistoryButtonClick,
      openDeleteSalesHistoryDialog: salesHistoryUIContext.openDeleteSalesHistoryDialog,
      openEditSaleHistoryPage: salesHistoryUIContext.openEditSaleHistoryPage,
      openUpdateSalesHistoryStatusDialog:
        salesHistoryUIContext.openUpdateSalesHistoryStatusDialog,
      openFetchSalesHistoryDialog: salesHistoryUIContext.openFetchSalesHistoryDialog,
    };
  }, [salesHistoryUIContext]);

  return (
    <Card>
      <CardHeader title="Sales History list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={SalesHistoryUIProps.newSaleHistoryButtonClick}
          >
            History
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <SalesHistoryFilter />
        {SalesHistoryUIProps.ids.length > 0 && (
          <>
            {/* <SalesHistoryGrouping /> */}
          </>
        )}
        <SalesHistoryTable />
      </CardBody>
    </Card>
  );
}
