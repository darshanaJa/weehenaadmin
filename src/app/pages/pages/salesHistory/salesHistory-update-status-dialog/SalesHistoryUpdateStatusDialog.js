import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { SalesHistorytatusCssClasses } from "../SalesHistoryUIHelpers";
import * as actions from "../../../_redux/salesHistory/salesHistoryActions";
import { useSalestHistoryUIContext } from "../SalesHistoryUIContext";

const selectedSalesHistory = (entities, ids) => {
  const _SalesHistory = [];
  ids.forEach((id) => {
    const SaleHistory = entities.find((el) => el.id === id);
    if (SaleHistory) {
      _SalesHistory.push(SaleHistory);
    }
  });
  return _SalesHistory;
};

export function SalesHistoryUpdateStatusDialog({ show, onHide }) {
  // SalesHistory UI Context
  const SalesHistoryUIContext = useSalestHistoryUIContext();
  const SalesHistoryUIProps = useMemo(() => {
    return {
      ids: SalesHistoryUIContext.ids,
      setIds: SalesHistoryUIContext.setIds,
      queryParams: SalesHistoryUIContext.queryParams,
    };
  }, [SalesHistoryUIContext]);

  // SalesHistory Redux state
  const { SalesHistory, isLoading } = useSelector(
    (state) => ({
      SalesHistory: selectedSalesHistory(state.SalesHistory.entities, SalesHistoryUIProps.ids),
      isLoading: state.SalesHistory.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected SalesHistory we should close modal
  useEffect(() => {
    if (SalesHistoryUIProps.ids || SalesHistoryUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SalesHistoryUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing SaleHistory by ids
    dispatch(actions.updateSalesHistoryStatus(SalesHistoryUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchSalesHistory(SalesHistoryUIProps.queryParams)).then(
          () => {
            // clear selections list
            SalesHistoryUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected SalesHistory
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SalesHistory.map((SaleHistory) => (
              <div className="list-timeline-item mb-3" key={SaleHistory.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SalesHistorytatusCssClasses[SaleHistory.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SaleHistory.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SaleHistory.manufacture}, {SaleHistory.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${SalesHistorytatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
