import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./SalesHistoryUIHelpers";

const SalesHistoryUIContext = createContext();

export function useSalestHistoryUIContext() {
  return useContext(SalesHistoryUIContext);
}

export const SalesHistoryUIConsumer = SalesHistoryUIContext.Consumer;

export function SalesHistoryUIProvider({ salesHistoryUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newSaleHistoryButtonClick: salesHistoryUIEvents.newSaleHistoryButtonClick,
    openEditSaleHistoryPage: salesHistoryUIEvents.openEditSaleHistoryPage,
    openDeleteSaleHistoryDialog: salesHistoryUIEvents.openDeleteSaleHistoryDialog,
    openDeleteSalesHistoryDialog: salesHistoryUIEvents.openDeleteSalesHistoryDialog,
    openFetchSalesHistoryDialog: salesHistoryUIEvents.openFetchSalesHistoryDialog,
    openUpdateSalesHistoryStatusDialog:
      salesHistoryUIEvents.openUpdateSalesHistoryStatusDialog,
  };

  return (
    <SalesHistoryUIContext.Provider value={value}>
      {children}
    </SalesHistoryUIContext.Provider>
  );
}
