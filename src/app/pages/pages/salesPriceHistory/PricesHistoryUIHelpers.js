export const PricesHistorytatusCssClasses = ["success", "info", ""];
export const PricesHistorytatusTitles = ["Selling", "Sold"];
export const PriceHistoryConditionCssClasses = ["success", "danger", ""];
export const PriceHistoryConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    stk_history_id: ""
  },
  stk_history_id:null,
  sortOrder: "asc", // asc||desc
  sortField: "stk_history_id",
  pageNumber: 1,
  pageSize: 10
};
// export const AVAILABLE_COLORS = [
//   "Red",
//   "CadetBlue",
//   "Eagle",
//   "Gold",
//   "LightSlateGrey",
//   "RoyalBlue",
//   "Crimson",
//   "Blue",
//   "Sienna",
//   "Indigo",
//   "Green",
//   "Violet",
//   "GoldenRod",
//   "OrangeRed",
//   "Khaki",
//   "Teal",
//   "Purple",
//   "Orange",
//   "Pink",
//   "Black",
//   "DarkTurquoise"
// ];

// export const AVAILABLE_MANUFACTURES = [
//   "Pontiac",
//   "Kia",
//   "Lotus",
//   "Subaru",
//   "Jeep",
//   "Isuzu",
//   "Mitsubishi",
//   "Oldsmobile",
//   "Chevrolet",
//   "Chrysler",
//   "Audi",
//   "Suzuki",
//   "GMC",
//   "Cadillac",
//   "Infinity",
//   "Mercury",
//   "Dodge",
//   "Ram",
//   "Lexus",
//   "Lamborghini",
//   "Honda",
//   "Nissan",
//   "Ford",
//   "Hyundai",
//   "Saab",
//   "Toyota"
// ];
