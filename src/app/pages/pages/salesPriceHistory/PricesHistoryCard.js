import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { PricesHistoryFilter } from "./PricesHistory-filter/PricesHistoryFilter";
import { PricesHistoryTable } from "./PricesHistory-table/PricesHistoryTable";
// import { PricesHistoryGrouping } from "./PricesHistory-grouping/PricesHistoryGrouping";
import { usePricesHistoryUIContext } from "./PricesHistoryUIContext";
import { notify } from "../../../config/Toastify";

export function PricesHistoryCard() {
  const PricesHistoryUIContext = usePricesHistoryUIContext();
  const PricesHistoryUIProps = useMemo(() => {
    return {
      ids: PricesHistoryUIContext.ids,
      queryParams: PricesHistoryUIContext.queryParams,
      setQueryParams: PricesHistoryUIContext.setQueryParams,
      newPriceHistoryButtonClick: PricesHistoryUIContext.newPriceHistoryButtonClick,
      openDeletePricesHistoryDialog: PricesHistoryUIContext.openDeletePricesHistoryDialog,
      openEditPriceHistoryPage: PricesHistoryUIContext.openEditPriceHistoryPage,
      openUpdatePricesHistoryStatusDialog:PricesHistoryUIContext.openUpdatePricesHistoryStatusDialog,
      openFetchPricesHistoryDialog: PricesHistoryUIContext.openFetchPricesHistoryDialog,
    };
  }, [PricesHistoryUIContext]);

  return (
    <Card>
      <CardHeader title="PriceHistory list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={PricesHistoryUIProps.newPriceHistoryButtonClick}
          >
            New PriceHistory
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <PricesHistoryFilter />
        {PricesHistoryUIProps.ids.length > 0 && (
          <>
            {/* <PricesHistoryGrouping />e */}
          </>
        )}
        <PricesHistoryTable />
      </CardBody>
    </Card>
  );
}
