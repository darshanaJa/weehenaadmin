/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/PriceHistory/PricesHistoryActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { PriceHistoryEditForm } from "./PriceHistoryEditForm";
import { Specifications } from "../PriceHistory-specifications/Specifications";
import { SpecificationsUIProvider } from "../PriceHistory-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../PriceHistory-remarks/RemarksUIContext";
import { Remarks } from "../PriceHistory-remarks/Remarks";

const initPriceHistory = {
  // id : "",
  // sales_PriceHistory_fname : "",
  // sales_PriceHistory_lname : "",
  // sales_PriceHistory_nic : "",
  // sales_PriceHistory_mobile01 : "",
  // sales_PriceHistory_mobile02 : "",
  // sales_PriceHistory_email : "",
  // sales_PriceHistory_password : "",
  // sales_PriceHistory_image : ""
};

export function PriceHistoryEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, PriceHistoryForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.PricesHistory.actionsLoading,
      PriceHistoryForEdit: state.PricesHistory.PriceHistoryForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchPriceHistory(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New PriceHistory";
    if (PriceHistoryForEdit && id) {
      // _title = `Edit PriceHistory - ${PriceHistoryForEdit.sales_PriceHistory_fname} ${PriceHistoryForEdit.sales_PriceHistory_lname} - ${PriceHistoryForEdit.sales_PriceHistory_mobile01}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PriceHistoryForEdit, id]);

  const savePriceHistory = (values) => {
    if (!id) {
      dispatch(actions.createPriceHistory(values)).then(() => backToPricesHistoryList());
    } else {
      dispatch(actions.updatePriceHistory(values)).then(() => backToPricesHistoryList());
    }
  };

  const btnRef = useRef(); 

  const backToPricesHistoryList = () => {
    history.push(`/stocks/pricehistory`);
  };

  return (
    <Card>
      {actionsLoading && <ModalProgressBar />}
      <CardHeader title={title}>
        <CardHeaderToolbar>
          <button
            type="button"
            onClick={backToPricesHistoryList}
            className="btn btn-light"
          >
            <i className="fa fa-arrow-left"></i>
            Back
          </button>
          {`  `}
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ul className="nav nav-tabs nav-tabs-line " role="tablist">
          <li className="nav-item" onClick={() => setTab("basic")}>
            <a
              className={`nav-link ${tab === "basic" && "active"}`}
              data-toggle="tab"
              role="tab"
              aria-selected={(tab === "basic").toString()}
            >
              Basic info
            </a>
          </li>
          {id && (
            <>
              {" "}
              
            </>
          )}
        </ul>
        <div className="mt-5">
          {tab === "basic" && (
            <PriceHistoryEditForm
              actionsLoading={actionsLoading}
              PriceHistory={PriceHistoryForEdit || initPriceHistory}
              btnRef={btnRef}
              savePriceHistory={savePriceHistory}
            />
          )}
          {tab === "remarks" && id && (
            <RemarksUIProvider currentPriceHistoryId={id}>
              <Remarks />
            </RemarksUIProvider>
          )}
          {tab === "specs" && id && (
            <SpecificationsUIProvider currentPriceHistoryId={id}>
              <Specifications />
            </SpecificationsUIProvider>
          )}
        </div>
      </CardBody>
    </Card>
  );
}
