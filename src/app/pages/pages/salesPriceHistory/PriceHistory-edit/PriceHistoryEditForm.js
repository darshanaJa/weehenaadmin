import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { MainStocks_URL_GET } from "../../../_redux/MainStock/MainStocksCrud";
import axios from "axios";

// Validation schema
const PriceHistoryEditSchema = Yup.object().shape({
  id: Yup.string(),
  old_default_price: Yup.number()
    .required("Default Price is required")
});

export function PriceHistoryEditForm({
  PriceHistory,
  btnRef,
  savePriceHistory,
}) {

  const [shop,setShop]=useState([]);

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])
  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={PriceHistory}
        validationSchema={PriceHistoryEditSchema}
        onSubmit={(values) => {
          savePriceHistory(values);
          console.log(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">

              <div className="form-group row">
              <div className="col-lg-4">
                  <Select name="mstockItemId" label="Item Name">
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.item_id} >
                            {item.item_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="old_default_price"
                      component={Input}
                      placeholder=" Old Default Price"
                      label="Old Default Price"
                    />
                  </div>

                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
          
               
              </div>
              
             


              <div className="form-group row">
              </div>

              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
