import React from "react";
import {
  PricesHistorytatusCssClasses,
  PricesHistorytatusTitles
} from "../../PricesHistoryUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      PricesHistorytatusCssClasses[row.status]
    } label-inline`}
  >
    {PricesHistorytatusTitles[row.status]}
  </span>
);
