import React from "react";
import {
  PriceHistoryConditionCssClasses,
  PriceHistoryConditionTitles
} from "../../PricesHistoryUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        PriceHistoryConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        PriceHistoryConditionCssClasses[row.condition]
      }`}
    >
      {PriceHistoryConditionTitles[row.condition]}
    </span>
  </>
);
