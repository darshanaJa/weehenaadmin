// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/PriceHistory/PricesHistoryActions";
import * as uiHelpers from "../PricesHistoryUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { usePricesHistoryUIContext } from "../PricesHistoryUIContext";

export function PricesHistoryTable() {
  // PricesHistory UI Context
  const PricesHistoryUIContext = usePricesHistoryUIContext();
  const PricesHistoryUIProps = useMemo(() => {
    return {
      ids: PricesHistoryUIContext.ids,
      setIds: PricesHistoryUIContext.setIds,
      queryParams: PricesHistoryUIContext.queryParams,
      setQueryParams: PricesHistoryUIContext.setQueryParams,
      openEditPriceHistoryPage: PricesHistoryUIContext.openEditPriceHistoryPage,
      openDeletePriceHistoryDialog: PricesHistoryUIContext.openDeletePriceHistoryDialog,
    };
  }, [PricesHistoryUIContext]);

  // Getting curret state of PricesHistory list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.PricesHistory }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // PricesHistory Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    PricesHistoryUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchPricesHistory(PricesHistoryUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PricesHistoryUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: "stk_history_id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "history_created",
      text: "History Created",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "history_updated",
      text: "History Upadated",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "old_default_price",
      text: "Default Price",
      sort: true,
      sortCaret: sortCaret,
    },
    // {
    //   dataField: "sales_PriceHistory_mobile01",
    //   text: "Mobile 01",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "sales_PriceHistory_mobile02",
    //   text: "Mobile 02",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.PriceColumnFormatter,
    // },
    // {
    //   dataField: "sales_PriceHistory_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_PriceHistory_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "sales_PriceHistory_image",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditPriceHistoryPage: PricesHistoryUIProps.openEditPriceHistoryPage,
        openDeletePriceHistoryDialog: PricesHistoryUIProps.openDeletePriceHistoryDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: PricesHistoryUIProps.queryParams.pageSize,
    page: PricesHistoryUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  PricesHistoryUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: PricesHistoryUIProps.ids,
                  setIds: PricesHistoryUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
