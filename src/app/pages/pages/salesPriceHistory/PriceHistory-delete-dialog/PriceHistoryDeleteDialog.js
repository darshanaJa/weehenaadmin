/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/PriceHistory/PricesHistoryActions";
import { usePricesHistoryUIContext } from "../PricesHistoryUIContext";

export function PriceHistoryDeleteDialog({ id, show, onHide }) {
  // PricesHistory UI Context
  const PricesHistoryUIContext = usePricesHistoryUIContext();
  const PricesHistoryUIProps = useMemo(() => {
    return {
      setIds: PricesHistoryUIContext.setIds,
      queryParams: PricesHistoryUIContext.queryParams,
    };
  }, [PricesHistoryUIContext]);

  // PricesHistory Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.PricesHistory.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deletePriceHistory = () => {
    // server request for deleting PriceHistory by id
    dispatch(actions.deletePriceHistory(id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchPricesHistory(PricesHistoryUIProps.queryParams));
      // clear selections list
      PricesHistoryUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          PriceHistory Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this PriceHistory?</span>
        )}
        {isLoading && <span>PriceHistory is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deletePriceHistory}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
