import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./PricesHistoryUIHelpers";

const PricesHistoryUIContext = createContext();

export function usePricesHistoryUIContext() {
  return useContext(PricesHistoryUIContext);
}

export const PricesHistoryUIConsumer = PricesHistoryUIContext.Consumer;

export function PricesHistoryUIProvider({ PricesHistoryUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newPriceHistoryButtonClick: PricesHistoryUIEvents.newPriceHistoryButtonClick,
    openEditPriceHistoryPage: PricesHistoryUIEvents.openEditPriceHistoryPage,
    openDeletePriceHistoryDialog: PricesHistoryUIEvents.openDeletePriceHistoryDialog,
    openDeletePricesHistoryDialog: PricesHistoryUIEvents.openDeletePricesHistoryDialog,
    openFetchPricesHistoryDialog: PricesHistoryUIEvents.openFetchPricesHistoryDialog,
    openUpdatePricesHistoryStatusDialog: PricesHistoryUIEvents.openUpdatePricesHistoryStatusDialog,
  };

  return (
    <PricesHistoryUIContext.Provider value={value}>
      {children}
    </PricesHistoryUIContext.Provider>
  );
}
