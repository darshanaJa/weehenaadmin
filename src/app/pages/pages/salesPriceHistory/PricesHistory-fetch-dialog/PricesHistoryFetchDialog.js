import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { PricesHistorytatusCssClasses } from "../PricesHistoryUIHelpers";
import { usePricesHistoryUIContext } from "../PricesHistoryUIContext";

const selectedPricesHistory = (entities, ids) => {
  const _PricesHistory = [];
  ids.forEach((id) => {
    const PriceHistory = entities.find((el) => el.id === id);
    if (PriceHistory) {
      _PricesHistory.push(PriceHistory);
    }
  });
  return _PricesHistory;
};

export function PricesHistoryFetchDialog({ show, onHide }) {
  // PricesHistory UI Context
  const PricesHistoryUIContext = usePricesHistoryUIContext();
  const PricesHistoryUIProps = useMemo(() => {
    return {
      ids: PricesHistoryUIContext.ids,
      queryParams: PricesHistoryUIContext.queryParams,
    };
  }, [PricesHistoryUIContext]);

  // PricesHistory Redux state
  const { PricesHistory } = useSelector(
    (state) => ({
      PricesHistory: selectedPricesHistory(state.PricesHistory.entities, PricesHistoryUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!PricesHistoryUIProps.ids || PricesHistoryUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PricesHistoryUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {PricesHistory.map((PriceHistory) => (
              <div className="list-timeline-item mb-3" key={PriceHistory.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      PricesHistorytatusCssClasses[PriceHistory.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {PriceHistory.id}
                  </span>{" "}
                  <span className="ml-5">
                    {PriceHistory.manufacture}, {PriceHistory.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
