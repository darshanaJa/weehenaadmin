import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { PricesHistorytatusCssClasses } from "../PricesHistoryUIHelpers";
import * as actions from "../../../_redux/PriceHistory/PricesHistoryActions";
import { usePricesHistoryUIContext } from "../PricesHistoryUIContext";

const selectedPricesHistory = (entities, ids) => {
  const _PricesHistory = [];
  ids.forEach((id) => {
    const PriceHistory = entities.find((el) => el.id === id);
    if (PriceHistory) {
      _PricesHistory.push(PriceHistory);
    }
  });
  return _PricesHistory;
};

export function PricesHistoryUpdateStatusDialog({ show, onHide }) {
  // PricesHistory UI Context
  const PricesHistoryUIContext = usePricesHistoryUIContext();
  const PricesHistoryUIProps = useMemo(() => {
    return {
      ids: PricesHistoryUIContext.ids,
      setIds: PricesHistoryUIContext.setIds,
      queryParams: PricesHistoryUIContext.queryParams,
    };
  }, [PricesHistoryUIContext]);

  // PricesHistory Redux state
  const { PricesHistory, isLoading } = useSelector(
    (state) => ({
      PricesHistory: selectedPricesHistory(state.PricesHistory.entities, PricesHistoryUIProps.ids),
      isLoading: state.PricesHistory.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected PricesHistory we should close modal
  useEffect(() => {
    if (PricesHistoryUIProps.ids || PricesHistoryUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PricesHistoryUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing PriceHistory by ids
    dispatch(actions.updatePricesHistoryStatus(PricesHistoryUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchPricesHistory(PricesHistoryUIProps.queryParams)).then(
          () => {
            // clear selections list
            PricesHistoryUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected PricesHistory
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {PricesHistory.map((PriceHistory) => (
              <div className="list-timeline-item mb-3" key={PriceHistory.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      PricesHistorytatusCssClasses[PriceHistory.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {PriceHistory.id}
                  </span>{" "}
                  <span className="ml-5">
                    {PriceHistory.manufacture}, {PriceHistory.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${PricesHistorytatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
