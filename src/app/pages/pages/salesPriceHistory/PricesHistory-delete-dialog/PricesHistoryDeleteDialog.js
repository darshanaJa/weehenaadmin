/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/PriceHistory/PricesHistoryActions";
import { usePricesHistoryUIContext } from "../PricesHistoryUIContext";

export function PricesHistoryDeleteDialog({ show, onHide }) {
  // PricesHistory UI Context
  const PricesHistoryUIContext = usePricesHistoryUIContext();
  const PricesHistoryUIProps = useMemo(() => {
    return {
      ids: PricesHistoryUIContext.ids,
      setIds: PricesHistoryUIContext.setIds,
      queryParams: PricesHistoryUIContext.queryParams,
    };
  }, [PricesHistoryUIContext]);

  // PricesHistory Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.PricesHistory.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected PricesHistory we should close modal
  useEffect(() => {
    if (!PricesHistoryUIProps.ids || PricesHistoryUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PricesHistoryUIProps.ids]);

  const deletePricesHistory = () => {
    // server request for deleting PriceHistory by seleted ids
    dispatch(actions.deletePricesHistory(PricesHistoryUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchPricesHistory(PricesHistoryUIProps.queryParams)).then(() => {
        // clear selections list
        PricesHistoryUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          PricesHistory Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected PricesHistory?</span>
        )}
        {isLoading && <span>PricesHistory are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deletePricesHistory}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
