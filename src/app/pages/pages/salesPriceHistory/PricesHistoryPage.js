import React from "react";
import { Route } from "react-router-dom";
import { PricesHistoryLoadingDialog } from "./PricesHistory-loading-dialog/PricesHistoryLoadingDialog";
import { PriceHistoryDeleteDialog } from "./PriceHistory-delete-dialog/PriceHistoryDeleteDialog";
import { PricesHistoryDeleteDialog } from "./PricesHistory-delete-dialog/PricesHistoryDeleteDialog";
import { PricesHistoryFetchDialog } from "./PricesHistory-fetch-dialog/PricesHistoryFetchDialog";
import { PricesHistoryUpdateStatusDialog } from "./PricesHistory-update-status-dialog/PricesHistoryUpdateStatusDialog";
import { PricesHistoryCard } from "./PricesHistoryCard";
import { PricesHistoryUIProvider } from "./PricesHistoryUIContext";

export function PricesHistoryPage({ history }) {
  const PricesHistoryUIEvents = {
    newPriceHistoryButtonClick: () => {
      history.push("/stocks/pricehistory/new");
    },
    openEditPriceHistoryPage: (id) => {
      history.push(`/stocks/pricehistory/${id}/edit`);
    },
    openDeletePriceHistoryDialog: (id) => {
      history.push(`/stocks/pricehistory/${id}/delete`);
    },
    openDeletePricesHistoryDialog: () => {
      history.push(`/stocks/pricehistory/deletePricesHistory`);
    },
    openFetchPricesHistoryDialog: () => {
      history.push(`/stocks/pricehistory/fetch`);
    },
    openUpdatePricesHistoryStatusDialog: () => {
      history.push("/stocks/pricehistory/updateStatus");
    },
  };

  return (
    <PricesHistoryUIProvider PricesHistoryUIEvents={PricesHistoryUIEvents}>
      <PricesHistoryLoadingDialog />
      <Route path="/stocks/pricehistory/deletePricesHistory">
        {({ history, match }) => (
          <PricesHistoryDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/pricehistory");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/pricehistory/:id/delete">
        {({ history, match }) => (
          <PriceHistoryDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/stocks/pricehistory");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/pricehistory/fetch">
        {({ history, match }) => (
          <PricesHistoryFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/pricehistory");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/pricehistory/updateStatus">
        {({ history, match }) => (
          <PricesHistoryUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/pricehistory");
            }}
          />
        )}
      </Route>
      <PricesHistoryCard />
    </PricesHistoryUIProvider>
  );
}