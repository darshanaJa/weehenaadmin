import React from "react";
import {
  MainStockConditionCssClasses,
  MainStockConditionTitles
} from "../../MainStocksUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        MainStockConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        MainStockConditionCssClasses[row.condition]
      }`}
    >
      {MainStockConditionTitles[row.condition]}
    </span>
  </>
);
