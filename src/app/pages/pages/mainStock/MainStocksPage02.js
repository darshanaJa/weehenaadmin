import React from "react";
import { Route } from "react-router-dom";
// import { MainStocksLoadingDialog } from "./mainStocks-loading-dialog/MainStocksLoadingDialog";
import { MainStockDeleteDialog } from "./mainStock-delete-dialog/MainStockDeleteDialog";
import { MainStocksDeleteDialog } from "./mainStocks-delete-dialog/MainStocksDeleteDialog";
import { MainStocksFetchDialog } from "./mainStocks-fetch-dialog/MainStocksFetchDialog";
import { MainStocksUpdateStatusDialog } from "./mainStocks-update-status-dialog/MainStocksUpdateStatusDialog";
import { MainStocksCard02 } from "./MainStocksCard02";
import { MainStocksUIProvider } from "./MainStocksUIContext";
// import { MainStocksTable } from "./MainStockReportTable/MainStocksTable";


export function MainStocksPage02({ history }) {
  const MainStocksUIEvents = {
    newMainStockButtonClick: () => {
      history.push("/stocks/mainstocks");
    },
    reportMainStockButtonClick: () => {
      history.push("/stocks/mainstocks/report");
    },
    // openEditMainStockPage: (id) => {
    //   history.push(`/stocks/mainstocks/${id}/edit`);
    // },
    // openDeleteMainStockDialog: (id) => {
    //   history.push(`/stocks/mainstocks/${id}/delete`);
    // },
    // openDeleteMainStocksDialog: () => {
    //   history.push(`/stocks/mainstocks/deleteMainStocks`);
    // },
    // openFetchMainStocksDialog: () => {
    //   history.push(`/stocks/mainstocks/fetch`);
    // },
    // openUpdateMainStocksStatusDialog: () => {
    //   history.push("/stocks/mainstocks/updateStatus");
    // },
  };

  return (
    <MainStocksUIProvider MainStocksUIEvents={MainStocksUIEvents}>
      {/* <MainStocksLoadingDialog /> */}
      {/* <Route path="/stocks/mainstocks/report">
        {({ history, match }) => (
          <MainStocksTable
            show={match != null}
            onHide={() => {
              history.push("/stocks/mainstocks");
            }}
          />
        )}
      </Route> */}
      <Route path="/stocks/mainstocks/deleteMainStocks">
        {({ history, match }) => (
          <MainStocksDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/mainstocks/report");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/mainstocks/:id/delete">
        {({ history, match }) => (
          <MainStockDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/stocks/mainstocks/report");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/mainstocks/fetch">
        {({ history, match }) => (
          <MainStocksFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/mainstocks/report");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/mainstocks/updateStatus">
        {({ history, match }) => (
          <MainStocksUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/mainstocks/report");
            }}
          />
        )}
      </Route>
      <MainStocksCard02 />
      {/* <MainStocksTable /> */}
    </MainStocksUIProvider>
  );
}
