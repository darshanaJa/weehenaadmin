import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { MainStockstatusCssClasses } from "../MainStocksUIHelpers";
import { useMainStocksUIContext } from "../MainStocksUIContext";

const selectedMainStocks = (entities, ids) => {
  const _MainStocks = [];
  ids.forEach((id) => {
    const MainStock = entities.find((el) => el.id === id);
    if (MainStock) {
      _MainStocks.push(MainStock);
    }
  });
  return _MainStocks;
};

export function MainStocksFetchDialog({ show, onHide }) {
  // MainStocks UI Context
  const MainStocksUIContext = useMainStocksUIContext();
  const MainStocksUIProps = useMemo(() => {
    return {
      ids: MainStocksUIContext.ids,
      queryParams: MainStocksUIContext.queryParams,
    };
  }, [MainStocksUIContext]);

  // MainStocks Redux state
  const { MainStocks } = useSelector(
    (state) => ({
      MainStocks: selectedMainStocks(state.MainStocks.entities, MainStocksUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!MainStocksUIProps.ids || MainStocksUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [MainStocksUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {MainStocks.map((MainStock) => (
              <div className="list-timeline-item mb-3" key={MainStock.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      MainStockstatusCssClasses[MainStock.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {MainStock.id}
                  </span>{" "}
                  <span className="ml-5">
                    {MainStock.manufacture}, {MainStock.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
