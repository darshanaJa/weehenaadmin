import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./MainStocksUIHelpers";

const MainStocksUIContext = createContext();

export function useMainStocksUIContext() {
  return useContext(MainStocksUIContext);
}

export const MainStocksUIConsumer = MainStocksUIContext.Consumer;

export function MainStocksUIProvider({ MainStocksUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    reportMainStockButtonClick:MainStocksUIEvents.reportMainStockButtonClick,
    newMainStockButtonClick: MainStocksUIEvents.newMainStockButtonClick,
    openEditMainStockPage: MainStocksUIEvents.openEditMainStockPage,
    openDeleteMainStockDialog: MainStocksUIEvents.openDeleteMainStockDialog,
    openDeleteMainStocksDialog: MainStocksUIEvents.openDeleteMainStocksDialog,
    openFetchMainStocksDialog: MainStocksUIEvents.openFetchMainStocksDialog,
    openUpdateMainStocksStatusDialog: MainStocksUIEvents.openUpdateMainStocksStatusDialog,
  };

  return (
    <MainStocksUIContext.Provider value={value}>
      {children}
    </MainStocksUIContext.Provider>
  );
}
