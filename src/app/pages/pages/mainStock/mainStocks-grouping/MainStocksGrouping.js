import React, { useMemo } from "react";
import { useMainStocksUIContext } from "../MainStocksUIContext";

export function MainStocksGrouping() {
  // MainStocks UI Context
  const MainStocksUIContext = useMainStocksUIContext();
  const MainStocksUIProps = useMemo(() => {
    return {
      ids: MainStocksUIContext.ids,
      setIds: MainStocksUIContext.setIds,
      openDeleteMainStocksDialog: MainStocksUIContext.openDeleteMainStocksDialog,
      openFetchMainStocksDialog: MainStocksUIContext.openFetchMainStocksDialog,
      openUpdateMainStocksStatusDialog:
        MainStocksUIContext.openUpdateMainStocksStatusDialog,
    };
  }, [MainStocksUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{MainStocksUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={MainStocksUIProps.openDeleteMainStocksDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={MainStocksUIProps.openFetchMainStocksDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={MainStocksUIProps.openUpdateMainStocksStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
