import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { MainStockstatusCssClasses } from "../MainStocksUIHelpers";
import * as actions from "../../../_redux/MainStock/MainStocksActions";
import { useMainStocksUIContext } from "../MainStocksUIContext";

const selectedMainStocks = (entities, ids) => {
  const _MainStocks = [];
  ids.forEach((id) => {
    const MainStock = entities.find((el) => el.id === id);
    if (MainStock) {
      _MainStocks.push(MainStock);
    }
  });
  return _MainStocks;
};

export function MainStocksUpdateStatusDialog({ show, onHide }) {
  // MainStocks UI Context
  const MainStocksUIContext = useMainStocksUIContext();
  const MainStocksUIProps = useMemo(() => {
    return {
      ids: MainStocksUIContext.ids,
      setIds: MainStocksUIContext.setIds,
      queryParams: MainStocksUIContext.queryParams,
    };
  }, [MainStocksUIContext]);

  // MainStocks Redux state
  const { MainStocks, isLoading } = useSelector(
    (state) => ({
      MainStocks: selectedMainStocks(state.MainStocks.entities, MainStocksUIProps.ids),
      isLoading: state.MainStocks.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected MainStocks we should close modal
  useEffect(() => {
    if (MainStocksUIProps.ids || MainStocksUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [MainStocksUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing MainStock by ids
    dispatch(actions.updateMainStocksStatus(MainStocksUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchMainStocks(MainStocksUIProps.queryParams)).then(
          () => {
            // clear selections list
            MainStocksUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected MainStocks
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {MainStocks.map((MainStock) => (
              <div className="list-timeline-item mb-3" key={MainStock.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      MainStockstatusCssClasses[MainStock.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {MainStock.id}
                  </span>{" "}
                  <span className="ml-5">
                    {MainStock.manufacture}, {MainStock.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${MainStockstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
