import React from "react";
import {
  MainStockstatusCssClasses,
  MainStockstatusTitles
} from "../../MainStocksUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      MainStockstatusCssClasses[row.status]
    } label-inline`}
  >
    {MainStockstatusTitles[row.status]}
  </span>
);
