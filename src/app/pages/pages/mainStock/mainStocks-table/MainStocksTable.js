// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/MainStock/MainStocksActions";
import * as uiHelpers from "../MainStocksUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useMainStocksUIContext } from "../MainStocksUIContext";

export function MainStocksTable() {
  // MainStocks UI Context
  const MainStocksUIContext = useMainStocksUIContext();
  const MainStocksUIProps = useMemo(() => {
    return {
      ids: MainStocksUIContext.ids,
      setIds: MainStocksUIContext.setIds,
      queryParams: MainStocksUIContext.queryParams,
      setQueryParams: MainStocksUIContext.setQueryParams,
      openEditMainStockPage: MainStocksUIContext.openEditMainStockPage,
      openDeleteMainStockDialog: MainStocksUIContext.openDeleteMainStockDialog,
    };
  }, [MainStocksUIContext]);

  // Getting curret state of MainStocks list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.MainStocks }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // MainStocks Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    MainStocksUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchMainStocks(MainStocksUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [MainStocksUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    // {
    //   dataField: "id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "item_name",
      text: "Item Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "item_default_price",
      text: "Default Price",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "item_quantity",
      text: "Quantity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "item_current_quantity",
      text: "Current Quantity",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "mstock_description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "item_registered_by",
      text: "Register By",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "item_registered_date",
      text: "Registerd Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "mainstock_updated_date",
      text: "Update Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditMainStockPage: MainStocksUIProps.openEditMainStockPage,
        openDeleteMainStockDialog: MainStocksUIProps.openDeleteMainStockDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: MainStocksUIProps.queryParams.pageSize,
    page: MainStocksUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  MainStocksUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: MainStocksUIProps.ids,
                  setIds: MainStocksUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
