import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { MainStocks_URL_GET } from "../../../_redux/MainStock/MainStocksCrud";
import axios from "axios";
import { useSelector } from "react-redux";
import { notifyWarning } from "../../../../config/Toastify";

// Validation schema
const MainStockEditSchema = Yup.object().shape({
    // id: Yup.string(),
  item_name: Yup.string()
  .min(2, "Minimum 2 symbols")
  .max(50, "Maximum 50 symbols")
  .required("item Name is required"),
  item_default_price: Yup.number()
  .required("Default Price is required"),
  item_quantity: Yup.number()
  .required("Quantity is required"),
  // .readOnly,
  item_current_quantity: Yup.number().required("Current Quantity is required"),
  mstock_description: Yup.string().required("Description is required")
   
});

export function MainStockCreateForm({
  MainStock,
  btnRef,
  saveMainStock,
}) {

  const newEntities = useSelector(state => (state.MainStocks.entities))
  console.log(newEntities)

  const [shop,setShop]=useState([]);
  const [itemName,setItemName]=useState('');

  console.log(shop)

  let newItemName

  if(newEntities !== null){
     newItemName = newEntities.find(entity => entity.item_name===itemName)

    console.log(newItemName)
    if(newItemName){
      notifyWarning("Already Taken Stock");
    }

  }

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
      });
      
  },[])

  
  return (
    <>
      {notifyWarning()}
      <Formik
        enableReinitialize={true}
        initialValues={MainStock}
        validationSchema={MainStockEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveMainStock(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="item_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                    onKeyUp={(e)=>{setItemName(e.target.value)}}
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="item_default_price"
                    component={Input}
                    placeholder="Default Price"
                    label="Default Price"
                  />
                </div>
              </div>
              <div className="form-group row">
              <div className="col-lg-4">
                  <Field
                    type="text"
                    name="item_quantity"
                    component={Input}
                    placeholder="Quantity"
                    label="Quantity"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                      type="number"
                      name="item_current_quantity"
                      component={Input}
                      placeholder="Current Quantity"
                      label="Current Quantity"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-8">
                  {/* <label>Description</label> */}
                  <Field
                    type="text"
                    placeholder="Description"
                    component={Input}
                    label="Description"
                    name="mstock_description"
                    // as="textarea"
                    
                  />
                </div>

                  <div className="col-lg-4">
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                    <button type="submit" 
                    hidden={newItemName}
                     className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
              </div>

              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
