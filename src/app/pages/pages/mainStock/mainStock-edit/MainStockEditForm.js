import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { useSelector } from "react-redux";
import { notifyWarning } from "../../../../config/Toastify";
import Admin from "../../../../config/Admin";

// Validation schema
const MainStockEditSchema = Yup.object().shape({
  item_name: Yup.string()
  .min(2, "Minimum 2 symbols")
  .max(50, "Maximum 50 symbols")
  .required("item Name is required"),
  item_default_price: Yup.number()
  .required("Default Price is required"),
  item_quantity: Yup.number()
  .required("Quantity is required"),
  item_current_quantity: Yup.number().required("Current Quantity is required"),
  mstock_description: Yup.string().required("Description is required")
   
});

export function MainStockEditForm({
  MainStock,
  btnRef,
  saveMainStock,
}) {

  const [itemName,setItemName]=useState('');

  const newEntities = useSelector(state => (state.MainStocks.entities))
  console.log(newEntities)

  let newItem

  if(newEntities !== null){
    const newItemName = newEntities.filter(entity => entity.item_name!==MainStock.item_name)
    newItem = newItemName.find(entity => entity.item_name===itemName)
    console.log(newItemName)
    console.log(newItem)
   if(newItem){
     notifyWarning("Already Taken Stock");
   }

 }
  
  return (
    <>
      {notifyWarning()}
      <Formik
        enableReinitialize={true}
        initialValues={MainStock}
        validationSchema={MainStockEditSchema}
        onSubmit={(values) => {
          console.log(values)
          saveMainStock(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    name="item_id"
                    component={Input}
                    placeholder="ID"
                    label="ID"
                    readonly="readonly"
                    
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="item_name"
                    component={Input}
                    placeholder="Item Name"
                    label="Item Name"
                    onKeyUp={(e)=>{setItemName(e.target.value)}}
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="item_default_price"
                    component={Input}
                    placeholder="Default Price"
                    label="Default Price"
                  />
                </div>
              </div>
              <div className="form-group row">
              <div className="col-lg-4">
                  <Field
                    type="text"
                    name="item_quantity"
                    component={Input}
                    placeholder="Quantity"
                    label="Quantity"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                      type="number"
                      name="item_current_quantity"
                      component={Input}
                      placeholder="Current Quantity"
                      label="Current Quantity"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-8">
                  {/* <label>Description</label> */}
                  <Field
                    type="text"
                    placeholder="Description"
                    component={Input}
                    label="Description"
                    name="mstock_description"
                    // as="textarea"
                    
                  />
                </div>

                  <div className="col-lg-4">
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                    <button type="submit"
                     hidden={newItem}
                     className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={MainStock.item_registered_by}
                  createDate={MainStock.item_registered_date} 
                  updateDate={MainStock.mainstock_updated_date} 
                />
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
