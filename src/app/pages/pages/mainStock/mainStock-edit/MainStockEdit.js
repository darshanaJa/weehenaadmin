/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/MainStock/MainStocksActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { MainStockEditForm } from "./MainStockEditForm";
import { Specifications } from "../mainStock-specifications/Specifications";
import { SpecificationsUIProvider } from "../mainStock-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../mainStock-remarks/RemarksUIContext";
import { Remarks } from "../mainStock-remarks/Remarks";
import { MainStockCreateForm } from "./MainStockCreateForm";

const initMainStock = {
  // id : "",
  item_name : "",
  item_default_price : "",
  item_quantity : 75,
  item_current_quantity : 5,
  mstock_description : "aaaaaa"
};


export function MainStockEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, MainStockForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.MainStocks.actionsLoading,
      MainStockForEdit: state.MainStocks.MainStockForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchMainStock(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New MainStock";
    if (MainStockForEdit && id) {
      // _title = `Edit MainStock - ${MainStockForEdit.item_name} ${MainStockForEdit.item_default_price} - ${MainStockForEdit.item_name}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [MainStockForEdit, id]);

  const saveMainStock = (values) => {
    if (!id) {
      dispatch(actions.createMainStock(values)).then(() => backToMainStocksList());
      // {notify("create")}
    } else {
      dispatch(actions.updateMainStock(values)).then(() => backToMainStocksList());
    }
  };

  const btnRef = useRef();

  const backToMainStocksList = () => {
    history.push(`/stocks/mainstocks`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToMainStocksList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <MainStockEditForm
                actionsLoading={actionsLoading}
                MainStock={MainStockForEdit || initMainStock}
                btnRef={btnRef}
                saveMainStock={saveMainStock}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentMainStockId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentMainStockId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToMainStocksList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    MainStock remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    MainStock specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <MainStockCreateForm
                actionsLoading={actionsLoading}
                MainStock={MainStockForEdit || initMainStock}
                btnRef={btnRef}
                saveMainStock={saveMainStock}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentMainStockId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentMainStockId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }
}
