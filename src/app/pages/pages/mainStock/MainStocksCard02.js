import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { MainStocksFilter } from "./mainStocks-filter/MainStocksFilter";
// import { MainStocksTable } from "./mainStocks-table/MainStocksTable";
import { MainStocksGrouping } from "./mainStocks-grouping/MainStocksGrouping";
import { useMainStocksUIContext } from "./MainStocksUIContext";
import { notify } from "../../../config/Toastify";
import { MainStocksTable1 } from "./MainStockReportTable/MainStocksTable";

export function MainStocksCard02() {

 

    // const [name,setName] = useState()
  
    // setName(msg)
    // console.log(msg)
    // notify(msg)
  
    // useEffect(()=>{
    //   console.log(msg)
    //   // notify("abcd")
    // },[])

  const MainStocksUIContext = useMainStocksUIContext();
  const MainStocksUIProps = useMemo(() => {
    return {
      ids: MainStocksUIContext.ids,
      queryParams: MainStocksUIContext.queryParams,
      setQueryParams: MainStocksUIContext.setQueryParams,
      newMainStockButtonClick: MainStocksUIContext.newMainStockButtonClick,
      openDeleteMainStocksDialog: MainStocksUIContext.openDeleteMainStocksDialog,
      openEditMainStockPage: MainStocksUIContext.openEditMainStockPage,
      openUpdateMainStocksStatusDialog:MainStocksUIContext.openUpdateMainStocksStatusDialog,
      openFetchMainStocksDialog: MainStocksUIContext.openFetchMainStocksDialog,
      reportMainStockButtonClick:MainStocksUIContext.reportMainStockButtonClick,
    };
  }, [MainStocksUIContext]);



  return (
    <Card>
      <CardHeader title="Report list">
        <CardHeaderToolbar>
        {notify()}
          {/* <button
            type="button"
            className="btn btn-primary"
            onClick={MainStocksUIProps.reportMainStockButtonClick}
          >
            Main Stock Report
          </button> */}
          <></>
          <button
            type="button"
            // className="btn btn-primary"
            className="btn btn-light"
            onClick={MainStocksUIProps.newMainStockButtonClick}
          >
            {/* Back */}
            <i className="fa fa-arrow-left"></i>
              Back
            </button>
          {/* </button> */}
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <MainStocksFilter />
        {MainStocksUIProps.ids.length > 0 && (
          <>
            <MainStocksGrouping />
          </>
        )}
        {/* <MainStocksTable /> */}
        <MainStocksTable1 />
      </CardBody>
    </Card>
  );
}
