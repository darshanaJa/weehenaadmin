import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { SpareSuppliersFilter } from "./SpareSuppliers-filter/SpareSuppliersFilter";
import { SpareSuppliersTable } from "./SpareSuppliers-table/SpareSuppliersTable";
import { SpareSuppliersGrouping } from "./SpareSuppliers-grouping/SpareSuppliersGrouping";
import { useSpareSuppliersUIContext } from "./SpareSuppliersUIContext";
import { notify } from "../../../config/Toastify";

export function SpareSuppliersCard(msg){

  // const [name,setName] = useState()

  // setName(msg)
  // console.log(msg)

  // useEffect(()=>{
  //   console.log(msg)
  //   // notify("abcd")
  // },[])

  const SpareSuppliersUIContext = useSpareSuppliersUIContext();
  const SpareSuppliersUIProps = useMemo(() => {
    return {
      ids: SpareSuppliersUIContext.ids,
      queryParams: SpareSuppliersUIContext.queryParams,
      setQueryParams: SpareSuppliersUIContext.setQueryParams,
      newSpareSupplierButtonClick: SpareSuppliersUIContext.newSpareSupplierButtonClick,
      openDeleteSpareSuppliersDialog: SpareSuppliersUIContext.openDeleteSpareSuppliersDialog,
      openEditSpareSupplierPage: SpareSuppliersUIContext.openEditSpareSupplierPage,
      openUpdateSpareSuppliersStatusDialog:SpareSuppliersUIContext.openUpdateSpareSuppliersStatusDialog,
      openFetchSpareSuppliersDialog: SpareSuppliersUIContext.openFetchSpareSuppliersDialog,
    };
  }, [SpareSuppliersUIContext]);

  return (
    <>
    <Card>
      {notify()}
      <CardHeader title="Spare Part Supplier list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={SpareSuppliersUIProps.newSpareSupplierButtonClick}
          >
            New Spare Part
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <SpareSuppliersFilter />
        {SpareSuppliersUIProps.ids.length > 0 && (
          <>
            <SpareSuppliersGrouping />
          </>
        )}
        <SpareSuppliersTable />
      </CardBody>
    </Card>
    </>
  );
}
