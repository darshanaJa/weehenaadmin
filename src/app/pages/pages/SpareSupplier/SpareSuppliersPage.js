import React from "react";
import { Route } from "react-router-dom";
import { SpareSuppliersLoadingDialog } from "./SpareSuppliers-loading-dialog/SpareSuppliersLoadingDialog";
import { SpareSupplierDeleteDialog } from "./SpareSupplier-delete-dialog/SpareSupplierDeleteDialog";
import { SpareSuppliersDeleteDialog } from "./SpareSuppliers-delete-dialog/SpareSuppliersDeleteDialog";
import { SpareSuppliersFetchDialog } from "./SpareSuppliers-fetch-dialog/SpareSuppliersFetchDialog";
import { SpareSuppliersUpdateStatusDialog } from "./SpareSuppliers-update-status-dialog/SpareSuppliersUpdateStatusDialog";
import { SpareSuppliersCard } from "./SpareSuppliersCard";
import { SpareSuppliersUIProvider } from "./SpareSuppliersUIContext";
import { notify } from "../../../config/Toastify";

// window.location.reload()

export function SpareSuppliersPage({ history }) {
  const SpareSuppliersUIEvents = {
    newSpareSupplierButtonClick: () => {
      history.push("/suplier/sparepartsuplier/new");
    },
    openEditSpareSupplierPage: (id) => {
      history.push(`/suplier/sparepartsuplier/${id}/edit`);
    },
    openDeleteSpareSupplierDialog: (sales_SpareSupplier_id) => {
      history.push(`/suplier/sparepartsuplier/${sales_SpareSupplier_id}/delete`);
    },
    openDeleteSpareSuppliersDialog: () => {
      history.push(`/suplier/sparepartsuplier/deleteSpareSuppliers`);
    },
    openFetchSpareSuppliersDialog: () => {
      history.push(`/suplier/sparepartsuplier/fetch`);
    },
    openUpdateSpareSuppliersStatusDialog: () => {
      history.push("/suplier/sparepartsuplier/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <SpareSuppliersUIProvider SpareSuppliersUIEvents={SpareSuppliersUIEvents}>
      <SpareSuppliersLoadingDialog />
      <Route path="/suplier/sparepartsuplier/deleteSpareSuppliers">
        {({ history, match }) => (
          <SpareSuppliersDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/suplier/sparepartsuplier");
            }}
          />
        )}
      </Route>
      <Route path="/suplier/sparepartsuplier/:sales_SpareSupplier_id/delete">
        {({ history, match }) => (
          <SpareSupplierDeleteDialog
            show={match != null}
            sales_SpareSupplier_id={match && match.params.sales_SpareSupplier_id}
            onHide={() => {
              history.push("/suplier/sparepartsuplier");
            }}
          />
        )}
      </Route>
      <Route path="/suplier/sparepartsuplier/fetch">
        {({ history, match }) => (
          <SpareSuppliersFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/suplier/sparepartsuplier");
            }}
          />
        )}
      </Route>
      <Route path="/suplier/sparepartsuplier/updateStatus">
        {({ history, match }) => (
          <SpareSuppliersUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/suplier/sparepartsuplier");
            }}
          />
        )}
      </Route>
      <SpareSuppliersCard />
    </SpareSuppliersUIProvider>
    </>
  );
}
