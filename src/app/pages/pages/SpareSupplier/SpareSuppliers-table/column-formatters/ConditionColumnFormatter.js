import React from "react";
import {
  SpareSupplierConditionCssClasses,
  SpareSupplierConditionTitles
} from "../../SpareSuppliersUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        SpareSupplierConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        SpareSupplierConditionCssClasses[row.condition]
      }`}
    >
      {SpareSupplierConditionTitles[row.condition]}
    </span>
  </>
);
