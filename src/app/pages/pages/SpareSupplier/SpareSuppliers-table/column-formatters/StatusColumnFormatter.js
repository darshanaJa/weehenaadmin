import React from "react";
import {
  SpareSupplierstatusCssClasses,
  SpareSupplierstatusTitles
} from "../../SpareSuppliersUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      SpareSupplierstatusCssClasses[row.status]
    } label-inline`}
  >
    {SpareSupplierstatusTitles[row.status]}
  </span>
);
