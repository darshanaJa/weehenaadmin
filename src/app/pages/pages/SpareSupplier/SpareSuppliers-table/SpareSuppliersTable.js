// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/SpareSupplier/SpareSuppliersActions";
import * as uiHelpers from "../SpareSuppliersUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useSpareSuppliersUIContext } from "../SpareSuppliersUIContext";

export function SpareSuppliersTable() {
  // SpareSuppliers UI Context
  const SpareSuppliersUIContext = useSpareSuppliersUIContext();
  const SpareSuppliersUIProps = useMemo(() => {
    return {
      ids: SpareSuppliersUIContext.ids,
      setIds: SpareSuppliersUIContext.setIds,
      queryParams: SpareSuppliersUIContext.queryParams,
      setQueryParams: SpareSuppliersUIContext.setQueryParams,
      openEditSpareSupplierPage: SpareSuppliersUIContext.openEditSpareSupplierPage,
      openDeleteSpareSupplierDialog: SpareSuppliersUIContext.openDeleteSpareSupplierDialog,
    };
  }, [SpareSuppliersUIContext]);

  // Getting curret state of SpareSuppliers list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.SpareSuppliers }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // SpareSuppliers Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    SpareSuppliersUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchSpareSuppliers(SpareSuppliersUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareSuppliersUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_SpareSupplier_id);

  const columns = [
    // {
    //   dataField: "sales_SpareSupplier_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "supplier_name",
      text: "Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "supplier_comp_name",
      text: "Company Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "supp_contact1",
      text: "Mobile 01",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "supp_contact2",
      text: "Mobile 02",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "supp_email",
      text: "Email",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    {
      dataField: "supp_address",
      text: "Address",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ConditionColumnFormatter,
    },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditSpareSupplierPage: SpareSuppliersUIProps.openEditSpareSupplierPage,
        openDeleteSpareSupplierDialog: SpareSuppliersUIProps.openDeleteSpareSupplierDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: SpareSuppliersUIProps.queryParams.pageSize,
    page: SpareSuppliersUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_SpareSupplier_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  SpareSuppliersUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: SpareSuppliersUIProps.ids,
                  setIds: SpareSuppliersUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
