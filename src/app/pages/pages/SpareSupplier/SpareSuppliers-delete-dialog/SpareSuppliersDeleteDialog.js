/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/SpareSupplier/SpareSuppliersActions";
import { useSpareSuppliersUIContext } from "../SpareSuppliersUIContext";

export function SpareSuppliersDeleteDialog({ show, onHide }) {
  // SpareSuppliers UI Context
  const SpareSuppliersUIContext = useSpareSuppliersUIContext();
  const SpareSuppliersUIProps = useMemo(() => {
    return {
      ids: SpareSuppliersUIContext.ids,
      setIds: SpareSuppliersUIContext.setIds,
      queryParams: SpareSuppliersUIContext.queryParams,
    };
  }, [SpareSuppliersUIContext]);

  // SpareSuppliers Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SpareSuppliers.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected SpareSuppliers we should close modal
  useEffect(() => {
    if (!SpareSuppliersUIProps.ids || SpareSuppliersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareSuppliersUIProps.ids]);

  const deleteSpareSuppliers = () => {
    // server request for deleting SpareSupplier by seleted ids
    dispatch(actions.deleteSpareSuppliers(SpareSuppliersUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSpareSuppliers(SpareSuppliersUIProps.queryParams)).then(() => {
        // clear selections list
        SpareSuppliersUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SpareSuppliers Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected SpareSuppliers?</span>
        )}
        {isLoading && <span>SpareSuppliers are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSpareSuppliers}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
