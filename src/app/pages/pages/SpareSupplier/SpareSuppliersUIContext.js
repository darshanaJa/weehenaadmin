import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./SpareSuppliersUIHelpers";

const SpareSuppliersUIContext = createContext();

export function useSpareSuppliersUIContext() {
  return useContext(SpareSuppliersUIContext);
}

export const SpareSuppliersUIConsumer = SpareSuppliersUIContext.Consumer;

export function SpareSuppliersUIProvider({ SpareSuppliersUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newSpareSupplierButtonClick: SpareSuppliersUIEvents.newSpareSupplierButtonClick,
    openEditSpareSupplierPage: SpareSuppliersUIEvents.openEditSpareSupplierPage,
    openDeleteSpareSupplierDialog: SpareSuppliersUIEvents.openDeleteSpareSupplierDialog,
    openDeleteSpareSuppliersDialog: SpareSuppliersUIEvents.openDeleteSpareSuppliersDialog,
    openFetchSpareSuppliersDialog: SpareSuppliersUIEvents.openFetchSpareSuppliersDialog,
    openUpdateSpareSuppliersStatusDialog: SpareSuppliersUIEvents.openUpdateSpareSuppliersStatusDialog,
  };

  return (
    <SpareSuppliersUIContext.Provider value={value}>
      {children}
    </SpareSuppliersUIContext.Provider>
  );
}
