import React, { useMemo } from "react";
import { useSpareSuppliersUIContext } from "../SpareSuppliersUIContext";

export function SpareSuppliersGrouping() {
  // SpareSuppliers UI Context
  const SpareSuppliersUIContext = useSpareSuppliersUIContext();
  const SpareSuppliersUIProps = useMemo(() => {
    return {
      ids: SpareSuppliersUIContext.ids,
      setIds: SpareSuppliersUIContext.setIds,
      openDeleteSpareSuppliersDialog: SpareSuppliersUIContext.openDeleteSpareSuppliersDialog,
      openFetchSpareSuppliersDialog: SpareSuppliersUIContext.openFetchSpareSuppliersDialog,
      openUpdateSpareSuppliersStatusDialog:
        SpareSuppliersUIContext.openUpdateSpareSuppliersStatusDialog,
    };
  }, [SpareSuppliersUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{SpareSuppliersUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={SpareSuppliersUIProps.openDeleteSpareSuppliersDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SpareSuppliersUIProps.openFetchSpareSuppliersDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SpareSuppliersUIProps.openUpdateSpareSuppliersStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
