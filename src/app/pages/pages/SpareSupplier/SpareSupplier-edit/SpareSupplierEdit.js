/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/SpareSupplier/SpareSuppliersActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { SpareSupplierEditForm } from "./SpareSupplierEditForm";
import { SpareSupplierCreateForm } from "./SpareSupplierCreateForm";
import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../SpareSupplier-specifications/Specifications";
import { SpecificationsUIProvider } from "../SpareSupplier-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../SpareSupplier-remarks/RemarksUIContext";
import { Remarks } from "../SpareSupplier-remarks/Remarks";
// import { SpecificationsTable } from "../SpareSupplier-specifications/SpecificationsTable";

const initSpareSupplier = {
  
  // sales_SpareSupplier_fname : "Darshana",
  // sales_SpareSupplier_lname : "aaaaaa",
  // sales_SpareSupplier_nic : "960520386V",
  // sales_SpareSupplier_mobile_1 : "0123456789",
  // sales_SpareSupplier_mobile_2 : "0123456789",
  // sales_SpareSupplier_email : "aa@gmail.com",
  // address:"matale",
  // password:"abcd1234",
  // profile_pic : ""

  supplier_name : "",
  supplier_comp_name : "",
  supp_contact1 : "",
  supp_contact2 : "",
  supp_email : "",
  sales_SpareSupplier_email : "",
  supp_address:"",
  // password:""
  // profile_pic : []

};

const initSpareSupplierPassword = {
  curent_password : "",
  new_password : "",
  confirm_password : ""

};

export function SpareSupplierEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, SpareSupplierForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.SpareSuppliers.actionsLoading,
      SpareSupplierForEdit: state.SpareSuppliers.SpareSupplierForEdit,
    }),
    shallowEqual
  );




  // useEffect(() => {
  //   return(<SpecificationsTable
  //     actionsLoading={actionsLoading}
  //     SpareSupplier={initSpareSupplierPassword}
  //     btnRef={btnRef}
  //     savePassword={savePassword}
  //   /> )
    
  // });

  

  useEffect(() => {
    dispatch(actions.fetchSpareSupplier(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New SpareSupplier";
    if (SpareSupplierForEdit && id) {
      // _title = `Edit SpareSupplier - ${SpareSupplierForEdit.sales_SpareSupplier_fname} ${SpareSupplierForEdit.sales_SpareSupplier_lname} - ${SpareSupplierForEdit.sales_SpareSupplier_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareSupplierForEdit, id]);

  const saveSpareSupplier = (values) => {
    if (!id) {
      dispatch(actions.createSpareSupplier(values,id)).then(() => backToSpareSuppliersList());
    } else {
      dispatch(actions.updateSpareSupplier(values,id)).then(() => backToSpareSuppliersList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateSpareSupplierPassword(values,id)).then(() => backToSpareSuppliersList());
  };

  const btnRef = useRef();  
  // const saveSpareSupplierClick = () => {
  //   if (btnRef && btnRef.current) {
  //     btnRef.current.click();
  //   }
  // };

  const backToSpareSuppliersList = () => {
    // history.push(`/sales/SpareSupplier`);
    history.push(`/suplier/sparepartsuplier`);
    // window.location.reload(`/sales/SpareSupplier`)
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSpareSuppliersList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {/* <button  className="btn btn-light ml-2">
              <i className="fa fa-redo"></i>
              Reset
            </button> */}
            {`  `}
            {/* <button
              type="submit"
              className="btn btn-primary ml-2"
              onClick={saveSpareSupplierClick}
            >
              Save
            </button> */}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                {/* <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    SpareSupplier remarks
                  </a>
                </li> */}
                {/* <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Reset Password
                  </a>
                </li> */}
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SpareSupplierEditForm
                actionsLoading={actionsLoading}
                SpareSupplier={SpareSupplierForEdit || initSpareSupplier}
                btnRef={btnRef}
                saveSpareSupplier={saveSpareSupplier}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSpareSupplierId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSpareSupplierId={id}>
                <PasswordReset
                  actionsLoading={actionsLoading}
                  SpareSupplier={initSpareSupplierPassword}
                  btnRef={btnRef}
                  savePassword={savePassword}
    />
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSpareSuppliersList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {/* <button  className="btn btn-light ml-2">
              <i className="fa fa-redo"></i>
              Reset
            </button> */}
            {`  `}
            {/* <button
              type="submit"
              className="btn btn-primary ml-2"
              onClick={saveSpareSupplierClick}
            >
              Save
            </button> */}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    SpareSupplier remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    SpareSupplier specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SpareSupplierCreateForm
                actionsLoading={actionsLoading}
                SpareSupplier={SpareSupplierForEdit || initSpareSupplier}
                btnRef={btnRef}
                saveSpareSupplier={saveSpareSupplier}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSpareSupplierId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSpareSupplierId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
