// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
// import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import Admin from "../../../../config/Admin";
// import { Image_Url } from "../../../../config/config";
// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   SpareSupplierstatusTitles,
//   SpareSupplierConditionTitles,
// } from "../SpareSuppliersUIHelpers";

// Validation schema
const SpareSupplierEditSchema = Yup.object().shape({
  // id: Yup.string(),
  supplier_name: Yup.string()
    .required("Name is required")
    .min(2, "Name must be at least 2 characters"),
    supplier_comp_name: Yup.string()
    .required("Name is required")
    .min(2, "Company Name must be at least 2 characters"),
    supp_address: Yup.string()
    .required("Address is required")
    .min(2, "Address must be at least 2 characters"),
    // password: Yup.string()
    // .required("Password is required")
    // .min(5, "Password must be at least 5 characters"),
    // sales_SpareSupplier_nic: Yup.string()
    // .required("SpareSupplier NIC is required")
    // .min(10, "NIC be at least 9 characters"),
    supp_email: Yup.string().email()
    .required("Email is required"),
    supp_contact1: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
    supp_contact2: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
  
  // profile_pic: Yup.string()
  // .required("Required")
});


export function SpareSupplierEditForm({
  SpareSupplier,
  btnRef,
  saveSpareSupplier,
}) {

  // console.log(SpareSupplier.profile_pic)

//   const submitImage=()=>{
//       return(
//         <img className="SpareSupplierImg" alt="SpareSupplier" src={url} />
//       );
//   }

//   const pic=SpareSupplier.profile_pic
//   console.log(pic)
//   const url = Image_Url+pic
//   console.log(url)



  // const bodyFormData = new FormData();

  // const [profile_pic,set_Profile_pic]=useState();

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={SpareSupplier}
        validationSchema={SpareSupplierEditSchema}
        onSubmit={(values)  => {
          // bodyFormData.append('sales_SpareSupplier_fname',values.sales_SpareSupplier_fname);
          // bodyFormData.append('sales_SpareSupplier_lname',values.sales_SpareSupplier_lname);
          // bodyFormData.append('sales_SpareSupplier_nic',values.sales_SpareSupplier_nic);
          // bodyFormData.append('sales_SpareSupplier_mobile_1',values.sales_SpareSupplier_mobile_1);
          // bodyFormData.append('sales_SpareSupplier_mobile_2',values.sales_SpareSupplier_mobile_2);
          // bodyFormData.append('sales_SpareSupplier_email',values.sales_SpareSupplier_email);
          // bodyFormData.append('password',values.password);
          // bodyFormData.append('address',values.address);
          // bodyFormData.append('profile_image',profile_pic);

          console.log(values);
          saveSpareSupplier(values);

        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                {/* <div className="col-lg-4">
                  <Field
                    name="id"
                    component={Input}
                    placeholder="ID"
                    label="ID"
                  />
                </div> */}
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="supplier_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="supplier_comp_name"
                    component={Input}
                    placeholder="Company Name"
                    label="Company Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                      type="email"
                      name="supp_email"
                      component={Input}
                      placeholder="Email"
                      label="Email"
                    />
                  </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="supp_contact1"
                      component={Input}
                      placeholder="Mobile 01"
                      label="Mobile 01"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="supp_contact2"
                    component={Input}
                    placeholder="Mobile 02"
                    label="Mobile 02"
                    // customFeedbackLabel="Please enter "
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="supp_address"
                    component={Input}
                    placeholder="address"
                    label="Address"
                  />
                </div>
              </div>

              
            <div className="form-group row">
                  {/* <div className="col-lg-4">
                    <div className="form-group row">
                      <input className="SpareSupplierImageBtn + dwnfile" type="file" name="profile_pic"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    </div>
                  </div>  */}
                  <div className="col-lg-4">
                    {/* <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button> */}
                    <button type="submit" className="btn btn-primary ml-2"> Save</button>
                  </div>
                  
                </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>

            <div>
                {/* <div className="form-group row">
                   <div className="col-lg-4">
                      {submitImage()}
                    </div>
                </div> */}
                {/* <div className="form-group row"> */}
                  {/* <div className="col-lg-4">
                    <label>Password</label>
                    <Field
                      name="sales_SpareSupplier_password"
                      className="form-control"
                    />
                </div>
                  <div className="col-lg-4">
                      <button type="submit" className='btn btn-primary ml-2 + downbtn'> Reset Password</button>
                  </div> */}

                {/* <div className="form-group row">
                   <div className="col-lg-4">
                      {submitImage()}
                    </div>
                </div> */}
             
                {/* </div> */}
            </div>

            <Admin 
              adminId={SpareSupplier.created_by}
              createDate={SpareSupplier.created_date} 
              updateDate={SpareSupplier.updated_date} 
            />

          </>
        )}
      </Formik>
    </>
  );
}
