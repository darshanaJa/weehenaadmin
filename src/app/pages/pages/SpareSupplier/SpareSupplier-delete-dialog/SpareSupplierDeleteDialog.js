/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/SpareSupplier/SpareSuppliersActions";
import { useSpareSuppliersUIContext } from "../SpareSuppliersUIContext";

export function SpareSupplierDeleteDialog({ sales_SpareSupplier_id, show, onHide }) {
  // console.log(sales_SpareSupplier_id)
  // SpareSuppliers UI Context
  const SpareSuppliersUIContext = useSpareSuppliersUIContext();
  const SpareSuppliersUIProps = useMemo(() => {
    return {
      setIds: SpareSuppliersUIContext.setIds,
      queryParams: SpareSuppliersUIContext.queryParams,
    };
  }, [SpareSuppliersUIContext]);

  // SpareSuppliers Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.SpareSuppliers.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_SpareSupplier_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_SpareSupplier_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteSpareSupplier = () => {
    // server request for deleting SpareSupplier by id
    dispatch(actions.deleteSpareSupplier(sales_SpareSupplier_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSpareSuppliers(SpareSuppliersUIProps.queryParams));
      // clear selections list
      SpareSuppliersUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          SpareSupplier Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this SpareSupplier?</span>
        )}
        {isLoading && <span>SpareSupplier is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSpareSupplier}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
