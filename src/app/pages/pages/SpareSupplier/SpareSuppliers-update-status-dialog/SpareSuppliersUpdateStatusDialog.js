import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { SpareSupplierstatusCssClasses } from "../SpareSuppliersUIHelpers";
import * as actions from "../../../_redux/SpareSupplier/SpareSuppliersActions";
import { useSpareSuppliersUIContext } from "../SpareSuppliersUIContext";

const selectedSpareSuppliers = (entities, ids) => {
  const _SpareSuppliers = [];
  ids.forEach((id) => {
    const SpareSupplier = entities.find((el) => el.id === id);
    if (SpareSupplier) {
      _SpareSuppliers.push(SpareSupplier);
    }
  });
  return _SpareSuppliers;
};

export function SpareSuppliersUpdateStatusDialog({ show, onHide }) {
  // SpareSuppliers UI Context
  const SpareSuppliersUIContext = useSpareSuppliersUIContext();
  const SpareSuppliersUIProps = useMemo(() => {
    return {
      ids: SpareSuppliersUIContext.ids,
      setIds: SpareSuppliersUIContext.setIds,
      queryParams: SpareSuppliersUIContext.queryParams,
    };
  }, [SpareSuppliersUIContext]);

  // SpareSuppliers Redux state
  const { SpareSuppliers, isLoading } = useSelector(
    (state) => ({
      SpareSuppliers: selectedSpareSuppliers(state.SpareSuppliers.entities, SpareSuppliersUIProps.ids),
      isLoading: state.SpareSuppliers.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected SpareSuppliers we should close modal
  useEffect(() => {
    if (SpareSuppliersUIProps.ids || SpareSuppliersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareSuppliersUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing SpareSupplier by ids
    dispatch(actions.updateSpareSuppliersStatus(SpareSuppliersUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchSpareSuppliers(SpareSuppliersUIProps.queryParams)).then(
          () => {
            // clear selections list
            SpareSuppliersUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected SpareSuppliers
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SpareSuppliers.map((SpareSupplier) => (
              <div className="list-timeline-item mb-3" key={SpareSupplier.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SpareSupplierstatusCssClasses[SpareSupplier.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SpareSupplier.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SpareSupplier.manufacture}, {SpareSupplier.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${SpareSupplierstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
