import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { SpareSupplierstatusCssClasses } from "../SpareSuppliersUIHelpers";
import { useSpareSuppliersUIContext } from "../SpareSuppliersUIContext";

const selectedSpareSuppliers = (entities, ids) => {
  const _SpareSuppliers = [];
  ids.forEach((id) => {
    const SpareSupplier = entities.find((el) => el.id === id);
    if (SpareSupplier) {
      _SpareSuppliers.push(SpareSupplier);
    }
  });
  return _SpareSuppliers;
};

export function SpareSuppliersFetchDialog({ show, onHide }) {
  // SpareSuppliers UI Context
  const SpareSuppliersUIContext = useSpareSuppliersUIContext();
  const SpareSuppliersUIProps = useMemo(() => {
    return {
      ids: SpareSuppliersUIContext.ids,
      queryParams: SpareSuppliersUIContext.queryParams,
    };
  }, [SpareSuppliersUIContext]);

  // SpareSuppliers Redux state
  const { SpareSuppliers } = useSelector(
    (state) => ({
      SpareSuppliers: selectedSpareSuppliers(state.SpareSuppliers.entities, SpareSuppliersUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!SpareSuppliersUIProps.ids || SpareSuppliersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SpareSuppliersUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {SpareSuppliers.map((SpareSupplier) => (
              <div className="list-timeline-item mb-3" key={SpareSupplier.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SpareSupplierstatusCssClasses[SpareSupplier.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {SpareSupplier.id}
                  </span>{" "}
                  <span className="ml-5">
                    {SpareSupplier.manufacture}, {SpareSupplier.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
