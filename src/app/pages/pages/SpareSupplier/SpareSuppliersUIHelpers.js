export const SpareSupplierstatusCssClasses = ["success", "info", ""];
export const SpareSupplierstatusTitles = ["Selling", "Sold"];
export const SpareSupplierConditionCssClasses = ["success", "danger", ""];
export const SpareSupplierConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_SpareSupplier_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    supplier_name: ""
  },
  supplier_name:null,
  sortOrder: "asc", // asc||desc
  sortField: "supplier_name",
  pageNumber: 1,
  pageSize: 10
  // value:
};
// export const AVAILABLE_COLORS = [
//   "Red",
//   "CadetBlue",
//   "Eagle",
//   "Gold",
//   "LightSlateGrey",
//   "RoyalBlue",
//   "Crimson",
//   "Blue",
//   "Sienna",
//   "Indigo",
//   "Green",
//   "Violet",
//   "GoldenRod",
//   "OrangeRed",
//   "Khaki",
//   "Teal",
//   "Purple",
//   "Orange",
//   "Pink",
//   "Black",
//   "DarkTurquoise"
// ];

// export const AVAILABLE_MANUFACTURES = [
//   "Pontiac",
//   "Kia",
//   "Lotus",
//   "Subaru",
//   "Jeep",
//   "Isuzu",
//   "Mitsubishi",
//   "Oldsmobile",
//   "Chevrolet",
//   "Chrysler",
//   "Audi",
//   "Suzuki",
//   "GMC",
//   "Cadillac",
//   "Infinity",
//   "Mercury",
//   "Dodge",
//   "Ram",
//   "Lexus",
//   "Lamborghini",
//   "Honda",
//   "Nissan",
//   "Ford",
//   "Hyundai",
//   "Saab",
//   "Toyota"
// ];
