import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./AgentsUIHelpers";

const AgentsUIContext = createContext();

export function useAgentsUIContext() {
  return useContext(AgentsUIContext);
}

export const AgentsUIConsumer = AgentsUIContext.Consumer;

export function AgentsUIProvider({ agentsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newAgentButtonClick: agentsUIEvents.newAgentButtonClick,
    openEditAgentPage: agentsUIEvents.openEditAgentPage,
    openDeleteAgentDialog: agentsUIEvents.openDeleteAgentDialog,
    openDeleteAgentsDialog: agentsUIEvents.openDeleteAgentsDialog,
    openFetchAgentsDialog: agentsUIEvents.openFetchAgentsDialog,
    openUpdateAgentsStatusDialog: agentsUIEvents.openUpdateAgentsStatusDialog,
  };

  return (
    <AgentsUIContext.Provider value={value}>
      {children}
    </AgentsUIContext.Provider>
  );
}
