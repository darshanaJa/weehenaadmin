export const AgentstatusCssClasses = ["success", "info", ""];
export const AgentstatusTitles = ["Selling", "Sold"];
export const AgentConditionCssClasses = ["success", "danger", ""];
export const AgentConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_agent_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    sales_agent_fname: ""
  },
  sales_agent_fname:null,
  sortOrder: "asc", // asc||desc
  sortField: "sales_agent_fname",
  pageNumber: 1,
  pageSize: 10
  // value:
};
// export const AVAILABLE_COLORS = [
//   "Red",
//   "CadetBlue",
//   "Eagle",
//   "Gold",
//   "LightSlateGrey",
//   "RoyalBlue",
//   "Crimson",
//   "Blue",
//   "Sienna",
//   "Indigo",
//   "Green",
//   "Violet",
//   "GoldenRod",
//   "OrangeRed",
//   "Khaki",
//   "Teal",
//   "Purple",
//   "Orange",
//   "Pink",
//   "Black",
//   "DarkTurquoise"
// ];

// export const AVAILABLE_MANUFACTURES = [
//   "Pontiac",
//   "Kia",
//   "Lotus",
//   "Subaru",
//   "Jeep",
//   "Isuzu",
//   "Mitsubishi",
//   "Oldsmobile",
//   "Chevrolet",
//   "Chrysler",
//   "Audi",
//   "Suzuki",
//   "GMC",
//   "Cadillac",
//   "Infinity",
//   "Mercury",
//   "Dodge",
//   "Ram",
//   "Lexus",
//   "Lamborghini",
//   "Honda",
//   "Nissan",
//   "Ford",
//   "Hyundai",
//   "Saab",
//   "Toyota"
// ];
