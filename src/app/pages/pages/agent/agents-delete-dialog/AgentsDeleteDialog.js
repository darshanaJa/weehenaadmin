/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/agent/agentsActions";
import { useAgentsUIContext } from "../AgentsUIContext";

export function AgentsDeleteDialog({ show, onHide }) {
  // Agents UI Context
  const agentsUIContext = useAgentsUIContext();
  const agentsUIProps = useMemo(() => {
    return {
      ids: agentsUIContext.ids,
      setIds: agentsUIContext.setIds,
      queryParams: agentsUIContext.queryParams,
    };
  }, [agentsUIContext]);

  // Agents Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Agents.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Agents we should close modal
  useEffect(() => {
    if (!agentsUIProps.ids || agentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [agentsUIProps.ids]);

  const deleteAgents = () => {
    // server request for deleting Agent by seleted ids
    dispatch(actions.deleteAgents(agentsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchAgents(agentsUIProps.queryParams)).then(() => {
        // clear selections list
        agentsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Agents Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Agents?</span>
        )}
        {isLoading && <span>Agents are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteAgents}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
