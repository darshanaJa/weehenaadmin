import React, { useMemo } from "react";
import { useAgentsUIContext } from "../AgentsUIContext";

export function AgentsGrouping() {
  // Agents UI Context
  const agentsUIContext = useAgentsUIContext();
  const agentsUIProps = useMemo(() => {
    return {
      ids: agentsUIContext.ids,
      setIds: agentsUIContext.setIds,
      openDeleteAgentsDialog: agentsUIContext.openDeleteAgentsDialog,
      openFetchAgentsDialog: agentsUIContext.openFetchAgentsDialog,
      openUpdateAgentsStatusDialog:
        agentsUIContext.openUpdateAgentsStatusDialog,
    };
  }, [agentsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{agentsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={agentsUIProps.openDeleteAgentsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={agentsUIProps.openFetchAgentsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={agentsUIProps.openUpdateAgentsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
