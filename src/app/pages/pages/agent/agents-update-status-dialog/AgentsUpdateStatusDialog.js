import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { AgentstatusCssClasses } from "../AgentsUIHelpers";
import * as actions from "../../../_redux/agent/agentsActions";
import { useAgentsUIContext } from "../AgentsUIContext";

const selectedAgents = (entities, ids) => {
  const _Agents = [];
  ids.forEach((id) => {
    const Agent = entities.find((el) => el.id === id);
    if (Agent) {
      _Agents.push(Agent);
    }
  });
  return _Agents;
};

export function AgentsUpdateStatusDialog({ show, onHide }) {
  // Agents UI Context
  const agentsUIContext = useAgentsUIContext();
  const agentsUIProps = useMemo(() => {
    return {
      ids: agentsUIContext.ids,
      setIds: agentsUIContext.setIds,
      queryParams: agentsUIContext.queryParams,
    };
  }, [agentsUIContext]);

  // Agents Redux state
  const { Agents, isLoading } = useSelector(
    (state) => ({
      Agents: selectedAgents(state.Agents.entities, agentsUIProps.ids),
      isLoading: state.Agents.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Agents we should close modal
  useEffect(() => {
    if (agentsUIProps.ids || agentsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [agentsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Agent by ids
    dispatch(actions.updateAgentsStatus(agentsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchAgents(agentsUIProps.queryParams)).then(
          () => {
            // clear selections list
            agentsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Agents
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Agents.map((Agent) => (
              <div className="list-timeline-item mb-3" key={Agent.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      AgentstatusCssClasses[Agent.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Agent.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Agent.manufacture}, {Agent.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${AgentstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
