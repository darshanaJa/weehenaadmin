// // import React, {useState,useEffect} from "react"
// import { compose, withProps } from "recompose"
// import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
// import { Api_Login } from '../../../../config/config';
// import axios from "axios";

// const agentLocation = Api_Login + '/api/sales-agent-location/search'
// const[department, setDepartment] = useState([]);

// const MyMapComponent = compose(

//   // useEffect(() => {
//   //     axios({
//   //       method: 'post',
//   //       baseURL: agentLocation,
//   //       data: {
//   //         "filter": [{"salesAgentSalesAgentId": "c21476e0-dfd9-4f13-bb89-d0d09cbc9605"}],
//   //         "limit": 10,
//   //         "skip": 0,
//   //         "sort":"DESC"
//   //       }
//   //       })
//   //       .then((res) => {
//   //         console.log(res.data.data.results[0].lattitude)
//   //         setDepartment(res.data.data.results[0])
//   //       })
//   //       .catch(function (response) {
//   //           // console.log(response);
//   //       });
//   //   },[]),



//   withProps({
//     googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
//     loadingElement: <div style={{ height: `100%` }} />,
//     containerElement: <div style={{ height: `400px` }} />,
//     mapElement: <div style={{ height: `100%` }} />,
//   }),
//   withScriptjs,
//   withGoogleMap
// )((props) =>
//   <GoogleMap
//     defaultZoom={8}
//     defaultCenter={[{ lat: -34.397, lng: 150.644 },{ lat: -34.397, lng: 130.644 }]}
//   >
//     {props.isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 },{ lat: -34.397, lng: 130.644 }} onClick={props.onMarkerClick} />}
//   </GoogleMap>
// )

// export default class MapContainer extends React.PureComponent {
//   state = {
//     isMarkerShown: false,
//   }

//   componentDidMount() {
//     this.delayedShowMarker()
//   }

//   delayedShowMarker = () => {
//     setTimeout(() => {
//       this.setState({ isMarkerShown: true })
//     }, 3000)
//   }

//   handleMarkerClick = () => {
//     this.setState({ isMarkerShown: false })
//     this.delayedShowMarker()
//   }

//   render() {
//     return (
//       <MyMapComponent
//         isMarkerShown={this.state.isMarkerShown}
//         onMarkerClick={this.handleMarkerClick}
//       />
//     )
//   }
// }

import React from 'react';
import {Map,Marker, GoogleApiWrapper} from 'google-maps-react';
// import { Api_Login } from '../../../../config/config';
// import axios from "axios";

// const agentLocation = Api_Login + '/api/sales-agent-location/search'

const containerStyle = {
  position: 'relative',  
  width: '750',
  height: '500px'
}

export function MapContainer({
  google,
  onMarkerClick,
  onInfoWindowClose,
  onMapClicked,
  id,
  lattitude,
  longitude,
  locationTitel,
  timelocation
}){

    return (
      <>
      {console.log("#######",lattitude,longitude)}
      {/* <Map 
        containerStyle={containerStyle}
        google={google} 
        initialCenter={{
          lat: 7.6584,
          lng: 79.9881
        }}
        zoom={12}
        onClick={onMapClicked}       
      > */}
      <Map google={google}
        // style={{width: '100%', height: '100%', position: 'relative'}}
        // className={'map'}
        initialCenter={{
          lat: 7.6584,
          lng: 79.9881
        }}
        containerStyle={containerStyle}
        zoom={10}>
        <Marker
          title={"Date - " + locationTitel + "  Time - " + timelocation}
          name={'SOMA'}
          position={{lat: lattitude, lng: longitude}} />
      
        {/* <Marker onClick={onMarkerClick}
                name={'Current location'} />
 
        <InfoWindow onClose={onInfoWindowClose}>
            <div>
              <h1>abcd</h1>
            </div>
        </InfoWindow> */}
      </Map>
  </>
    );
  // }
}
 
export default GoogleApiWrapper({
  apiKey: ("AIzaSyDEBjyqHLZe0x1zcSUcq0X9AHiYNTsUh1g")
})(MapContainer)