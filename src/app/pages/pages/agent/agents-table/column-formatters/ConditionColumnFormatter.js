import React from "react";
import {
  AgentConditionCssClasses,
  AgentConditionTitles
} from "../../AgentsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        AgentConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        AgentConditionCssClasses[row.condition]
      }`}
    >
      {AgentConditionTitles[row.condition]}
    </span>
  </>
);
