import React from "react";
import {
  AgentstatusCssClasses,
  AgentstatusTitles
} from "../../AgentsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      AgentstatusCssClasses[row.status]
    } label-inline`}
  >
    {AgentstatusTitles[row.status]}
  </span>
);
