// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/agent/agentsActions";
import * as uiHelpers from "../AgentsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useAgentsUIContext } from "../AgentsUIContext";

export function AgentsTable() {
  // Agents UI Context
  const agentsUIContext = useAgentsUIContext();
  const agentsUIProps = useMemo(() => {
    return {
      ids: agentsUIContext.ids,
      setIds: agentsUIContext.setIds,
      queryParams: agentsUIContext.queryParams,
      setQueryParams: agentsUIContext.setQueryParams,
      openEditAgentPage: agentsUIContext.openEditAgentPage,
      openDeleteAgentDialog: agentsUIContext.openDeleteAgentDialog,
    };
  }, [agentsUIContext]);

  // Getting curret state of Agents list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Agents }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Agents Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    agentsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchAgents(agentsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [agentsUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_agent_id);

  const columns = [
    // {
    //   dataField: "sales_agent_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "sales_agent_fname",
      text: "F Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "sales_agent_lname",
      text: "L Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "sales_agent_nic",
      text: "NIC",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "sales_agent_mobile_1",
      text: "Mobile 01",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "sales_agent_mobile_2",
      text: "Mobile 02",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "sales_agent_email",
      text: "Email",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    // {
    //   dataField: "sales_agent_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditAgentPage: agentsUIProps.openEditAgentPage,
        openDeleteAgentDialog: agentsUIProps.openDeleteAgentDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: agentsUIProps.queryParams.pageSize,
    page: agentsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_agent_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  agentsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: agentsUIProps.ids,
                  setIds: agentsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
