/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/agent/agentsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { AgentEditForm } from "./AgentEditForm";
import { AgentCreateForm } from "./AgentCreateForm";
import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../agent-specifications/Specifications";
import { SpecificationsUIProvider } from "../agent-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../agent-remarks/RemarksUIContext";
import MapContainer from "../agent-remarks/Remarks"
import { Api_Login } from '../../../../config/config';
import axios from "axios";
import moment from 'moment';

const initAgent = {

  sales_agent_fname : "",
  sales_agent_lname : "",
  sales_agent_nic : "",
  sales_agent_mobile_1 : "",
  sales_agent_mobile_2 : "",
  sales_agent_email : "",
  address:"",
  password:""

};

const initAgentPassword = {
  curent_password : "",
  new_password : "",
  confirm_password : ""

};

export function AgentEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, AgentForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Agents.actionsLoading,
      AgentForEdit: state.Agents.AgentForEdit,
    }),
    shallowEqual
  );
  const agentLocation = Api_Login + '/api/sales-agent-location/search'

  const entity = useSelector(state => (state.Agents.entities))
  console.log(entity)

  const[lati, setlattitude] = useState(7.6584);
  const[logintu, setlongitude] = useState(79.9881);
  const[locationTitel,setLocationTitel]=useState();
  const[timelocation,setTimeLocation]=useState();

  useEffect(() => {
    axios({
      method: 'post',
      baseURL: agentLocation,
      data: {
        "filter": [{"salesAgentSalesAgentId": id}],
        "sort":"DESC"
      }
      })
      .then((res) => {
        
        setLocationTitel(moment(res.data.data[0].created_date).format("yyyy-MMM-DD"))
        setTimeLocation(moment(res.data.data[0].created_date).format("HH:mm"))
        setlongitude(res.data.data[0].longitude)
        setlattitude(res.data.data[0].lattitude)

        console.log(res.data.data[0].longitude)
        console.log(res.data.data[0].lattitude)
      })
      .catch(function (response) {
          // console.log(response);
      });
  })


  

  useEffect(() => {
    dispatch(actions.fetchAgent(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Agent";
    if (AgentForEdit && id) {
      // _title = `Edit Agent - ${AgentForEdit.sales_agent_fname} ${AgentForEdit.sales_agent_lname} - ${AgentForEdit.sales_agent_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [AgentForEdit, id]);

  const saveAgent = (values) => {
    if (!id) {
      dispatch(actions.createAgent(values,id)).then(() => backToAgentsList());
    } else {
      dispatch(actions.updateAgent(values,id)).then(() => backToAgentsList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateAgentPassword(values,id)).then(() => backToAgentsList());
  };

  const btnRef = useRef();  

  const backToAgentsList = () => {
    history.push(`/sales/agent`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToAgentsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Agent Location
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Reset Password
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <AgentEditForm
                actionsLoading={actionsLoading}
                Agent={AgentForEdit || initAgent}
                btnRef={btnRef}
                saveAgent={saveAgent}
                entities={entity}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentAgentId={id}>
                <MapContainer lattitude={lati} id={id} longitude={logintu} locationTitel={locationTitel} timelocation={timelocation} />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentAgentId={id}>
                <PasswordReset
                  actionsLoading={actionsLoading}
                  Agent={initAgentPassword}
                  btnRef={btnRef}
                  savePassword={savePassword}
                />
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToAgentsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Agent remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Agent specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <AgentCreateForm
                actionsLoading={actionsLoading}
                Agent={AgentForEdit || initAgent}
                btnRef={btnRef}
                saveAgent={saveAgent}
                savePassword={savePassword}
                entities={entity}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentAgentId={id}>
                <MapContainer />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentAgentId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
