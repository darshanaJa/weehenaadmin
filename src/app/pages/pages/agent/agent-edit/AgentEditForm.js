import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { Image_Url } from "../../../../config/config";
import { notifyWarning } from "../../../../config/Toastify";
import Admin from "../../../../config/Admin";

const AgentEditSchema = Yup.object().shape({
  sales_agent_fname: Yup.string()
    .required("Name is required")
    .min(2, "First Name must be at least 2 characters"),
  sales_agent_lname: Yup.string()
    .required("Name is required")
    .min(2, "Last Name must be at least 2 characters"),
  address: Yup.string()
    .required("Address is required")
    .min(2, "Address must be at least 2 characters"),
  sales_agent_nic: Yup.string()
    .required("Agent NIC is required")
    .min(10, "NIC be at least 9 characters"),
  sales_agent_email: Yup.string().email()
    .required("Email is required"),
  sales_agent_mobile_1: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
  sales_agent_mobile_2: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
});

export function AgentEditForm({
  Agent,
  btnRef,
  saveAgent,
  entities
}) {

  const [email,setEmail]=useState('');
  const [nic,setNic]=useState('');

  let newEmail
  let newNic

  console.log(Agent.sales_agent_nic)

  if(entities !== null){
     const newAgentEmail = entities.filter(entity => entity.sales_agent_email!==Agent.sales_agent_email)
     newEmail = newAgentEmail.find(entity => entity.sales_agent_email===email)
     console.log(entities)
     console.log(newAgentEmail)
     console.log(newEmail)

    newNic = newAgentEmail.find(entity => entity.sales_agent_nic===nic)
     console.log(newNic)

    if(newEmail){
      notifyWarning("Already Taken Email");
      console.log(newEmail.sales_agent_email);
    }
    if(newNic){
      notifyWarning("Already Taken Nic");
    }

  }

  console.log(Agent)

  const submitImage=()=>{
      return(
        <img className="agentImg" alt="agent" src={url} />
      );
  }

  const pic=Agent.profile_pic
  const url = Image_Url+pic

  const bodyFormData = new FormData();

  const [profile_pic,set_Profile_pic]=useState();

  return (
    <div>
      {notifyWarning()}
      <Formik
        enableReinitialize={true}
        initialValues={Agent}
        validationSchema={AgentEditSchema}
        onSubmit={(values)  => {
          bodyFormData.append('sales_agent_fname',values.sales_agent_fname);
          bodyFormData.append('sales_agent_lname',values.sales_agent_lname);
          bodyFormData.append('sales_agent_nic',values.sales_agent_nic);
          bodyFormData.append('sales_agent_mobile_1',values.sales_agent_mobile_1);
          bodyFormData.append('sales_agent_mobile_2',values.sales_agent_mobile_2);
          bodyFormData.append('sales_agent_email',values.sales_agent_email);
          bodyFormData.append('password',values.password);
          bodyFormData.append('address',values.address);
          bodyFormData.append('profile_image',profile_pic);

          console.log(bodyFormData);
          saveAgent(bodyFormData);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="sales_agent_fname"
                    component={Input}
                    placeholder="First Name"
                    label="Fisrt Name"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="sales_agent_lname"
                    component={Input}
                    placeholder="Last Name"
                    label="Last Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="sales_agent_nic"
                    component={Input}
                    placeholder="number"
                    label="NIC"
                    onKeyUp={(e) => {setNic(e.target.value)}}
                  />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="sales_agent_mobile_1"
                      component={Input}
                      placeholder="Mobile 01"
                      label="Mobile 01"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="sales_agent_mobile_2"
                    component={Input}
                    placeholder="Mobile 02"
                    label="Mobile 02"
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="address"
                    component={Input}
                    placeholder="address"
                    label="Address"
                  />
                </div>
              </div>

              <div className="form-group row">
                  <div className="col-lg-4">
                    <Field
                        type="email"
                        name="sales_agent_email"
                        component={Input}
                        placeholder="Email"
                        label="Email"
                        onKeyUp={(e)=>{setEmail(e.target.value)}}
                      />
                  </div>
                  <div className="col-lg-4">
                      <input className="agentImageBtn + dwnfileEdit" type="file" name="profile_pic"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                  </div> 
                  <div className="col-lg-4">
                    <button type="submit"
                    hidden={newEmail || newNic}
                    className="btn btn-primary ml-2 + downbtnedit"> Save</button>
                    </div>
                </div>  
                <div className="form-group row">
                  {submitImage()}
                </div>

                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={Agent.created_by}
                  createDate={Agent.created_date} 
                  updateDate={Agent.updated_date} 
                />
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
