import React from "react";
import { Route } from "react-router-dom";
import { AgentsLoadingDialog } from "./agents-loading-dialog/AgentsLoadingDialog";
import { AgentDeleteDialog } from "./agent-delete-dialog/AgentDeleteDialog";
import { AgentsDeleteDialog } from "./agents-delete-dialog/AgentsDeleteDialog";
import { AgentsFetchDialog } from "./agents-fetch-dialog/AgentsFetchDialog";
import { AgentsUpdateStatusDialog } from "./agents-update-status-dialog/AgentsUpdateStatusDialog";
import { AgentsCard } from "./AgentsCard";
import { AgentsUIProvider } from "./AgentsUIContext";
import { notify } from "../../../config/Toastify";

// window.location.reload()

export function AgentsPage({ history }) {
  const agentsUIEvents = {
    newAgentButtonClick: () => {
      history.push("/sales/agent/new");
    },
    openEditAgentPage: (id) => {
      history.push(`/sales/agent/${id}/edit`);
    },
    openDeleteAgentDialog: (sales_agent_id) => {
      history.push(`/sales/agent/${sales_agent_id}/delete`);
    },
    openDeleteAgentsDialog: () => {
      history.push(`/sales/agent/deleteAgents`);
    },
    openFetchAgentsDialog: () => {
      history.push(`/sales/agent/fetch`);
    },
    openUpdateAgentsStatusDialog: () => {
      history.push("/sales/agent/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <AgentsUIProvider agentsUIEvents={agentsUIEvents}>
      <AgentsLoadingDialog />
      <Route path="/sales/agent/deleteAgents">
        {({ history, match }) => (
          <AgentsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/agent");
            }}
          />
        )}
      </Route>
      <Route path="/sales/agent/:sales_agent_id/delete">
        {({ history, match }) => (
          <AgentDeleteDialog
            show={match != null}
            sales_agent_id={match && match.params.sales_agent_id}
            onHide={() => {
              history.push("/sales/agent");
            }}
          />
        )}
      </Route>
      <Route path="/sales/agent/fetch">
        {({ history, match }) => (
          <AgentsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/agent");
            }}
          />
        )}
      </Route>
      <Route path="/sales/agent/updateStatus">
        {({ history, match }) => (
          <AgentsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/sales/agent");
            }}
          />
        )}
      </Route>
      <AgentsCard />
    </AgentsUIProvider>
    </>
  );
}
