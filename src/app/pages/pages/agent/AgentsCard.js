import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { AgentsFilter } from "./agents-filter/AgentsFilter";
import { AgentsTable } from "./agents-table/AgentsTable";
// import { AgentsGrouping } from "./agents-grouping/AgentsGrouping";
import { useAgentsUIContext } from "./AgentsUIContext";
import { notify } from "../../../config/Toastify";
// import MapContainer from "./agent-remarks/Remarks";

export function AgentsCard(msg){

  // const [name,setName] = useState()

  // setName(msg)
  // console.log(msg)

  // useEffect(()=>{
  //   console.log(msg)
  //   // notify("abcd")
  // },[])

  const agentsUIContext = useAgentsUIContext();
  const agentsUIProps = useMemo(() => {
    return {
      ids: agentsUIContext.ids,
      queryParams: agentsUIContext.queryParams,
      setQueryParams: agentsUIContext.setQueryParams,
      newAgentButtonClick: agentsUIContext.newAgentButtonClick,
      openDeleteAgentsDialog: agentsUIContext.openDeleteAgentsDialog,
      openEditAgentPage: agentsUIContext.openEditAgentPage,
      openUpdateAgentsStatusDialog:agentsUIContext.openUpdateAgentsStatusDialog,
      openFetchAgentsDialog: agentsUIContext.openFetchAgentsDialog,
    };
  }, [agentsUIContext]);

  return (
    <>
    {/* <MapContainer /> */}
    <Card>
      {notify()}
      <CardHeader title="Agent list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={agentsUIProps.newAgentButtonClick}
          >
            New Agent
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <AgentsFilter />
        {agentsUIProps.ids.length > 0 && (
          <>
            {/* <AgentsGrouping /> */}
          </>
        )}
        <AgentsTable />
      </CardBody>
    </Card>
    </>
  );
}
