// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useState, useEffect } from "react";
// import { shallowEqual, useDispatch, useSelector } from "react-redux";
// import BootstrapTable from "react-bootstrap-table-next";
// import paginationFactory, {
//   PaginationProvider,
// } from "react-bootstrap-table2-paginator";
// import * as actions from "../../../_redux/remarks/remarksActions";
// import { ActionsColumnFormatter } from "./column-formatters/ActionsColumnFormatter";
// import * as uiHelpers from "./RemarksUIHelper";
// import { Pagination } from "../../../../../_metronic/_partials/controls";
// import {
//   getSelectRow,
//   getHandlerTableChange,
//   NoRecordsFoundMessage,
//   PleaseWaitMessage,
//   sortCaret,
// } from "../../../../../_metronic/_helpers";
// import { useRemarksUIContext } from "./RemarksUIContext";
import Pr  from "../../../../config/p1";
import { ChickenCopBatches_URL } from "../../../_redux/ChickenCopBatch/ChickenCopBatchesCrud";
import axios from 'axios';
import Switch from '@material-ui/core/Switch';
import { notify } from "../../../../config/Toastify";
import { Api_Login } from "../../../../config/config";
// Api_Login

export function RemarksTable({id}) {

  const [name, setname] = useState('')
  const [date, setdate] = useState('')
  const [day, setday] = useState('')
  const [farm, setfarm] = useState('')
  const [Active, setActive] = useState('')

  const [chView, setView] = useState(false)

  const url = Api_Login + '/api/chickencoop/update';

  useEffect(()=>{
    axios({
      method: 'Post',
      baseURL: ChickenCopBatches_URL,
      data:{
        "filter": [{"chickcoop.chickcoop_id": id}],
        "sort": "DESC"
      }
      })
      .then((res) => {
        console.log(res.data.data.results[0])
        setname(res.data.data.results[0].coopbatch.batchname)
        setdate(res.data.data.results[0].coopbatch.chick_batch_registered_date)
        setday(res.data.data.results[0].coopbatch.chicken_days_from_birth)
        setfarm(res.data.data.results[0].buyback.buyback_name)
        setActive(res.data.data.results[0].chickcoop.active)
        console.log(res.data.data.results[0])
        console.log(res.data.data.results[0].buyback.buyback_name)
      })
  },[id])

  console.log(date)
  console.log(day)
  console.log(name)

  const handleChangeView =(event)=>{
    if(event.target.checked===true)
    {
      axios.put(url+`/${id}`, {"active":"off"});
      setView(event.target.checked)
      notify("Sucessfully Active Coop....")
    }
    else{
      axios.put(url+`/${id}`, {"active":"Active"});
      setView(event.target.checked)
      notify("Sucessfully Closed Coop....")
    }
    console.log(event.target.checked)
  }

  useEffect(() => {
    if(Active==="off"){
    setView(true);
  }
  // console.log(act)
  }, [Active])



  // Remarks UI Context
  
  return (
    <>
      <p>Farm Name - {farm}</p>
      <p>Batch Name - {name} </p>
      <p>Coop Batch Quantity - {day} </p>
      {/* <p>Coop Batch Days - {day} </p> */}
      {/* <Pr 
        day1={day} 
        createDate={date} 
      /> */}
      <p>Coop Status</p>
          <Switch
            checked={chView}
            value={chView}
            onChange={handleChangeView}
            color="primary"
            inputProps={{ 'aria-label': 'secondary checkbox' }}
      />
      {chView===true && <p><b>Started Coop</b></p>}
      {chView===false && <p><b>Cloesd Coop</b></p>}
      <Pr 
        day1={day} 
        createDate={date} 
      />
      {/* <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bordered={false}
                bootstrap4
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  remarksUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: remarksUIProps.ids,
                  setIds: remarksUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider> */}
    </>
  );
}
