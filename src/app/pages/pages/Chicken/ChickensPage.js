import React from "react";
import { Route } from "react-router-dom";
import { ChickensLoadingDialog } from "./Chickens-loading-dialog/ChickensLoadingDialog";
import { ChickenDeleteDialog } from "./Chicken-delete-dialog/ChickenDeleteDialog";
import { ChickensDeleteDialog } from "./Chickens-delete-dialog/ChickensDeleteDialog";
import { ChickensFetchDialog } from "./Chickens-fetch-dialog/ChickensFetchDialog";
import { ChickensUpdateStatusDialog } from "./Chickens-update-status-dialog/ChickensUpdateStatusDialog";
import { ChickensCard } from "./ChickensCard";
import { ChickensUIProvider } from "./ChickensUIContext";
import { notify } from "../../../config/Toastify";

export function ChickensPage({ history }) {
  const ChickensUIEvents = {
    newChickenButtonClick: () => {
      history.push("/buyback/coop/new");
    },
    openEditChickenPage: (id) => {
      history.push(`/buyback/coop/${id}/edit`);
    },
    openDeleteChickenDialog: (sales_Chicken_id) => {
      history.push(`/buyback/coop/${sales_Chicken_id}/delete`);
    },
    openDeleteChickensDialog: () => {
      history.push(`/buyback/coop/deleteChickens`);
    },
    openFetchChickensDialog: () => {
      history.push(`/buyback/coop/fetch`);
    },
    openUpdateChickensStatusDialog: () => {
      history.push("/buyback/coop/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <ChickensUIProvider ChickensUIEvents={ChickensUIEvents}>
      <ChickensLoadingDialog />
      <Route path="/buyback/coop/deleteChickens">
        {({ history, match }) => (
          <ChickensDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/coop");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/coop/:sales_Chicken_id/delete">
        {({ history, match }) => (
          <ChickenDeleteDialog
            show={match != null}
            sales_Chicken_id={match && match.params.sales_Chicken_id}
            onHide={() => {
              history.push("/buyback/coop");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/coop/fetch">
        {({ history, match }) => (
          <ChickensFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/coop");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/coop/updateStatus">
        {({ history, match }) => (
          <ChickensUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/coop");
            }}
          />
        )}
      </Route>
      <ChickensCard />
    </ChickensUIProvider>
    </>
  );
}
