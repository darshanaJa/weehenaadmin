import React from "react";
import {
  ChickenstatusCssClasses,
  ChickenstatusTitles
} from "../../ChickensUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      ChickenstatusCssClasses[row.status]
    } label-inline`}
  >
    {ChickenstatusTitles[row.status]}
  </span>
);
