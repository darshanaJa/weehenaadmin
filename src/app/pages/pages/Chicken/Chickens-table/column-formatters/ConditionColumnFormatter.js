import React from "react";
import {
  ChickenConditionCssClasses,
  ChickenConditionTitles
} from "../../ChickensUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        ChickenConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        ChickenConditionCssClasses[row.condition]
      }`}
    >
      {ChickenConditionTitles[row.condition]}
    </span>
  </>
);
