// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Chicken/ChickensActions";
import * as uiHelpers from "../ChickensUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useChickensUIContext } from "../ChickensUIContext";

export function ChickensTable() {
  // Chickens UI Context
  const ChickensUIContext = useChickensUIContext();
  const ChickensUIProps = useMemo(() => {
    return {
      ids: ChickensUIContext.ids,
      setIds: ChickensUIContext.setIds,
      queryParams: ChickensUIContext.queryParams,
      setQueryParams: ChickensUIContext.setQueryParams,
      openEditChickenPage: ChickensUIContext.openEditChickenPage,
      openDeleteChickenDialog: ChickensUIContext.openDeleteChickenDialog,
    };
  }, [ChickensUIContext]);

  // Getting curret state of Chickens list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Chickens }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Chickens Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    ChickensUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchChickens(ChickensUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Chicken_id);

  const columns = [
    {
      dataField: "chickcoop.buyback_name",
      text: "Farm Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chickcoop_name",
      text: "Coop Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chickcoop_quantity",
      text: "Quntity",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chickcoop_description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
    },
    // {
    //   dataField: "sales_Chicken_mobile_1",
    //   text: "Mobile 01",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "sales_Chicken_mobile_2",
    //   text: "Mobile 02",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.PriceColumnFormatter,
    // },
    // {
    //   dataField: "sales_Chicken_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_Chicken_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditChickenPage: ChickensUIProps.openEditChickenPage,
        openDeleteChickenDialog: ChickensUIProps.openDeleteChickenDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ChickensUIProps.queryParams.pageSize,
    page: ChickensUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Chicken_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ChickensUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ChickensUIProps.ids,
                  setIds: ChickensUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
