import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./ChickensUIHelpers";

const ChickensUIContext = createContext();

export function useChickensUIContext() {
  return useContext(ChickensUIContext);
}

export const ChickensUIConsumer = ChickensUIContext.Consumer;

export function ChickensUIProvider({ ChickensUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newChickenButtonClick: ChickensUIEvents.newChickenButtonClick,
    openEditChickenPage: ChickensUIEvents.openEditChickenPage,
    openDeleteChickenDialog: ChickensUIEvents.openDeleteChickenDialog,
    openDeleteChickensDialog: ChickensUIEvents.openDeleteChickensDialog,
    openFetchChickensDialog: ChickensUIEvents.openFetchChickensDialog,
    openUpdateChickensStatusDialog: ChickensUIEvents.openUpdateChickensStatusDialog,
  };

  return (
    <ChickensUIContext.Provider value={value}>
      {children}
    </ChickensUIContext.Provider>
  );
}
