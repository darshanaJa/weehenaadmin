import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const ChickenEditSchema = Yup.object().shape({
  // id: Yup.string(),
  chickcoop_name: Yup.string()
    .required("Name is required")
    .min(2, "Name must be at least 2 characters"),
    chickcoop_quantity: Yup.number()
    .required("Quntity is required")
    .min(2, "Quntity must be at least 2 characters"),
    chickcoop_description: Yup.string()
    .required("Description is required")
    .min(2, "Description must be at least 2 characters"),
  
});


export function ChickenCreateForm({
  Chicken,
  btnRef,
  saveChicken,
  farm
}) {

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Chicken}
        validationSchema={ChickenEditSchema}
        onSubmit={(values)  => {
          console.log(values)
         
          saveChicken(values);

        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Select name="buybackId" label="Farm Name" 
                // onChange={(e)=>{setFarmValue(e.target.value)}}
                // value={farmValue}
                >
                      <option>Choose One</option>
                      {farm.map((item) => (
                          <option value={item.id} >
                            {item.buyback_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="chickcoop_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chickcoop_quantity"
                    component={Input}
                    placeholder="Quntity"
                    label="Quntity"
                  />
                </div>
              </div>

             

            <div className="form-group row">
              <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chickcoop_description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
                 
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
                  
                </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>

            <div>
                
            </div>
          </>
        )}
      </Formik>
    </>
  );
}
