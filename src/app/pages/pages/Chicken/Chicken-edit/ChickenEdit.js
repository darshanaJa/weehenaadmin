/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Chicken/ChickensActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { ChickenEditForm } from "./ChickenEditForm";
import { ChickenCreateForm } from "./ChickenCreateForm";
// import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../Chicken-specifications/Specifications";
import { SpecificationsUIProvider } from "../Chicken-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Chicken-remarks/RemarksUIContext";
import { Remarks } from "../Chicken-remarks/Remarks";
import { Farms_URL } from "../../../_redux/Farm/FarmsCrud";
import axios from "axios";
// import { SpecificationsTable } from "../Chicken-specifications/SpecificationsTable";

const initChicken = {

  chickcoop_name : "",
  chickcoop_quantity : "",
  chickcoop_description : "",

};

// const initChickenPassword = {
//   curent_password : "",
//   new_password : "",
//   confirm_password : ""

// };

export function ChickenEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const [farm,setFarm]= useState([]);
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, ChickenForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Chickens.actionsLoading,
      ChickenForEdit: state.Chickens.ChickenForEdit,
    }),
    shallowEqual
  );

  useEffect(()=>{
    axios({
      method: 'Post',
      baseURL: Farms_URL,
      data:{
        "filter": [{"status": "1" }],
      }
      })
      .then((res) => {
        setFarm(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])


  useEffect(() => {
    dispatch(actions.fetchChicken(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Chicken";
    if (ChickenForEdit && id) {
      // _title = `Edit Chicken - ${ChickenForEdit.sales_Chicken_fname} ${ChickenForEdit.sales_Chicken_lname} - ${ChickenForEdit.sales_Chicken_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickenForEdit, id]);

  const saveChicken = (values) => {
    if (!id) {
      dispatch(actions.createChicken(values,id)).then(() => backToChickensList());
    } else {
      dispatch(actions.updateChicken(values,id)).then(() => backToChickensList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateChickenPassword(values,id)).then(() => backToChickensList());
  };

  const btnRef = useRef();  
  

  const backToChickensList = () => {
    history.push(`/buyback/coop`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickensList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                   Current State
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Coop History
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickenEditForm
                actionsLoading={actionsLoading}
                Chicken={ChickenForEdit || initChicken}
                btnRef={btnRef}
                saveChicken={saveChicken}
                farm={farm}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider id={id} currentChickenId={id}>
                <Remarks id={id} />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider id={id} currentChickenId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickensList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Chicken remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Chicken specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickenCreateForm
                actionsLoading={actionsLoading}
                Chicken={ChickenForEdit || initChicken}
                btnRef={btnRef}
                saveChicken={saveChicken}
                savePassword={savePassword}
                farm={farm}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentChickenId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentChickenId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
