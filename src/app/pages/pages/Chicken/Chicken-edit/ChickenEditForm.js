import React, {useState, useEffect} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input, Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import Switch from '@material-ui/core/Switch';
import { notify } from "../../../../config/Toastify";
import axios from "axios";
import { Api_Login } from "../../../../config/config";
import Admin from "../../../../config/Admin";

// Validation schema
const ChickenEditSchema = Yup.object().shape({
  // id: Yup.string(),
  chickcoop_name: Yup.string()
    .required("Name is required")
    .min(2, "Name must be at least 2 characters"),
    chickcoop_quantity: Yup.number()
    .required("Quntity is required")
    .min(2, "Quntity must be at least 2 characters"),
    chickcoop_description: Yup.string()
    .required("Description is required")
    .min(2, "Description must be at least 2 characters"),
});


export function ChickenEditForm({
  Chicken,
  btnRef,
  saveChicken,
  farm
}) {

  console.log(Chicken.chickcoop_id)

  useEffect(() => {
    if(Chicken.active==="off"){
    setView(true);
  }
  // console.log(act)
  }, [Chicken.active])

  const [chView, setView] = useState(false)

  const url = Api_Login + '/api/chickencoop/update';

  const handleChangeView =(event)=>{
    if(event.target.checked===true)
    {
      axios.put(url+`/${Chicken.chickcoop_id}`, {"active":"off"});
      setView(event.target.checked)
      notify("Sucessfully Active Coop....")
    }
    else{
      axios.put(url+`/${Chicken.chickcoop_id}`, {"active":"Active"});
      setView(event.target.checked)
      notify("Sucessfully Closed Coop....")
    }
    console.log(event.target.checked)
  }


  return (
    <>
    {notify()}
      <Formik
        enableReinitialize={true}
        initialValues={Chicken}
        validationSchema={ChickenEditSchema}
        onSubmit={(values)  => {
         
          saveChicken(values);

        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Select name="chickcoop.id" label="Farm Name">
                        <option>Choose One</option>
                        {farm.map((item) => (
                            <option value={item.id} >
                              {item.buyback_name}
                            </option>    
                      ))}
                  </Select>
                </div>
                 <div className="col-lg-4">
                <Field
                    type="text"
                    name="chickcoop_name"
                    component={Input}
                    placeholder="Name"
                    label="Name"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chickcoop_quantity"
                    component={Input}
                    placeholder="Quntity"
                    label="Quntity"
                  />
                </div>
                
              </div>

            <div className="form-group row">
            <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chickcoop_description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
              <div className="col-lg-4">
                <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
              </div>
                <div className="col-lg-4">
                  <p>Coop Status</p>
                      <Switch
                       checked={chView}
                       value={chView}
                       onChange={handleChangeView}
                       color="primary"
                       inputProps={{ 'aria-label': 'secondary checkbox' }}
                  />
                  {chView===true && <p>Started Coop</p>}
                  {chView===false && <p>Cloesd Coop</p>}
                    {/* <button type="submit" hidden={togle===true} className="btn btn-primary ml-2"> Closed </button> */}
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={Chicken.created_by}
                  createDate={Chicken.created_date} 
                  updateDate={Chicken.updated_date} 
                />
           
            </Form>

            <div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
}
