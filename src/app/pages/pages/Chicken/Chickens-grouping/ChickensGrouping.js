import React, { useMemo } from "react";
import { useChickensUIContext } from "../ChickensUIContext";

export function ChickensGrouping() {
  // Chickens UI Context
  const ChickensUIContext = useChickensUIContext();
  const ChickensUIProps = useMemo(() => {
    return {
      ids: ChickensUIContext.ids,
      setIds: ChickensUIContext.setIds,
      openDeleteChickensDialog: ChickensUIContext.openDeleteChickensDialog,
      openFetchChickensDialog: ChickensUIContext.openFetchChickensDialog,
      openUpdateChickensStatusDialog:
        ChickensUIContext.openUpdateChickensStatusDialog,
    };
  }, [ChickensUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{ChickensUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={ChickensUIProps.openDeleteChickensDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ChickensUIProps.openFetchChickensDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ChickensUIProps.openUpdateChickensStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
