import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { ChickensFilter } from "./Chickens-filter/ChickensFilter";
import { ChickensTable } from "./Chickens-table/ChickensTable";
// import { ChickensGrouping } from "./Chickens-grouping/ChickensGrouping";
import { useChickensUIContext } from "./ChickensUIContext";
import { notify } from "../../../config/Toastify";

export function ChickensCard(msg){

  const ChickensUIContext = useChickensUIContext();
  const ChickensUIProps = useMemo(() => {
    return {
      ids: ChickensUIContext.ids,
      queryParams: ChickensUIContext.queryParams,
      setQueryParams: ChickensUIContext.setQueryParams,
      newChickenButtonClick: ChickensUIContext.newChickenButtonClick,
      openDeleteChickensDialog: ChickensUIContext.openDeleteChickensDialog,
      openEditChickenPage: ChickensUIContext.openEditChickenPage,
      openUpdateChickensStatusDialog:ChickensUIContext.openUpdateChickensStatusDialog,
      openFetchChickensDialog: ChickensUIContext.openFetchChickensDialog,
    };
  }, [ChickensUIContext]);

  return (
    <>
    <Card>
      {notify()}
      <CardHeader title="Chicken Coop list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={ChickensUIProps.newChickenButtonClick}
          >
            New Chicken Coop
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ChickensFilter />
        {ChickensUIProps.ids.length > 0 && (
          <>
            {/* <ChickensGrouping /> */}
          </>
        )}
        <ChickensTable />
      </CardBody>
    </Card>
    </>
  );
}
