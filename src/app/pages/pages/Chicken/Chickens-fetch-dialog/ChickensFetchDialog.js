import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { ChickenstatusCssClasses } from "../ChickensUIHelpers";
import { useChickensUIContext } from "../ChickensUIContext";

const selectedChickens = (entities, ids) => {
  const _Chickens = [];
  ids.forEach((id) => {
    const Chicken = entities.find((el) => el.id === id);
    if (Chicken) {
      _Chickens.push(Chicken);
    }
  });
  return _Chickens;
};

export function ChickensFetchDialog({ show, onHide }) {
  // Chickens UI Context
  const ChickensUIContext = useChickensUIContext();
  const ChickensUIProps = useMemo(() => {
    return {
      ids: ChickensUIContext.ids,
      queryParams: ChickensUIContext.queryParams,
    };
  }, [ChickensUIContext]);

  // Chickens Redux state
  const { Chickens } = useSelector(
    (state) => ({
      Chickens: selectedChickens(state.Chickens.entities, ChickensUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!ChickensUIProps.ids || ChickensUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Chickens.map((Chicken) => (
              <div className="list-timeline-item mb-3" key={Chicken.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ChickenstatusCssClasses[Chicken.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Chicken.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Chicken.manufacture}, {Chicken.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
