/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Chicken/ChickensActions";
import { useChickensUIContext } from "../ChickensUIContext";

export function ChickenDeleteDialog({ sales_Chicken_id, show, onHide }) {
  // console.log(sales_Chicken_id)
  // Chickens UI Context
  const ChickensUIContext = useChickensUIContext();
  const ChickensUIProps = useMemo(() => {
    return {
      setIds: ChickensUIContext.setIds,
      queryParams: ChickensUIContext.queryParams,
    };
  }, [ChickensUIContext]);

  // Chickens Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Chickens.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Chicken_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Chicken_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteChicken = () => {
    // server request for deleting Chicken by id
    dispatch(actions.deleteChicken(sales_Chicken_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickens(ChickensUIProps.queryParams));
      // clear selections list
      ChickensUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Chicken Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Chicken?</span>
        )}
        {isLoading && <span>Chicken is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChicken}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
