/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Chicken/ChickensActions";
import { useChickensUIContext } from "../ChickensUIContext";

export function ChickensDeleteDialog({ show, onHide }) {
  // Chickens UI Context
  const ChickensUIContext = useChickensUIContext();
  const ChickensUIProps = useMemo(() => {
    return {
      ids: ChickensUIContext.ids,
      setIds: ChickensUIContext.setIds,
      queryParams: ChickensUIContext.queryParams,
    };
  }, [ChickensUIContext]);

  // Chickens Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Chickens.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Chickens we should close modal
  useEffect(() => {
    if (!ChickensUIProps.ids || ChickensUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensUIProps.ids]);

  const deleteChickens = () => {
    // server request for deleting Chicken by seleted ids
    dispatch(actions.deleteChickens(ChickensUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickens(ChickensUIProps.queryParams)).then(() => {
        // clear selections list
        ChickensUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Chickens Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Chickens?</span>
        )}
        {isLoading && <span>Chickens are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChickens}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
