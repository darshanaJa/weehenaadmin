export const ChickenstatusCssClasses = ["success", "info", ""];
export const ChickenstatusTitles = ["Selling", "Sold"];
export const ChickenConditionCssClasses = ["success", "danger", ""];
export const ChickenConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_Chicken_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    chickcoop_name: ""
  },
  chickcoop_name:null,
  sortOrder: "asc", // asc||desc
  sortField: "chickcoop_name",
  pageNumber: 1,
  pageSize: 10
  // value:
};
