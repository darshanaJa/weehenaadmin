import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ChickenstatusCssClasses } from "../ChickensUIHelpers";
import * as actions from "../../../_redux/Chicken/ChickensActions";
import { useChickensUIContext } from "../ChickensUIContext";

const selectedChickens = (entities, ids) => {
  const _Chickens = [];
  ids.forEach((id) => {
    const Chicken = entities.find((el) => el.id === id);
    if (Chicken) {
      _Chickens.push(Chicken);
    }
  });
  return _Chickens;
};

export function ChickensUpdateStatusDialog({ show, onHide }) {
  // Chickens UI Context
  const ChickensUIContext = useChickensUIContext();
  const ChickensUIProps = useMemo(() => {
    return {
      ids: ChickensUIContext.ids,
      setIds: ChickensUIContext.setIds,
      queryParams: ChickensUIContext.queryParams,
    };
  }, [ChickensUIContext]);

  // Chickens Redux state
  const { Chickens, isLoading } = useSelector(
    (state) => ({
      Chickens: selectedChickens(state.Chickens.entities, ChickensUIProps.ids),
      isLoading: state.Chickens.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Chickens we should close modal
  useEffect(() => {
    if (ChickensUIProps.ids || ChickensUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Chicken by ids
    dispatch(actions.updateChickensStatus(ChickensUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchChickens(ChickensUIProps.queryParams)).then(
          () => {
            // clear selections list
            ChickensUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Chickens
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Chickens.map((Chicken) => (
              <div className="list-timeline-item mb-3" key={Chicken.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ChickenstatusCssClasses[Chicken.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Chicken.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Chicken.manufacture}, {Chicken.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${ChickenstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
