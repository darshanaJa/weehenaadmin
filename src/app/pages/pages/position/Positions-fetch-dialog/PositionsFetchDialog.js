import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { PositionstatusCssClasses } from "../PositionsUIHelpers";
import { usePositionsUIContext } from "../PositionsUIContext";

const selectedPositions = (entities, ids) => {
  const _Positions = [];
  ids.forEach((id) => {
    const Position = entities.find((el) => el.id === id);
    if (Position) {
      _Positions.push(Position);
    }
  });
  return _Positions;
};

export function PositionsFetchDialog({ show, onHide }) {
  // Positions UI Context
  const PositionsUIContext = usePositionsUIContext();
  const PositionsUIProps = useMemo(() => {
    return {
      ids: PositionsUIContext.ids,
      queryParams: PositionsUIContext.queryParams,
    };
  }, [PositionsUIContext]);

  // Positions Redux state
  const { Positions } = useSelector(
    (state) => ({
      Positions: selectedPositions(state.Positions.entities, PositionsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!PositionsUIProps.ids || PositionsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PositionsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Positions.map((Position) => (
              <div className="list-timeline-item mb-3" key={Position.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      PositionstatusCssClasses[Position.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Position.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Position.manufacture}, {Position.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
