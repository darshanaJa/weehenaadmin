import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./PositionsUIHelpers";

const PositionsUIContext = createContext();

export function usePositionsUIContext() {
  return useContext(PositionsUIContext);
}

export const PositionsUIConsumer = PositionsUIContext.Consumer;

export function PositionsUIProvider({ PositionsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newPositionButtonClick: PositionsUIEvents.newPositionButtonClick,
    openEditPositionPage: PositionsUIEvents.openEditPositionPage,
    openDeletePositionDialog: PositionsUIEvents.openDeletePositionDialog,
    openDeletePositionsDialog: PositionsUIEvents.openDeletePositionsDialog,
    openFetchPositionsDialog: PositionsUIEvents.openFetchPositionsDialog,
    openUpdatePositionsStatusDialog: PositionsUIEvents.openUpdatePositionsStatusDialog,
  };

  return (
    <PositionsUIContext.Provider value={value}>
      {children}
    </PositionsUIContext.Provider>
  );
}
