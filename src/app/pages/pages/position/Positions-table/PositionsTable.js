// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Position/PositionsActions";
import * as uiHelpers from "../PositionsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { usePositionsUIContext } from "../PositionsUIContext";

export function PositionsTable() {
  // Positions UI Context
  const PositionsUIContext = usePositionsUIContext();
  const PositionsUIProps = useMemo(() => {
    return {
      ids: PositionsUIContext.ids,
      setIds: PositionsUIContext.setIds,
      queryParams: PositionsUIContext.queryParams,
      setQueryParams: PositionsUIContext.setQueryParams,
      openEditPositionPage: PositionsUIContext.openEditPositionPage,
      openDeletePositionDialog: PositionsUIContext.openDeletePositionDialog,
    };
  }, [PositionsUIContext]);

  // Getting curret state of Positions list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Positions }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Positions Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    PositionsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchPositions(PositionsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PositionsUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    // {
    //   dataField: "id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "position_name",
      text: "Position Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "posi_slug",
      text: "Position Slug",
      sort: true,
      sortCaret: sortCaret,
    },
     {
      dataField: "depar.department_name",
      text: "Department Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "pos_descrip",
      text: "Position Description",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "pos_sys_permission",
      text: "Position permission",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    // {
    //   dataField: "mstock_description",
    //   text: "Description",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.PriceColumnFormatter,
    // },
    // {
    //   dataField: "item_registered_by",
    //   text: "Register By",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "item_registered_date",
    //   text: "Registerd Date",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "Position_updated_date",
    //   text: "Update Date",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditPositionPage: PositionsUIProps.openEditPositionPage,
        openDeletePositionDialog: PositionsUIProps.openDeletePositionDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: PositionsUIProps.queryParams.pageSize,
    page: PositionsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  PositionsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: PositionsUIProps.ids,
                  setIds: PositionsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
