import React from "react";
import {
  PositionConditionCssClasses,
  PositionConditionTitles
} from "../../PositionsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        PositionConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        PositionConditionCssClasses[row.condition]
      }`}
    >
      {PositionConditionTitles[row.condition]}
    </span>
  </>
);
