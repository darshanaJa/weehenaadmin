import React from "react";
import {
  PositionstatusCssClasses,
  PositionstatusTitles
} from "../../PositionsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      PositionstatusCssClasses[row.status]
    } label-inline`}
  >
    {PositionstatusTitles[row.status]}
  </span>
);
