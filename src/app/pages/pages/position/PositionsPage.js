import React from "react";
import { Route } from "react-router-dom";
import { PositionsLoadingDialog } from "./Positions-loading-dialog/PositionsLoadingDialog";
import { PositionDeleteDialog } from "./Position-delete-dialog/PositionDeleteDialog";
import { PositionsDeleteDialog } from "./Positions-delete-dialog/PositionsDeleteDialog";
import { PositionsFetchDialog } from "./Positions-fetch-dialog/PositionsFetchDialog";
import { PositionsUpdateStatusDialog } from "./Positions-update-status-dialog/PositionsUpdateStatusDialog";
import { PositionsCard } from "./PositionsCard";
import { PositionsUIProvider } from "./PositionsUIContext";

export function PositionsPage({ history }) {
  const PositionsUIEvents = {
    newPositionButtonClick: () => {
      history.push("/hr/position/new");
    },
    openEditPositionPage: (id) => {
      history.push(`/hr/position/${id}/edit`);
    },
    openDeletePositionDialog: (id) => {
      history.push(`/hr/position/${id}/delete`);
    },
    openDeletePositionsDialog: () => {
      history.push(`/hr/position/deletePositions`);
    },
    openFetchPositionsDialog: () => {
      history.push(`/hr/position/fetch`);
    },
    openUpdatePositionsStatusDialog: () => {
      history.push("/hr/position/updateStatus");
    },
  };

  return (
    <PositionsUIProvider PositionsUIEvents={PositionsUIEvents}>
      <PositionsLoadingDialog />
      <Route path="/hr/position/deletePositions">
        {({ history, match }) => (
          <PositionsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/position");
            }}
          />
        )}
      </Route>
      <Route path="/hr/position/:id/delete">
        {({ history, match }) => (
          <PositionDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push("/hr/position");
            }}
          />
        )}
      </Route>
      <Route path="/hr/position/fetch">
        {({ history, match }) => (
          <PositionsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/position");
            }}
          />
        )}
      </Route>
      <Route path="/hr/position/updateStatus">
        {({ history, match }) => (
          <PositionsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/position");
            }}
          />
        )}
      </Route>
      <PositionsCard />
    </PositionsUIProvider>
  );
}
