import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { PositionsFilter } from "./Positions-filter/PositionsFilter";
import { PositionsTable } from "./Positions-table/PositionsTable";
// import { PositionsGrouping } from "./Positions-grouping/PositionsGrouping";
import { usePositionsUIContext } from "./PositionsUIContext";
import { notify } from "../../../config/Toastify";

export function PositionsCard() {

 

    // const [name,setName] = useState()
  
    // setName(msg)
    // console.log(msg)
    // notify(msg)
  
    // useEffect(()=>{
    //   console.log(msg)
    //   // notify("abcd")
    // },[])

  const PositionsUIContext = usePositionsUIContext();
  const PositionsUIProps = useMemo(() => {
    return {
      ids: PositionsUIContext.ids,
      queryParams: PositionsUIContext.queryParams,
      setQueryParams: PositionsUIContext.setQueryParams,
      newPositionButtonClick: PositionsUIContext.newPositionButtonClick,
      openDeletePositionsDialog: PositionsUIContext.openDeletePositionsDialog,
      openEditPositionPage: PositionsUIContext.openEditPositionPage,
      openUpdatePositionsStatusDialog:PositionsUIContext.openUpdatePositionsStatusDialog,
      openFetchPositionsDialog: PositionsUIContext.openFetchPositionsDialog,
    };
  }, [PositionsUIContext]);


  return (
    <Card>
      <CardHeader title="Position list">
        <CardHeaderToolbar>
        {notify()}
          <button
            type="button"
            className="btn btn-primary"
            onClick={PositionsUIProps.newPositionButtonClick}
          >
            New Position
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <PositionsFilter />
        {PositionsUIProps.ids.length > 0 && (
          <>
            {/* <PositionsGrouping /> */}
          </>
        )}
        <PositionsTable />
      </CardBody>
    </Card>
  );
}
