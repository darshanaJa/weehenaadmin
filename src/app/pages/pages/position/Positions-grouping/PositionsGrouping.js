import React, { useMemo } from "react";
import { usePositionsUIContext } from "../PositionsUIContext";

export function PositionsGrouping() {
  // Positions UI Context
  const PositionsUIContext = usePositionsUIContext();
  const PositionsUIProps = useMemo(() => {
    return {
      ids: PositionsUIContext.ids,
      setIds: PositionsUIContext.setIds,
      openDeletePositionsDialog: PositionsUIContext.openDeletePositionsDialog,
      openFetchPositionsDialog: PositionsUIContext.openFetchPositionsDialog,
      openUpdatePositionsStatusDialog:
        PositionsUIContext.openUpdatePositionsStatusDialog,
    };
  }, [PositionsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{PositionsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={PositionsUIProps.openDeletePositionsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={PositionsUIProps.openFetchPositionsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={PositionsUIProps.openUpdatePositionsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
