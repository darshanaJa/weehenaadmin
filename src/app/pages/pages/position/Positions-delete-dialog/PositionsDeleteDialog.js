/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Position/PositionsActions";
import { usePositionsUIContext } from "../PositionsUIContext";

export function PositionsDeleteDialog({ show, onHide }) {
  // Positions UI Context
  const PositionsUIContext = usePositionsUIContext();
  const PositionsUIProps = useMemo(() => {
    return {
      ids: PositionsUIContext.ids,
      setIds: PositionsUIContext.setIds,
      queryParams: PositionsUIContext.queryParams,
    };
  }, [PositionsUIContext]);

  // Positions Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Positions.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Positions we should close modal
  useEffect(() => {
    if (!PositionsUIProps.ids || PositionsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PositionsUIProps.ids]);

  const deletePositions = () => {
    // server request for deleting Position by seleted ids
    dispatch(actions.deletePositions(PositionsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchPositions(PositionsUIProps.queryParams)).then(() => {
        // clear selections list
        PositionsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Positions Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Positions?</span>
        )}
        {isLoading && <span>Positions are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deletePositions}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
