/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Position/PositionsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { PositionEditForm } from "./PositionEditForm";
import { Specifications } from "../Position-specifications/Specifications";
import { SpecificationsUIProvider } from "../Position-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Position-remarks/RemarksUIContext";
import { Remarks } from "../Position-remarks/Remarks";
import { PositionCreateForm } from "./PositionCreateForm";
// import { notify } from "../../../../config/Toastify";

const initPosition = {
  // id : "",
  position_name : "",
  posi_slug : "",
  pos_descrip : "",
  pos_sys_permission : "",
};


export function PositionEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, PositionForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Positions.actionsLoading,
      PositionForEdit: state.Positions.PositionForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchPosition(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Position";
    if (PositionForEdit && id) {
      // _title = `Edit Position - ${PositionForEdit.item_name} ${PositionForEdit.item_default_price} - ${PositionForEdit.item_name}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PositionForEdit, id]);

  const savePosition = (values) => {
    if (!id) {
      dispatch(actions.createPosition(values)).then(() => backToPositionsList());
      // {notify("create")}
    } else {
      dispatch(actions.updatePosition(values)).then(() => backToPositionsList());
    }
  };

  const btnRef = useRef();

  const backToPositionsList = () => {
    history.push(`/hr/position`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToPositionsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
           
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
               
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <PositionEditForm
                actionsLoading={actionsLoading}
                Position={PositionForEdit || initPosition}
                btnRef={btnRef}
                savePosition={savePosition}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentPositionId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentPositionId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToPositionsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
           
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Position remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Position specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <PositionCreateForm
                actionsLoading={actionsLoading}
                Position={PositionForEdit || initPosition}
                btnRef={btnRef}
                savePosition={savePosition}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentPositionId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentPositionId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }
}
