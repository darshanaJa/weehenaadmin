import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input,Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { Positions_URL_GET } from "../../../_redux/Position/PositionsCrud";
import axios from "axios";
import { Departments_URL } from "../../../_redux/Department/DepartmentsCrud";

// Validation schema
const PositionEditSchema = Yup.object().shape({
  position_name: Yup.string()
  .min(2, "Minimum 2 symbols")
  .max(50, "Maximum 50 symbols")
  .required("Postion Name is required"),
  posi_slug: Yup.string()
  .min(1, "Minimum 1 symbols")
  .max(50, "Maximum 50 symbols")
  .required("Postion slug is required"),
  pos_sys_permission: Yup.string()
  .min(1, "Minimum 1 symbols")
  .max(50, "Maximum 50 symbols")
  .required("Postion Permisions is required"),
  pos_descrip: Yup.string().required("Description is required")
   
});

export function PositionCreateForm({
  Position,
  btnRef,
  savePosition,
}) {

  const [shop,setShop]=useState([]);
  const[department, setDepartment] = useState([]);

  console.log(shop)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Positions_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Departments_URL,
      data: {
        "filter": [{"status": "1"}],
        "limit": 10,
        "skip": 0,
        "sort":"DESC"
      }
      })
      .then((res) => {
        console.log(res.data.data.results)
        setDepartment(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  

  // console.log(itemName)
  
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Position}
        validationSchema={PositionEditSchema}
        onSubmit={(values) => {
          console.log(values)
          savePosition(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field type="search"
                    name="position_name" 
                    component={Input}
                    placeholder="Postion name"
                    label="Postion name" 
                    list="position"
                    autocomplete="off" />
                  <datalist id="position">
                    <option>Driver</option>
                    <option>Helper</option>
                    <option>Dispatch </option>
                    <option>Return Check</option>
                  </datalist>
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="posi_slug"
                    component={Input}
                    placeholder="Postion Slug"
                    label="Postion Slug"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="pos_sys_permission"
                    component={Input}
                    placeholder="Postion Permisions"
                    label="Postion Permisions"
                  />
                </div>
              </div>
            <div className="form-group row">
              <div className="col-lg-4">
                  <Select name="departmentid" label="Dapartment Name">
                      <option>Choose One</option>
                      {department.map((item) => (
                          <option value={item.id} >
                            {item.department_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-8">
                  {/* <label>Description</label> */}
                  <Field
                    type="text"
                    placeholder="Description"
                    component={Input}
                    label="Description"
                    name="pos_descrip"
                    // as="textarea"
                    
                  />
                </div>
              </div>

              <div className="form-group row">

                  <div className="col-lg-4">
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                    <button type="submit" className="btn btn-primary ml-2"> Save</button>
                  </div>
              </div>
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
