import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { PositionstatusCssClasses } from "../PositionsUIHelpers";
import * as actions from "../../../_redux/Position/PositionsActions";
import { usePositionsUIContext } from "../PositionsUIContext";

const selectedPositions = (entities, ids) => {
  const _Positions = [];
  ids.forEach((id) => {
    const Position = entities.find((el) => el.id === id);
    if (Position) {
      _Positions.push(Position);
    }
  });
  return _Positions;
};

export function PositionsUpdateStatusDialog({ show, onHide }) {
  // Positions UI Context
  const PositionsUIContext = usePositionsUIContext();
  const PositionsUIProps = useMemo(() => {
    return {
      ids: PositionsUIContext.ids,
      setIds: PositionsUIContext.setIds,
      queryParams: PositionsUIContext.queryParams,
    };
  }, [PositionsUIContext]);

  // Positions Redux state
  const { Positions, isLoading } = useSelector(
    (state) => ({
      Positions: selectedPositions(state.Positions.entities, PositionsUIProps.ids),
      isLoading: state.Positions.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Positions we should close modal
  useEffect(() => {
    if (PositionsUIProps.ids || PositionsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [PositionsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Position by ids
    dispatch(actions.updatePositionsStatus(PositionsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchPositions(PositionsUIProps.queryParams)).then(
          () => {
            // clear selections list
            PositionsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Positions
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Positions.map((Position) => (
              <div className="list-timeline-item mb-3" key={Position.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      PositionstatusCssClasses[Position.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Position.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Position.manufacture}, {Position.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${PositionstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
