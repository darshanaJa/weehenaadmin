import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const ChickensAgeEditSchema = Yup.object().shape({
  chicken_days_from_birth: Yup.number()
    .required("Days is required")
    .min(1, "Days must be at least 1 characters"),
  chicken_description: Yup.string()
    .required("Descripion is required")
    .min(1, "Descripion must be at least 1 characters"),
});


export function ChickensAgeCreateForm({
  ChickensAge,
  btnRef,
  saveChickensAge,
}) {


  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={ChickensAge}
        validationSchema={ChickensAgeEditSchema}
        onSubmit={(values)  => {
          console.log(values);            
          saveChickensAge(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="chicken_days_from_birth"
                    component={Input}
                    placeholder="Days"
                    label="Days"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="chicken_description"
                    component={Input}
                    placeholder="Description"
                    label="Descption"
                  />
                </div>
                <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
              </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
