/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/ChickensAge/ChickensAgesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { ChickensAgeEditForm } from "./ChickensAgeEditForm";
import { ChickensAgeCreateForm } from "./ChickensAgeCreateForm";
import { Specifications } from "../ChickensAge-specifications/Specifications";
import { SpecificationsUIProvider } from "../ChickensAge-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../ChickensAge-remarks/RemarksUIContext";
import { Remarks } from "../ChickensAge-remarks/Remarks";

const initChickensAge = {

  chicken_days_from_birth : "",
  chicken_description : "",

};

export function ChickensAgeEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, ChickensAgeForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.ChickensAges.actionsLoading,
      ChickensAgeForEdit: state.ChickensAges.ChickensAgeForEdit,
    }),
    shallowEqual
  );

  useEffect(() => {
    dispatch(actions.fetchChickensAge(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New ChickensAge";
    if (ChickensAgeForEdit && id) {
      // _title = `Edit ChickensAge - ${ChickensAgeForEdit.buyback_name} ${ChickensAgeForEdit.buyback_contact} - ${ChickensAgeForEdit.sales_ChickensAge_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensAgeForEdit, id]);

  const saveChickensAge = (values) => {
    if (!id) {
      dispatch(actions.createChickensAge(values,id)).then(() => backToChickensAgesList());
    } else {
      dispatch(actions.updateChickensAge(values,id)).then(() => backToChickensAgesList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateChickensAgePassword(values,id)).then(() => backToChickensAgesList());
  };

  const btnRef = useRef();

  const backToChickensAgesList = () => {
    history.push(`/buyback/chicken`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickensAgesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickensAgeEditForm
                actionsLoading={actionsLoading}
                ChickensAge={ChickensAgeForEdit || initChickensAge}
                btnRef={btnRef}
                saveChickensAge={saveChickensAge}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentChickensAgeId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentChickensAgeId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToChickensAgesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    ChickensAge remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    ChickensAge specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ChickensAgeCreateForm
                actionsLoading={actionsLoading}
                ChickensAge={ChickensAgeForEdit || initChickensAge}
                btnRef={btnRef}
                saveChickensAge={saveChickensAge}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentChickensAgeId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentChickensAgeId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
