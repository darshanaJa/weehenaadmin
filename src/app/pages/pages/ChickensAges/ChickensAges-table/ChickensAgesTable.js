// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/ChickensAge/ChickensAgesActions";
import * as uiHelpers from "../ChickensAgesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useChickensAgesUIContext } from "../ChickensAgesUIContext";

export function ChickensAgesTable() {
  // ChickensAges UI Context
  const ChickensAgesUIContext = useChickensAgesUIContext();
  const ChickensAgesUIProps = useMemo(() => {
    return {
      ids: ChickensAgesUIContext.ids,
      setIds: ChickensAgesUIContext.setIds,
      queryParams: ChickensAgesUIContext.queryParams,
      setQueryParams: ChickensAgesUIContext.setQueryParams,
      openEditChickensAgePage: ChickensAgesUIContext.openEditChickensAgePage,
      openDeleteChickensAgeDialog: ChickensAgesUIContext.openDeleteChickensAgeDialog,
    };
  }, [ChickensAgesUIContext]);

  // Getting curret state of ChickensAges list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.ChickensAges }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // ChickensAges Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    ChickensAgesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchChickensAges(ChickensAgesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensAgesUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_ChickensAge_id);

  const columns = [
    // {
    //   dataField: "sales_ChickensAge_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "chicken_id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chicken_days_from_birth",
      text: "Days",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "chicken_description",
      text: "Descripion",
      sort: true,
      sortCaret: sortCaret,
    },
    // {
    //   dataField: "buyback_ChickensAge_quantity",
    //   text: "Quantity",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_passed_experiance",
    //   text: "Passed Experiance",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ColorColumnFormatter,
    // },
    // {
    //   dataField: "buyback_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_ChickensAge_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditChickensAgePage: ChickensAgesUIProps.openEditChickensAgePage,
        openDeleteChickensAgeDialog: ChickensAgesUIProps.openDeleteChickensAgeDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ChickensAgesUIProps.queryParams.pageSize,
    page: ChickensAgesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_ChickensAge_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ChickensAgesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ChickensAgesUIProps.ids,
                  setIds: ChickensAgesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
