import React from "react";
import {
  ChickensAgestatusCssClasses,
  ChickensAgestatusTitles
} from "../../ChickensAgesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      ChickensAgestatusCssClasses[row.status]
    } label-inline`}
  >
    {ChickensAgestatusTitles[row.status]}
  </span>
);
