import React from "react";
import {
  ChickensAgeConditionCssClasses,
  ChickensAgeConditionTitles
} from "../../ChickensAgesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        ChickensAgeConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        ChickensAgeConditionCssClasses[row.condition]
      }`}
    >
      {ChickensAgeConditionTitles[row.condition]}
    </span>
  </>
);
