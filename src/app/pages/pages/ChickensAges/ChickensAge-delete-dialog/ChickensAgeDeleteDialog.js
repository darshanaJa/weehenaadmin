/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/ChickensAge/ChickensAgesActions";
import { useChickensAgesUIContext } from "../ChickensAgesUIContext";

export function ChickensAgeDeleteDialog({ sales_ChickensAge_id, show, onHide }) {
  // console.log(sales_ChickensAge_id)
  // ChickensAges UI Context
  const ChickensAgesUIContext = useChickensAgesUIContext();
  const ChickensAgesUIProps = useMemo(() => {
    return {
      setIds: ChickensAgesUIContext.setIds,
      queryParams: ChickensAgesUIContext.queryParams,
    };
  }, [ChickensAgesUIContext]);

  // ChickensAges Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.ChickensAges.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_ChickensAge_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_ChickensAge_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteChickensAge = () => {
    // server request for deleting ChickensAge by id
    dispatch(actions.deleteChickensAge(sales_ChickensAge_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickensAges(ChickensAgesUIProps.queryParams));
      // clear selections list
      ChickensAgesUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          ChickensAge Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this ChickensAge?</span>
        )}
        {isLoading && <span>ChickensAge is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChickensAge}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
