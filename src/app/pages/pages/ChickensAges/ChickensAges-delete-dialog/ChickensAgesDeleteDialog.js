/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/ChickensAge/ChickensAgesActions";
import { useChickensAgesUIContext } from "../ChickensAgesUIContext";

export function ChickensAgesDeleteDialog({ show, onHide }) {
  // ChickensAges UI Context
  const ChickensAgesUIContext = useChickensAgesUIContext();
  const ChickensAgesUIProps = useMemo(() => {
    return {
      ids: ChickensAgesUIContext.ids,
      setIds: ChickensAgesUIContext.setIds,
      queryParams: ChickensAgesUIContext.queryParams,
    };
  }, [ChickensAgesUIContext]);

  // ChickensAges Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.ChickensAges.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected ChickensAges we should close modal
  useEffect(() => {
    if (!ChickensAgesUIProps.ids || ChickensAgesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensAgesUIProps.ids]);

  const deleteChickensAges = () => {
    // server request for deleting ChickensAge by seleted ids
    dispatch(actions.deleteChickensAges(ChickensAgesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchChickensAges(ChickensAgesUIProps.queryParams)).then(() => {
        // clear selections list
        ChickensAgesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          ChickensAges Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected ChickensAges?</span>
        )}
        {isLoading && <span>ChickensAges are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteChickensAges}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
