import React, { useMemo } from "react";
import { useChickensAgesUIContext } from "../ChickensAgesUIContext";

export function ChickensAgesGrouping() {
  // ChickensAges UI Context
  const ChickensAgesUIContext = useChickensAgesUIContext();
  const ChickensAgesUIProps = useMemo(() => {
    return {
      ids: ChickensAgesUIContext.ids,
      setIds: ChickensAgesUIContext.setIds,
      openDeleteChickensAgesDialog: ChickensAgesUIContext.openDeleteChickensAgesDialog,
      openFetchChickensAgesDialog: ChickensAgesUIContext.openFetchChickensAgesDialog,
      openUpdateChickensAgesStatusDialog:
        ChickensAgesUIContext.openUpdateChickensAgesStatusDialog,
    };
  }, [ChickensAgesUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{ChickensAgesUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={ChickensAgesUIProps.openDeleteChickensAgesDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ChickensAgesUIProps.openFetchChickensAgesDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ChickensAgesUIProps.openUpdateChickensAgesStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
