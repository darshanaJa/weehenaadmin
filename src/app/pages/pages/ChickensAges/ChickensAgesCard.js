import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { ChickensAgesFilter } from "./ChickensAges-filter/ChickensAgesFilter";
import { ChickensAgesTable } from "./ChickensAges-table/ChickensAgesTable";
// import { ChickensAgesGrouping } from "./ChickensAges-grouping/ChickensAgesGrouping";
import { useChickensAgesUIContext } from "./ChickensAgesUIContext";
import { notify } from "../../../config/Toastify";

export function ChickensAgesCard() {
  const ChickensAgesUIContext = useChickensAgesUIContext();
  const ChickensAgesUIProps = useMemo(() => {
    return {
      ids: ChickensAgesUIContext.ids,
      queryParams: ChickensAgesUIContext.queryParams,
      setQueryParams: ChickensAgesUIContext.setQueryParams,
      newChickensAgeButtonClick: ChickensAgesUIContext.newChickensAgeButtonClick,
      openDeleteChickensAgesDialog: ChickensAgesUIContext.openDeleteChickensAgesDialog,
      openEditChickensAgePage: ChickensAgesUIContext.openEditChickensAgePage,
      openUpdateChickensAgesStatusDialog:ChickensAgesUIContext.openUpdateChickensAgesStatusDialog,
      openFetchChickensAgesDialog: ChickensAgesUIContext.openFetchChickensAgesDialog,
    };
  }, [ChickensAgesUIContext]);

  return (
    <Card>
      {notify()}
      <CardHeader title="ChickensAge list">
        <CardHeaderToolbar>
        
          <button
            type="button"
            className="btn btn-primary"
            onClick={ChickensAgesUIProps.newChickensAgeButtonClick}
          >
            New ChickensAge
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ChickensAgesFilter />
        {ChickensAgesUIProps.ids.length > 0 && (
          <>
            {/* <ChickensAgesGrouping /> */}
          </>
        )}
        <ChickensAgesTable />
      </CardBody>
    </Card>
  );
}
