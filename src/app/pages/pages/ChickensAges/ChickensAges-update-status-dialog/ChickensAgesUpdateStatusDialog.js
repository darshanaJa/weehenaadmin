import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ChickensAgestatusCssClasses } from "../ChickensAgesUIHelpers";
import * as actions from "../../../_redux/ChickensAge/ChickensAgesActions";
import { useChickensAgesUIContext } from "../ChickensAgesUIContext";

const selectedChickensAges = (entities, ids) => {
  const _ChickensAges = [];
  ids.forEach((id) => {
    const ChickensAge = entities.find((el) => el.id === id);
    if (ChickensAge) {
      _ChickensAges.push(ChickensAge);
    }
  });
  return _ChickensAges;
};

export function ChickensAgesUpdateStatusDialog({ show, onHide }) {
  // ChickensAges UI Context
  const ChickensAgesUIContext = useChickensAgesUIContext();
  const ChickensAgesUIProps = useMemo(() => {
    return {
      ids: ChickensAgesUIContext.ids,
      setIds: ChickensAgesUIContext.setIds,
      queryParams: ChickensAgesUIContext.queryParams,
    };
  }, [ChickensAgesUIContext]);

  // ChickensAges Redux state
  const { ChickensAges, isLoading } = useSelector(
    (state) => ({
      ChickensAges: selectedChickensAges(state.ChickensAges.entities, ChickensAgesUIProps.ids),
      isLoading: state.ChickensAges.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected ChickensAges we should close modal
  useEffect(() => {
    if (ChickensAgesUIProps.ids || ChickensAgesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ChickensAgesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing ChickensAge by ids
    dispatch(actions.updateChickensAgesStatus(ChickensAgesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchChickensAges(ChickensAgesUIProps.queryParams)).then(
          () => {
            // clear selections list
            ChickensAgesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected ChickensAges
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {ChickensAges.map((ChickensAge) => (
              <div className="list-timeline-item mb-3" key={ChickensAge.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ChickensAgestatusCssClasses[ChickensAge.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {ChickensAge.id}
                  </span>{" "}
                  <span className="ml-5">
                    {ChickensAge.manufacture}, {ChickensAge.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${ChickensAgestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
