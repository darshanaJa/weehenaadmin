import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./ChickensAgesUIHelpers";

const ChickensAgesUIContext = createContext();

export function useChickensAgesUIContext() {
  return useContext(ChickensAgesUIContext);
}

export const ChickensAgesUIConsumer = ChickensAgesUIContext.Consumer;

export function ChickensAgesUIProvider({ ChickensAgesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newChickensAgeButtonClick: ChickensAgesUIEvents.newChickensAgeButtonClick,
    openEditChickensAgePage: ChickensAgesUIEvents.openEditChickensAgePage,
    openDeleteChickensAgeDialog: ChickensAgesUIEvents.openDeleteChickensAgeDialog,
    openDeleteChickensAgesDialog: ChickensAgesUIEvents.openDeleteChickensAgesDialog,
    openFetchChickensAgesDialog: ChickensAgesUIEvents.openFetchChickensAgesDialog,
    openUpdateChickensAgesStatusDialog: ChickensAgesUIEvents.openUpdateChickensAgesStatusDialog,
  };

  return (
    <ChickensAgesUIContext.Provider value={value}>
      {children}
    </ChickensAgesUIContext.Provider>
  );
}
