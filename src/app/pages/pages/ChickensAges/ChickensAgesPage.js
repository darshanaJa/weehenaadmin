import React from "react";
import { Route } from "react-router-dom";
import { ChickensAgesLoadingDialog } from "./ChickensAges-loading-dialog/ChickensAgesLoadingDialog";
import { ChickensAgeDeleteDialog } from "./ChickensAge-delete-dialog/ChickensAgeDeleteDialog";
import { ChickensAgesDeleteDialog } from "./ChickensAges-delete-dialog/ChickensAgesDeleteDialog";
import { ChickensAgesFetchDialog } from "./ChickensAges-fetch-dialog/ChickensAgesFetchDialog";
import { ChickensAgesUpdateStatusDialog } from "./ChickensAges-update-status-dialog/ChickensAgesUpdateStatusDialog";
import { ChickensAgesCard } from "./ChickensAgesCard";
import { ChickensAgesUIProvider } from "./ChickensAgesUIContext";

export function ChickensAgesPage({ history }) {
  const ChickensAgesUIEvents = {
    newChickensAgeButtonClick: () => {
      history.push("/buyback/chicken/new");
    },
    openEditChickensAgePage: (id) => {
      history.push(`/buyback/chicken/${id}/edit`);
    },
    openDeleteChickensAgeDialog: (sales_ChickensAge_id) => {
      history.push(`/buyback/chicken/${sales_ChickensAge_id}/delete`);
    },
    openDeleteChickensAgesDialog: () => {
      history.push(`/buyback/chicken/deleteChickensAges`);
    },
    openFetchChickensAgesDialog: () => {
      history.push(`/buyback/chicken/fetch`);
    },
    openUpdateChickensAgesStatusDialog: () => {
      history.push("/buyback/chicken/updateStatus");
    },
  };

  return (
    <ChickensAgesUIProvider ChickensAgesUIEvents={ChickensAgesUIEvents}>
      <ChickensAgesLoadingDialog />
      <Route path="/buyback/chicken/deleteChickensAges">
        {({ history, match }) => (
          <ChickensAgesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/chicken");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/chicken/:sales_ChickensAge_id/delete">
        {({ history, match }) => (
          <ChickensAgeDeleteDialog
            show={match != null}
            sales_ChickensAge_id={match && match.params.sales_ChickensAge_id}
            onHide={() => {
              history.push("/buyback/chicken");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/chicken/fetch">
        {({ history, match }) => (
          <ChickensAgesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/chicken");
            }}
          />
        )}
      </Route>
      <Route path="/buyback/chicken/updateStatus">
        {({ history, match }) => (
          <ChickensAgesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/buyback/chicken");
            }}
          />
        )}
      </Route>
      <ChickensAgesCard />
    </ChickensAgesUIProvider>
  );
}
