import React from 'react';
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';
import { notifyWarning } from "../../../config/Toastify";
import moment from 'moment';

const containerStyle = {
  position: 'relative',  
  width: '750',
  height: '600px'
}

export function MapContainer({
  google,
  onMapClicked,
  lattitude,
  longitude,
  locationTitel,
  timelocation,
  locationType,
  locationArray,
  lactionToname,
  numbera,
  locationArray3,
  locationArray1,
  locationArray2
}){

  console.log(locationType)
  console.log(locationArray2.length)
  console.log(locationArray.length)
  console.log(locationArray1.length)
  console.log(lactionToname)

  const locationArray5 = true




  
  if(locationType===0 ||locationType==null){
    if(locationArray2.length === 0 && numbera=== 2){
      return (
        <>
        {console.log("#######",lattitude,longitude)}
        <Map 
          containerStyle={containerStyle}
          google={google} 
          initialCenter={{
            lat: 7.6584,
            lng: 79.9881
          }}
          zoom={10}
          onClick={onMapClicked}
          
        >
        </Map>
    </>
      );
    }
    if(locationArray5){
      return (
        <>
        {console.log("#######",lattitude,longitude)}
        <Map 
          containerStyle={containerStyle}
          google={google} 
          initialCenter={{
            lat: 7.6584,
            lng: 79.9881
          }}
          zoom={10}
          onClick={onMapClicked}
          
        >
        </Map>
    </>
      );
    }
    
  }
  if(locationType==='1'){
    console.log("#######",lattitude,longitude)
    if(locationArray1.length=== 0){
      return (
        <>
        {console.log("#######",lattitude,longitude)}
        <Map 
          containerStyle={containerStyle}
          google={google} 
          initialCenter={{
            lat: 7.6584,
            lng: 79.9881
          }}
          zoom={10}
          onClick={onMapClicked}
          
        >
        </Map>
    </>
      );
    }

    if(locationArray1.length>0){
      console.log(locationArray1[0])
      return (
        <>
        {console.log("#######",lattitude,longitude)}
        <Map 
          containerStyle={containerStyle}
          google={google} 
          initialCenter={{
            lat: 7.6584,
            lng: 79.9881
          }}
          zoom={10}
          
        >
          <Marker
            title={"Name - "+ locationArray1[0].sales_agent_fname + " " + locationArray1[0].sales_agent_lname + "  Last Updated - " + locationTitel + " " + timelocation}
            name={'aaaaaaaaaaaaaaaaa'}
            position={{lat:locationArray1[0].lattitude, lng: locationArray1[0].longitude}} 
        
            />
        </Map>
    </>
      );
    }
    
    
  }
  if(locationType==='2'){
    if(locationArray2.length=== 0 || locationArray==null || locationArray===undefined || locationArray===''){
      return (
        <>
        {console.log("#######",lattitude,longitude)}
        <Map 
          containerStyle={containerStyle}
          google={google} 
          initialCenter={{
            lat: 7.6584,
            lng: 79.9881
          }}
          zoom={10}
          onClick={onMapClicked}
          
        >
        </Map>
    </>
      );
    }
    return(
          <>
          {notifyWarning()}
          {console.log("#######",lattitude,longitude)}
          <Map 
            containerStyle={containerStyle}
            google={google} 
            initialCenter={{
              lat: 7.6584,
              lng: 79.9881
            }}
            zoom={10}
            
          >
            {locationArray2.map((item) => (
                <Marker
                title={"Name - "+ item.salesAgent_sales_agent_fname + " " + item.salesAgent_sales_agent_lname + "  Date - " + (moment(item.SalesAgentLocationEntity_created_date).format("yyyy-MMM-DD")) + "  Time - " + (moment(item.SalesAgentLocationEntity_created_date).format("HH:mm"))}
                name={'SOMA'}
                position={{lat:item.SalesAgentLocationEntity_lattitude, lng: item.SalesAgentLocationEntity_longitude}} />
            ))}

          </Map>
      </>
        );
    
    
  }
  if(locationType==='3'){
    if(locationArray.length=== 0 || locationArray==null || locationArray===undefined || locationArray===''){
      return (
        <>
        {console.log("#######",lattitude,longitude)}
        <Map 
          containerStyle={containerStyle}
          google={google} 
          initialCenter={{
            lat: 7.6584,
            lng: 79.9881
          }}
          zoom={10}
          onClick={onMapClicked}
          
        >
        </Map>
    </>
      );
    }
    if(locationArray3.length>0){
      return (
        <>
        {notifyWarning()}
        {console.log("#######",lattitude,longitude)}
        <Map 
          containerStyle={containerStyle}
          google={google} 
          initialCenter={{
            lat: 7.6584,
            lng: 79.9881
          }}
          zoom={10}
          
        >
          {locationArray3.map((item) => (
              <Marker
              title={"Name - "+ item.salesAgent.sales_agent_fname + " " + item.salesAgent.sales_agent_lname + "  Date - " + (moment(item.created_date).format("yyyy-MMM-DD")) + "  Time - " + (moment(item.created_date).format("HH:mm"))}
              name={'SOMA'}
              position={{lat:item.lattitude, lng: item.longitude}} />
          ))}
        </Map>
    </>
      );
    }
    
  }
}
 
export default GoogleApiWrapper({
  apiKey: ("AIzaSyDEBjyqHLZe0x1zcSUcq0X9AHiYNTsUh1g")
})(MapContainer)