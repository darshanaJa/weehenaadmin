import React, {useState} from "react";
import {
  Card,
  CardBody,
  CardHeader,
} from "../../../../_metronic/_partials/controls";
import MapContainer from "./Remarks";
import {AgentCreateForm} from "./AgentCreateForm"
import { notifyWarning } from "../../../config/Toastify";

export function AgentsCard(){

  const[lati, setlattitude] = useState(0);
  const[logintu, setlongitude] = useState(0);
  const[locationTitel,setLocationTitel]=useState();
  const[timelocation,setTimeLocation]=useState();
  const[locationType,setLocationType]=useState(0);
  const[locationArray3,setLocationArray3]=useState();
  const[locationArray,setLocationArray]=useState(0);
  const[locationArray2,setLocationArray2]=useState([]);
  const[locationArray1,setLocationArray1]=useState([]);
  const[lactionToname,setLactionToname]=useState();
  const[numbera,setNumber]=useState(0);


  console.log(locationType)

  
  


  return (
    <>
    {notifyWarning()}
    <Card>
      <CardHeader title="Track Sale Agents"> 
      </CardHeader>
      <CardBody>
            
            <AgentCreateForm 
              setlattitude={setlattitude} 
              setlongitude={setlongitude}
              setLocationTitel={setLocationTitel}
              setTimeLocation={setTimeLocation}
              setLocationType={setLocationType}
              setLocationArray={setLocationArray}
              locationType={locationType}
              setLactionToname={setLactionToname}
              locationArray={locationArray}
              setNumber={setNumber}
              lactionToname={lactionToname}
              setLocationArray3={setLocationArray3}
              setLocationArray1={setLocationArray1}
              setLocationArray2={setLocationArray2}
            />

        <MapContainer
          lattitude={lati}
          longitude={logintu}
          locationTitel={locationTitel}
          timelocation={timelocation}
          locationType={locationType}
          locationArray={locationArray}
          lactionToname={lactionToname}
          numbera={numbera}
          locationArray3={locationArray3}
          locationArray1={locationArray1}
          locationArray2={locationArray2}
         />
      </CardBody>
    </Card>
    </>
  );
}
