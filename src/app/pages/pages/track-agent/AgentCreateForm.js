import React, {useState, useEffect} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import "../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { Input,Select} from "../../../../_metronic/_partials/controls";
import axios from 'axios';
import { AGENTS_GET_URL } from "../../_redux/agent/agentsCrud";
import { Api_Login } from "../../../../app/config/config";
import moment from 'moment';
import { notifyWarning } from "../../../config/Toastify";



export function AgentCreateForm({
  Agent,
  btnRef,
  saveAgent,
  entities,
  setlattitude,
  setlongitude,
  setLocationTitel,
  setTimeLocation,
  setLocationType,
  setLocationArray,
  locationType,
  setLactionToname,
  locationArray,
  setNumber,
  lactionToname,
  setLocationArray3,
  setLocationArray1,
  setLocationArray2
}) {

const[agent, setAgent] = useState([]);

const StockBatchEditSchema = Yup.object().shape({});

const [reset,setReset]=useState(0);
const [agentnameFinal,setAgentNameFinal]=useState(0);
const[locationTypeFinal,setLocationTypeFinal]=useState(0);


const initStockBatch = {
  ageName:'0',
  from:'',
  to:'',
  ageLocation:'',
};

const agnetnamehandler =(e)=>{
  console.log(e.target.value)
  setAgentNameFinal(e.target.value)
}

const LocationtypeHandler =(e)=>{
  console.log(e.target.value)
  setLocationTypeFinal(e.target.value)
}

const resetHandler =(e)=> {
  setReset(0)
  setLocationType(0)
  setAgentNameFinal(0)
  setLocationTypeFinal(0)
}

useEffect(()=>{
  axios({
    method: 'get',
    baseURL: AGENTS_GET_URL
    })
    .then((res) => {
      setAgent(res.data)
    })
    .catch(function (response) {
    });

    
},[])

let newName

if(agent !== null){
  newName = agent.find(entity => entity.sales_agent_id===agentnameFinal)
}

console.log(agentnameFinal)

  return (
    <>
      {notifyWarning()}
          <Formik
            enableReinitialize={true}
            initialValues={initStockBatch}
            validationSchema={StockBatchEditSchema}
            onSubmit={(values) => {

              if(agentnameFinal===0){
                return notifyWarning("Please select Agent Name")
              }
              if(newName && locationTypeFinal===0){
                return notifyWarning("Please select Location Type")
              }
              if(locationTypeFinal==='2' && values.from===''){
                return notifyWarning("Please select From Date")
              }
              if(locationTypeFinal==='2' && values.to===''){
                return notifyWarning("Please select To Date")
              }
              else{
                setReset(1)
                  if(agentnameFinal==='1'){

                    console.log(agentnameFinal)
                    console.log(values.from)
                    console.log(values.to)
                    console.log(locationTypeFinal)

                  return(
                    axios({
                    method: 'get',
                    baseURL: Api_Login + '/api/sales-agent-location/all/salesagntlocation',
                  
                    })
                    .then((res) => {
                      console.log(res.data)
                      setTimeout(() => {

                        setLocationArray3(res.data)
                        setLocationType('3')
                        setLocationArray1([])
                        setLocationArray2([])

  
                        console.log(locationTypeFinal)
                        
                      }, 1000);
                
                    })
                    .catch(function (response) {
                        console.log(response);
                        console.log("erooooooooooooo")
                    })
                    )
                  }
                  if(newName && locationTypeFinal==='1'){
                    //  values.from==='' && values.to===''){

                    console.log(agentnameFinal)
                    console.log(values.from)
                    console.log(values.to)
                    console.log(locationTypeFinal)

                  return(
                    axios({
                      method: 'post',
                      baseURL: Api_Login + '/api/sales-agent-location/search',
                      data:{
                        "filter": [{"salesAgentSalesAgentId": agentnameFinal}],
                        "sort": "DESC",
                      }
                    })
                    .then((res) => {

                      setTimeout(() => {
                        if(res.data.data.length > 0){

                          setLocationType('1')
                          setLocationArray1(res.data.data)
                          setLocationArray2([])

                          setLocationArray3([])

                          
                          console.log(res.data.data[0])
                          
                          setLactionToname(res.data.data[0])
                          setlongitude(res.data.data[0].lattitude)
                          setlattitude(res.data.data[0].longitude)
                          setLocationTitel(moment(res.data.data[0].created_date).format("yyyy-MMM-DD"))
                          setTimeLocation(moment(res.data.data[0].created_date).format("HH:mm"))
                        }
                        else{
                          setLocationArray1(res.data.data)
                          setLocationType('1')
                          setNumber(2)
                      }
                      // setLocationArray(res.data.data)
                      console.log(locationTypeFinal)
                      }, 1000);
                      
                      
                     
                      
                      
                      
                
                    })
                    .catch(function (response) {

                      setTimeout(() => {
                        if(locationArray.length === 0){
                          setLocationType('1')
                        }
                        
                      }, 100);
                      // setLocationType(values.ageLocation)
                        console.log(response);
                        console.log("erooooooooooooo")
                    })
                  ) 
                    
                  }
                  else{
                    


                    setNumber(2)

                    console.log(agentnameFinal)
                    console.log(values.from)
                    console.log(values.to)
                    console.log(locationTypeFinal)

                    return(
                      axios({
                        method: 'post',
                        baseURL: Api_Login + '/api/sales-agent-location/search',
                        data:{
                          "filter": [{"salesAgentSalesAgentId": agentnameFinal}],
                          "sort": "DESC",
                          "from": values.from,
                          "to": values.to,
                      }
                        })
                        .then((res) => {
                          // setShop(res.data)
                          setTimeout(() => {
                            
                            setLocationType(locationTypeFinal)
                            setLocationArray(res.data.data.results)
                            setLocationArray2(res.data.data.results)
                            console.log(res.data.data.results)
                          }, 1000);
    
                          console.log("aaaaaaaaaaaaaaaaaa")
                    
                        })
                        .catch(function (response) {
                            console.log(response);
                            console.log("erooooooooooooo")
                        })
                      
                    )
                }
                    



              }
            }}
          >
            {({ handleSubmit }) => (
              <>
                <Form className="form form-label-right">
                  <div className="form-group row">
                    <div className="col-lg-3">
                      <Select name="ageName" 
                        value={agentnameFinal} 
                        label="Agent Name" 
                        onChange={agnetnamehandler}
                      >
                          <option value="0">Choose One</option>
                          <option value="1">All</option>
                          {agent.map((item) => (
                              <option value={item.sales_agent_id} >
                                {item.sales_agent_fname}
                              </option>    
                          ))}
                      </Select>
                    </div>
                    {newName &&<div className="col-lg-3">
                      <Select name="ageLocation" value={locationTypeFinal} label="Location Type" 
                        onChange={LocationtypeHandler}
                      >
                          <option value="0">Choose One</option>
                          <option value="1">Last Location</option>
                          <option value="2">Location History</option>
                      </Select>
                    </div>}
                    {newName  && locationTypeFinal==='2' && <div className="col-lg-2">
                      <Field
                        type="date"
                        name="from"
                        component={Input}
                        placeholder="From Date"
                        label="From Date"
                      />
                    </div>}
                    {newName  && locationTypeFinal==='2' && <div className="col-lg-2">
                      <Field
                        type="date"
                        name="to"
                        component={Input}
                        placeholder="To Date"
                        label="To Date"
                      />
                    </div>}
                    <div className="col-lg-2">
                        <button type="submit" className="btn btn-primary ml-2 + downbtn"> Find </button>
                        <button type="reset" hidden={reset===0}  className="btn btn-light ml-2 + downbtn" onClick={resetHandler}>Reset</button>
                         </div>
                  </div>
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    onSubmit={() => handleSubmit()}
                  ></button>
                  
                </Form>
                
              </>
            )}
          </Formik>
         
    </>
  );
}
