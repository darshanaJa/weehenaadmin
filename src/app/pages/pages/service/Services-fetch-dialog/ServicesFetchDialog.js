import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { ServicestatusCssClasses } from "../ServicesUIHelpers";
import { useServicesUIContext } from "../ServicesUIContext";

const selectedServices = (entities, ids) => {
  const _Services = [];
  ids.forEach((id) => {
    const Service = entities.find((el) => el.id === id);
    if (Service) {
      _Services.push(Service);
    }
  });
  return _Services;
};

export function ServicesFetchDialog({ show, onHide }) {
  // Services UI Context
  const ServicesUIContext = useServicesUIContext();
  const ServicesUIProps = useMemo(() => {
    return {
      ids: ServicesUIContext.ids,
      queryParams: ServicesUIContext.queryParams,
    };
  }, [ServicesUIContext]);

  // Services Redux state
  const { Services } = useSelector(
    (state) => ({
      Services: selectedServices(state.Services.entities, ServicesUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!ServicesUIProps.ids || ServicesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ServicesUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Services.map((Service) => (
              <div className="list-timeline-item mb-3" key={Service.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ServicestatusCssClasses[Service.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Service.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Service.manufacture}, {Service.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
