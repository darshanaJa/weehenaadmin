/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Service/ServicesActions";
import { useServicesUIContext } from "../ServicesUIContext";

export function ServicesDeleteDialog({ show, onHide }) {
  // Services UI Context
  const ServicesUIContext = useServicesUIContext();
  const ServicesUIProps = useMemo(() => {
    return {
      ids: ServicesUIContext.ids,
      setIds: ServicesUIContext.setIds,
      queryParams: ServicesUIContext.queryParams,
    };
  }, [ServicesUIContext]);

  // Services Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Services.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Services we should close modal
  useEffect(() => {
    if (!ServicesUIProps.ids || ServicesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ServicesUIProps.ids]);

  const deleteServices = () => {
    // server request for deleting Service by seleted ids
    dispatch(actions.deleteServices(ServicesUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchServices(ServicesUIProps.queryParams)).then(() => {
        // clear selections list
        ServicesUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Services Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Services?</span>
        )}
        {isLoading && <span>Services are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteServices}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
