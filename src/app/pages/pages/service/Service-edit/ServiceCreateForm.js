// Form is based on Formik
import React, {useState,useEffect} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input,Input1,Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { useDispatch,useSelector } from "react-redux";
import { ServicesSlice } from "../../../_redux/Service/ServicesSlice";
// import { Sparegrns_URL } from "../../../_redux/Sparegrn/SparegrnsCrud";
import axios from "axios";
import { notify, notifyWarning } from "../../../../config/Toastify";
import { Vechicales_URL } from "../../../_redux/Vechicale/VechicalesCrud";
import { SpareStocks_URL } from "../../../_redux/SpareStock/SpareStocksCrud";


// Validation schema
const ServiceEditSchema = Yup.object().shape({
  // id: Yup.string(),
  description: Yup.string()
    .required("Description is required")
    .min(2, "Description must be at least 2 characters"),
  service_milage: Yup.number()
    .required("Milage is required")
    .min(2, "Milage must be at least 2 characters"),
  service_next_milage: Yup.number()
    .required("Milage is required")
    .min(2, "Milage must be at least 2 characters"),
});


export function ServiceCreateForm({
  Service,
  btnRef,
  saveService,
}) {

  const {actions} = ServicesSlice;
  const dispatch = useDispatch();

  const spareIt =  useSelector(state => state.Services.spareItems)

  // console.log(spareIt)

  const [shop,setShop]=useState([]);

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: SpareStocks_URL,
      data:{ 
        "filter": [{"status": "1" }]
      }
      })
      .then((res) => {
        console.log(res.data.data.results)
        setShop(res.data.data.results)
        console.log(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  const [shop2,setShop2]=useState([]);

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Vechicales_URL,
      data:{ 
        "filter": [{"status": "1" }]
      }
      })
      .then((res) => {
        setShop2(res.data.data.results)
        console.log(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  const bodyFormData = new FormData();

  const [profile_pic,set_Profile_pic]=useState();
  const [spareId,setspareId]=useState();
  const [sparename,setsparename]=useState();

  const sparePartidhandler =(e)=> {
    setspareId(e.target.value)
  }

  const sparePertQuntityHandler =(e)=> {
    setsparename(e.target.value)
  }
  const submitTodoHandler =()=> {
    if(spareId==null || spareId==='Choose One' || spareId==='')
    {
      return(notifyWarning('Plese Select Spare Part Name'));
      
    }
    if(sparename==null ||sparename==='')
    {
      return(notifyWarning('Plese Select Spare Part Quantity'));
    }
    else{
      dispatch(actions.AddSpareItem({
        id:spareId,
        quantity:sparename
      }))
    }
    setspareId('');
    setsparename('');
  }

  // const editHandler =(id)=>{

  // }

  // const deleteHandler =(id)=>{
  //   dispatch(actions.SpareDeleteHandler(id))
  // }

  const obj2= JSON.stringify(spareIt);

  return (
    <>
      {notify()}
      <Formik
        enableReinitialize={true}
        initialValues={Service}
        validationSchema={ServiceEditSchema}
        onSubmit={(values)  => {
          bodyFormData.append('description',values.description);
          bodyFormData.append('service_milage',values.service_milage);
          bodyFormData.append('service_next_milage',values.service_next_milage);
          bodyFormData.append('vehic',values.vehic);
          bodyFormData.append('spare_parts',obj2);
          bodyFormData.append('vehi_image',profile_pic);

          console.log(bodyFormData);
          saveService(bodyFormData);

          dispatch(actions.SpareItemReset())


        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="service_milage"
                    component={Input}
                    placeholder="Service Milage"
                    label="Service Milage"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="service_next_milage"
                    component={Input}
                    placeholder="Next Service Milage"
                    label="Next Service Milage"
                  />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Select name="spare_parts" label="Spare Parts" value={spareId}  onChange={sparePartidhandler}>
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.id} >
                            {item.spare_part_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="spare_parts"
                      component={Input1}
                      placeholder="Spare Parts"
                      label="Spare Parts"
                      value={sparename}
                      onChange={sparePertQuntityHandler}
                    />
                </div>
                <div className="col-lg-4">
                  <button hidden={!(spareId && sparename)} type="button" onClick={submitTodoHandler} className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                </div>
              </div>

              {spareIt.map((item) => (
                       
                       <div className="form-group row">
                       <div className="col-lg-4">
                         <Field
                             type="text"
                             name="spare_parts"
                             component={Input1}
                             placeholder="Spare Parts"
                             label="Spare Parts"
                             value={item.grnsparestock}
                           />
                       </div>
                       <div className="col-lg-4">
                         <Field
                             type="text"
                             name="spare_parts"
                             component={Input1}
                             placeholder="Spare Parts"
                             label="Spare Parts"
                             value={item.sp_quantity}
                           />
                       </div>
                       {/* <div className="col-lg-2">
                        <button type="button" 
                        // hidden={!(idQuntity && price)}
                         onClick={editHandler(item.grnsparestock)}
                          className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                       </div> */}
                       <div className="col-lg-2">
                        <button type="button" 
                        // hidden={!(idQuntity && price)}
                         onClick={()=> {dispatch(actions.SpareDeleteHandler(item.grnsparestock))}}
                          className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                       </div>
                       {/* <div className="col-lg-2">
                        <button type="button" 
                        // hidden={!(idQuntity && price)}
                        //  onClick={submitTodoHandler}
                          className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                       </div> */}
                    </div>
                       
                      ))}

              <div className="form-group row">
              <div className="col-lg-4">
                   <Select name="vehic" label="Vechicle">
                      <option>Choose One</option>
                      {shop2.map((item) => (
                          <option value={item.vehicle_id} >
                            {item.vehi_reg_number}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                    <div className="form-group row">
                    <p className="empFont">Add Vichicale Image</p>
                      <input className="agentImageBtn2" type="file" name="profile_pic"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    </div>
                </div>
                <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
              </div>
           

                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>

            <div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
}
