// Form is based on Formik
import React, {useState,useEffect, useReducer} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input,Input1,Select } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { useDispatch,useSelector } from "react-redux";
import { ServicesSlice } from "../../../_redux/Service/ServicesSlice";
// import { Sparegrns_URL } from "../../../_redux/Sparegrn/SparegrnsCrud";
import axios from "axios";
import { notify, notifyWarning } from "../../../../config/Toastify";
import { Vechicales_URL } from "../../../_redux/Vechicale/VechicalesCrud";
import { Image_Url } from "../../../../config/config";
import { Services_GET_ID_URL } from "../../../_redux/Service/ServicesCrud";
import { SpareStocks_URL } from "../../../_redux/SpareStock/SpareStocksCrud";
import Admin from "../../../../config/Admin";

// Validation schema
const ServiceEditSchema = Yup.object().shape({
  // id: Yup.string(),
  description: Yup.string()
    .required("Description is required")
    .min(2, "Description must be at least 2 characters"),
  service_milage: Yup.number()
    .required("Milage is required")
    .min(2, "Milage must be at least 2 characters"),
  service_next_milage: Yup.number()
    .required("Milage is required")
    .min(2, "Milage must be at least 2 characters"),
});


export function ServiceEditForm({
  service2,
  Service,
  btnRef,
  saveService,
  image2,
  spare,
  bacd,
  id,
  adminId,
  createDate,
  updateDate
}) {

  const {actions} = ServicesSlice;
  const dispatch = useDispatch();

  
  const [profile_pic,set_Profile_pic]=useState();
  const [spareId,setspareId]=useState();
  const [sparename,setsparename]=useState();
  const [shop,setShop]=useState([]);
  const [spareMap,setSpareMap]=useState([]);

  const spareIt =  useSelector(state => state.Services.spareItems)

    const submitImage=()=>{
      return(
        <img className="agentImg" alt="Service" src={url} />
      );
  }

  console.log(service2)
  

  // const abMap = bacd.servicesparepart
  // const abMap2 = {abMap}

  
  // console.log(bacd.servicesparepart);
  
  const pic = image2.vehi_image
  const url = Image_Url+pic
  
  console.log(spareIt)
  
  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Services_GET_ID_URL + `/${id}`,
    })
    .then((res) => {
      setSpareMap(res.data.servicesparepart)
      console.log(res.data.servicesparepart)
      arrayDispatch({type:'add', item:res.data.servicesparepart})
    })
    .catch(function (response) {
      console.log(response);
    });
    
  },[id])

  console.log(spareMap)

  // if(spareMap.length > 0){
  //   spareMap.map((item) => (
  //     dispatch(actions.AddSpareItem({
  //       id:item.id,
  //       quantity:item.sp_quantity
  //     }))
  //   ))
  // }

//   useEffect(() => {
//     arrayDispatch({type:'add', item:Items4})
//     // spareMap.map((item) => (
//     //   dispatch(actions.AddSpareItem({
//     //     id:item.id,
//     //     quantity:item.sp_quantity
//     //   }))
//     // ))
//   // }, [spareMap])
// }, [dispatch])

  
  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: SpareStocks_URL,
      data:{ 
        "filter": [{"status": "1" }]
      }
      })
      .then((res) => {
        setShop(res.data.data.results)
        console.log(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  const [shop2,setShop2]=useState([]);

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Vechicales_URL,
      data:{ 
        "filter": [{"status": "1" }]
      }
      })
      .then((res) => {
        setShop2(res.data.data.results)
        console.log(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  const bodyFormData = new FormData();
  

  const sparePartidhandler =(e)=> {
    setspareId(e.target.value)
    console.log(spareIt)

  }

  const sparePertQuntityHandler =(e)=> {
    setsparename(e.target.value)
  }


  const arrayInitialState = {
    sapreArry2:[]
  }

  const arrayReducer =(state, action)=>{
    if(action.type === 'add'){
      return(
          spareMap.map((item) => (
          dispatch(actions.AddSpareItem({
            id:item.id,
            quantity:item.sp_quantity
          }))
        )) 
      );
  }
    return arrayInitialState;
  }

  const [arrayState, arrayDispatch] = useReducer(arrayReducer, arrayInitialState)
  console.log(arrayState);

  // const arrayhandler= useCallback(()=>  {
  //   spareMap.map((item) => (
  //         dispatch(actions.AddSpareItem({
  //           id:item.id,
  //           quantity:item.sp_quantity
  //         }))
  //       )) 
  // },[spareMap])

  // console.log(fiId)
  // const fliterId = spareIt.find((el) => el.grnsparestock === spareId)
  // console.log(fliterId)
  
  
  //   const fiId = parseInt(spareId)

  //   console.log(spareMap)
  //   const fliterId2 = spareIt.find((el) => el.id === fiId)
  //   console.log(fliterId2)

  

  const submitTodoHandler =()=> {
    // if(fliterId || fliterId2)
    // {
    //   return(notifyWarning('Plese Select Spare Part Name'));
    // }
    if(spareId==null || spareId==='Choose One' || spareId==='')
    {
      return(notifyWarning('Plese Select Spare Part Name'));
      
    }
    if(sparename==null ||sparename==='')
    {
      return(notifyWarning('Plese Select Spare Part Quantity'));
    }
    else{
      dispatch(actions.AddSpareItem({
        id:spareId,
        quantity:sparename
      }))
    }
    setspareId('');
    setsparename('');
  }

  // const editHandler =(id)=>{

  // }

  // const deleteHandler =(id)=>{
  //   dispatch(actions.SpareDeleteHandler(id))
  // }

  const obj2= JSON.stringify(spareIt);

  return (
    <>
      {notify()}
      <Formik
        enableReinitialize={true}
        initialValues={Service}
        validationSchema={ServiceEditSchema}
        onSubmit={(values)  => {
          bodyFormData.append('description',values.description);
          bodyFormData.append('service_milage',values.service_milage);
          bodyFormData.append('service_next_milage',values.service_next_milage);
          bodyFormData.append('vehic',values.vehic);
          bodyFormData.append('spare_parts',obj2);
          bodyFormData.append('vehi_image',profile_pic);

          console.log(bodyFormData);
          saveService(bodyFormData);

          dispatch(actions.SpareItemReset())


        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="service_milage"
                    component={Input}
                    placeholder="Service Milage"
                    label="Service Milage"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="service_next_milage"
                    component={Input}
                    placeholder="Next Service Milage"
                    label="Next Service Milage"
                  />
                </div>
              </div>

              {/* <div className="form-group row">
                <div className="col-lg-4">
                  <button style={{ display: "none" }}  onClick={arrayhandler()} />
                </div>
              </div>   */}

              <div className="form-group row">
                <div className="col-lg-4">
                    <Select name="spare_parts" label="Spare Parts" value={spareId}  onChange={sparePartidhandler}>
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.id} >
                            {item.spare_part_name}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="spare_parts"
                      component={Input1}
                      placeholder="Spare Parts"
                      label="Spare Parts"
                      value={sparename}
                      onChange={sparePertQuntityHandler}
                    />
                </div>
                <div className="col-lg-4">
                  <button hidden={!(spareId && sparename)} type="button" onClick={submitTodoHandler} className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                </div>
              </div>

              {spareIt.map((item) => (
                       
                       <div className="form-group row">
                       <div className="col-lg-4">
                         <Field
                             type="text"
                             name="spare_parts"
                             component={Input1}
                             placeholder="Spare Parts"
                             label="Spare Parts"
                             value={item.grnsparestock}
                           />
                       </div>
                       <div className="col-lg-4">
                         <Field
                             type="text"
                             name="spare_parts"
                             component={Input1}
                             placeholder="Spare Parts"
                             label="Spare Parts"
                             value={item.sp_quantity}
                           />
                       </div>
                       {/* <div className="col-lg-2">
                        <button type="button" 
                        // hidden={!(idQuntity && price)}
                         onClick={editHandler(item.grnsparestock)}
                          className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                       </div> */}
                       <div className="col-lg-2">
                        <button type="button" 
                        // hidden={!(idQuntity && price)}
                         onClick={()=> {dispatch(actions.SpareDeleteHandler(item.grnsparestock))}}
                          className="btnform + downbtn" ><i class="fa fa-trash"></i></button>
                       </div>
                       {/* <div className="col-lg-2">
                        <button type="button" 
                        // hidden={!(idQuntity && price)}
                        //  onClick={submitTodoHandler}
                          className="btnform + downbtn" ><i class="fa fa-plus"></i></button>
                       </div> */}
                    </div>
                       
                      ))}

              <div className="form-group row">
              <div className="col-lg-4">
                   <Select name="vehic" label="Vechicle">
                      <option>Choose One</option>
                      {shop2.map((item) => (
                          <option value={item.vehicle_id} >
                            {item.vehi_reg_number}
                          </option>    
                      ))}
                  </Select>
                </div>
                <div className="col-lg-4">
                    <div className="form-group row">
                    <p className="empFont">Add Vichicale Image</p>
                      <input className="agentImageBtn2" type="file" name="profile_pic"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    </div>
                </div>
                <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
              </div>
              <div className="form-group row">
                {submitImage()}
               </div>
           

                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>

                <Admin 
                  adminId={adminId}
                  createDate={createDate} 
                  updateDate={updateDate}
                />
           
            </Form>

            <div>
            </div>
          </>
        )}
      </Formik>
    </>
  );
}





// import React, {useState, useEffect} from "react";
// import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
// import { Input, Input1, Select } from "../../../../../_metronic/_partials/controls";
// import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
// import { Image_Url } from "../../../../config/config";
// import axios from "axios";
// import { Vechicales_URL } from "../../../_redux/Vechicale/VechicalesCrud";

// // Validation schema
// const ServiceEditSchema = Yup.object().shape({
//   // id: Yup.string(),
//   description: Yup.string()
//     .required("Description is required")
//     .min(2, "Description must be at least 2 characters"),
//   service_milage: Yup.number()
//     .required("Milage is required")
//     .min(2, "Milage must be at least 2 characters"),
//   service_next_milage: Yup.number()
//     .required("Milage is required")
//     .min(2, "Milage must be at least 2 characters"),
// });


// export function ServiceEditForm({
//   Service,
//   btnRef,
//   saveService,
//   image2,
//   spare
// }) {

//   // console.log(image2.vehi_image)

//   const submitImage=()=>{
//       return(
//         <img className="agentImg" alt="Service" src={url} />
//       );
//   }

//   console.log(spare);

//   // const [shop,setShop]=useState([]);

//   // useEffect(()=>{
//   //   axios({
//   //     method: 'post',
//   //     baseURL: Sparegrns_URL,
//   //     data:{ 
//   //       "filter": [{"status": "1" }]
//   //     }
//   //     })
//   //     .then((res) => {
//   //       setShop(res.data.data.results)
//   //       console.log(res.data.data.results)
//   //     })
//   //     .catch(function (response) {
//   //         // console.log(response);
//   //     });
      
//   // },[])

//   const pic = image2.vehi_image
//   const url = Image_Url+pic



//   const bodyFormData = new FormData();

//   const [profile_pic,set_Profile_pic]=useState();

//   const [shop,setShop]=useState([]);

//   useEffect(()=>{
//     axios({
//       method: 'post',
//       baseURL: Vechicales_URL,
//       data:{ 
//         "filter": [{"status": "1" }]
//       }
//       })
//       .then((res) => {
//         setShop(res.data.data.results)
//         console.log(res.data.data.results)
//       })
//       .catch(function (response) {
//           // console.log(response);
//       });
      
//   },[])

//   return (
//     <>
//       {/* {notify()} */}
//       <Formik
//         enableReinitialize={true}
//         initialValues={Service}
//         validationSchema={ServiceEditSchema}
//         onSubmit={(values)  => {
//           bodyFormData.append('description',values.description);
//           bodyFormData.append('service_milage',values.service_milage);
//           bodyFormData.append('service_next_milage',values.service_next_milage);
//           bodyFormData.append('vehic',values.vehic);
//           // bodyFormData.append('spare_parts',obj2);
//           bodyFormData.append('vehi_image',profile_pic);

//           console.log(bodyFormData);
//           saveService(bodyFormData);

//           // dispatch(actions.SpareItemReset())


//         }}
//       >
//         {({ handleSubmit }) => (
//           <>
//             <Form className="form form-label-right">
//               <div className="form-group row">
//                 <div className="col-lg-4">
//                 <Field
//                     type="text"
//                     name="description"
//                     component={Input}
//                     placeholder="Description"
//                     label="Description"
//                   />
//                 </div>
      
//                 <div className="col-lg-4">
//                   <Field
//                     type="text"
//                     name="service_milage"
//                     component={Input}
//                     placeholder="Service Milage"
//                     label="Service Milage"
//                   />
//                 </div>
//                 <div className="col-lg-4">
//                   <Field
//                     type="text"
//                     name="service_next_milage"
//                     component={Input}
//                     placeholder="Next Service Milage"
//                     label="Next Service Milage"
//                   />
//                 </div>
//               </div>
//               <div className="form-group row">
//                 <div className="col-lg-4">
//                    <Select name="vehic" label="Vechicle">
//                       {/* <option>Choose One</option> */}
//                       {shop.map((item) => (
//                           <option value={item.vehicle_id} >
//                             {item.vehi_reg_number}
//                           </option>    
//                       ))}
//                   </Select>
//                 </div>
//                 <div className="col-lg-4">
//                     <div className="form-group row">
//                     <p className="empFont">Add Vichicale Image</p>
//                       <input className="agentImageBtn2" type="file" name="profile_pic"
//                         onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
//                        />
//                     </div>
//                 </div>
//                 <div className="col-lg-4">
//                     <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
//                   </div>
//               </div>
           

//                 <button
//                 type="submit"
//                 style={{ display: "none" }}
//                 ref={btnRef}
//                 onSubmit={() => handleSubmit()}
//               ></button>
//               <div className="form-group row">
//                   {submitImage()}
//                 </div>
           
//             </Form>

//             <div>
//             </div>
//           </>
//         )}
//       </Formik>
//     </>
//   );
// }
