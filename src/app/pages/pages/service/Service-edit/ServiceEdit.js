/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Service/ServicesActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { ServiceEditForm } from "./ServiceEditForm";
import { ServiceCreateForm } from "./ServiceCreateForm";
import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../Service-specifications/Specifications";
import { SpecificationsUIProvider } from "../Service-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Service-remarks/RemarksUIContext";
import { Remarks } from "../Service-remarks/Remarks";
import { serviceSliceActions } from "../../../_redux/Service/ServicesSlice";
import axios from "axios";
import { Services_GET_ID_URL } from "../../../_redux/Service/ServicesCrud";

const initServicePassword = {
  curent_password : "",
  new_password : "",
  confirm_password : ""

};

export function ServiceEdit({
  history,
  match: {
    params: { id },
  },
}) {

  // const {actions2} = ServicesSlice;
  // const dispatch = useDispatch();

  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  const [adminId, setadminId] = useState('')
  const [createDate1, setcreateDate1] = useState('')
  const [updateDate, setupdateDate] = useState('')
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, ServiceForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Services.actionsLoading,
      ServiceForEdit: state.Services.ServiceForEdit,
    }),
    shallowEqual
  );

  const detail ={...ServiceForEdit}
  // console.log(detail)
  // console.log(detail.vehic)
  const vech = detail.vehic
  const vechical = {...vech}
  const spareMap = detail.servicesparepart
  const spareMap2 = spareMap
  
  // console.log(vechical.vehicle_id)

  const initService = {

    description : detail.description,
    service_milage : detail.service_milage,
    service_next_milage : detail.service_next_milage,
    vehic : vechical.vehicle_id,
  
  };
  // info.id=ServiceForEdit.id

  useEffect(() => {
    dispatch(actions.fetchService(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Service";
    if (ServiceForEdit && id) {
      // _title = `Edit Service - ${ServiceForEdit.sales_Service_fname} ${ServiceForEdit.sales_Service_lname} - ${ServiceForEdit.sales_Service_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ServiceForEdit, id]);

  const saveService = (values) => {
    if (!id) {
      dispatch(actions.createService(values,id)).then(() => backToServicesList());
    } else {
      dispatch(actions.updateService(values,id)).then(() => backToServicesList());
    }
  };

  useEffect(() => {
    axios({
      method: 'get',
      baseURL: Services_GET_ID_URL + `/${id}`
      })
      .then((res) => {
        setadminId(res.data.created_by.id);
        setcreateDate1(res.data.vehicle_service_date);
        setupdateDate(res.data.vehicle_service_updated);
        
      })
      .catch(function (response) {
      });
  }, [id])

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateServicePassword(values,id)).then(() => backToServicesList());
  };

  const btnRef = useRef();  

  const backToServicesList = () => {
    history.push(`/transport/services`);
    dispatch(serviceSliceActions.SpareItemReset())

  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToServicesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Reset Password
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ServiceEditForm
                actionsLoading={actionsLoading}
                service2={ServiceForEdit}
                Service={initService}
                btnRef={btnRef}
                saveService={saveService}
                image2={detail}
                spare={spareMap2}
                bacd={ServiceForEdit}
                id={id}
                adminId={adminId}
                createDate={createDate1}
                updateDate={updateDate}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentServiceId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentServiceId={id}>
                <PasswordReset
                  actionsLoading={actionsLoading}
                  Service={initServicePassword}
                  btnRef={btnRef}
                  savePassword={savePassword}
    />
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToServicesList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Service remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Service specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <ServiceCreateForm
                actionsLoading={actionsLoading}
                Service={ServiceForEdit || initService}
                btnRef={btnRef}
                saveService={saveService}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentServiceId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentServiceId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
