// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Service/ServicesActions";
import * as uiHelpers from "../ServicesUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useServicesUIContext } from "../ServicesUIContext";

export function ServicesTable() {
  // Services UI Context
  const ServicesUIContext = useServicesUIContext();
  const ServicesUIProps = useMemo(() => {
    return {
      ids: ServicesUIContext.ids,
      setIds: ServicesUIContext.setIds,
      queryParams: ServicesUIContext.queryParams,
      setQueryParams: ServicesUIContext.setQueryParams,
      openEditServicePage: ServicesUIContext.openEditServicePage,
      openDeleteServiceDialog: ServicesUIContext.openDeleteServiceDialog,
    };
  }, [ServicesUIContext]);
  // Getting curret state of Services list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Services }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Services Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    ServicesUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchServices(ServicesUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ServicesUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Service_id);

  const columns = [
    // {
    //   dataField: "sales_Service_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "service_milage",
      text: "Service Milage",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "service_next_milage",
      text: "Next Serive",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "vehicle_service_date",
      text: "Service Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "vehicle_service_updated",
      text: "Service Update Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.StatusColumnFormatter,
    },
    // {
    //   dataField: "sales_Service_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditServicePage: ServicesUIProps.openEditServicePage,
        openDeleteServiceDialog: ServicesUIProps.openDeleteServiceDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: ServicesUIProps.queryParams.pageSize,
    page: ServicesUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Service_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  ServicesUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: ServicesUIProps.ids,
                  setIds: ServicesUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
