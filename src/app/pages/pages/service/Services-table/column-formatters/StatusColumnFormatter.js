import React from "react";
import {
  ServicestatusCssClasses,
  ServicestatusTitles
} from "../../ServicesUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      ServicestatusCssClasses[row.status]
    } label-inline`}
  >
    {ServicestatusTitles[row.status]}
  </span>
);
