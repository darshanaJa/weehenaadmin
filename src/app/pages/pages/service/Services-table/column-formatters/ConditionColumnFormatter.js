import React from "react";
import {
  ServiceConditionCssClasses,
  ServiceConditionTitles
} from "../../ServicesUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        ServiceConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        ServiceConditionCssClasses[row.condition]
      }`}
    >
      {ServiceConditionTitles[row.condition]}
    </span>
  </>
);
