import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { ServicesFilter } from "./Services-filter/ServicesFilter";
import { ServicesTable } from "./Services-table/ServicesTable";
// import { ServicesGrouping } from "./Services-grouping/ServicesGrouping";
import { useServicesUIContext } from "./ServicesUIContext";
import { notify } from "../../../config/Toastify";

export function ServicesCard(msg){

  const ServicesUIContext = useServicesUIContext();
  const ServicesUIProps = useMemo(() => {
    return {
      ids: ServicesUIContext.ids,
      queryParams: ServicesUIContext.queryParams,
      setQueryParams: ServicesUIContext.setQueryParams,
      newServiceButtonClick: ServicesUIContext.newServiceButtonClick,
      openDeleteServicesDialog: ServicesUIContext.openDeleteServicesDialog,
      openEditServicePage: ServicesUIContext.openEditServicePage,
      openUpdateServicesStatusDialog:ServicesUIContext.openUpdateServicesStatusDialog,
      openFetchServicesDialog: ServicesUIContext.openFetchServicesDialog,
    };
  }, [ServicesUIContext]);

  return (
    <>
    <Card>
      {notify()}
      <CardHeader title="Service list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={ServicesUIProps.newServiceButtonClick}
          >
            New Service
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <ServicesFilter />
        {ServicesUIProps.ids.length > 0 && (
          <>
            {/* <ServicesGrouping /> */}
          </>
        )}
        <ServicesTable />
      </CardBody>
    </Card>
    </>
  );
}
