import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ServicestatusCssClasses } from "../ServicesUIHelpers";
import * as actions from "../../../_redux/Service/ServicesActions";
import { useServicesUIContext } from "../ServicesUIContext";

const selectedServices = (entities, ids) => {
  const _Services = [];
  ids.forEach((id) => {
    const Service = entities.find((el) => el.id === id);
    if (Service) {
      _Services.push(Service);
    }
  });
  return _Services;
};

export function ServicesUpdateStatusDialog({ show, onHide }) {
  // Services UI Context
  const ServicesUIContext = useServicesUIContext();
  const ServicesUIProps = useMemo(() => {
    return {
      ids: ServicesUIContext.ids,
      setIds: ServicesUIContext.setIds,
      queryParams: ServicesUIContext.queryParams,
    };
  }, [ServicesUIContext]);

  // Services Redux state
  const { Services, isLoading } = useSelector(
    (state) => ({
      Services: selectedServices(state.Services.entities, ServicesUIProps.ids),
      isLoading: state.Services.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Services we should close modal
  useEffect(() => {
    if (ServicesUIProps.ids || ServicesUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ServicesUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Service by ids
    dispatch(actions.updateServicesStatus(ServicesUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchServices(ServicesUIProps.queryParams)).then(
          () => {
            // clear selections list
            ServicesUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Services
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Services.map((Service) => (
              <div className="list-timeline-item mb-3" key={Service.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      ServicestatusCssClasses[Service.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Service.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Service.manufacture}, {Service.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${ServicestatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
