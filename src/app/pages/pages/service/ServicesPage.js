import React from "react";
import { Route } from "react-router-dom";
import { ServicesLoadingDialog } from "./Services-loading-dialog/ServicesLoadingDialog";
import { ServiceDeleteDialog } from "./Service-delete-dialog/ServiceDeleteDialog";
import { ServicesDeleteDialog } from "./Services-delete-dialog/ServicesDeleteDialog";
import { ServicesFetchDialog } from "./Services-fetch-dialog/ServicesFetchDialog";
import { ServicesUpdateStatusDialog } from "./Services-update-status-dialog/ServicesUpdateStatusDialog";
import { ServicesCard } from "./ServicesCard";
import { ServicesUIProvider } from "./ServicesUIContext";
import { notify } from "../../../config/Toastify";

// window.location.reload()

export function ServicesPage({ history }) {
  const ServicesUIEvents = {
    newServiceButtonClick: () => {
      history.push("/transport/services/new");
    },
    openEditServicePage: (id) => {
      history.push(`/transport/services/${id}/edit`);
    },
    openDeleteServiceDialog: (sales_Service_id) => {
      history.push(`/transport/services/${sales_Service_id}/delete`);
    },
    openDeleteServicesDialog: () => {
      history.push(`/transport/services/deleteServices`);
    },
    openFetchServicesDialog: () => {
      history.push(`/transport/services/fetch`);
    },
    openUpdateServicesStatusDialog: () => {
      history.push("/transport/services/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <ServicesUIProvider ServicesUIEvents={ServicesUIEvents}>
      <ServicesLoadingDialog />
      <Route path="/transport/services/deleteServices">
        {({ history, match }) => (
          <ServicesDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/services");
            }}
          />
        )}
      </Route>
      <Route path="/transport/services/:sales_Service_id/delete">
        {({ history, match }) => (
          <ServiceDeleteDialog
            show={match != null}
            sales_Service_id={match && match.params.sales_Service_id}
            onHide={() => {
              history.push("/transport/services");
            }}
          />
        )}
      </Route>
      <Route path="/transport/services/fetch">
        {({ history, match }) => (
          <ServicesFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/services");
            }}
          />
        )}
      </Route>
      <Route path="/transport/services/updateStatus">
        {({ history, match }) => (
          <ServicesUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/transport/services");
            }}
          />
        )}
      </Route>
      <ServicesCard />
    </ServicesUIProvider>
    </>
  );
}
