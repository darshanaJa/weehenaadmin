export const ServicestatusCssClasses = ["success", "info", ""];
export const ServicestatusTitles = ["Selling", "Sold"];
export const ServiceConditionCssClasses = ["success", "danger", ""];
export const ServiceConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_Service_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    sales_Service_fname: ""
  },
  sales_Service_fname:null,
  sortOrder: "asc", // asc||desc
  sortField: "sales_Service_fname",
  pageNumber: 1,
  pageSize: 10
  // value:
};
