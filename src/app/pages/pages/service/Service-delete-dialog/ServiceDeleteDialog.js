/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Service/ServicesActions";
import { useServicesUIContext } from "../ServicesUIContext";

export function ServiceDeleteDialog({ sales_Service_id, show, onHide }) {
  // console.log(sales_Service_id)
  // Services UI Context
  const ServicesUIContext = useServicesUIContext();
  const ServicesUIProps = useMemo(() => {
    return {
      setIds: ServicesUIContext.setIds,
      queryParams: ServicesUIContext.queryParams,
    };
  }, [ServicesUIContext]);

  // Services Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Services.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Service_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Service_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteService = () => {
    // server request for deleting Service by id
    dispatch(actions.deleteService(sales_Service_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchServices(ServicesUIProps.queryParams));
      // clear selections list
      ServicesUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Service Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Service?</span>
        )}
        {isLoading && <span>Service is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteService}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
