import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./ServicesUIHelpers";

const ServicesUIContext = createContext();

export function useServicesUIContext() {
  return useContext(ServicesUIContext);
}

export const ServicesUIConsumer = ServicesUIContext.Consumer;

export function ServicesUIProvider({ ServicesUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newServiceButtonClick: ServicesUIEvents.newServiceButtonClick,
    openEditServicePage: ServicesUIEvents.openEditServicePage,
    openDeleteServiceDialog: ServicesUIEvents.openDeleteServiceDialog,
    openDeleteServicesDialog: ServicesUIEvents.openDeleteServicesDialog,
    openFetchServicesDialog: ServicesUIEvents.openFetchServicesDialog,
    openUpdateServicesStatusDialog: ServicesUIEvents.openUpdateServicesStatusDialog,
  };

  return (
    <ServicesUIContext.Provider value={value}>
      {children}
    </ServicesUIContext.Provider>
  );
}
