import React, { useMemo } from "react";
import { useServicesUIContext } from "../ServicesUIContext";

export function ServicesGrouping() {
  // Services UI Context
  const ServicesUIContext = useServicesUIContext();
  const ServicesUIProps = useMemo(() => {
    return {
      ids: ServicesUIContext.ids,
      setIds: ServicesUIContext.setIds,
      openDeleteServicesDialog: ServicesUIContext.openDeleteServicesDialog,
      openFetchServicesDialog: ServicesUIContext.openFetchServicesDialog,
      openUpdateServicesStatusDialog:
        ServicesUIContext.openUpdateServicesStatusDialog,
    };
  }, [ServicesUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{ServicesUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={ServicesUIProps.openDeleteServicesDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ServicesUIProps.openFetchServicesDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={ServicesUIProps.openUpdateServicesStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
