/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Acces/AccessActions";
import { useAccessUIContext } from "../AccessUIContext";

export function AccesDeleteDialog({ sales_Acces_id, show, onHide }) {
  // console.log(sales_Acces_id)
  // Access UI Context
  const AccessUIContext = useAccessUIContext();
  const AccessUIProps = useMemo(() => {
    return {
      setIds: AccessUIContext.setIds,
      queryParams: AccessUIContext.queryParams,
    };
  }, [AccessUIContext]);

  // Access Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Access.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Acces_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Acces_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteAcces = () => {
    // server request for deleting Acces by id
    dispatch(actions.deleteAcces(sales_Acces_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchAccess(AccessUIProps.queryParams));
      // clear selections list
      AccessUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Acces Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Acces?</span>
        )}
        {isLoading && <span>Acces is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteAcces}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
