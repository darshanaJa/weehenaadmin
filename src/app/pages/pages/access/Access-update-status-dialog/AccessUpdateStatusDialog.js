import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { AccesstatusCssClasses } from "../AccessUIHelpers";
import * as actions from "../../../_redux/Acces/AccessActions";
import { useAccessUIContext } from "../AccessUIContext";

const selectedAccess = (entities, ids) => {
  const _Access = [];
  ids.forEach((id) => {
    const Acces = entities.find((el) => el.id === id);
    if (Acces) {
      _Access.push(Acces);
    }
  });
  return _Access;
};

export function AccessUpdateStatusDialog({ show, onHide }) {
  // Access UI Context
  const AccessUIContext = useAccessUIContext();
  const AccessUIProps = useMemo(() => {
    return {
      ids: AccessUIContext.ids,
      setIds: AccessUIContext.setIds,
      queryParams: AccessUIContext.queryParams,
    };
  }, [AccessUIContext]);

  // Access Redux state
  const { Access, isLoading } = useSelector(
    (state) => ({
      Access: selectedAccess(state.Access.entities, AccessUIProps.ids),
      isLoading: state.Access.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Access we should close modal
  useEffect(() => {
    if (AccessUIProps.ids || AccessUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [AccessUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Acces by ids
    dispatch(actions.updateAccessStatus(AccessUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchAccess(AccessUIProps.queryParams)).then(
          () => {
            // clear selections list
            AccessUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Access
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Access.map((Acces) => (
              <div className="list-timeline-item mb-3" key={Acces.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      AccesstatusCssClasses[Acces.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Acces.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Acces.manufacture}, {Acces.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${AccesstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
