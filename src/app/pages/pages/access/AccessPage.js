import React from "react";
import { Route } from "react-router-dom";
import { AccessLoadingDialog } from "./Access-loading-dialog/AccessLoadingDialog";
import { AccesDeleteDialog } from "./Acces-delete-dialog/AccesDeleteDialog";
import { AccessDeleteDialog } from "./Access-delete-dialog/AccessDeleteDialog";
import { AccessFetchDialog } from "./Access-fetch-dialog/AccessFetchDialog";
import { AccessUpdateStatusDialog } from "./Access-update-status-dialog/AccessUpdateStatusDialog";
import { AccessCard } from "./AccessCard";
import { AccessUIProvider } from "./AccessUIContext";
import { notify } from "../../../config/Toastify";

// window.location.reload()

export function AccessPage({ history }) {
  const AccessUIEvents = {
    newAccesButtonClick: () => {
      history.push("/access/permisions/new");
    },
    openEditAccesPage: (id) => {
      history.push(`/access/permisions/${id}/edit`);
    },
    openDeleteAccesDialog: (sales_Acces_id) => {
      history.push(`/access/permisions/${sales_Acces_id}/delete`);
    },
    openDeleteAccessDialog: () => {
      history.push(`/access/permisions/deleteAccess`);
    },
    openFetchAccessDialog: () => {
      history.push(`/access/permisions/fetch`);
    },
    openUpdateAccessStatusDialog: () => {
      history.push("/access/permisions/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <AccessUIProvider AccessUIEvents={AccessUIEvents}>
      <AccessLoadingDialog />
      <Route path="/access/permisions/deleteAccess">
        {({ history, match }) => (
          <AccessDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/access/permisions");
            }}
          />
        )}
      </Route>
      <Route path="/access/permisions/:sales_Acces_id/delete">
        {({ history, match }) => (
          <AccesDeleteDialog
            show={match != null}
            sales_Acces_id={match && match.params.sales_Acces_id}
            onHide={() => {
              history.push("/access/permisions");
            }}
          />
        )}
      </Route>
      <Route path="/access/permisions/fetch">
        {({ history, match }) => (
          <AccessFetchDialog
            show={match != null}
            onHide={() => {
              history.push("access/permisions");
            }}
          />
        )}
      </Route>
      <Route path="/sales/Acces/updateStatus">
        {({ history, match }) => (
          <AccessUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/access/permisions");
            }}
          />
        )}
      </Route>
      <AccessCard />
    </AccessUIProvider>
    </>
  );
}
