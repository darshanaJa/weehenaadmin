/* eslint-disable no-restricted-imports */
import React, { useState, useEffect } from "react";
import { shallowEqual, useSelector } from "react-redux";
import { Modal } from "react-bootstrap";
import { ModalProgressBar } from "../../../../../../_metronic/_partials/controls";
// import { SpecificationEditForm } from "./SpecificationEditForm";

export function SpecificationEditDialogHeader({ id }) {
  const [title, setTitle] = useState("");
  // Specs Redux state
  const { specificationForEdit, actionsLoading } = useSelector(
    (state) => ({
      specificationForEdit: state.specifications.specificationForEdit,
      actionsLoading: state.specifications.actionsLoading,
    }),
    shallowEqual
  );

  useEffect(() => {
    let _title = id ? "" : "New Module Names";
    // if (specificationForEdit && id) {
      if (specificationForEdit || id) {
      _title = "Edit Module Name";
    }

    // console.log(id)

    setTitle(_title);
    // eslint-disable-next-line
  }, [specificationForEdit, actionsLoading]);
  return (
    <>
      {/* <SpecificationEditForm idedit={id} /> */}
      {actionsLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">{title}</Modal.Title>
      </Modal.Header>
    </>
  );
}
