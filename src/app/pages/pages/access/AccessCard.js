import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { AccessFilter } from "./Access-filter/AccessFilter";
import { AccessTable } from "./Access-table/AccessTable";
// import { AccessGrouping } from "./Access-grouping/AccessGrouping";
import { useAccessUIContext } from "./AccessUIContext";
import { notify } from "../../../config/Toastify";
// import MapContainer from "./Acces-remarks/Remarks";

export function AccessCard(msg){

  // const [name,setName] = useState()

  // setName(msg)
  // console.log(msg)

  // useEffect(()=>{
  //   console.log(msg)
  //   // notify("abcd")
  // },[])

  const AccessUIContext = useAccessUIContext();
  const AccessUIProps = useMemo(() => {
    return {
      ids: AccessUIContext.ids,
      queryParams: AccessUIContext.queryParams,
      setQueryParams: AccessUIContext.setQueryParams,
      newAccesButtonClick: AccessUIContext.newAccesButtonClick,
      openDeleteAccessDialog: AccessUIContext.openDeleteAccessDialog,
      openEditAccesPage: AccessUIContext.openEditAccesPage,
      openUpdateAccessStatusDialog:AccessUIContext.openUpdateAccessStatusDialog,
      openFetchAccessDialog: AccessUIContext.openFetchAccessDialog,
    };
  }, [AccessUIContext]);

  return (
    <>
    {/* <MapContainer /> */}
    <Card>
      {notify()}
      <CardHeader title="Acces list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={AccessUIProps.newAccesButtonClick}
          >
            New Acces
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <AccessFilter />
        {AccessUIProps.ids.length > 0 && (
          <>
            {/* <AccessGrouping /> */}
          </>
        )}
        <AccessTable />
      </CardBody>
    </Card>
    </>
  );
}
