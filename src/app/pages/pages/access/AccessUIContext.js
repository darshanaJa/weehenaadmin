import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./AccessUIHelpers";

const AccessUIContext = createContext();

export function useAccessUIContext() {
  return useContext(AccessUIContext);
}

export const AccessUIConsumer = AccessUIContext.Consumer;

export function AccessUIProvider({ AccessUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newAccesButtonClick: AccessUIEvents.newAccesButtonClick,
    openEditAccesPage: AccessUIEvents.openEditAccesPage,
    openDeleteAccesDialog: AccessUIEvents.openDeleteAccesDialog,
    openDeleteAccessDialog: AccessUIEvents.openDeleteAccessDialog,
    openFetchAccessDialog: AccessUIEvents.openFetchAccessDialog,
    openUpdateAccessStatusDialog: AccessUIEvents.openUpdateAccessStatusDialog,
  };

  return (
    <AccessUIContext.Provider value={value}>
      {children}
    </AccessUIContext.Provider>
  );
}
