import React from "react";
import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { Input } from "../../../../../_metronic/_partials/controls";
import axios from 'axios';
// import { AGENTS_GET_URL } from "../../_redux/agent/agentsCrud";
import { Api_Login } from "../../../../config/config";
// import moment from 'moment';
import { notifyWarning } from "../../../../config/Toastify";



export function Register(
){


const initStockBatch = {
  ageName:'0',
  from:'',
  to:'',
  ageLocation:'',
};


// const resetHandler =(e)=> {
//   setReset(0)
//   setLocationType(0)
//   setAgentNameFinal(0)
//   setLocationTypeFinal(0)
// }


  return (
    <>
      {notifyWarning()}
          <Formik
            enableReinitialize={true}
            initialValues={initStockBatch}
            // validationSchema={StockBatchEditSchema}
            onSubmit={(values) => {

                axios({
                    method: 'post',
                    baseURL: Api_Login + '/api/access-modules/add',
                    data:{module_name:values.from}
                    })
                    .then((res) => {
                      console.log(res.data);
                    })
                    .catch(function (response) {
                        console.log(response);
                    })
                
            }}
          >
            {({ handleSubmit }) => (
              <>
                <Form className="form form-label-right">
                  <div className="form-group row">
                    <div className="col-lg-4">
                      <Field
                        type="text"
                        name="from"
                        component={Input}
                        placeholder="Module Name"
                        label="Module Name"
                      />
                    </div>
                      <div className="col-lg-2">
                        <button type="submit" className="btn btn-primary ml-2 + downbtn"> Find </button>
                      </div>
                      <div className="col-lg-2">  
                        <button type="reset"  className="btn btn-light ml-2 + downbtn"
                        //  onClick={resetHandler}
                        >Reset</button>
                      </div>   
                    {/* </div> */}
                  </div>
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    onSubmit={() => handleSubmit()}
                  ></button>
                  
                </Form>
                
              </>
            )}
          </Formik>
         
    </>
  );
}
