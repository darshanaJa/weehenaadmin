import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

const AccesEditSchema = Yup.object().shape({
  curent_password: Yup.string()
    .required("Curent Passwrod is required")
    .min(2, "Curerent password"),
  new_password: Yup.string()
    .min(3, "Minimum 3 symbols")
    .max(50, "Maximum 50 symbols")
    .required(),
    //   intl.formatMessage({
    //     id: "AUTH.VALIDATION.REQUIRED_FIELD",
    //   })
    // ),
  confirm_password: Yup.string()
  .required()
  .when("new_password", {
    is: (val) => (val && val.length > 0 ? true : false),
    then: Yup.string().oneOf(
      [Yup.ref("new_password")],
      "Password and Confirm Password didn't match"
    ),
  }),
});


export function PasswordReset({
  Acces,
  btnRef,
  savePassword,
}) {

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={Acces}
        validationSchema={AccesEditSchema}
        onSubmit={(values)  => {
          console.log(values);
          savePassword(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="curent_password"
                    component={Input}
                    placeholder="Current Password"
                    label="Current Password"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="new_password"
                    component={Input}
                    placeholder="New Password"
                    label="New Password"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="confirm_password"
                    component={Input}
                    placeholder="Confirm Password"
                    label="Confirm Password"
                  />
                </div>
              </div>
              <div className="form-group row">
                <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2">Reset Password</button>
                 </div>
              </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>
   

  );
}
