/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Acces/AccessActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { AccesEditForm } from "./AccesEditForm";
import { AccesCreateForm } from "./AccesCreateForm";
// import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../Acces-specifications/Specifications";
import { SpecificationsUIProvider } from "../Acces-specifications/SpecificationsUIContext";
import { Specifications2 } from "../Acces-remarks/Specifications";
import { SpecificationsUIProvider2 } from "../Acces-remarks/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
// import { RemarksUIProvider } from "../Acces-remarks/RemarksUIContext";
// import MapContainer from "../Acces-remarks/Remarks"
// import { Api_Login } from '../../../../config/config';
// import Register from './Register';
// import Permisions from './Permision'
import { Access_GET_ID_URL } from "../../../_redux/Acces/AccessCrud";

import axios from "axios";
// import moment from 'moment';
import { Items_URL } from "../../../_redux/specificationsModule/specificationsCrud";
import { Items_URL2 } from "../../../_redux/specificationsPermission/specificationsCrud";

const initAcces = {

  sales_Acces_fname : "",
  sales_Acces_lname : "",
  sales_Acces_nic : "",
  sales_Acces_mobile_1 : "",
  sales_Acces_mobile_2 : "",
  sales_Acces_email : "",
  address:"",
  password:""

};

// const initAccesPassword = {
//   curent_password : "",
//   new_password : "",
//   confirm_password : ""

// };

export function AccesEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // const [valu, setvalu] = useState({id:''});

  // const [permision, setpermision] = useState([])
  // const [module, setmodule] = useState([])
  const [updatePermision, setupdatePermision] = useState([])
  const [updatePermisonLevel, setupdatePermisonLevel] = useState([])

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, AccesForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Access.actionsLoading,
      AccesForEdit: state.Access.AccesForEdit,
    }),
    shallowEqual
  );

  const [shop,setShop]=useState([]);

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Access_GET_ID_URL + `/${id}`
      })
      .then((res) => {
        setShop(res.data)
        console.log(res.data)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[id])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Items_URL,
      data: {
        "filter": [{"status": "1"}],
        "sort": "DESC",
        "limit": "", 
        "skip": ""
      }
      })
      .then((res) => {
        // setShop(res.data)
        setupdatePermisonLevel(res.data.data.results)
        console.log(res.data.data.results)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Items_URL2,
      data: {
        "filter": [{"status": "1"}],
        "sort": "DESC",
        "limit": "", 
        "skip": ""
      }
      })
      .then((res) => {
        setupdatePermision(res.data.data.results)
        console.log(res.data.data.results)

      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[])

  
  

  // const info = {}
  // const valu2 = AccesForEdit

  // info.id=valu2.id
  // setvalu(info)

  console.log(AccesForEdit);
  // console.log(valu.id);
  // setvalu(AccesForEdit.access_created_date)
  // console.log(AccesForEdit.access_created_date)



 
  useEffect(() => {
    dispatch(actions.fetchAcces(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Acces";
    if (AccesForEdit && id) {
      // _title = `Edit Acces - ${AccesForEdit.sales_Acces_fname} ${AccesForEdit.sales_Acces_lname} - ${AccesForEdit.sales_Acces_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [AccesForEdit, id]);

  const saveAcces = (values) => {
    if (!id) {
      dispatch(actions.createAcces(values,id)).then(() => backToAccessList());
    } else {
      dispatch(actions.updateAcces(values,id)).then(() => backToAccessList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateAccesPassword(values,id)).then(() => backToAccessList());
  };

  const btnRef = useRef();  

  const backToAccessList = () => {
    history.push(`/access/permisions`);
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToAccessList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <AccesEditForm
                actionsLoading={actionsLoading}
                Acces={AccesForEdit}
                btnRef={btnRef}
                saveAcces={saveAcces}
                idAccess={id}
                ab={shop.permission_view}
                moduleName={updatePermisonLevel}
                permisionLevel={updatePermision}
                // entities={entity}
              />
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentAccesId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
            {tab === "remarks" && id && (
              <SpecificationsUIProvider2 currentAccesId={id}>
                <Specifications2 />
              </SpecificationsUIProvider2>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToAccessList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {`  `}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            { (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Module
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Permission Level
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <AccesCreateForm
                actionsLoading={actionsLoading}
                Acces={AccesForEdit || initAcces}
                btnRef={btnRef}
                saveAcces={saveAcces}
                savePassword={savePassword}
                moduleName={updatePermisonLevel}
                permisionLevel={updatePermision}
                // entities={entity}
              />
            )}
            {tab === "remarks" && (
              <SpecificationsUIProvider2 currentAccesId={id}>
                <Specifications2 setupdatePermisonLevel={setupdatePermisonLevel} />
              </SpecificationsUIProvider2>
            )}
            {tab === "specs"  && (
              <SpecificationsUIProvider currentAccesId={id}>
                <Specifications setupdatePermision={setupdatePermision} />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
