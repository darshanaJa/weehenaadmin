import React, {useState, useEffect} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { notify } from "../../../../config/Toastify";
import Switch from '@material-ui/core/Switch';
// import Switch from '@material-ui/core/Switch';
import axios from "axios";
import { Access_GET_ID_URL } from "../../../_redux/Acces/AccessCrud";

const AccesEditSchema = Yup.object().shape({
  // sales_Acces_fname: Yup.string()
  //   .required("Name is required")
  //   .min(2, "First Name must be at least 2 characters"),
  // sales_Acces_lname: Yup.string()
  //   .required("Name is required")
  //   .min(2, "Last Name must be at least 2 characters"),
  // address: Yup.string()
  //   .required("Address is required")
  //   .min(2, "Address must be at least 2 characters"),
  //   password: Yup.string()
  //   .required("Password is required")
  //   .min(5, "Password must be at least 5 characters"),
  // sales_Acces_nic: Yup.string()
  //   .required("Acces NIC is required")
  //   .min(10, "NIC be at least 9 characters"),
  // sales_Acces_email: Yup.string().email()
  //   .required("Email is required"),
  // sales_Acces_mobile_1: Yup.string()
  //   .required("Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  // sales_Acces_mobile_2: Yup.string()
  //   .required("Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  
  // profile_pic: Yup.string()
  // .required("Required")
});


export function AccesEditForm({
  Acces,
  btnRef,
  saveAcces,
  entities,
  idAccess,
  ab,
  moduleName,
  permisionLevel
}) {

  // const bodyFormData = new FormData();

  const [chAdd, setAdd] = useState(false)
  const [chEdit, setEdit] = useState(false)
  const [chDelete, setDelete] = useState(false)
  const [chView, setView] = useState(false)
  // const [ModuleId, setModuleId] = useState()
  const [levelID, setlevelID] = useState()

  // const [profile_pic,set_Profile_pic]=useState();
  // const [email,setEmail]=useState('');
  // const [nic,setNic]=useState('');

  // let newEmail
  // let newNic

  // console.log(Acces.id)
  // const [shop,setShop]=useState([]);

  


  // setTimeout(() => {

    // const [state, setState] = React.useState({
    //   checkedA: ab,
    //   checkedB: true,
    // });
  
    // const handleChange = (event) => {
    //   setState({ ...state, [event.target.name]: event.target.checked });
    // };
    
  //   const [state, setState] = React.useState({
  //     checkedA: true,
  //     checkedB: shop.permission_add,
  //     checkedC: shop.permission_edit,
  //     checkedD: shop.permission_delete,
  //   },[shop]);
  // // }, 1000);

  
  // const [id, setid] = useState('')
  const [moduleId, setmoduleId] = useState('')

  const initStockBatch = {
    // access_id:id,
    // access_module_id:moduleId,
    // permission_view:state.checkedA,
    // permission_add:state.checkedB,
    // permission_edit:state.checkedC,
    // permission_delete:state.checkedD,

    access_id:levelID,
    access_module_id:moduleId,
    permission_view:chView,
    permission_add:chAdd,
    permission_edit:chEdit,
    permission_delete:chDelete,
    // checkedA:true,
    // checkedB:''
    
    // access_module_id:shop.access_module.id,
    // permission_view:shop.permission_view,
    // permission_view:'',
    // permission_add:shop.permission_add,
    // permission_edit:shop.permission_edit,
    // permission_delete:shop.permission_delete,


  };

  


  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Access_GET_ID_URL + `/${idAccess}`
      })
      .then((res) => {
        // setShop(res.data)
        setAdd(res.data.permission_add)
        setView(res.data.permission_view)
        setEdit(res.data.permission_edit)
        setDelete(res.data.permission_delete)
        setlevelID(res.data.access.id)
        setmoduleId(res.data.access_module.module_name)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[idAccess])

  
  // const handleChange = name => event => {
  //   setState({ ...state, [name]: event.target.checked });
  //   console.log(event.target.checked)
  // };

  // if(entities !== null){
  //    newEmail = entities.find(entity => entity.sales_Acces_email===email)
  //    newNic = entities.find(entity => entity.sales_Acces_nic===nic)
  //   console.log(newEmail)
  //   if(newEmail){
  //     notifyWarning("Already Taken Email");
  //     console.log(newEmail.sales_Acces_email);
  //   }
  //   if(newNic){
  //     notifyWarning("Already Taken Nic");
  //   }

  // }

  

  

  // ch=true

  console.log(chAdd)

  const handleChangeAdd =(event)=>{
    console.log(event.target.checked)
    setAdd(event.target.checked)
  }

  const handleChangeView =(event)=>{
    console.log(event.target.checked)
    setView(event.target.checked)
  }

  const handleChangeEdit =(event)=>{
    console.log(event.target.checked)
    setEdit(event.target.checked)
  }

  const handleChangeDelete =(event)=>{
    console.log(event.target.checked)
    setDelete(event.target.checked)
  }


  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={initStockBatch}
        validationSchema={AccesEditSchema}
        onSubmit={(values)  => {

          console.log(values);
          saveAcces(values)

        }}
      >
        {({ handleSubmit }) => (
          <>
          {notify()}
          <Form className="form form-label-right">
                  <div className="form-group row">
                    {/* <div className="col-lg-4">
                    <p><b> Access Module Name </b></p>
                      <Field
                        type="text"
                        name="access_module.module_name"
                        component={Input1}
                        placeholder=""
                        // label="Access Module"
                        value={moduleId}
                        readOnly="readOnly"
                        // onChange={(e) => {setmoduleId(e.target.value)}}
                        // onChange={(e) => {setmoduleId(e.target.value)}}
                      />
                    </div> */}
                  {/* </div> */}
                  {/* <div className="form-group row">   */}
                    <div className="col-lg-4">
                    <p><b> Add Access Level </b></p>
                      <Field
                        type="text"
                        name="access_idz"
                        component={Input}
                        placeholder=""
                        // label="Access Level"
                        // value={levelID}
                        // onChange={(e) => {setlevelID(e.target.value)}}
                        // onChange={(e) => {setid(e.target.value)}}
                      />
                      {/* <Select name="access_id" label="Permision Level Name" 
                        onChange={(e) => {setlevelID(e.target.value)}}
                        value={levelID}
                      >
                        <option>Choose One</option>
                        {permisionLevel.map((item) => (
                            <option value={item.id} >
                              {item.access_level}
                            </option>    
                        ))}
                      </Select> */}
                    </div>
                    <div className="col-lg-4">
                      <button type="button" className="btnform + downbtnAccess" ><i class="fa fa-plus"></i></button>
                    </div>
                  </div>

                  <p><b> Sale Agent </b></p>
                  <div className="form-group row">
                    <div className="col-lg-3">
                      <p>Permision View</p>
                      <Switch
                       checked={chView}
                       value={chView}
                       onChange={handleChangeView}
                       color="primary"
                       inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                    <div className="col-lg-3">
                      <p>Permision Add</p>
                      <Switch
                        checked={chAdd}
                        value={chAdd}
                        onChange={handleChangeAdd}
                        color="primary"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                    <div className="col-lg-3">
                      <p>Permision Edit</p>
                      <Switch
                      checked={chEdit}
                      value={chEdit}
                      onChange={handleChangeEdit}
                      color="primary"
                      inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                    <div className="col-lg-3">
                      <p>Permision Delete</p>
                      <Switch
                       checked={chDelete}
                       value={chDelete}
                       onChange={handleChangeDelete}
                       color="primary"
                       inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                  </div>  

                  <p><b> Gate Keeper </b></p>
                  <div className="form-group row">
                    <div className="col-lg-3">
                      <p>Permision View</p>
                      <Switch
                       checked={chView}
                       value={chView}
                       onChange={handleChangeView}
                       color="primary"
                       inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                    <div className="col-lg-3">
                      <p>Permision Add</p>
                      <Switch
                        checked={chAdd}
                        value={chAdd}
                        onChange={handleChangeAdd}
                        color="primary"
                        inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                    <div className="col-lg-3">
                      <p>Permision Edit</p>
                      <Switch
                      checked={chEdit}
                      value={chEdit}
                      onChange={handleChangeEdit}
                      color="primary"
                      inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                    <div className="col-lg-3">
                      <p>Permision Delete</p>
                      <Switch
                       checked={chDelete}
                       value={chDelete}
                       onChange={handleChangeDelete}
                       color="primary"
                       inputProps={{ 'aria-label': 'secondary checkbox' }}
                      />
                    </div>
                  </div>  



                  <div className="form-group row">  
                      <div className="col-lg-2">
                        <button type="submit" className="btn btn-primary ml-2"> Save </button>
                      </div>
                      {/* <div className="col-lg-2">  
                        <button type="reset"  className="btn btn-light ml-2 + downbtn"
                        //  onClick={resetHandler}
                        >Reset</button>
                      </div>    */}
                    {/* </div> */}
                  </div>
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    onSubmit={() => handleSubmit()}
                  ></button>
                  
                </Form>

            <div>
               
            </div>
          </>
        )}
      </Formik>
    </>
  )
}
