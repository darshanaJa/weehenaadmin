import React from "react";
import {
  AccesConditionCssClasses,
  AccesConditionTitles
} from "../../AccessUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        AccesConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        AccesConditionCssClasses[row.condition]
      }`}
    >
      {AccesConditionTitles[row.condition]}
    </span>
  </>
);
