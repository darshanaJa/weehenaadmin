/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";

export const ActionsColumnFormatter = (
  cellContent,
  row,
  rowIndex,
  { openEditChickensAgePage, openDeleteChickensAgeDialog }
) => (
  <>
    <OverlayTrigger
      overlay={<Tooltip id="Access-edit-tooltip">Edit Acces</Tooltip>}
    >
      <a
        className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
        onClick={() => openEditChickensAgePage(row.id)}
      >
        <span className="svg-icon svg-icon-md svg-icon-primary">
          <SVG
            src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
          />
        </span>
      </a>
    </OverlayTrigger>

    <> </>
    <OverlayTrigger
       overlay={<Tooltip id="Access-delete-tooltip">Delete Acces</Tooltip>}
    >
      <a
        className="btn btn-icon btn-light btn-hover-danger btn-sm"
        onClick={() => openDeleteChickensAgeDialog(row.id)}
      >
        <span className="svg-icon svg-icon-md svg-icon-danger">
          <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
        </span>
      </a>
    </OverlayTrigger>
  </>
);




// import React from "react";
// import { OverlayTrigger, Tooltip } from "react-bootstrap";
// import SVG from "react-inlinesvg";
// import { toAbsoluteUrl } from "../../../../../../_metronic/_helpers";

// export const ActionsColumnFormatter = (
//   cellContent,
//   row,
//   rowIndex,
//   { openEditAccesPage, openDeleteAccesDialog }
// ) => (
//   <>
//     <OverlayTrigger
//       overlay={<Tooltip id="Access-edit-tooltip">Edit Acces</Tooltip>}
//     >
//       <a
//         className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
//         onClick={() => openEditAccesPage(row.id)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-primary">
//           <SVG
//             src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
//           />
//         </span>
//       </a>
//     </OverlayTrigger>

//     <> </>
//     <OverlayTrigger
//       overlay={<Tooltip id="Access-delete-tooltip">Delete Acces</Tooltip>}
//     >
//       <a
//         className="btn btn-icon btn-light btn-hover-danger btn-sm"
//         onClick={() => openDeleteAccesDialog(row.id)}
//       >
//         <span className="svg-icon svg-icon-md svg-icon-danger">
//           <SVG src={toAbsoluteUrl("/media/svg/icons/General/Trash.svg")} />
//         </span>
//       </a>
//     </OverlayTrigger>
//   </>
// );
