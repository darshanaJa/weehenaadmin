import React from "react";
import {
  AccesstatusCssClasses,
  AccesstatusTitles
} from "../../AccessUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      AccesstatusCssClasses[row.status]
    } label-inline`}
  >
    {AccesstatusTitles[row.status]}
  </span>
);
