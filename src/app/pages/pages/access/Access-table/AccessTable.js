import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Acces/AccessActions";
import * as uiHelpers from "../AccessUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useAccessUIContext } from "../AccessUIContext";

export function AccessTable() {
  // Access UI Context
  const AccessUIContext = useAccessUIContext();
  const AccessUIProps = useMemo(() => {
    return {
      ids: AccessUIContext.ids,
      setIds: AccessUIContext.setIds,
      queryParams: AccessUIContext.queryParams,
      setQueryParams: AccessUIContext.setQueryParams,
      openEditAccesPage: AccessUIContext.openEditAccesPage,
      openDeleteAccesDialog: AccessUIContext.openDeleteAccesDialog,
    };
  }, [AccessUIContext]);

  // Getting curret state of Access list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Access }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Access Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    AccessUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchAccess(AccessUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [AccessUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Acces_id);

  const columns = [
    // {
    //   dataField: "sales_Acces_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "created_at",
      text: "Create Permision Date",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "updated_at",
      text: "Update Permision Date",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "access.access_level",
      text: "Access Level",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "access.access_created_date",
      text: "Access Level Create Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "access.access_updated_date",
      text: "Access Level Update Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "access_module.module_name",
      text: "Module Name",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "access_module.created_date",
      text: "Module Name Create Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    {
      dataField: "access_module.updated_date",
      text: "Module Name Update Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    // {
    //   dataField: "sales_Acces_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_Acces_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditAccesPage: AccessUIProps.openEditAccesPage,
        openDeleteAccesDialog: AccessUIProps.openDeleteAccesDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: AccessUIProps.queryParams.pageSize,
    page: AccessUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Acces_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  AccessUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: AccessUIProps.ids,
                  setIds: AccessUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
