/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Acces/AccessActions";
import { useAccessUIContext } from "../AccessUIContext";

export function AccessDeleteDialog({ show, onHide }) {
  // Access UI Context
  const AccessUIContext = useAccessUIContext();
  const AccessUIProps = useMemo(() => {
    return {
      ids: AccessUIContext.ids,
      setIds: AccessUIContext.setIds,
      queryParams: AccessUIContext.queryParams,
    };
  }, [AccessUIContext]);

  // Access Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Access.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Access we should close modal
  useEffect(() => {
    if (!AccessUIProps.ids || AccessUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [AccessUIProps.ids]);

  const deleteAccess = () => {
    // server request for deleting Acces by seleted ids
    dispatch(actions.deleteAccess(AccessUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchAccess(AccessUIProps.queryParams)).then(() => {
        // clear selections list
        AccessUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Access Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Access?</span>
        )}
        {isLoading && <span>Access are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteAccess}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
