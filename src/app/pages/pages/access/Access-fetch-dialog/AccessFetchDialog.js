import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { AccesstatusCssClasses } from "../AccessUIHelpers";
import { useAccessUIContext } from "../AccessUIContext";

const selectedAccess = (entities, ids) => {
  const _Access = [];
  ids.forEach((id) => {
    const Acces = entities.find((el) => el.id === id);
    if (Acces) {
      _Access.push(Acces);
    }
  });
  return _Access;
};

export function AccessFetchDialog({ show, onHide }) {
  // Access UI Context
  const AccessUIContext = useAccessUIContext();
  const AccessUIProps = useMemo(() => {
    return {
      ids: AccessUIContext.ids,
      queryParams: AccessUIContext.queryParams,
    };
  }, [AccessUIContext]);

  // Access Redux state
  const { Access } = useSelector(
    (state) => ({
      Access: selectedAccess(state.Access.entities, AccessUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!AccessUIProps.ids || AccessUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [AccessUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Access.map((Acces) => (
              <div className="list-timeline-item mb-3" key={Acces.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      AccesstatusCssClasses[Acces.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Acces.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Acces.manufacture}, {Acces.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
