/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Requests/RequestsActions";
import { useRequestsUIContext } from "../RequestsUIContext";

export function RequestsDeleteDialog({ show, onHide }) {
  // Requests UI Context
  const RequestsUIContext = useRequestsUIContext();
  const RequestsUIProps = useMemo(() => {
    return {
      ids: RequestsUIContext.ids,
      setIds: RequestsUIContext.setIds,
      queryParams: RequestsUIContext.queryParams,
    };
  }, [RequestsUIContext]);

  // Requests Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Requests.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Requests we should close modal
  useEffect(() => {
    if (!RequestsUIProps.ids || RequestsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [RequestsUIProps.ids]);

  const deleteRequests = () => {
    // server request for deleting Request by seleted ids
    dispatch(actions.deleteRequests(RequestsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchRequests(RequestsUIProps.queryParams)).then(() => {
        // clear selections list
        RequestsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Requests Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Requests?</span>
        )}
        {isLoading && <span>Requests are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteRequests}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
