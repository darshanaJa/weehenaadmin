import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { getSalesTicketById } from "../../../_redux/salesTickets/salesTicketsCrud";
import { getSpecificationById } from "../../../_redux/specificationsItem/specificationsCrud";


// Validation schema
const RequestEditSchema = Yup.object().shape({
  
});

export function RequestForm({
  Request,
  btnRef,
  saveRequest,
  saleId,
  todos,
  setTodos,
  setSaleItemId,
  saleItemId,
  saleItId,
  setSaleItId
}) {
  const [saleItem,setSaleItem]=useState([]);
  const [saleItemQuntity,setSaleItemQuntity]=useState([]);
  const [saleItemPrice,setSaleItemPrice]=useState([]);
  const [stockName,setStockName]=useState([]);


const stockNameHandler=(e)=>{
    setSaleItem(e.target.value)
  }

  const quntityHandler=(e)=>{
    setSaleItemQuntity(e.target.value)
}

const priceHandler=(e)=>{
    setSaleItemPrice(e.target.value)    
}

console.log(saleItId)

  

  useEffect(()=>{
    console.log("abcd")
    getSalesTicketById(saleId)
      .then((res) => {
        console.log(res.data)
        setSaleItemId(res.data.sticketitem)
        // setSaleTicketdId(res.data.data.results)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[])

  useEffect(()=>{
    console.log("abcd")
    getSpecificationById(saleItId)
      .then((res) => {

        // setSaleItId('')
        setSaleItem('')
        setSaleItemPrice('')
        setSaleItemQuntity('')

        console.log(res.data)
        setSaleItemPrice(res.data.mstock.item_default_price)
        setSaleItemQuntity(res.data.mstock.item_current_quantity)
        setSaleItem(res.data.mstock.item_id)
        setStockName(res.data.mstock.item_name)
        // setSaleTicketdId(res.data.data.results)
      })
      .catch(function (response) {
          console.log(response);
      });
      
  },[saleItId])

  console.log(saleItem)
  console.log(saleId)
  console.log(saleItemId)


  const submitTodoHandler = (e) =>{

      e.preventDefault();
      setTodos([
        ...todos,
        {sales_ticket_item:saleItId, main_stock_item:saleItem, qty:saleItemQuntity, price:saleItemPrice}
      ]);
        setSaleItId('')
        setSaleItem('')
        setSaleItemPrice('')
        setSaleItemQuntity('')
        setStockName('')
      
      console.log(todos)
    // }

}

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Request}
        validationSchema={RequestEditSchema}
        onSubmit={(values) => {
          console.log(values);
         
          saveRequest(values);
          // saveRequest(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
        <Form className="form form-label-right">
            
            
       
            
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
