import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input2 } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss"
// import { Image_Url } from "../../../../config/config";
import { useDispatch } from "react-redux";
import * as actions from "../../../_redux/Requests/RequestsActions";

// Validation schema
const RequestEditSchema = Yup.object().shape({

  
});

export function RequestEditForm({
  Request,
  btnRef,
  saveRequest,
  history
}) {

  const idRe = Request.id

  const dispatch = useDispatch();

  const aprovedRequest = () => {
    console.log(idRe)
    dispatch(actions.aprovedRequest(idRe))
    history.push(`/sales/requests`);
    // .then(() => {
    //   dispatch(actions.fetchRequests(RequestsUIProps.queryParams));
    // });
  };

  const rejectRequest = () => {
    dispatch(actions.rejectRequest(idRe))
    history.push(`/sales/requests`);
    // .then(() => {
    //   dispatch(actions.fetchRequests(RequestsUIProps.queryParams));
    // });
  };

  return (
    <>
       <Formik
        enableReinitialize={true}
        initialValues={Request}
        validationSchema={RequestEditSchema}
        onSubmit={(values) => {
          console.log(values);
          saveRequest(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="retail_store.store_name"
                    component={Input2}
                    placeholder="Retail Store Name"
                    label="Retail Store Name"
                    readOnly="readOnly"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="string"
                    name="sticket.sales_ticket_id"
                    component={Input2}
                    placeholder="Sale Ticket ID"
                    label="Sale Ticket ID"
                    readOnly="readOnly"
                  />
                </div>
                <div className="col-lg-4" >
                  <Field
                    type="string"
                    name="comment"
                    component={Input2}
                    placeholder="Comment"
                    label="Comment"
                    readOnly="readOnly"
                  />
                </div>
              </div>

              {Request.items.map((item) => (
              <div className="form-group row">
                <div className="col-lg-3">
                    <Field
                    type="string"
                    name="sticket.sales_ticket_id"
                    value={item.sales_ticket_item.sales_ticket_item_id}
                    component={Input2}
                    placeholder="Sale ticket Item Id"
                    label="sale ticket Item Id"
                  />
                </div>
                <div className="col-lg-3">
                    <Field
                    type="string"
                    name="price"
                    value={item.main_stock_item.item_name}
                    component={Input2}
                    placeholder="Main stock Name"
                    label="Main stock Name"
                  />
                </div>
                <div className="col-lg-3">
                    <Field
                      type="string"
                      name="qty"
                      value={item.price}
                      component={Input2}
                      placeholder="Quntity"
                      label="Quntity"
                    />
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="string"
                      name="price"
                      value={item.price}
                      component={Input2}
                      placeholder="Price"
                      label="Price"
                    />
                  </div>
                </div>
                ))}
             <div className="form-group row">
                  <div className="col-lg-4">
                  <button type="submit" onClick={aprovedRequest} className="btn btn-primary ml-2" >Approval</button>
                  <button type="submit" onClick={rejectRequest} className="btn btn-primary ml-2" >Reject</button>
                 </div>  
                </div> 
              
            
              <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
}
