import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { RequeststatusCssClasses } from "../RequestsUIHelpers";
import { useRequestsUIContext } from "../RequestsUIContext";

const selectedRequests = (entities, ids) => {
  const _Requests = [];
  ids.forEach((id) => {
    const Request = entities.find((el) => el.id === id);
    if (Request) {
      _Requests.push(Request);
    }
  });
  return _Requests;
};

export function RequestsFetchDialog({ show, onHide }) {
  // Requests UI Context
  const RequestsUIContext = useRequestsUIContext();
  const RequestsUIProps = useMemo(() => {
    return {
      ids: RequestsUIContext.ids,
      queryParams: RequestsUIContext.queryParams,
    };
  }, [RequestsUIContext]);

  // Requests Redux state
  const { Requests } = useSelector(
    (state) => ({
      Requests: selectedRequests(state.Requests.entities, RequestsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!RequestsUIProps.ids || RequestsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [RequestsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Requests.map((Request) => (
              <div className="list-timeline-item mb-3" key={Request.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      RequeststatusCssClasses[Request.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Request.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Request.manufacture}, {Request.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
