/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Requests/RequestsActions";
import { useRequestsUIContext } from "../RequestsUIContext";

export function RequestDeleteDialog1({ id, show, onHide }) {
  // Requests UI Context
  const RequestsUIContext = useRequestsUIContext();
  const RequestsUIProps = useMemo(() => {
    return {
      setIds: RequestsUIContext.setIds,
      queryParams: RequestsUIContext.queryParams,
    };
  }, [RequestsUIContext]);

  // Requests Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Requests.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteRequest = () => {
    console.log(id)
    // server request for deleting Request by id
    dispatch(actions.deleteRequest(id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchRequests(RequestsUIProps.queryParams));
      // clear selections list
      RequestsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  // const aprovedRequest = () => {
  //   // server request for deleting Request by id
  //   dispatch(actions.aprovedRequest(id)).then(() => {
  //     // refresh list after deletion
  //     dispatch(actions.fetchRequests(RequestsUIProps.queryParams));
  //     // clear selections list
  //     RequestsUIProps.setIds([]);
  //     // closing delete modal
  //     onHide();
  //   });
  // };

  // const rejectRequest = () => {
  //   // server request for deleting Request by id
  //   dispatch(actions.rejectRequest(id)).then(() => {
  //     // refresh list after deletion
  //     dispatch(actions.fetchRequests(RequestsUIProps.queryParams));
  //     // clear selections list
  //     RequestsUIProps.setIds([]);
  //     // closing delete modal
  //     onHide();
  //   });
  // };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Request Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Request?</span>
        )}
        {isLoading && <span>Request is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          {/* <button
            type="button"
            onClick={rejectRequest}
            className="btn btn-primary ml-2"
          >
            Reject
          </button>
          <button
            type="button"
            onClick={aprovedRequest}
            className="btn btn-primary ml-2"
          >
            Approval
          </button> */}
          <button
            type="button"
            onClick={deleteRequest}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
