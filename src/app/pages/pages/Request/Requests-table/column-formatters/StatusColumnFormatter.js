import React from "react";
import {
  RequeststatusCssClasses,
  RequeststatusTitles
} from "../../RequestsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      RequeststatusCssClasses[row.status]
    } label-inline`}
  >
    {RequeststatusTitles[row.status]}
  </span>
);
