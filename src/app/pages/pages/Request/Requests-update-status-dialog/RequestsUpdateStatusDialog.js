import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { RequeststatusCssClasses } from "../RequestsUIHelpers";
import * as actions from "../../../_redux/Requests/RequestsActions";
import { useRequestsUIContext } from "../RequestsUIContext";

const selectedRequests = (entities, ids) => {
  const _Requests = [];
  ids.forEach((id) => {
    const Request = entities.find((el) => el.id === id);
    if (Request) {
      _Requests.push(Request);
    }
  });
  return _Requests;
};

export function RequestsUpdateStatusDialog({ show, onHide }) {
  // Requests UI Context
  const RequestsUIContext = useRequestsUIContext();
  const RequestsUIProps = useMemo(() => {
    return {
      ids: RequestsUIContext.ids,
      setIds: RequestsUIContext.setIds,
      queryParams: RequestsUIContext.queryParams,
    };
  }, [RequestsUIContext]);

  // Requests Redux state
  const { Requests, isLoading } = useSelector(
    (state) => ({
      Requests: selectedRequests(state.Requests.entities, RequestsUIProps.ids),
      isLoading: state.Requests.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Requests we should close modal
  useEffect(() => {
    if (RequestsUIProps.ids || RequestsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [RequestsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Request by ids
    dispatch(actions.updateRequestsStatus(RequestsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchRequests(RequestsUIProps.queryParams)).then(
          () => {
            // clear selections list
            RequestsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Requests
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Requests.map((Request) => (
              <div className="list-timeline-item mb-3" key={Request.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      RequeststatusCssClasses[Request.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Request.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Request.manufacture}, {Request.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${RequeststatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
