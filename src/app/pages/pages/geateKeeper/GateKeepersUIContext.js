import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./GateKeepersUIHelpers";

const GateKeepersUIContext = createContext();

export function useGateKeepersUIContext() {
  return useContext(GateKeepersUIContext);
}

export const GateKeepersUIConsumer = GateKeepersUIContext.Consumer;

export function GateKeepersUIProvider({ GateKeepersUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newGateKeeperButtonClick: GateKeepersUIEvents.newGateKeeperButtonClick,
    openEditGateKeeperPage: GateKeepersUIEvents.openEditGateKeeperPage,
    openDeleteGateKeeperDialog: GateKeepersUIEvents.openDeleteGateKeeperDialog,
    openDeleteGateKeepersDialog: GateKeepersUIEvents.openDeleteGateKeepersDialog,
    openFetchGateKeepersDialog: GateKeepersUIEvents.openFetchGateKeepersDialog,
    openUpdateGateKeepersStatusDialog: GateKeepersUIEvents.openUpdateGateKeepersStatusDialog,
  };

  return (
    <GateKeepersUIContext.Provider value={value}>
      {children}
    </GateKeepersUIContext.Provider>
  );
}
