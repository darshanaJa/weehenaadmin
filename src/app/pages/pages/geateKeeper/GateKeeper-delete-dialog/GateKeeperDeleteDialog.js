/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/GateKeeper/GateKeepersActions";
import { useGateKeepersUIContext } from "../GateKeepersUIContext";

export function GateKeeperDeleteDialog({ sales_GateKeeper_id, show, onHide }) {
  // console.log(sales_GateKeeper_id)
  // GateKeepers UI Context
  const GateKeepersUIContext = useGateKeepersUIContext();
  const GateKeepersUIProps = useMemo(() => {
    return {
      setIds: GateKeepersUIContext.setIds,
      queryParams: GateKeepersUIContext.queryParams,
    };
  }, [GateKeepersUIContext]);

  // GateKeepers Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.GateKeepers.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_GateKeeper_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_GateKeeper_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteGateKeeper = () => {
    // server request for deleting GateKeeper by id
    dispatch(actions.deleteGateKeeper(sales_GateKeeper_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchGateKeepers(GateKeepersUIProps.queryParams));
      // clear selections list
      GateKeepersUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          GateKeeper Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this GateKeeper?</span>
        )}
        {isLoading && <span>GateKeeper is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteGateKeeper}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
