import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { GateKeepersFilter } from "./GateKeepers-filter/GateKeepersFilter";
import { GateKeepersTable } from "./GateKeepers-table/GateKeepersTable";
// import { GateKeepersGrouping } from "./GateKeepers-grouping/GateKeepersGrouping";
import { useGateKeepersUIContext } from "./GateKeepersUIContext";
import { notify } from "../../../config/Toastify";

export function GateKeepersCard(msg){

  // const [name,setName] = useState()

  // setName(msg)
  // console.log(msg)

  // useEffect(()=>{
  //   console.log(msg)
  //   // notify("abcd")
  // },[])

  const GateKeepersUIContext = useGateKeepersUIContext();
  const GateKeepersUIProps = useMemo(() => {
    return {
      ids: GateKeepersUIContext.ids,
      queryParams: GateKeepersUIContext.queryParams,
      setQueryParams: GateKeepersUIContext.setQueryParams,
      newGateKeeperButtonClick: GateKeepersUIContext.newGateKeeperButtonClick,
      openDeleteGateKeepersDialog: GateKeepersUIContext.openDeleteGateKeepersDialog,
      openEditGateKeeperPage: GateKeepersUIContext.openEditGateKeeperPage,
      openUpdateGateKeepersStatusDialog:GateKeepersUIContext.openUpdateGateKeepersStatusDialog,
      openFetchGateKeepersDialog: GateKeepersUIContext.openFetchGateKeepersDialog,
    };
  }, [GateKeepersUIContext]);

  return (
    <>
    <Card>
      {notify()}
      <CardHeader title="GateKeeper list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={GateKeepersUIProps.newGateKeeperButtonClick}
          >
            New GateKeeper
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <GateKeepersFilter />
        {GateKeepersUIProps.ids.length > 0 && (
          <>
            {/* <GateKeepersGrouping /> */}
          </>
        )}
        <GateKeepersTable />
      </CardBody>
    </Card>
    </>
  );
}
