import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { GateKeeperstatusCssClasses } from "../GateKeepersUIHelpers";
import * as actions from "../../../_redux/GateKeeper/GateKeepersActions";
import { useGateKeepersUIContext } from "../GateKeepersUIContext";

const selectedGateKeepers = (entities, ids) => {
  const _GateKeepers = [];
  ids.forEach((id) => {
    const GateKeeper = entities.find((el) => el.id === id);
    if (GateKeeper) {
      _GateKeepers.push(GateKeeper);
    }
  });
  return _GateKeepers;
};

export function GateKeepersUpdateStatusDialog({ show, onHide }) {
  // GateKeepers UI Context
  const GateKeepersUIContext = useGateKeepersUIContext();
  const GateKeepersUIProps = useMemo(() => {
    return {
      ids: GateKeepersUIContext.ids,
      setIds: GateKeepersUIContext.setIds,
      queryParams: GateKeepersUIContext.queryParams,
    };
  }, [GateKeepersUIContext]);

  // GateKeepers Redux state
  const { GateKeepers, isLoading } = useSelector(
    (state) => ({
      GateKeepers: selectedGateKeepers(state.GateKeepers.entities, GateKeepersUIProps.ids),
      isLoading: state.GateKeepers.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected GateKeepers we should close modal
  useEffect(() => {
    if (GateKeepersUIProps.ids || GateKeepersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [GateKeepersUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing GateKeeper by ids
    dispatch(actions.updateGateKeepersStatus(GateKeepersUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchGateKeepers(GateKeepersUIProps.queryParams)).then(
          () => {
            // clear selections list
            GateKeepersUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected GateKeepers
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {GateKeepers.map((GateKeeper) => (
              <div className="list-timeline-item mb-3" key={GateKeeper.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      GateKeeperstatusCssClasses[GateKeeper.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {GateKeeper.id}
                  </span>{" "}
                  <span className="ml-5">
                    {GateKeeper.manufacture}, {GateKeeper.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${GateKeeperstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
