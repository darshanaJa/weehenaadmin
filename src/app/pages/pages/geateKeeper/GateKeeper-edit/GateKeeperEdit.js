/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/GateKeeper/GateKeepersActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { GateKeeperEditForm } from "./GateKeeperEditForm";
import { GateKeeperCreateForm } from "./GateKeeperCreateForm";
import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../GateKeeper-specifications/Specifications";
import { SpecificationsUIProvider } from "../GateKeeper-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../GateKeeper-remarks/RemarksUIContext";
import { Remarks } from "../GateKeeper-remarks/Remarks";
// import { SpecificationsTable } from "../GateKeeper-specifications/SpecificationsTable";

const initGateKeeper = {
  
  // sales_GateKeeper_fname : "Darshana",
  // sales_GateKeeper_lname : "aaaaaa",
  // sales_GateKeeper_nic : "960520386V",
  // sales_GateKeeper_mobile_1 : "0123456789",
  // sales_GateKeeper_mobile_2 : "0123456789",
  // sales_GateKeeper_email : "aa@gmail.com",
  // address:"matale",
  // password:"abcd1234",
  // profile_pic : ""

  fname : "",
  lname : "",
  gate_keeper_nic : "",
  contact_one : "",
  contact_two : "",
  password : "",
  keeper_email:"",

  // fname : "abcd",
  // lname : "abcdef",
  // gate_keeper_nic : "126464646984889V",
  // contact_one : "0123456789",
  // contact_two : "0123456789",
  // password : "1234",
  // keeper_email:"sdafadfjsadsnkj@gmail.com",
  // password:""
  // profile_pic : []

};

const initGateKeeperPassword = {
  curent_password : "",
  new_password : "",
  confirm_password : ""

};

export function GateKeeperEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, GateKeeperForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.GateKeepers.actionsLoading,
      GateKeeperForEdit: state.GateKeepers.GateKeeperForEdit,
    }),
    shallowEqual
  );

  // useEffect(() => {
  //   return(<SpecificationsTable
  //     actionsLoading={actionsLoading}
  //     GateKeeper={initGateKeeperPassword}
  //     btnRef={btnRef}
  //     savePassword={savePassword}
  //   /> )
    
  // });

  console.log(GateKeeperForEdit)

  useEffect(() => {
    dispatch(actions.fetchGateKeeper(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New GateKeeper";
    if (GateKeeperForEdit && id) {
      // _title = `Edit GateKeeper - ${GateKeeperForEdit.sales_GateKeeper_fname} ${GateKeeperForEdit.sales_GateKeeper_lname} - ${GateKeeperForEdit.sales_GateKeeper_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [GateKeeperForEdit, id]);

  const saveGateKeeper = (values) => {
    if (!id) {
      dispatch(actions.createGateKeeper(values,id)).then(() => backToGateKeepersList());
    } else {
      dispatch(actions.updateGateKeeper(values,id)).then(() => backToGateKeepersList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateGateKeeperPassword(values,id)).then(() => backToGateKeepersList());
  };

  const btnRef = useRef();  
  // const saveGateKeeperClick = () => {
  //   if (btnRef && btnRef.current) {
  //     btnRef.current.click();
  //   }
  // };

  const backToGateKeepersList = () => {
    // history.push(`/sales/GateKeeper`);
    history.push(`/hr/gatekeeper`);
    // window.location.reload(`/sales/GateKeeper`)
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToGateKeepersList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {/* <button  className="btn btn-light ml-2">
              <i className="fa fa-redo"></i>
              Reset
            </button> */}
            {`  `}
            {/* <button
              type="submit"
              className="btn btn-primary ml-2"
              onClick={saveGateKeeperClick}
            >
              Save
            </button> */}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                {/* <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    GateKeeper remarks
                  </a>
                </li> */}
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Reset Password
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <GateKeeperEditForm
                actionsLoading={actionsLoading}
                GateKeeper={GateKeeperForEdit || initGateKeeper}
                btnRef={btnRef}
                saveGateKeeper={saveGateKeeper}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentGateKeeperId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentGateKeeperId={id}>
                <PasswordReset
                  actionsLoading={actionsLoading}
                  GateKeeper={initGateKeeperPassword}
                  btnRef={btnRef}
                  savePassword={savePassword}
    />
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToGateKeepersList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            {/* <button  className="btn btn-light ml-2">
              <i className="fa fa-redo"></i>
              Reset
            </button> */}
            {`  `}
            {/* <button
              type="submit"
              className="btn btn-primary ml-2"
              onClick={saveGateKeeperClick}
            >
              Save
            </button> */}
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    GateKeeper remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    GateKeeper specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <GateKeeperCreateForm
                actionsLoading={actionsLoading}
                GateKeeper={GateKeeperForEdit || initGateKeeper}
                btnRef={btnRef}
                saveGateKeeper={saveGateKeeper}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentGateKeeperId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentGateKeeperId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
