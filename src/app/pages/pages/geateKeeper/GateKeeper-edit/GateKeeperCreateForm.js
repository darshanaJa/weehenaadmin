import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";

// Validation schema
const GateKeeperEditSchema = Yup.object().shape({
    fname: Yup.string()
    .required("Name is required")
    .min(2, "First Name must be at least 2 characters"),
    lname: Yup.string()
    .required("Name is required")
    .min(2, "Last Name must be at least 2 characters"),
    password: Yup.string()
    .required("Password is required")
    .min(5, "Password must be at least 5 characters"),
    gate_keeper_nic: Yup.string()
    .required("GateKeeper NIC is required")
    .min(10, "NIC be at least 9 characters"),
    keeper_email: Yup.string().email()
    .required("Email is required"),
    contact_one: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
    contact_two: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
});


export function GateKeeperCreateForm({
  GateKeeper,
  btnRef,
  saveGateKeeper,
}) {


const bodyFormData = new FormData();
const [profile_pic,set_Profile_pic]=useState();

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={GateKeeper}
        validationSchema={GateKeeperEditSchema}
        onSubmit={(values)  => {
          bodyFormData.append('fname',values.fname);
          bodyFormData.append('lname',values.lname);
          bodyFormData.append('gate_keeper_nic',values.gate_keeper_nic);
          bodyFormData.append('keeper_email',values.keeper_email);
          bodyFormData.append('contact_one',values.contact_one);
          bodyFormData.append('contact_two',values.contact_one);
          bodyFormData.append('password',values.password);
          bodyFormData.append('profile_pic',profile_pic);
          
          console.log(values);
          saveGateKeeper(bodyFormData);
          

        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="fname"
                    component={Input}
                    placeholder="First Name"
                    label="Fisrt Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="lname"
                    component={Input}
                    placeholder="Last Name"
                    label="Last Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="gate_keeper_nic"
                    component={Input}
                    placeholder="number"
                    label="NIC"
                  />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="contact_one"
                      component={Input}
                      placeholder="Mobile 01"
                      label="Mobile 01"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="contact_two"
                    component={Input}
                    placeholder="Mobile 02"
                    label="Mobile 02"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                      type="email"
                      name="keeper_email"
                      component={Input}
                      placeholder="Email"
                      label="Email"
                    />
                  </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                    <Field
                      type="text"
                      name="password"
                      component={Input}
                      placeholder="password"
                      label="Password"
                    />
                  </div>
                  <div className="col-lg-4">
                    <div className="form-group row">
                      <input className="agentImageBtn + dwnfile" type="file" name="profile_pic1"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    </div>
                  </div>  
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
                </div>  

              <div className="form-group row">
                  <div className="col-lg-4">
                    {/* <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button> */}
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
                  
              </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>

            <div>
              
            </div>
          </>
        )}
      </Formik>
    </>
  );
}
