// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
// import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
// import axios from "axios";
// import { Api_Login, Image_Url } from "../../../../config/config";
// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   GateKeeperstatusTitles,
//   GateKeeperConditionTitles,
// } from "../GateKeepersUIHelpers";

// Validation schema
const GateKeeperEditSchema = Yup.object().shape({
  // id: Yup.string(),
  curent_password: Yup.string()
    .required("Curent Passwrod is required")
    .min(2, "Curerent password"),
  new_password: Yup.string()
    .required("New Password is required")
    .min(2, "New password"),
  confirm_password: Yup.string()
    .required("Confirm Password is required")
    .min(2, "Confirm password")
  
  // profile_pic: Yup.string()
  // .required("Required")
});


export function PasswordReset({
  GateKeeper,
  btnRef,
  savePassword,
}) {

  // console.log(GateKeeper.profile_pic)

  // const submitImage=()=>{
  //     return(
  //       <img className="GateKeeperImg" alt="GateKeeper" src={url} />
  //     );
  // }

  // const pic=GateKeeper.profile_pic
  // console.log(pic)
  // const url = Image_Url+pic
  // console.log(url)

  // useEffect(()=>{
  //   console.log('try')
  //   axios({
  //     method: 'PUT',
  //     baseURL: `http://34.87.16.144/api/sales-GateKeeper/password/update/${GateKeeper.sales_GateKeeper_id}`
  //     })
  //     // console.log(baseURL)
  //     .then((res) => {
  //       let data = res.data;
  //       console.log(data);
  //       // setShop(res.data.data)
  //     })
  //     .catch(function (response) {
  //         // console.log(response);
  //     });
      
  // },[])

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={GateKeeper}
        validationSchema={GateKeeperEditSchema}
        onSubmit={(values)  => {
          console.log(values);
          savePassword(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="curent_password"
                    component={Input}
                    placeholder="Current Password"
                    label="Current Password"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="new_password"
                    component={Input}
                    placeholder="New Password"
                    label="New Password"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="confirm_password"
                    component={Input}
                    placeholder="Confirm Password"
                    label="Confirm Password"
                  />
                </div>
              </div>
              <div className="form-group row">
              <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2">Reset Password</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
              </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>
   

  );
}
