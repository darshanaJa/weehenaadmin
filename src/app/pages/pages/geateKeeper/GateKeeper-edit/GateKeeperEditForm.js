// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useEffect,useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
// import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
// import axios from "axios";
import { Image_Url } from "../../../../config/config";
import Admin from "../../../../config/Admin";
import axios from "axios";
import { GateKeepers_GET_ID_URL } from "../../../_redux/GateKeeper/GateKeepersCrud";

// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   GateKeeperstatusTitles,
//   GateKeeperConditionTitles,
// } from "../GateKeepersUIHelpers";

// Validation schema
const GateKeeperEditSchema = Yup.object().shape({
  // id: Yup.string(),
    fname: Yup.string()
    .required("Name is required")
    .min(2, "First Name must be at least 2 characters"),
    lname: Yup.string()
    .required("Name is required")
    .min(2, "Last Name must be at least 2 characters"),
  // address: Yup.string()
  //   .required("Address is required")
  //   .min(2, "Address must be at least 2 characters"),
    // password: Yup.string()
    // .required("Password is required")
    // .min(5, "Password must be at least 5 characters"),
    gate_keeper_nic: Yup.string()
    .required("GateKeeper NIC is required")
    .min(10, "NIC be at least 9 characters"),
    keeper_email: Yup.string().email()
    .required("Email is required"),
    contact_one: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
    contact_two: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
  
  // profile_pic: Yup.string()
  // .required("Required")
});


export function GateKeeperEditForm({
  GateKeeper,
  btnRef,
  saveGateKeeper,
}) {

  console.log(GateKeeper)

  // console.log(GateKeeper.profile_pic)

  const submitImage=()=>{
      return(
        <img className="shopImg" alt="GateKeeper" src={url} />
      );
  }

  const pic=GateKeeper.profile_pic
  // console.log(pic)
  const url = Image_Url+pic
  console.log(url)

  const [idAdmin,setIdAdmin]=useState('');

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: GateKeepers_GET_ID_URL + `/${GateKeeper.id}`
      })
      .then((res) => {
        setIdAdmin(res.data.created_by.id)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[GateKeeper.id])



  const bodyFormData = new FormData();
  const [profile_pic,set_Profile_pic]=useState();
  
    // const [profile_pic,set_Profile_pic]=useState();
  
    return (
      <>
        <Formik
          enableReinitialize={true}
          initialValues={GateKeeper}
          validationSchema={GateKeeperEditSchema}
          onSubmit={(values)  => {
            bodyFormData.append('fname',values.fname);
            bodyFormData.append('lname',values.lname);
            bodyFormData.append('gate_keeper_nic',values.gate_keeper_nic);
            bodyFormData.append('keeper_email',values.keeper_email);
            bodyFormData.append('contact_one',values.contact_one);
            bodyFormData.append('contact_two',values.contact_one);
            // bodyFormData.append('password',values.password);
            // bodyFormData.append('address',values.address);
            bodyFormData.append('profile_pic',profile_pic);
            
            console.log(values);
            saveGateKeeper(bodyFormData);
            
  
          }}
        >
          {({ handleSubmit }) => (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  {/* <div className="col-lg-4">
                    <Field
                      name="id"
                      component={Input}
                      placeholder="ID"
                      label="ID"
                    />
                  </div> */}
                  <div className="col-lg-4">
                  <Field
                      type="text"
                      name="fname"
                      component={Input}
                      placeholder="First Name"
                      label="Fisrt Name"
                    />
                  </div>
        
                  <div className="col-lg-4">
                    <Field
                      type="text"
                      name="lname"
                      component={Input}
                      placeholder="Last Name"
                      label="Last Name"
                    />
                  </div>
                  <div className="col-lg-4">
                    <Field
                      type="text"
                      name="gate_keeper_nic"
                      component={Input}
                      placeholder="number"
                      label="NIC"
                    />
                  </div>
                </div>
  
                <div className="form-group row">
                  <div className="col-lg-4">
                    <Field
                        type="text"
                        name="contact_one"
                        component={Input}
                        placeholder="Mobile 01"
                        label="Mobile 01"
                        // customFeedbackLabel="Please enter Price"
                      />
                  </div>
                  <div className="col-lg-4">
                    <Field
                      type="text"
                      name="contact_two"
                      component={Input}
                      placeholder="Mobile 02"
                      label="Mobile 02"
                      // customFeedbackLabel="Please enter "
                    />
                  </div>
                  <div className="col-lg-4">
                    <Field
                        type="email"
                        name="keeper_email"
                        component={Input}
                        placeholder="Email"
                        label="Email"
                      />
                    </div>
                  {/* <div className="col-lg-4">
                  <Field
                      type="text"
                      name="address"
                      component={Input}
                      placeholder="address"
                      label="Address"
                    />
                  </div> */}
                </div>
  
                <div className="form-group row">
                  {/* <div className="col-lg-4">
                      <Field
                        type="text"
                        name="password"
                        component={Input}
                        placeholder="password"
                        label="Password"
                        // customFeedbackLabel="Please enter "
                      />
                    </div> */}
                    <div className="col-lg-4">
                      <div className="form-group row">
                        <input className="agentImageBtn + dwnfile" type="file" name="profile_pic1"
                          onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                         />
                      </div>
                    </div>  
                    <div className="col-lg-4">
                      {/* <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button> */}
                      <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                    </div>
                    
                  
                  </div>  
  
              <div className="form-group row">
                    {/* <div className="col-lg-4">
                      <div className="form-group row">
                        <input className="GateKeeperImageBtn + dwnfile" type="file" name="profile_pic"
                          onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                         />
                      </div>
                    </div>  */}
                    <div className="col-lg-4">
                      {/* <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button> */}
                      {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                    </div>
                    
                  </div>
                  <button
                  type="submit"
                  style={{ display: "none" }}
                  ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
             
              </Form>
  
              <div>
                  <div className="form-group row">
                     <div className="col-lg-4">
                        {submitImage()}
                      </div>
                  </div>
              </div>

              <Admin 
                  adminId={idAdmin}
                  createDate={GateKeeper.batch_created_date} 
                  updateDate={GateKeeper.batch_updated_date} 
                />


            </>
          )}
        </Formik>
      </>
    );
}
