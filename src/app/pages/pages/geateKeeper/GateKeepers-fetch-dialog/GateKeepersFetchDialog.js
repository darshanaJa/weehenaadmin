import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { GateKeeperstatusCssClasses } from "../GateKeepersUIHelpers";
import { useGateKeepersUIContext } from "../GateKeepersUIContext";

const selectedGateKeepers = (entities, ids) => {
  const _GateKeepers = [];
  ids.forEach((id) => {
    const GateKeeper = entities.find((el) => el.id === id);
    if (GateKeeper) {
      _GateKeepers.push(GateKeeper);
    }
  });
  return _GateKeepers;
};

export function GateKeepersFetchDialog({ show, onHide }) {
  // GateKeepers UI Context
  const GateKeepersUIContext = useGateKeepersUIContext();
  const GateKeepersUIProps = useMemo(() => {
    return {
      ids: GateKeepersUIContext.ids,
      queryParams: GateKeepersUIContext.queryParams,
    };
  }, [GateKeepersUIContext]);

  // GateKeepers Redux state
  const { GateKeepers } = useSelector(
    (state) => ({
      GateKeepers: selectedGateKeepers(state.GateKeepers.entities, GateKeepersUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!GateKeepersUIProps.ids || GateKeepersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [GateKeepersUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {GateKeepers.map((GateKeeper) => (
              <div className="list-timeline-item mb-3" key={GateKeeper.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      GateKeeperstatusCssClasses[GateKeeper.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {GateKeeper.id}
                  </span>{" "}
                  <span className="ml-5">
                    {GateKeeper.manufacture}, {GateKeeper.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
