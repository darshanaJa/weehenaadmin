/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/GateKeeper/GateKeepersActions";
import { useGateKeepersUIContext } from "../GateKeepersUIContext";

export function GateKeepersDeleteDialog({ show, onHide }) {
  // GateKeepers UI Context
  const GateKeepersUIContext = useGateKeepersUIContext();
  const GateKeepersUIProps = useMemo(() => {
    return {
      ids: GateKeepersUIContext.ids,
      setIds: GateKeepersUIContext.setIds,
      queryParams: GateKeepersUIContext.queryParams,
    };
  }, [GateKeepersUIContext]);

  // GateKeepers Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.GateKeepers.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected GateKeepers we should close modal
  useEffect(() => {
    if (!GateKeepersUIProps.ids || GateKeepersUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [GateKeepersUIProps.ids]);

  const deleteGateKeepers = () => {
    // server request for deleting GateKeeper by seleted ids
    dispatch(actions.deleteGateKeepers(GateKeepersUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchGateKeepers(GateKeepersUIProps.queryParams)).then(() => {
        // clear selections list
        GateKeepersUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          GateKeepers Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected GateKeepers?</span>
        )}
        {isLoading && <span>GateKeepers are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteGateKeepers}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
