// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/GateKeeper/GateKeepersActions";
import * as uiHelpers from "../GateKeepersUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useGateKeepersUIContext } from "../GateKeepersUIContext";

export function GateKeepersTable() {
  // GateKeepers UI Context
  const GateKeepersUIContext = useGateKeepersUIContext();
  const GateKeepersUIProps = useMemo(() => {
    return {
      ids: GateKeepersUIContext.ids,
      setIds: GateKeepersUIContext.setIds,
      queryParams: GateKeepersUIContext.queryParams,
      setQueryParams: GateKeepersUIContext.setQueryParams,
      openEditGateKeeperPage: GateKeepersUIContext.openEditGateKeeperPage,
      openDeleteGateKeeperDialog: GateKeepersUIContext.openDeleteGateKeeperDialog,
    };
  }, [GateKeepersUIContext]);

  // Getting curret state of GateKeepers list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.GateKeepers }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // GateKeepers Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    GateKeepersUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchGateKeepers(GateKeepersUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [GateKeepersUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_GateKeeper_id);

  const columns = [
    // {
    //   dataField: "sales_GateKeeper_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "id",
      text: "First Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "fname",
      text: "First Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "lname",
      text: "Last Name",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "contact_one",
      text: "Mobile 01",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "contact_two",
      text: "Mobile 02",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "keeper_email",
      text: "Email",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "gate_keeper_nic",
      text: "NIC",
      sort: true,
      sortCaret: sortCaret,
    },
    // {
    //   dataField: "sales_GateKeeper_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    {
      dataField: "action",
      text: "Actions",
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditGateKeeperPage: GateKeepersUIProps.openEditGateKeeperPage,
        openDeleteGateKeeperDialog: GateKeepersUIProps.openDeleteGateKeeperDialog,
      },
      classes: "text-right pr-0",
      headerClasses: "text-right pr-3",
      style: {
        minWidth: "100px",
      },
    },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: GateKeepersUIProps.queryParams.pageSize,
    page: GateKeepersUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_GateKeeper_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  GateKeepersUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: GateKeepersUIProps.ids,
                  setIds: GateKeepersUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
