import React from "react";
import {
  GateKeeperConditionCssClasses,
  GateKeeperConditionTitles
} from "../../GateKeepersUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        GateKeeperConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        GateKeeperConditionCssClasses[row.condition]
      }`}
    >
      {GateKeeperConditionTitles[row.condition]}
    </span>
  </>
);
