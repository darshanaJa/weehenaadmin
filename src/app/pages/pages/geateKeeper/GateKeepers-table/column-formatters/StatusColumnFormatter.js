import React from "react";
import {
  GateKeeperstatusCssClasses,
  GateKeeperstatusTitles
} from "../../GateKeepersUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      GateKeeperstatusCssClasses[row.status]
    } label-inline`}
  >
    {GateKeeperstatusTitles[row.status]}
  </span>
);
