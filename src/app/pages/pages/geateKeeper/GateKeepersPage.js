import React from "react";
import { Route } from "react-router-dom";
import { GateKeepersLoadingDialog } from "./GateKeepers-loading-dialog/GateKeepersLoadingDialog";
import { GateKeeperDeleteDialog } from "./GateKeeper-delete-dialog/GateKeeperDeleteDialog";
import { GateKeepersDeleteDialog } from "./GateKeepers-delete-dialog/GateKeepersDeleteDialog";
import { GateKeepersFetchDialog } from "./GateKeepers-fetch-dialog/GateKeepersFetchDialog";
import { GateKeepersUpdateStatusDialog } from "./GateKeepers-update-status-dialog/GateKeepersUpdateStatusDialog";
import { GateKeepersCard } from "./GateKeepersCard";
import { GateKeepersUIProvider } from "./GateKeepersUIContext";
import { notify } from "../../../config/Toastify";

// window.location.reload()

export function GateKeepersPage({ history }) {
  const GateKeepersUIEvents = {
    newGateKeeperButtonClick: () => {
      history.push("/hr/gatekeeper/new");
    },
    openEditGateKeeperPage: (id) => {
      history.push(`/hr/gatekeeper/${id}/edit`);
    },
    openDeleteGateKeeperDialog: (sales_GateKeeper_id) => {
      history.push(`/sales/GateKeeper/${sales_GateKeeper_id}/delete`);
    },
    openDeleteGateKeepersDialog: () => {
      history.push(`/hr/gatekeeper/deleteGateKeepers`);
    },
    openFetchGateKeepersDialog: () => {
      history.push(`/hr/gatekeeper/fetch`);
    },
    openUpdateGateKeepersStatusDialog: () => {
      history.push("/hr/gatekeeper/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <GateKeepersUIProvider GateKeepersUIEvents={GateKeepersUIEvents}>
      <GateKeepersLoadingDialog />
      <Route path="/hr/gatekeeper/deleteGateKeepers">
        {({ history, match }) => (
          <GateKeepersDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/gatekeeper");
            }}
          />
        )}
      </Route>
      <Route path="/hr/gatekeeper/:sales_GateKeeper_id/delete">
        {({ history, match }) => (
          <GateKeeperDeleteDialog
            show={match != null}
            sales_GateKeeper_id={match && match.params.sales_GateKeeper_id}
            onHide={() => {
              history.push("/hr/gatekeeper");
            }}
          />
        )}
      </Route>
      <Route path="/hr/gatekeeper/fetch">
        {({ history, match }) => (
          <GateKeepersFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/gatekeeper");
            }}
          />
        )}
      </Route>
      <Route path="/hr/gatekeeper/updateStatus">
        {({ history, match }) => (
          <GateKeepersUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/hr/gatekeeper");
            }}
          />
        )}
      </Route>
      <GateKeepersCard />
    </GateKeepersUIProvider>
    </>
  );
}
