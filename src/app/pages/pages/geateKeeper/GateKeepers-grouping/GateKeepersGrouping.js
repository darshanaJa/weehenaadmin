import React, { useMemo } from "react";
import { useGateKeepersUIContext } from "../GateKeepersUIContext";

export function GateKeepersGrouping() {
  // GateKeepers UI Context
  const GateKeepersUIContext = useGateKeepersUIContext();
  const GateKeepersUIProps = useMemo(() => {
    return {
      ids: GateKeepersUIContext.ids,
      setIds: GateKeepersUIContext.setIds,
      openDeleteGateKeepersDialog: GateKeepersUIContext.openDeleteGateKeepersDialog,
      openFetchGateKeepersDialog: GateKeepersUIContext.openFetchGateKeepersDialog,
      openUpdateGateKeepersStatusDialog:
        GateKeepersUIContext.openUpdateGateKeepersStatusDialog,
    };
  }, [GateKeepersUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{GateKeepersUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={GateKeepersUIProps.openDeleteGateKeepersDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={GateKeepersUIProps.openFetchGateKeepersDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={GateKeepersUIProps.openUpdateGateKeepersStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
