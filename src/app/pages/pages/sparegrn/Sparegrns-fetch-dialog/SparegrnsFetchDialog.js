import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useSelector } from "react-redux";
import { SparegrnstatusCssClasses } from "../SparegrnsUIHelpers";
import { useSparegrnsUIContext } from "../SparegrnsUIContext";

const selectedSparegrns = (entities, ids) => {
  const _Sparegrns = [];
  ids.forEach((id) => {
    const Sparegrn = entities.find((el) => el.id === id);
    if (Sparegrn) {
      _Sparegrns.push(Sparegrn);
    }
  });
  return _Sparegrns;
};

export function SparegrnsFetchDialog({ show, onHide }) {
  // Sparegrns UI Context
  const SparegrnsUIContext = useSparegrnsUIContext();
  const SparegrnsUIProps = useMemo(() => {
    return {
      ids: SparegrnsUIContext.ids,
      queryParams: SparegrnsUIContext.queryParams,
    };
  }, [SparegrnsUIContext]);

  // Sparegrns Redux state
  const { Sparegrns } = useSelector(
    (state) => ({
      Sparegrns: selectedSparegrns(state.Sparegrns.entities, SparegrnsUIProps.ids),
    }),
    shallowEqual
  );

  // if there weren't selected ids we should close modal
  useEffect(() => {
    if (!SparegrnsUIProps.ids || SparegrnsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SparegrnsUIProps.ids]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Fetch selected elements
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Sparegrns.map((Sparegrn) => (
              <div className="list-timeline-item mb-3" key={Sparegrn.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SparegrnstatusCssClasses[Sparegrn.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Sparegrn.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Sparegrn.manufacture}, {Sparegrn.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-primary btn-elevate"
          >
            Ok
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
