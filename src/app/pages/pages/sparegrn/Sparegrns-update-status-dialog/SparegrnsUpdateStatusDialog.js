import React, { useEffect, useState, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { SparegrnstatusCssClasses } from "../SparegrnsUIHelpers";
import * as actions from "../../../_redux/Sparegrn/SparegrnsActions";
import { useSparegrnsUIContext } from "../SparegrnsUIContext";

const selectedSparegrns = (entities, ids) => {
  const _Sparegrns = [];
  ids.forEach((id) => {
    const Sparegrn = entities.find((el) => el.id === id);
    if (Sparegrn) {
      _Sparegrns.push(Sparegrn);
    }
  });
  return _Sparegrns;
};

export function SparegrnsUpdateStatusDialog({ show, onHide }) {
  // Sparegrns UI Context
  const SparegrnsUIContext = useSparegrnsUIContext();
  const SparegrnsUIProps = useMemo(() => {
    return {
      ids: SparegrnsUIContext.ids,
      setIds: SparegrnsUIContext.setIds,
      queryParams: SparegrnsUIContext.queryParams,
    };
  }, [SparegrnsUIContext]);

  // Sparegrns Redux state
  const { Sparegrns, isLoading } = useSelector(
    (state) => ({
      Sparegrns: selectedSparegrns(state.Sparegrns.entities, SparegrnsUIProps.ids),
      isLoading: state.Sparegrns.actionsLoading,
    }),
    shallowEqual
  );

  // if there weren't selected Sparegrns we should close modal
  useEffect(() => {
    if (SparegrnsUIProps.ids || SparegrnsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SparegrnsUIProps.ids]);

  const [status, setStatus] = useState(0);

  const dispatch = useDispatch();
  const updateStatus = () => {
    // server request for updateing Sparegrn by ids
    dispatch(actions.updateSparegrnsStatus(SparegrnsUIProps.ids, status)).then(
      () => {
        // refresh list after deletion
        dispatch(actions.fetchSparegrns(SparegrnsUIProps.queryParams)).then(
          () => {
            // clear selections list
            SparegrnsUIProps.setIds([]);
            // closing delete modal
            onHide();
          }
        );
      }
    );
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Status has been updated for selected Sparegrns
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="overlay overlay-block cursor-default">
        {isLoading && (
          <div className="overlay-layer bg-transparent">
            <div className="spinner spinner-lg spinner-warning" />
          </div>
        )}
        <div className="list-timeline list-timeline-skin-light padding-30">
          <div className="list-timeline-items">
            {Sparegrns.map((Sparegrn) => (
              <div className="list-timeline-item mb-3" key={Sparegrn.id}>
                <span className="list-timeline-text">
                  <span
                    className={`label label-lg label-light-${
                      SparegrnstatusCssClasses[Sparegrn.status]
                    } label-inline`}
                    style={{ width: "60px" }}
                  >
                    ID: {Sparegrn.id}
                  </span>{" "}
                  <span className="ml-5">
                    {Sparegrn.manufacture}, {Sparegrn.model}
                  </span>
                </span>
              </div>
            ))}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer className="form">
        <div className="form-group">
          <select
            className={`form-control ${SparegrnstatusCssClasses[status]}`}
            value={status}
            onChange={(e) => setStatus(+e.target.value)}
          >
            <option value="0">Selling</option>
            <option value="1">Sold</option>
          </select>
        </div>
        <div className="form-group">
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={updateStatus}
            className="btn btn-primary btn-elevate"
          >
            Update Status
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
