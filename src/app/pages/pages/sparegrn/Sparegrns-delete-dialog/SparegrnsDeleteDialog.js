/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Sparegrn/SparegrnsActions";
import { useSparegrnsUIContext } from "../SparegrnsUIContext";

export function SparegrnsDeleteDialog({ show, onHide }) {
  // Sparegrns UI Context
  const SparegrnsUIContext = useSparegrnsUIContext();
  const SparegrnsUIProps = useMemo(() => {
    return {
      ids: SparegrnsUIContext.ids,
      setIds: SparegrnsUIContext.setIds,
      queryParams: SparegrnsUIContext.queryParams,
    };
  }, [SparegrnsUIContext]);

  // Sparegrns Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Sparegrns.actionsLoading }),
    shallowEqual
  );

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  // if there weren't selected Sparegrns we should close modal
  useEffect(() => {
    if (!SparegrnsUIProps.ids || SparegrnsUIProps.ids.length === 0) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SparegrnsUIProps.ids]);

  const deleteSparegrns = () => {
    // server request for deleting Sparegrn by seleted ids
    dispatch(actions.deleteSparegrns(SparegrnsUIProps.ids)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSparegrns(SparegrnsUIProps.queryParams)).then(() => {
        // clear selections list
        SparegrnsUIProps.setIds([]);
        // closing delete modal
        onHide();
      });
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Sparegrns Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete selected Sparegrns?</span>
        )}
        {isLoading && <span>Sparegrns are deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSparegrns}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
