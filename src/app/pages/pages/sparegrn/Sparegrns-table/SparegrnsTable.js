// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import * as actions from "../../../_redux/Sparegrn/SparegrnsActions";
import * as uiHelpers from "../SparegrnsUIHelpers";
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
} from "../../../../../_metronic/_helpers";
// import * as columnFormatters from "./column-formatters";
import { Pagination } from "../../../../../_metronic/_partials/controls";
import { useSparegrnsUIContext } from "../SparegrnsUIContext";

export function SparegrnsTable() {
  // Sparegrns UI Context
  const SparegrnsUIContext = useSparegrnsUIContext();
  const SparegrnsUIProps = useMemo(() => {
    return {
      ids: SparegrnsUIContext.ids,
      setIds: SparegrnsUIContext.setIds,
      queryParams: SparegrnsUIContext.queryParams,
      setQueryParams: SparegrnsUIContext.setQueryParams,
      openEditSparegrnPage: SparegrnsUIContext.openEditSparegrnPage,
      openDeleteSparegrnDialog: SparegrnsUIContext.openDeleteSparegrnDialog,
    };
  }, [SparegrnsUIContext]);

  // Getting curret state of Sparegrns list from store (Redux)
  const { currentState } = useSelector(
    (state) => ({ currentState: state.Sparegrns }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Sparegrns Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    SparegrnsUIProps.setIds([]);
    // server call by queryParams
    dispatch(actions.fetchSparegrns(SparegrnsUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SparegrnsUIProps.queryParams, dispatch]);
  // Table columns

  // console.log(sales_Sparegrn_id);

  const columns = [
    // {
    //   dataField: "sales_Sparegrn_id",
    //   text: "ID",
    //   sort: true,
    //   sortCaret: sortCaret,
    // },
    {
      dataField: "grn_id",
      text: "ID",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "expiry_date",
      text: "Expire Date",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "GrN_created",
      text: "Gren Created Date",
      sort: true,
      sortCaret: sortCaret,
    },
    {
      dataField: "GrN_updated",
      text: "Grn Upadteed Date",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.ColorColumnFormatter,
    },
    {
      dataField: "description",
      text: "Description",
      sort: true,
      sortCaret: sortCaret,
      // formatter: columnFormatters.PriceColumnFormatter,
    },
    // {
    //   dataField: "sales_Sparegrn_email",
    //   text: "Email",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.StatusColumnFormatter,
    // },
    // {
    //   dataField: "sales_Sparegrn_password",
    //   text: "Password",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "profile_pic",
    //   text: "Image",
    //   sort: true,
    //   sortCaret: sortCaret,
    //   // formatter: columnFormatters.ConditionColumnFormatter,
    // },
    // {
    //   dataField: "action",
    //   text: "Actions",
    //   formatter: columnFormatters.ActionsColumnFormatter,
    //   formatExtraData: {
    //     openEditSparegrnPage: SparegrnsUIProps.openEditSparegrnPage,
    //     openDeleteSparegrnDialog: SparegrnsUIProps.openDeleteSparegrnDialog,
    //   },
    //   classes: "text-right pr-0",
    //   headerClasses: "text-right pr-3",
    //   style: {
    //     minWidth: "100px",
    //   },
    // },
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: SparegrnsUIProps.queryParams.pageSize,
    page: SparegrnsUIProps.queryParams.pageNumber,
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                bordered={false}
                remote
                keyField="sales_Sparegrn_id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  SparegrnsUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: SparegrnsUIProps.ids,
                  setIds: SparegrnsUIProps.setIds,
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
