import React from "react";
import {
  SparegrnConditionCssClasses,
  SparegrnConditionTitles
} from "../../SparegrnsUIHelpers";

export const ConditionColumnFormatter = (cellContent, row) => (
  <>
    <span
      className={`badge badge-${
        SparegrnConditionCssClasses[row.condition]
      } badge-dot`}
    ></span>
    &nbsp;
    <span
      className={`font-bold font-${
        SparegrnConditionCssClasses[row.condition]
      }`}
    >
      {SparegrnConditionTitles[row.condition]}
    </span>
  </>
);
