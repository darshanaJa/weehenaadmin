import React from "react";
import {
  SparegrnstatusCssClasses,
  SparegrnstatusTitles
} from "../../SparegrnsUIHelpers";

export const StatusColumnFormatter = (cellContent, row) => (
  <span
    className={`label label-lg label-light-${
      SparegrnstatusCssClasses[row.status]
    } label-inline`}
  >
    {SparegrnstatusTitles[row.status]}
  </span>
);
