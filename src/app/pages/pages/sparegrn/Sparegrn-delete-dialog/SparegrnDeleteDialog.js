/* eslint-disable no-restricted-imports */
import React, { useEffect, useMemo } from "react";
import { Modal } from "react-bootstrap";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import * as actions from "../../../_redux/Sparegrn/SparegrnsActions";
import { useSparegrnsUIContext } from "../SparegrnsUIContext";

export function SparegrnDeleteDialog({ sales_Sparegrn_id, show, onHide }) {
  // console.log(sales_Sparegrn_id)
  // Sparegrns UI Context
  const SparegrnsUIContext = useSparegrnsUIContext();
  const SparegrnsUIProps = useMemo(() => {
    return {
      setIds: SparegrnsUIContext.setIds,
      queryParams: SparegrnsUIContext.queryParams,
    };
  }, [SparegrnsUIContext]);

  // Sparegrns Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    (state) => ({ isLoading: state.Sparegrns.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!sales_Sparegrn_id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sales_Sparegrn_id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteSparegrn = () => {
    // server request for deleting Sparegrn by id
    dispatch(actions.deleteSparegrn(sales_Sparegrn_id)).then(() => {
      // refresh list after deletion
      dispatch(actions.fetchSparegrns(SparegrnsUIProps.queryParams));
      // clear selections list
      SparegrnsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {isLoading && <ModalProgressBar variant="query" />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Sparegrn Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this Sparegrn?</span>
        )}
        {isLoading && <span>Sparegrn is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteSparegrn}
            className="btn btn-primary ml-2"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
