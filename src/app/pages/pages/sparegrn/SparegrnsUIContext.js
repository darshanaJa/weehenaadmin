import React, { createContext, useContext, useState, useCallback } from "react";
import { isEqual, isFunction } from "lodash";
import { initialFilter } from "./SparegrnsUIHelpers";

const SparegrnsUIContext = createContext();

export function useSparegrnsUIContext() {
  return useContext(SparegrnsUIContext);
}

export const SparegrnsUIConsumer = SparegrnsUIContext.Consumer;

export function SparegrnsUIProvider({ SparegrnsUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback((nextQueryParams) => {
    setQueryParamsBase((prevQueryParams) => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    newSparegrnButtonClick: SparegrnsUIEvents.newSparegrnButtonClick,
    openEditSparegrnPage: SparegrnsUIEvents.openEditSparegrnPage,
    openDeleteSparegrnDialog: SparegrnsUIEvents.openDeleteSparegrnDialog,
    openDeleteSparegrnsDialog: SparegrnsUIEvents.openDeleteSparegrnsDialog,
    openFetchSparegrnsDialog: SparegrnsUIEvents.openFetchSparegrnsDialog,
    openUpdateSparegrnsStatusDialog: SparegrnsUIEvents.openUpdateSparegrnsStatusDialog,
  };

  return (
    <SparegrnsUIContext.Provider value={value}>
      {children}
    </SparegrnsUIContext.Provider>
  );
}
