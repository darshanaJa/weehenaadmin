import React, { useMemo } from "react";
import { useSparegrnsUIContext } from "../SparegrnsUIContext";

export function SparegrnsGrouping() {
  // Sparegrns UI Context
  const SparegrnsUIContext = useSparegrnsUIContext();
  const SparegrnsUIProps = useMemo(() => {
    return {
      ids: SparegrnsUIContext.ids,
      setIds: SparegrnsUIContext.setIds,
      openDeleteSparegrnsDialog: SparegrnsUIContext.openDeleteSparegrnsDialog,
      openFetchSparegrnsDialog: SparegrnsUIContext.openFetchSparegrnsDialog,
      openUpdateSparegrnsStatusDialog:
        SparegrnsUIContext.openUpdateSparegrnsStatusDialog,
    };
  }, [SparegrnsUIContext]);

  return (
    <div className="form">
      <div className="row align-items-center form-group-actions margin-top-20 margin-bottom-20">
        <div className="col-xl-12">
          <div className="form-group form-group-inline">
            <div className="form-label form-label-no-wrap">
              <label className="-font-bold font-danger-">
                <span>
                  Selected records count: <b>{SparegrnsUIProps.ids.length}</b>
                </span>
              </label>
            </div>
            <div>
              <button
                type="button"
                className="btn btn-danger font-weight-bolder font-size-sm"
                onClick={SparegrnsUIProps.openDeleteSparegrnsDialog}
              >
                <i className="fa fa-trash"></i> Delete All
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SparegrnsUIProps.openFetchSparegrnsDialog}
              >
                <i className="fa fa-stream"></i> Fetch Selected
              </button>
              &nbsp;
              <button
                type="button"
                className="btn btn-light-primary font-weight-bolder font-size-sm"
                onClick={SparegrnsUIProps.openUpdateSparegrnsStatusDialog}
              >
                <i className="fa fa-sync-alt"></i> Update Status
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
