import React from "react";
import { Route } from "react-router-dom";
import { SparegrnsLoadingDialog } from "./Sparegrns-loading-dialog/SparegrnsLoadingDialog";
import { SparegrnDeleteDialog } from "./Sparegrn-delete-dialog/SparegrnDeleteDialog";
import { SparegrnsDeleteDialog } from "./Sparegrns-delete-dialog/SparegrnsDeleteDialog";
import { SparegrnsFetchDialog } from "./Sparegrns-fetch-dialog/SparegrnsFetchDialog";
import { SparegrnsUpdateStatusDialog } from "./Sparegrns-update-status-dialog/SparegrnsUpdateStatusDialog";
import { SparegrnsCard } from "./SparegrnsCard";
import { SparegrnsUIProvider } from "./SparegrnsUIContext";
import { notify } from "../../../config/Toastify";

// window.location.reload()

export function SparegrnsPage({ history }) {
  const SparegrnsUIEvents = {
    newSparegrnButtonClick: () => {
      history.push("/stocks/sparepartgrn/new");
    },
    openEditSparegrnPage: (id) => {
      history.push(`/stocks/sparepartgrn/${id}/edit`);
    },
    openDeleteSparegrnDialog: (sales_Sparegrn_id) => {
      history.push(`/stocks/sparepartgrn/${sales_Sparegrn_id}/delete`);
    },
    openDeleteSparegrnsDialog: () => {
      history.push(`/stocks/sparepartgrn/deleteSparegrns`);
    },
    openFetchSparegrnsDialog: () => {
      history.push(`/stocks/sparepartgrn/fetch`);
    },
    openUpdateSparegrnsStatusDialog: () => {
      history.push("/stocks/sparepartgrn/updateStatus");
    },
  };

  return (
    <>
    {notify()}
    <SparegrnsUIProvider SparegrnsUIEvents={SparegrnsUIEvents}>
      <SparegrnsLoadingDialog />
      <Route path="/stocks/sparepartgrn/deleteSparegrns">
        {({ history, match }) => (
          <SparegrnsDeleteDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/sparepartgrn");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/sparepartgrn/:sales_Sparegrn_id/delete">
        {({ history, match }) => (
          <SparegrnDeleteDialog
            show={match != null}
            sales_Sparegrn_id={match && match.params.sales_Sparegrn_id}
            onHide={() => {
              history.push("/stocks/sparepartgrn");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/sparepartgrn/fetch">
        {({ history, match }) => (
          <SparegrnsFetchDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/sparepartgrn");
            }}
          />
        )}
      </Route>
      <Route path="/stocks/sparepartgrn/updateStatus">
        {({ history, match }) => (
          <SparegrnsUpdateStatusDialog
            show={match != null}
            onHide={() => {
              history.push("/stocks/sparepartgrn");
            }}
          />
        )}
      </Route>
      <SparegrnsCard />
    </SparegrnsUIProvider>
    </>
  );
}
