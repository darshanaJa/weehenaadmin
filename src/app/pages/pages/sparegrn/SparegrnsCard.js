import React, {useMemo} from "react";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../_metronic/_partials/controls";
import { SparegrnsFilter } from "./Sparegrns-filter/SparegrnsFilter";
import { SparegrnsTable } from "./Sparegrns-table/SparegrnsTable";
import { SparegrnsGrouping } from "./Sparegrns-grouping/SparegrnsGrouping";
import { useSparegrnsUIContext } from "./SparegrnsUIContext";
import { notify } from "../../../config/Toastify";

export function SparegrnsCard(msg){

  // const [name,setName] = useState()

  // setName(msg)
  // console.log(msg)

  // useEffect(()=>{
  //   console.log(msg)
  //   // notify("abcd")
  // },[])

  const SparegrnsUIContext = useSparegrnsUIContext();
  const SparegrnsUIProps = useMemo(() => {
    return {
      ids: SparegrnsUIContext.ids,
      queryParams: SparegrnsUIContext.queryParams,
      setQueryParams: SparegrnsUIContext.setQueryParams,
      newSparegrnButtonClick: SparegrnsUIContext.newSparegrnButtonClick,
      openDeleteSparegrnsDialog: SparegrnsUIContext.openDeleteSparegrnsDialog,
      openEditSparegrnPage: SparegrnsUIContext.openEditSparegrnPage,
      openUpdateSparegrnsStatusDialog:SparegrnsUIContext.openUpdateSparegrnsStatusDialog,
      openFetchSparegrnsDialog: SparegrnsUIContext.openFetchSparegrnsDialog,
    };
  }, [SparegrnsUIContext]);

  return (
    <>
    <Card>
      {notify()}
      <CardHeader title="Sparegrn list">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={SparegrnsUIProps.newSparegrnButtonClick}
          >
            New Sparegrn
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <SparegrnsFilter />
        {SparegrnsUIProps.ids.length > 0 && (
          <>
            <SparegrnsGrouping />
          </>
        )}
        <SparegrnsTable />
      </CardBody>
    </Card>
    </>
  );
}
