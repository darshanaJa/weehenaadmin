import React, {useState} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input } from "../../../../../_metronic/_partials/controls";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
// import axios from "axios";
import { Image_Url } from "../../../../config/config";

// Validation schema
const SparegrnEditSchema = Yup.object().shape({
  // id: Yup.string(),
  sales_Sparegrn_fname: Yup.string()
    .required("Name is required")
    .min(2, "First Name must be at least 2 characters"),
  sales_Sparegrn_lname: Yup.string()
    .required("Name is required")
    .min(2, "Last Name must be at least 2 characters"),
  address: Yup.string()
    .required("Address is required")
    .min(2, "Address must be at least 2 characters"),
  sales_Sparegrn_nic: Yup.string()
    .required("Sparegrn NIC is required")
    .min(10, "NIC be at least 9 characters"),
  sales_Sparegrn_email: Yup.string().email()
    .required("Email is required"),
  sales_Sparegrn_mobile_1: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
  sales_Sparegrn_mobile_2: Yup.string()
    .required("Required")
    .matches(/^[0-9]+$/, "Must be only digits")
    .min(10, "Contact number be at least 10 numbers")
    .max(10, "Contact number be at least 10 numbers"),
});


export function SparegrnEditForm({
  Sparegrn,
  btnRef,
  saveSparegrn,
}) {

  console.log(Sparegrn)

  // console.log(Sparegrn.profile_pic)

  const submitImage=()=>{
      return(
        <img className="SparegrnImg" alt="Sparegrn" src={url} />
      );
  }

  const pic=Sparegrn.profile_pic
  // console.log(pic)
  const url = Image_Url+pic

  const bodyFormData = new FormData();

  const [profile_pic,set_Profile_pic]=useState();

  return (
    <div>
      <Formik
        enableReinitialize={true}
        initialValues={Sparegrn}
        validationSchema={SparegrnEditSchema}
        onSubmit={(values)  => {
          bodyFormData.append('sales_Sparegrn_fname',values.sales_Sparegrn_fname);
          bodyFormData.append('sales_Sparegrn_lname',values.sales_Sparegrn_lname);
          bodyFormData.append('sales_Sparegrn_nic',values.sales_Sparegrn_nic);
          bodyFormData.append('sales_Sparegrn_mobile_1',values.sales_Sparegrn_mobile_1);
          bodyFormData.append('sales_Sparegrn_mobile_2',values.sales_Sparegrn_mobile_2);
          bodyFormData.append('sales_Sparegrn_email',values.sales_Sparegrn_email);
          bodyFormData.append('password',values.password);
          bodyFormData.append('address',values.address);
          bodyFormData.append('profile_image',profile_pic);

          console.log(bodyFormData);
          saveSparegrn(bodyFormData);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="sales_Sparegrn_fname"
                    component={Input}
                    placeholder="First Name"
                    label="Fisrt Name"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="sales_Sparegrn_lname"
                    component={Input}
                    placeholder="Last Name"
                    label="Last Name"
                  />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="sales_Sparegrn_nic"
                    component={Input}
                    placeholder="number"
                    label="NIC"
                  />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="sales_Sparegrn_mobile_1"
                      component={Input}
                      placeholder="Mobile 01"
                      label="Mobile 01"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="sales_Sparegrn_mobile_2"
                    component={Input}
                    placeholder="Mobile 02"
                    label="Mobile 02"
                    // customFeedbackLabel="Please enter "
                  />
                </div>
                <div className="col-lg-4">
                <Field
                    type="text"
                    name="address"
                    component={Input}
                    placeholder="address"
                    label="Address"
                  />
                </div>
              </div>

              <div className="form-group row">
                  <div className="col-lg-4">
                    <Field
                        type="email"
                        name="sales_Sparegrn_email"
                        component={Input}
                        placeholder="Email"
                        label="Email"
                      />
                  </div>
                  <div className="col-lg-4">
                    {/* <div className="form-group row"> */}
                      <input className="SparegrnImageBtn + dwnfileEdit" type="file" name="profile_pic"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    {/* </div> */}
                  </div> 
                  <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtnedit"> Save</button>
                    {/* <button type="submit" className="btn btn-primary ml-2"> Save</button> */}
                  </div>
                </div>  
                <div className="form-group row">
                  {submitImage()}
                </div>

                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>
          </>
        )}
      </Formik>
      
    </div>

  );
}
