/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid,jsx-a11y/role-supports-aria-props */
import React, { useEffect, useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { shallowEqual, useSelector } from "react-redux";
import * as actions from "../../../_redux/Sparegrn/SparegrnsActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar,
} from "../../../../../_metronic/_partials/controls";
import { SparegrnEditForm } from "./SparegrnEditForm";
import { SparegrnCreateForm } from "./SparegrnCreateForm";
import {PasswordReset} from "./PasswrodReset"
import { Specifications } from "../Sparegrn-specifications/Specifications";
import { SpecificationsUIProvider } from "../Sparegrn-specifications/SpecificationsUIContext";
import { useSubheader } from "../../../../../_metronic/layout";
import { ModalProgressBar } from "../../../../../_metronic/_partials/controls";
import { RemarksUIProvider } from "../Sparegrn-remarks/RemarksUIContext";
import { Remarks } from "../Sparegrn-remarks/Remarks";
// import { SpecificationsTable } from "../Sparegrn-specifications/SpecificationsTable";

const initSparegrn = {

  description : "",
  // spare_part_quantity : "",
  // spareparts : "",
  // sales_Sparegrn_mobile_1 : "",
  // sales_Sparegrn_mobile_2 : "",
  // sales_Sparegrn_email : "",
  // address:"",
  // password:""
  // profile_pic : []

};

const initSparegrnPassword = {
  curent_password : "",
  new_password : "",
  confirm_password : ""

};

export function SparegrnEdit({
  history,
  match: {
    params: { id },
  },
}) {
  // Subheader
  const suhbeader = useSubheader();

  // Tabs
  const [tab, setTab] = useState("basic");
  const [title, setTitle] = useState("");
  const dispatch = useDispatch();
  // const layoutDispatch = useContext(LayoutContext.Dispatch);
  const { actionsLoading, SparegrnForEdit } = useSelector(
    (state) => ({
      actionsLoading: state.Sparegrns.actionsLoading,
      SparegrnForEdit: state.Sparegrns.SparegrnForEdit,
    }),
    shallowEqual
  );
  

  useEffect(() => {
    dispatch(actions.fetchSparegrn(id));
  }, [id, dispatch]);

  useEffect(() => {
    let _title = id ? "" : "New Sparegrn";
    if (SparegrnForEdit && id) {
      // _title = `Edit Sparegrn - ${SparegrnForEdit.sales_Sparegrn_fname} ${SparegrnForEdit.sales_Sparegrn_lname} - ${SparegrnForEdit.sales_Sparegrn_mobile_1}`;
    }

    setTitle(_title);
    suhbeader.setTitle(_title);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [SparegrnForEdit, id]);

  const saveSparegrn = (values) => {
    if (!id) {
      dispatch(actions.createSparegrn(values,id)).then(() => backToSparegrnsList());
    } else {
      dispatch(actions.updateSparegrn(values,id)).then(() => backToSparegrnsList());
    }
  };

  const savePassword = (values) => {
    console.log(values)
      dispatch(actions.updateSparegrnPassword(values,id)).then(() => backToSparegrnsList());
  };

  const btnRef = useRef();  

  const backToSparegrnsList = () => {
    // history.push(`/sales/Sparegrn`);
    history.push(`/stocks/sparepartgrn`);
    // window.location.reload(`/sales/Sparegrn`)
  };

  if (id) {
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSparegrnsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
           
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
               
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SparegrnEditForm
                actionsLoading={actionsLoading}
                Sparegrn={SparegrnForEdit || initSparegrn}
                btnRef={btnRef}
                saveSparegrn={saveSparegrn}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSparegrnId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSparegrnId={id}>
                <PasswordReset
                  actionsLoading={actionsLoading}
                  Sparegrn={initSparegrnPassword}
                  btnRef={btnRef}
                  savePassword={savePassword}
    />
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }else{
    return (
      <Card>
        {actionsLoading && <ModalProgressBar />}
        <CardHeader title={title}>
          <CardHeaderToolbar>
            <button
              type="button"
              onClick={backToSparegrnsList}
              className="btn btn-light"
            >
              <i className="fa fa-arrow-left"></i>
              Back
            </button>
            {`  `}
            
          </CardHeaderToolbar>
        </CardHeader>
        <CardBody>
          <ul className="nav nav-tabs nav-tabs-line " role="tablist">
            <li className="nav-item" onClick={() => setTab("basic")}>
              <a
                className={`nav-link ${tab === "basic" && "active"}`}
                data-toggle="tab"
                role="tab"
                aria-selected={(tab === "basic").toString()}
              >
                Basic info
              </a>
            </li>
            {id && (
              <>
                {" "}
                <li className="nav-item" onClick={() => setTab("remarks")}>
                  <a
                    className={`nav-link ${tab === "remarks" && "active"}`}
                    data-toggle="tab"
                    role="button"
                    aria-selected={(tab === "remarks").toString()}
                  >
                    Sparegrn remarks
                  </a>
                </li>
                <li className="nav-item" onClick={() => setTab("specs")}>
                  <a
                    className={`nav-link ${tab === "specs" && "active"}`}
                    data-toggle="tab"
                    role="tab"
                    aria-selected={(tab === "specs").toString()}
                  >
                    Sparegrn specifications
                  </a>
                </li>
              </>
            )}
          </ul>
          <div className="mt-5">
            {tab === "basic" && (
              <SparegrnCreateForm
                actionsLoading={actionsLoading}
                Sparegrn={SparegrnForEdit || initSparegrn}
                btnRef={btnRef}
                saveSparegrn={saveSparegrn}
                savePassword={savePassword}
              />
            )}
            {tab === "remarks" && id && (
              <RemarksUIProvider currentSparegrnId={id}>
                <Remarks />
              </RemarksUIProvider>
            )}
            {tab === "specs" && id && (
              <SpecificationsUIProvider currentSparegrnId={id}>
                <Specifications />
              </SpecificationsUIProvider>
            )}
          </div>
        </CardBody>
      </Card>
    );
  }

  
}
