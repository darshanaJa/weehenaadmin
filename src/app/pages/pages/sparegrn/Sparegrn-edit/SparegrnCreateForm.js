// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React, {useState,useEffect} from "react";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input,Select } from "../../../../../_metronic/_partials/controls";
// import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
import { SpareSuppliers_URL } from "../../../_redux/SpareSupplier/SpareSuppliersCrud";
import axios from "axios";
// import { Image_Url } from "../../../../config/config";
// import {
//   AVAILABLE_COLORS,
//   AVAILABLE_MANUFACTURES,
//   SparegrnstatusTitles,
//   SparegrnConditionTitles,
// } from "../SparegrnsUIHelpers";

// Validation schema
const SparegrnEditSchema = Yup.object().shape({
  // id: Yup.string(),
  description: Yup.string()
    .required("Description is required")
    .min(2, "Description must be at least 2 characters"),
  expiry_date: Yup.date()
    .required("Date is required"),
    // .min(1, "Quantity must be at least 2 characters"),
    spsupplier: Yup.string()
    .required("Suplier is required"),
    // .min(2, "spareparts must be at least 2 characters"),
  //   password: Yup.string()
  //   .required("Password is required")
  //   .min(5, "Password must be at least 5 characters"),
  // sales_Sparegrn_nic: Yup.string()
  //   .required("Sparegrn NIC is required")
  //   .min(10, "NIC be at least 9 characters"),
  // sales_Sparegrn_email: Yup.string().email()
  //   .required("Email is required"),
  // sales_Sparegrn_mobile_1: Yup.string()
  //   .required("Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  // sales_Sparegrn_mobile_2: Yup.string()
  //   .required("Required")
  //   .matches(/^[0-9]+$/, "Must be only digits")
  //   .min(10, "Contact number be at least 10 numbers")
  //   .max(10, "Contact number be at least 10 numbers"),
  
  // profile_pic: Yup.string()
  // .required("Required")
});


export function SparegrnCreateForm({
  Sparegrn,
  btnRef,
  saveSparegrn,
}) {

  // console.log(Sparegrn.profile_pic)

//   const submitImage=()=>{
//       return(
//         <img className="SparegrnImg" alt="Sparegrn" src={url} />
//       );
//   }

//   const pic=Sparegrn.profile_pic
//   console.log(pic)
//   const url = Image_Url+pic
//   console.log(url)



  // const bodyFormData = new FormData();

  // const [profile_pic,set_Profile_pic]=useState();

  const [shop,setShop]=useState([]);

  

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: SpareSuppliers_URL,
      data: { 
        "filter": [{"status": "1" }],
          "sort": "DESC",
          "limit": 10, 
          "skip":0
      }
      })
      .then((res) => {
        setShop(res.data.data.results)
        console.log(res.data.data.results)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])
  

  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={Sparegrn}
        validationSchema={SparegrnEditSchema}
        onSubmit={(values)  => {
          // bodyFormData.append('sales_Sparegrn_fname',values.sales_Sparegrn_fname);
          // bodyFormData.append('sales_Sparegrn_lname',values.sales_Sparegrn_lname);
          // bodyFormData.append('sales_Sparegrn_nic',values.sales_Sparegrn_nic);
          // bodyFormData.append('sales_Sparegrn_mobile_1',values.sales_Sparegrn_mobile_1);
          // bodyFormData.append('sales_Sparegrn_mobile_2',values.sales_Sparegrn_mobile_2);
          // bodyFormData.append('sales_Sparegrn_email',values.sales_Sparegrn_email);
          // bodyFormData.append('password',values.password);
          // bodyFormData.append('address',values.address);
          // bodyFormData.append('profile_image',profile_pic);

          console.log(values);
          saveSparegrn(values);

        }}
      >
        {({ handleSubmit }) => (
          <>
            <Form className="form form-label-right">
              <div className="form-group row">
                {/* <div className="col-lg-4">
                  <Field
                    name="id"
                    component={Input}
                    placeholder="ID"
                    label="ID"
                  />
                </div> */}
                <div className="col-lg-4">
                <Field
                    type="date"
                    name="expiry_date"
                    component={Input}
                    placeholder="Expire Date"
                    label="Expire Date"
                  />
                </div>
      
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="description"
                    component={Input}
                    placeholder="Description"
                    label="Description"
                  />
                </div>
                <div className="col-lg-4">
                  <Select name="spsupplier" label="Spare Supplier">
                      <option>Choose One</option>
                      {shop.map((item) => (
                          <option value={item.spart_supplier_id} >
                            {item.supplier_name}
                          </option>    
                      ))}
                  </Select>
                </div>
              </div>

              <div className="form-group row">
                <div className="col-lg-4">
                  <Field
                      type="text"
                      name="grnsparepartstock.spare_part_quantity"
                      component={Input}
                      placeholder="Quanity"
                      label="Quanity"
                      // customFeedbackLabel="Please enter Price"
                    />
                </div>
                <div className="col-lg-4">
                  <Field
                    type="text"
                    name="grnsparepartstock.spareparts"
                    component={Input}
                    placeholder="Spare Parts"
                    label="Spare Parts"
                    // customFeedbackLabel="Please enter "
                  />
                </div>
                <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div>
                {/* <div className="col-lg-4">
                <Field
                    type="text"
                    name="address"
                    component={Input}
                    placeholder="address"
                    label="Address"
                  />
                </div> */}
              </div>

              {/* <div className="form-group row">
                <div className="col-lg-4">
                    <Field
                      type="text"
                      name="password"
                      component={Input}
                      placeholder="password"
                      label="Password"
                      // customFeedbackLabel="Please enter "
                    />
                  </div>
                  <div className="col-lg-4">
                  <Field
                      type="email"
                      name="sales_Sparegrn_email"
                      component={Input}
                      placeholder="Email"
                      label="Email"
                    />
                  </div>
                
                </div>   */}

            <div className="form-group row">
                  {/* <div className="col-lg-4">
                    <div className="form-group row">
                      <input className="SparegrnImageBtn + dwnfile" type="file" name="profile_pic"
                        onChange={(event) => {set_Profile_pic(event.currentTarget.files[0]); }}
                       />
                    </div>
                  </div>  */}
                  {/* <div className="col-lg-4">
                    <button type="submit" className="btn btn-primary ml-2 + downbtn"> Save</button>
                  </div> */}
                  
                </div>
                <button
                type="submit"
                style={{ display: "none" }}
                ref={btnRef}
                onSubmit={() => handleSubmit()}
              ></button>
           
            </Form>

            <div>
                {/* <div className="form-group row">
                   <div className="col-lg-4">
                      {submitImage()}
                    </div>
                </div> */}
                {/* <div className="form-group row"> */}
                  {/* <div className="col-lg-4">
                    <label>Password</label>
                    <Field
                      name="sales_Sparegrn_password"
                      className="form-control"
                    />
                </div>
                  <div className="col-lg-4">
                      <button type="submit" className='btn btn-primary ml-2 + downbtn'> Reset Password</button>
                  </div> */}

                {/* <div className="form-group row">
                   <div className="col-lg-4">
                      {submitImage()}
                    </div>
                </div> */}
             
                {/* </div> */}
            </div>
          </>
        )}
      </Formik>
    </>
  );
}
