export const SparegrnstatusCssClasses = ["success", "info", ""];
export const SparegrnstatusTitles = ["Selling", "Sold"];
export const SparegrnConditionCssClasses = ["success", "danger", ""];
export const SparegrnConditionTitles = ["New", "Used"];
export const defaultSorted = [{ dataField: "sales_Sparegrn_id", order: "asc" }];
export const sizePerPageList = [
  { text: "10", value: 10 },
  { text: "25", value: 25 },
  { text: "50", value: 50 }
];
export const initialFilter = {
  filter: {
    sales_Sparegrn_fname: ""
  },
  sales_Sparegrn_fname:null,
  sortOrder: "asc", // asc||desc
  sortField: "sales_Sparegrn_fname",
  pageNumber: 1,
  pageSize: 10
  // value:
};
// export const AVAILABLE_COLORS = [
//   "Red",
//   "CadetBlue",
//   "Eagle",
//   "Gold",
//   "LightSlateGrey",
//   "RoyalBlue",
//   "Crimson",
//   "Blue",
//   "Sienna",
//   "Indigo",
//   "Green",
//   "Violet",
//   "GoldenRod",
//   "OrangeRed",
//   "Khaki",
//   "Teal",
//   "Purple",
//   "Orange",
//   "Pink",
//   "Black",
//   "DarkTurquoise"
// ];

// export const AVAILABLE_MANUFACTURES = [
//   "Pontiac",
//   "Kia",
//   "Lotus",
//   "Subaru",
//   "Jeep",
//   "Isuzu",
//   "Mitsubishi",
//   "Oldsmobile",
//   "Chevrolet",
//   "Chrysler",
//   "Audi",
//   "Suzuki",
//   "GMC",
//   "Cadillac",
//   "Infinity",
//   "Mercury",
//   "Dodge",
//   "Ram",
//   "Lexus",
//   "Lamborghini",
//   "Honda",
//   "Nissan",
//   "Ford",
//   "Hyundai",
//   "Saab",
//   "Toyota"
// ];
