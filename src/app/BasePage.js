import React, { Suspense, lazy } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";
import { BuilderPage } from "./pages/BuilderPage";
import { MyPage } from "./pages/MyPage";
// import { DashboardPage } from "./pages/DashboardPage";
import { DashboardPage02 } from "./pages/DashboardPage02";
import { Demo2Dashboard } from "../_metronic/_partials/dashboards/Demo2Dashboard";
import { Demo4Dashboard } from "../_metronic/_partials/dashboards/Demo4Dashboard";
import { Demo6Dashboard } from "../_metronic/_partials/dashboards/Demo6Dashboard";


const GoogleMaterialPage = lazy(() =>
  import("./modules/GoogleMaterialExamples/GoogleMaterialPage")
);
const ReactBootstrapPage = lazy(() =>
  import("./modules/ReactBootstrapExamples/ReactBootstrapPage")
);
const saleseMenue = lazy(() =>
  import("./pages/pages/saleseMenue")
);
const stockMenue = lazy(() =>
  import("./pages/pages/stockMenue")
);
const buyBackMenue = lazy(() =>
  import("./pages/pages/buyBackMenue")
);
const suplierMenue = lazy(() =>
  import("./pages/pages/suplierMenue")
);
const ECommercePage02 = lazy(() =>
  import("./pages/pages/eCommercePage02")
);
const transportMenue = lazy(() =>
  import("./pages/pages/transportMenue")
);
const UserProfilepage = lazy(() =>
  import("./modules/UserProfile/UserProfilePage")
);

const hrMenue = lazy(() =>
  import("./pages/pages/hrMenue")
);

const accessMenue = lazy(() =>
import("./pages/pages/accessMenue")
);

const inandoutMenue = lazy(() =>
import("./pages/pages/inandoutMenue")
);

export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <ContentRoute path="/dashboard/sales" exact component={DashboardPage02} />
        <ContentRoute path="/dashboard/stocks" exact component={Demo6Dashboard} />
        <ContentRoute path="/dashboard/buyback" exact component={Demo2Dashboard} />
        <ContentRoute path="/dashboard" component={Demo4Dashboard} />
        <ContentRoute path="/builder" component={BuilderPage} />
        <ContentRoute path="/my-page" component={MyPage} />
        <Route path="/google-material" component={GoogleMaterialPage} />
        <Route path="/react-bootstrap" component={ReactBootstrapPage} />
        <Route path="/sales" component={saleseMenue} />
        <Route path="/hr" component={hrMenue} />
        <Route path="/access" component={accessMenue} />
        <Route path="/inandout" component={inandoutMenue} />
        <Route path="/stocks" component={stockMenue} />
        <Route path="/suplier" component={suplierMenue} />
        <Route path="/buyback" component={buyBackMenue} />
        <Route path="/transport" component={transportMenue} />
        <Route path="/e-commerce" component={ECommercePage02} />
        <Route path="/user-profile" component={UserProfilepage} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
