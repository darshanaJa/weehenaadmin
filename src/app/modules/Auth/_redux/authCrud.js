import axios from "axios";

import { Api_Login } from "../../../config/config";

export const LOGIN_URL = Api_Login + '/api/admin/login';


// export const LOGIN_URL = "api/auth/login";
export const REGISTER_URL = "api/auth/register";
export const REQUEST_PASSWORD_URL = "api/auth/forgot-password";

export const ME_URL = Api_Login + '/api/admin/by-tocken';

export function login(email, password) {
  return axios.post(LOGIN_URL, { "email" : email, "password": password },{headers:{'Content-Type':'application/json'}});
}

// export function login(email, password) {
//   return axios.post(LOGIN_URL, { email, password });
// }

export function register(email, fullname, username, password) {
  return axios.post(REGISTER_URL, { email, fullname, username, password });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function getUserByToken() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(ME_URL);
}

// export function getUserByToken(accessToken) {
//   // Authorization head should be fulfilled in interceptor.
//   console.log(accessToken)
//   return accessToken;
// }
