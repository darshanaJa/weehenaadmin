import React from "react";
import { notify } from "../../../app/config/Toastify";
import {
  MixedWidget5,
  MixedWidget7,
  MixedWidget8,
  ListsWidget11
} from "../widgets";

export function Demo2Dashboard() {
  return (
    <>
    {notify()}
      
      {/* begin::Row */}
      <div className="row">
        <div className="col-xl-12">
          <ListsWidget11 className="gutter-b card-stretch" />
        </div>
      </div> 
       {/* <br /> */}

      <div className="row">
        <div className="col-xl-12">
          <MixedWidget5 className="gutter-b card-stretch" />
        </div>
      </div> 
       <br />

      <div className="row">
        <div className="col-xl-12">
          <MixedWidget7 className="gutter-b card-stretch" />
        </div>
      </div>
      <br/>
      {/* <br /> */}
      
      <div className="row">
        <div className="col-lg-12 col-xxl-12">
          <MixedWidget8 className="card-stretch gutter-b" />
        </div>
      </div>
      {/* <br/> */}
      {/* <br/> */}
    </>
  );
}
