/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect,useState } from "react";
// import { Dropdown } from "react-bootstrap";
// import objectPath from "object-path";
// import ApexCharts from "apexcharts";
// import { DropdownCustomToggler, DropdownMenu2 } from "../../dropdowns";
// import { useHtmlClassService } from "../../../layout";
// import React from 'react'
import { Chart } from 'react-charts'
import axios from "axios";
import { Api_Login } from "../../../../app/config/config";
import ProgressBar from 'react-bootstrap/ProgressBar';
// import { ShoppingCartSharp } from "@material-ui/icons";
// import { Label } from "react-bootstrap";

export function MixedWidget9({ className, chartColor = "white" }) {
  
  const [shop,setShop]=useState();
  const [shop1,setShop1]=useState();
//   const [shop2,setShop2]=useState();
//   const [shop3,setShop3]=useState();

  const AF=shop/(shop+shop1)*100
  const DF=shop1/(shop+shop1)*100
//   const AC=shop2/(shop+shop1)*100
//   const DC=shop3/(shop+shop1)*100

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/sales-agent/all/active/agent-count',
      })
      .then((res) => {
        setShop(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/sales-agent/all/deactive/agent-count',
      })
      .then((res) => {
        setShop1(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

//   useEffect(()=>{
//     axios({
//       method: 'get',
//       baseURL: Api_Login + '/api/chickencoop/all/active/coop-count',
//       })
//       .then((res) => {
//         setShop2(res.data)
//       })
//       .catch(function (response) {
//       });
      
//   },[])

//   useEffect(()=>{
//     axios({
//       method: 'get',
//       baseURL: Api_Login + '/api/chickencoop/all/deactive/coop-count',
//       })
//       .then((res) => {
//         setShop3(res.data)
//       })
//       .catch(function (response) {
//       });
      
//   },[])

  console.log(shop)
  console.log(shop1)
//   console.log(shop2)
//   console.log(shop3)

  return (
    <>
      {/* begin::Tiles Widget 1 */}
      <div
        // className={`card card-custom bg-radial-gradient-danger ${className}`}
      >
        {/* begin::Header */}
        <div className="card-header border-0 pt-5">
          <br />
          <h3 className="fontColor">
            Sale Agent Details
          </h3>
          <br/>
          <div>
            <p><b>Active Agents</b></p>
            <ProgressBar 
              style={{
                color:"green",
              // width:"550px", 
                height: "35px"}}
                variant="warning"
              animated now={AF}
              label={<h6>{`Agent - ${shop}`}</h6>} 
            />
         </div>
         <br/>
         <div>
            <p><b>Deactivate Agents</b></p>    
            <ProgressBar 
              style={{
              // width:"550px", 
              height: "35px"}}
              variant="warning"
              // variant="success"
              animated now={DF}
              label={<h6>{`Agent - ${shop1}`}</h6>} 
            />
         </div>
         <br/>
         {/* <div>
            <p><b>Active Coops</b></p>
            <ProgressBar 
              style={{
              // width:"550px", 
              height: "35px"}}
              variant="warning"
              animated now={AC}
              label={<h6>{`Coop - ${shop2}`}</h6>} 
            />
         </div>
         <br/>
         <div>
          <p><b>Deactivate Coops</b></p>
            <ProgressBar 
              style={{
              // width:"550px", 
              height: "35px"}}
              variant="warning"
              animated now={DC}
              label={<h6>{`Coop - ${shop3}`}</h6>} 
            />
         </div> */}
        </div>
        <div className="card-body d-flex flex-column p-0">
         
         
        </div>
      </div>
    </>
  );
}


function MyChart() {

  const [shop,setShop]=useState([]);
  // const [shopName,setShopName]=useState([]);
  // const [dateE,setdateE]=useState();

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/main-stock/report/available-stock'
      })
      .then((res) => {
        setShop(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  console.log(shop)

  const data = React.useMemo(
    () => [
      {
        label: 'Series 1',
        data: [['Active Farm', 1], ['Deacivate Farm', 2],['Active Coop', 4], ['Deacivate Coop', 7]]
      }
    ],
    []
  )
 
  const axes = React.useMemo(
    () => [
      { primary: true, type: 'ordinal', position: 'bottom' },
      { position: 'left', type: 'linear', stacked: false }
    ],
    []
  )

  const series = React.useMemo(
    () => ({
      type: 'bar'
    }),
    []
  )
 
  const lineChart = (
    // A react-chart hyper-responsively and continuously fills the available
    // space of its parent element automatically
    <div
      style={{
        color:"red",
        // width: '900px',
        height: "400px"
      }}
    >
      <Chart data={data} series={series} axes={axes} />
    </div>
  )

  return lineChart;
}

export default MyChart;