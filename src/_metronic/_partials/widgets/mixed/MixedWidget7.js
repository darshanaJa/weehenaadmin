/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect,useState } from "react";
// import { Dropdown } from "react-bootstrap";
// import objectPath from "object-path";
// import ApexCharts from "apexcharts";
// import { DropdownCustomToggler, DropdownMenu2 } from "../../dropdowns";
// import { useHtmlClassService } from "../../../layout";
// import React from 'react'
import { Chart } from 'react-charts'
import axios from "axios";
import { Api_Login } from "../../../../app/config/config";
import { Formik, Form } from "formik";
// import * as Yup from "yup";
import {Select} from "../../controls";
import { notifyWarning } from "../../../../app/config/Toastify";
// import { ShoppingCartSharp } from "@material-ui/icons";
// import { Label } from "react-bootstrap";

export function MixedWidget7({ className, chartColor = "white" }) {
  // const uiService = useHtmlClassService();
  

  // useEffect(() => {
  //   const element = document.getElementById("kt_mixed_widget_4_chart");

  //   if (!element) {
  //     return;
  //   }

    // const options = MyChart();
  //   const chart = new ApexCharts(element, options);
  //   chart.render();
  //   return function cleanUp() {
  //     chart.destroy();
  //   };
  // }, [layoutProps]);

  return (
    <>
      {/* begin::Tiles Widget 1 */}
      <div
        // className={`card card-custom bg-radial-gradient-danger ${className}`}
      >
        {/* begin::Header */}
        <div className="card-header border-0 pt-5">
          <br />
          <h3 className="fontColor">
            Farm Coop Details
          </h3>
          <br/>
          {MyChart()}
        </div>
        <div className="card-body d-flex flex-column p-0">
         
         
        </div>
      </div>
    </>
  );
}


function MyChart() {

  const [shop,setShop]=useState([]);
  
  const [batchID, stBatchID] = useState([])
  const [batchValue, stBatchValue] = useState('acf483a9-0ac5-4b81-963c-a038919b543f')
  // const [shopName,setShopName]=useState([]);
  // const [dateE,setdateE]=useState();

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Api_Login + '/api/buyback-farm/search',
      data:{
        filter: [{id: batchValue}]
      }
      })
      .then((res) => {
        // setShop(res.data)
        console.log(res.data.data.results[0].buyback)
        setShop(res.data.data.results[0].buyback)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[batchValue])

  console.log(shop)

  const data = React.useMemo(() => [

      shop.map((item) => (
        [item.chickcoop_name,item.chickcoop_quantity]
    ))
    ],[shop]
  )
 
  const axes = React.useMemo(
    () => [
      { primary: true, type: 'ordinal', position: 'bottom' },
      { position: 'left', type: 'linear', stacked: false }
    ],
    []
  )

  const series = React.useMemo(
    () => ({
      type: 'bar'
    }),
    []
  )
 
  const lineChart = (
    // A react-chart hyper-responsively and continuously fills the available
    // space of its parent element automatically
    <div
      style={{
        // color:"white",
        // width: '900px',
        height: "400px"
      }}
    >
      <Chart data={data} series={series} axes={axes} />
    </div>
  )
  useEffect(()=>{
    axios({
      method: 'Post',
      baseURL: Api_Login + '/api/buyback-farm/search',
      data:{
        "filter": [{"status": "1" }],
      }
      })
      .then((res) => {
        stBatchID(res.data.data.results)
      })
  },[])

  console.log(batchID)

  if(shop.length===0){
    // notifyWarning("Sorry No Details....")
  }
  // else{

  return (<Formik
    enableReinitialize={true}
    // initialValues={initStockBatch}
    // validationSchema={StockBatchEditSchema}
    onSubmit={(values) => {
      console.log(values)
      // setValues(values)

      // setFrom('')
      // setTo('')

    }}
  >
    {({ handleSubmit }) => (
      <>
      {notifyWarning}
        <Form className="form form-label-right">
          <div className="form-group row">
            <div className="col-lg-3">
              <Select name="batch_name" label="Farm Name" 
              onChange={(e)=>stBatchValue(e.target.value)}
              value={batchValue}
              
              >
              <option>Choose One</option>
              {batchID.map((item) => (
                  <option value={item.id} >
                    {item.buyback_name}
                  </option>    
              ))}
              </Select>
            </div>
            
            <div className="col-lg-2">
               {/* <button  className="btn btn-light ml-2 + downbtn" 
                >Reset</button> */}
            </div>
          </div>
          <button
            type="submit"
            style={{ display: "none" }}
            // ref={btnRef}
            onSubmit={() => handleSubmit()}
          ></button>
          
        </Form>
        {lineChart}
      </>
    )}
  </Formik>)
}
// }