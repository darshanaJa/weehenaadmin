/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect,useState } from "react";
// import { Dropdown } from "react-bootstrap";
// import objectPath from "object-path";
// import ApexCharts from "apexcharts";
// import { DropdownCustomToggler, DropdownMenu2 } from "../../dropdowns";
// import { useHtmlClassService } from "../../../layout";
// import React from 'react'
import { Chart } from 'react-charts'
import axios from "axios";
import { Api_Login } from "../../../../app/config/config";
// import { ShoppingCartSharp } from "@material-ui/icons";
// import { Label } from "react-bootstrap";

export function MixedWidget4({ className, chartColor = "white" }) {
  // const uiService = useHtmlClassService();
  

  // useEffect(() => {
  //   const element = document.getElementById("kt_mixed_widget_4_chart");

  //   if (!element) {
  //     return;
  //   }

    // const options = MyChart();
  //   const chart = new ApexCharts(element, options);
  //   chart.render();
  //   return function cleanUp() {
  //     chart.destroy();
  //   };
  // }, [layoutProps]);

  return (
    <>
      {/* begin::Tiles Widget 1 */}
      <div
        // className={`card card-custom bg-radial-gradient-danger ${className}`}
      >
        {/* begin::Header */}
        <div className="card-header border-0 pt-5">
          <br />
          <h3 className="fontColor">
            Main Stock Quntity
          </h3>
          <br/>
          {MyChart()}
        </div>
        <div className="card-body d-flex flex-column p-0">
         
         
        </div>
      </div>
    </>
  );
}


function MyChart() {

  const [shop,setShop]=useState([]);
  // const [shopName,setShopName]=useState([]);
  // const [dateE,setdateE]=useState();

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/main-stock/report/available-stock'
      })
      .then((res) => {
        setShop(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  console.log(shop)

  const data = React.useMemo(() => [

      shop.map((item) => (
        [item.item_name,item.item_current_quantity]
    ))
    ],[shop]
  )
 
  const axes = React.useMemo(
    () => [
      { primary: true, type: 'ordinal', position: 'bottom' },
      { position: 'left', type: 'linear', stacked: false }
    ],
    []
  )

  const series = React.useMemo(
    () => ({
      type: 'bar'
    }),
    []
  )
 
  const lineChart = (
    // A react-chart hyper-responsively and continuously fills the available
    // space of its parent element automatically
    <div
      style={{
        // color:"white",
        // width: '900px',
        height: "400px"
      }}
    >
      <Chart data={data} series={series} axes={axes} />
    </div>
  )

  return lineChart;
}