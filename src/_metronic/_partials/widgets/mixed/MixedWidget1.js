/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect,useState } from "react";
// import { Dropdown } from "react-bootstrap";
// import objectPath from "obsject-path";
// import ApexCharts from "apexcharts";
// import { DropdownCustomToggler, DropdownMenu2 } from "../../dropdowns";
// import { useHtmlClassService } from "../../../layout";
// import React from 'react'
import { Chart } from 'react-charts'
import axios from "axios";
import { Api_Login } from "../../../../app/config/config";
// import { ShoppingCartSharp } from "@material-ui/icons";
// import { Label } from "react-bootstrap";
// import { toAbsoluteUrl } from "../../../_helpers";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input1} from "../../controls";

export function MixedWidget1({ className, chartColor = "white" }) {

  // const uiService = useHtmlClassService();
  

  return (
    <>
      {/* begin::Tiles Widget 1 */}
      <div
        // className={`card card-custom bg-radial-gradient-danger ${className}`}
      >
        {/* begin::Header */}
        <div className="card-header border-0 pt-5">
          <br />
          <h3 className="fontColor">
            Sale Tickets
          </h3>
          <br/>
            {MyChart()}
          </div>
        <div className="card-body d-flex flex-column p-0">
         
         
        </div>
      </div>
    </>
  );
}



function MyChart() {

  const StockBatchEditSchema = Yup.object().shape({
    // batch_quantity: Yup.number()
    //   .required("Quantity is required"),
  
  });


  const [values,setValues]=useState();
  const [from,setFrom]=useState();
  const [to,setTo]=useState();

  const [shop,setShop]=useState([]);
  // const [shopName,setShopName]=useState([]);
  // const [dateE,setdateE]=useState();

  const initStockBatch = {
    // filter : [],
    // itemid:item,s
    from:from,
    to:to
    // from:'2021-02-01',
    // mstockItemId:"",
  };

  const handlerFrom =(e)=>{
    setFrom(e.target.value)
  }

  const handlerTo =(e)=>{
    console.log(e.target.value)
    setTo(e.target.value)
  }


  const restValues =(e)=>{
    setValues('')
    e.preventDefault()
    // setItem('')
    setFrom('')
    setTo('')

  }

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Api_Login + '/api/sales-ticket/report/sales-ticket',
      data:{values
        // "filter": []/
        // "from": "2021-02-01",
        // "to": "2021-05-30"
    }
      })
      .then((res) => {
        setShop(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[values])

  console.log(shop)

  const data = React.useMemo(
    () => [

      shop.map((item) => (
        [item.SalesTicketEntity_stat,item.sales_ticket_stats_count]
    ))
    ],[shop]
  )
 
  const axes = React.useMemo(
    () => [
      { primary: true, type: 'ordinal', position: 'bottom' },
      { position: 'left', type: 'linear', stacked: false }
    ],
    []
  )

  const series = React.useMemo(
    () => ({
      type: 'bar'
    }),
    []
  )
 
  const lineChart = (
    <div
      style={{
        height: "400px"
      }}
    >
      <Chart data={data} series={series} axes={axes} />
    </div>
  )

  if(values==null || values===''){
    return (
      <>
      <Formik
            enableReinitialize={true}
            initialValues={initStockBatch}
            validationSchema={StockBatchEditSchema}
            onSubmit={(values) => {
              console.log(values)
              setValues(values)
  
              // setFrom('')
              // setTo('')
  
            }}
          >
            {({ handleSubmit }) => (
              <>
                <Form className="form form-label-right">
                  <div className="form-group row">
                    <div className="col-lg-3">
                      <Field
                        type="date"
                        name="from"
                        component={Input1}
                        placeholder="From Date"
                        label="From Date"
                        onChange={handlerFrom}
                      />
                    </div>
                    <div className="col-lg-3">
                      <Field
                        type="date"
                        name="to"
                        component={Input1}
                        placeholder="To Date"
                        label="To Date"
                        onChange={handlerTo}
                      />
                    </div>
                    <div className="col-lg-2">
                        <button type="submit" className="btn btn-primary ml-2 + downbtn"> Find </button>
                        {/* <button  className="btn btn-light ml-2 + downbtn" onClick={restValues}>Reset</button> */}
                        {/* <button  className="btn btn-light ml-2 + downbtn" onClick={restValues}>Reset</button> */}
                    </div>
                  </div>
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    // ref={btnRef}
                    onSubmit={() => handleSubmit()}
                  ></button>
                  
                </Form>
                
              </>
            )}
          </Formik>
          {lineChart}
          </>
    );
  }
  else{
    return (
      <>
      <Formik
            enableReinitialize={true}
            initialValues={initStockBatch}
            validationSchema={StockBatchEditSchema}
            onSubmit={(values) => {
              console.log(values)
              setValues(values)
  
              setFrom('')
              setTo('')
  
            }}
          >
            {({ handleSubmit }) => (
              <>
                <Form className="form form-label-right">
                  <div className="form-group row">
                    <div className="col-lg-3">
                      <Field
                        type="date"
                        name="from"
                        component={Input1}
                        placeholder="From Date"
                        label="From Date"
                        onChange={handlerFrom}
                      />
                    </div>
                    <div className="col-lg-3">
                      <Field
                        type="date"
                        name="to"
                        component={Input1}
                        placeholder="To Date"
                        label="To Date"
                        onChange={handlerTo}
                      />
                    </div>
                    <div className="col-lg-2">
                        {/* <button type="submit" className="btn btn-primary ml-2 + downbtn"> Find </button> */}
                        {/* <button  className="btn btn-light ml-2 + downbtn" onClick={restValues}>Reset</button> */}
                        <button  className="btn btn-light ml-2 + downbtn" onClick={restValues}>Reset</button>
                    </div>
                  </div>
                  <button
                    type="submit"
                    style={{ display: "none" }}
                    // ref={btnRef}
                    onSubmit={() => handleSubmit()}
                  ></button>
                  
                </Form>
                
              </>
            )}
          </Formik>
          {lineChart}
          </>
    );
  }

  
}