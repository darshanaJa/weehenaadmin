/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, {useState, useEffect} from "react";
import SVG from "react-inlinesvg";
// import { Dropdown } from "react-bootstrap";
// import { DropdownCustomToggler, DropdownMenu4 } from "../../dropdowns";
import { toAbsoluteUrl } from "../../../_helpers";
import axios from "axios";
import { Api_Login } from "../../../../app/config/config";

export function ListsWidget12({ className }) {


    const [shop,setShop]=useState();
    const [shop1,setShop1]=useState();
  //   const [shop2,setShop2]=useState();
  //   const [shop3,setShop3]=useState();
  
  //   const AC=shop2/(shop+shop1)*100
  //   const DC=shop3/(shop+shop1)*100
  
    useEffect(()=>{
      axios({
        method: 'get',
        baseURL: Api_Login + '/api/sales-agent/all/active/agent-count',
        })
        .then((res) => {
          setShop(res.data)
        })
        .catch(function (response) {
        });
        
    },[])
  
    useEffect(()=>{
      axios({
        method: 'get',
        baseURL: Api_Login + '/api/sales-agent/all/deactive/agent-count',
        })
        .then((res) => {
          setShop1(res.data)
        })
        .catch(function (response) {
        });
        
    },[])

  return (
    <>
      <div className={`card card-custom ${className}`}>
        {/* Header */}
        <div className="card-header border-0">
          <h3 className="card-title font-weight-bolder text-dark">Sale Agent Details</h3>

          {/* <div className="card-toolbar">
            <Dropdown className="dropdown-inline" alignRight>
              <Dropdown.Toggle
                id="dropdown-toggle-top"
                as={DropdownCustomToggler}
              >
                <i className="ki ki-bold-more-ver" />
              </Dropdown.Toggle>
              <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <DropdownMenu4 />
              </Dropdown.Menu>
            </Dropdown>
          </div> */}
        </div>

        {/* Body */}
        <div className="card-body pt-0">
        <div className="row">
        <div className="col-xl-6">
          <div className="d-flex align-items-center mb-9 bg-light-warning rounded p-5" style={{width:'300px'}}>
            <span className="svg-icon svg-icon-warning mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl("/media/svg/icons/Home/Library.svg")}
              ></SVG>
            </span>

            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
               Active Sale Agents Count
              </a>
              {/* <span className="text-muted font-weight-bold">Approved</span> */}
            </div>

            <span className="font-weight-bolder text-warning py-1 font-size-lg">
              {shop}
            </span>
          </div>
          </div>

        <div className="col-xl-6">
          <div className="d-flex align-items-center bg-light-success rounded p-5 mb-9" style={{width:'300px'}}>
            <span className="svg-icon svg-icon-success mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
              ></SVG>
            </span>
            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
                Deactivate Sale Agents Count
              </a>
              {/* <span className="text-muted font-weight-bold">Due in 2 Days</span> */}
            </div>

            <span className="font-weight-bolder text-success py-1 font-size-lg">
              {shop1}
            </span>
          </div>
          </div>
          </div>
        </div>
    </div>

          
     
    </>
  );
}
