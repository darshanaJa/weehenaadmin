/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
// import React from "react";
import React, {useEffect,useState} from "react";
// import { Dropdown } from "react-bootstrap";
// import { DropdownCustomToggler, DropdownMenu1 } from "../../dropdowns";
import { toAbsoluteUrl } from "../../../_helpers";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";
import { Input1,Select } from "../../controls";
// import { toAbsoluteUrl } from "../../../../../_metronic/_helpers";
// import "../../../../../_metronic/_assets/sass/customize/login_greeting.scss";
// import { MainStocks_URL_GET } from "../../../_redux/MainStock/MainStocksCrud";
import axios from "axios";
// import { SalesTickets_URL_FIND } from "../../../../app/pages/_redux/salesTickets/salesTicketsCrud";
import { Api_Login,Image_Url } from "../../../../app/config/config";
import { MainStocks_URL_GET } from "../../../../app/pages/_redux/MainStock/MainStocksCrud";
// import { notify, notifyWarning } from "../../../../app/config/Toastify";



export function ListsWidget15({ className }) {

  const [shop,setShop]=useState([]);
  const [daily,setDaily]=useState([]);
  const [daily2,setDaily2]=useState([]);
  const [values,setValues]=useState();
  const [item,setItem]=useState();
  const [from,setFrom]=useState();
  const [to,setTo]=useState();

  const StockBatchEditSchema = Yup.object().shape({
    // batch_quantity: Yup.number()
    //   .required("Quantity is required"),
  
  });

  const initStockBatch = {
    filter : [],
    itemid:item,
    from:from,
    to:to
    // from:'2021-02-01',
    // mstockItemId:"",
  };

  console.log(shop)

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: MainStocks_URL_GET
      })
      .then((res) => {
        setShop(res.data.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  

  // useEffect(()=>{
  //   axios({
  //     method: 'post',
  //     baseURL: SalesTickets_URL_FIND,
  //     data:{
  //       "filter": [{"status": "1" }],
  //       "sort": "DESC",
  //       "limit": '', 
  //       "skip":''
  //   }
  //     })
  //     .then((res) => {
  //       setShop(res.data.data.results)
  //     })
  //     .catch(function (response) {
  //         // console.log(response);
  //     });
      
  // },[])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Api_Login + '/api/stock-batch/report/daily-production',
      data:values
      })
      .then((res) => {
        setDaily(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[values])

  useEffect(()=>{
    axios({
      method: 'post',
      baseURL: Api_Login + '/api/stock-batch/report/daily-production',
      data:{
        "filter": []
    }
      })
      .then((res) => {
        setDaily2(res.data)
      })
      .catch(function (response) {
          // console.log(response);
      });
      
  },[])

  console.log(shop)
  console.log(daily)

  const restValues =(e)=>{
    setValues('')
    e.preventDefault()
    setItem('')
    setFrom('')
    setTo()

  }
  
  const handlerItem =(e)=>{
    setItem(e.target.value)
  }

  const handlerFrom =(e)=>{
    setFrom(e.target.value)
  }

  const handlerTo =(e)=>{
    setTo(e.target.value)
  }

  if(values==null ||values===[] || values==='')
  {
    return (
      <>
      
        {/* begin::List Widget 14 */}
        <div className={`card card-custom ${className}`}>
          {/* begin::Header */}
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">
              Daily Production Report 
            </h3>
          </div>
          {/* end::Header */}
  
          <Formik
          enableReinitialize={true}
          initialValues={initStockBatch}
          validationSchema={StockBatchEditSchema}
          onSubmit={(values) => {
            console.log(values)
            setValues(values)

            // values.preventDefault();
            // setItem()
            // setFrom()
            // setTo()


          }}
        >
          {({ handleSubmit }) => (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  <div className="col-lg-3 + rightField">
                    <Select name="itemid" label="Item Name"
                     onChange={handlerItem}
                    //  value={item}
                     >
                      <option>Choose One</option>
                       {shop.map((item) => (
                            <option value={item.item_id} >
                              {item.item_id}
                            </option>    
                        ))}
                    </Select>
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="date"
                      name="from"
                      component={Input1}
                      placeholder="From Date"
                      label="From Date"
                      onChange={handlerFrom}
                    />
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="date"
                      name="to"
                      component={Input1}
                      placeholder="To Date"
                      label="To Date"
                      onChange={handlerTo}
                    />
                  </div>
                  <div className="col-lg-2">
                      <button type="submit" className="btn btn-primary ml-2 + downbtn"> Find </button>
                  </div>
                </div>
                
                <button
                  type="submit"
                  style={{ display: "none" }}
                  // ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
              </Form>
            </>
          )}
        </Formik>
  
          {/* begin::Body */}
          <div className="card-body pt-2">
  
          {daily2.map((item) => (
            
            <div className="d-flex flex-wrap align-items-center mb-10">
            {/* begin::Symbol */}
            <div className="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
              
              <div
                className="symbol-label"
                // style={{
                //   backgroundImage: Image_Url + item.mstock.item_image
                // }}
                // style={{
                //   backgroundImage: 'https://storage.googleapis.com/weehena-storage-bucket1/main_stock/5a4c15b3-c76d-4acb-a876-95ae6dd86d78.png'
                  
                // }}
                style={{
                  backgroundImage: `url('${toAbsoluteUrl(
                    `${Image_Url +  item.mstock.item_image}`
                  )}')`,
                }}
              ></div>
            </div>
            {/* end::Symbol */}
  
            {/* begin::Title */}
            <div className="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
              <a
                href="#"
                className="text-dark-75 font-weight-bolder text-hover-primary font-size-lg"
              >
                {item.mstock.item_name}
              </a>
              <span className="text-muted font-weight-bold font-size-sm my-1">
              {item.mstock.mstock_description}
              </span>
              <span className="text-muted font-weight-bold font-size-sm">
                Item Quntity:
                <span className="text-primary font-weight-bold">{item.mstock.item_quantity}</span>
              </span>
            </div>
            {/* end::Title */}
  
            {/* begin::Info */}
            <div className="d-flex align-items-center py-lg-0 py-2">
              <div className="d-flex flex-column text-right">
                <span className="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">
                  Item Current Quntity: {item.mstock.item_current_quantity}
                </span>
                <span className="text-muted font-size-sm font-weight-bolder">
                  Item Default Price: {item.mstock.item_default_price}
                </span>
              </div>
            </div>
            {/* end::Info */}
          </div>
                        ))}
          </div>
          {/* end::Body */}
        </div>
        {/* end::List Widget 14 */}
      </>
    );
  }
  else{
    // if(daily.length==0){
    //   notifyWarning("Sorry Not Have Records...")
    // }
    return (
      <>
        {/* begin::List Widget 14 */}
        <div className={`card card-custom ${className}`}>
          {/* begin::Header */}
          <div className="card-header border-0">
            <h3 className="card-title font-weight-bolder text-dark">
              Market Leaders
            </h3>
          </div>
          {/* end::Header */}
  
          <Formik
          enableReinitialize={true}
          initialValues={initStockBatch}
          validationSchema={StockBatchEditSchema}
          onSubmit={(values) => {
            console.log(values)
            setValues(values)

            // setFrom('')
            // setItem('')
            // setTo('')

          }}
        >
          {({ handleSubmit }) => (
            <>
              <Form className="form form-label-right">
                <div className="form-group row">
                  <div className="col-lg-3 + rightField">
                    <Select name="itemid" label="Item Name" onChange={handlerItem}>
                      <option>Choose One</option>
                       {shop.map((item) => (
                            <option value={item.item_id} >
                              {item.item_id}
                            </option>    
                        ))}
                    </Select>
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="date"
                      name="from"
                      component={Input1}
                      placeholder="From Date"
                      label="From Date"
                      onChange={handlerFrom}
                    />
                  </div>
                  <div className="col-lg-3">
                    <Field
                      type="date"
                      name="to"
                      component={Input1}
                      placeholder="To Date"
                      label="To Date"
                      onChange={handlerTo}
                    />
                  </div>
                  <div className="col-lg-2">
                      <button type="submit" className="btn btn-primary ml-2 + downbtn"> Find </button>
                      <button  className="btn btn-light ml-2 + downbtn" onClick={restValues}>Reset</button>
                      {/* <button type="button" onClick={restValues}><i className="fa fa-redo"></i></button> */}
                  </div>
                </div>
                <button
                  type="submit"
                  style={{ display: "none" }}
                  // ref={btnRef}
                  onSubmit={() => handleSubmit()}
                ></button>
                
              </Form>
              
            </>
          )}
        </Formik>
  
          {/* begin::Body */}
          <div className="card-body pt-2">
  
          {daily.map((item) => (
            
            <div className="d-flex flex-wrap align-items-center mb-10">
            {/* begin::Symbol */}
            <div className="symbol symbol-60 symbol-2by3 flex-shrink-0 mr-4">
              
              <div
                className="symbol-label"
                // style={{
                //   backgroundImage: Image_Url + item.mstock.item_image
                // }}
                // style={{
                //   backgroundImage: 'https://storage.googleapis.com/weehena-storage-bucket1/main_stock/5a4c15b3-c76d-4acb-a876-95ae6dd86d78.png'
                  
                // }}
                style={{
                  backgroundImage: `url('${toAbsoluteUrl(
                    `${Image_Url + item.mstock.item_image}`
                  )}')`,
                }}
              ></div>
            </div>
            {/* end::Symbol */}
  
            {/* begin::Title */}
            <div className="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
              <a
                href="#"
                className="text-dark-75 font-weight-bolder text-hover-primary font-size-lg"
              >
                {item.mstock.item_name}
              </a>
              <span className="text-muted font-weight-bold font-size-sm my-1">
              {item.mstock.mstock_description}
              </span>
              <span className="text-muted font-weight-bold font-size-sm">
                Item Quntity:
                <span className="text-primary font-weight-bold">{item.mstock.item_quantity}</span>
              </span>
            </div>
            {/* end::Title */}
  
            {/* begin::Info */}
            <div className="d-flex align-items-center py-lg-0 py-2">
              <div className="d-flex flex-column text-right">
                <span className="text-dark-75 font-weight-bolder text-hover-primary font-size-lg">
                  Item Current Quntity: {item.mstock.item_current_quantity}
                </span>
                <span className="text-muted font-size-sm font-weight-bolder">
                  Item Default Price: {item.mstock.item_default_price}
                </span>
              </div>
            </div>
            {/* end::Info */}
          </div>
                        ))}
          </div>
          {/* end::Body */}
        </div>
        {/* end::List Widget 14 */}
      </>
    );
  }


  
}
