/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, {useState, useEffect} from "react";
import SVG from "react-inlinesvg";
// import { Dropdown } from "react-bootstrap";
// import { DropdownCustomToggler, DropdownMenu4 } from "../../dropdowns";
import { toAbsoluteUrl } from "../../../_helpers";
import axios from "axios";
import { Api_Login } from "../../../../app/config/config";

export function ListsWidget11({ className }) {


  const [approved, setapproved] = useState(5)
  const [suppler, setsuppler] = useState(5)
  const [vechicale, setvechicale] = useState(15)
  const [stock, setstock] = useState(10)
  const [shop,setShop]=useState();
  const [shop1,setShop1]=useState();

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/sales-agent/all/active/agent-count',
      })
      .then((res) => {
        setShop(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/sales-agent/all/deactive/agent-count',
      })
      .then((res) => {
        setShop1(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/special-price/all/active/specialprice-count',
      })
      .then((res) => {
        setapproved(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/supplier/all/active/supplier-count',
      })
      .then((res) => {
        setsuppler(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/vehicle/all/active/vehicle-count',
      })
      .then((res) => {
        setvechicale(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

  useEffect(()=>{
    axios({
      method: 'get',
      baseURL: Api_Login + '/api/main-stock/all/active/mainstock-count',
      })
      .then((res) => {
        setstock(res.data)
      })
      .catch(function (response) {
      });
      
  },[])

  return (
    <>
      <div className={`card card-custom ${className}`}>
        {/* Header */}
        <div className="card-header border-0">
          <h3 className="card-title font-weight-bolder text-dark">Admin Details</h3>

          {/* <div className="card-toolbar">
            <Dropdown className="dropdown-inline" alignRight>
              <Dropdown.Toggle
                id="dropdown-toggle-top"
                as={DropdownCustomToggler}
              >
                <i className="ki ki-bold-more-ver" />
              </Dropdown.Toggle>
              <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <DropdownMenu4 />
              </Dropdown.Menu>
            </Dropdown>
          </div> */}
        </div>

        {/* Body */}
        <div className="card-body pt-0">
        <div className="row">
        <div className="col-xl-6">
          <div className="d-flex align-items-center mb-9 bg-light-warning rounded p-5">
            <span className="svg-icon svg-icon-warning mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl("/media/svg/icons/Home/Library.svg")}
              ></SVG>
            </span>

            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
               Special Price Request
              </a>
              <span className="text-muted font-weight-bold">Approved</span>
            </div>

            <span className="font-weight-bolder text-warning py-1 font-size-lg">
              {approved}
            </span>
          </div>
          </div>
          <div className="col-xl-6">

          <div className="d-flex align-items-center bg-light-success rounded p-5 mb-9" >
            <span className="svg-icon svg-icon-success mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl("/media/svg/icons/Communication/Write.svg")}
              ></SVG>
            </span>
            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
                Register Suppliers Count
              </a>
              {/* <span className="text-muted font-weight-bold">Due in 2 Days</span> */}
            </div>

            <span className="font-weight-bolder text-success py-1 font-size-lg">
              {suppler}
            </span>
          </div>
          </div>
          </div>
          <div className="row">
          <div className="col-xl-6">
          <div className="d-flex align-items-center bg-light-danger rounded p-5 mb-9" >
            <span className="svg-icon svg-icon-danger mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/Communication/Group-chat.svg"
                )}
              ></SVG>
            </span>

            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
                Register Vehicale Count
              </a>
              {/* <span className="text-muted font-weight-bold">Due in 2 Days</span> */}
            </div>

            <span className="font-weight-bolder text-danger py-1 font-size-lg">
              {vechicale}
            </span>
          </div>
          </div>
          
          <div className="col-xl-6">
          <div className="d-flex align-items-center bg-light-info rounded p-5" >
            <span className="svg-icon svg-icon-info mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl("/media/svg/icons/General/Attachment2.svg")}
              ></SVG>
            </span>

            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
                Main Stocks Count
              </a>
              {/* <span className="text-muted font-weight-bold">Due in 2 Days</span> */}
            </div>

            <span className="font-weight-bolder text-info py-1 font-size-lg">
              {stock}
            </span>
          </div>
          </div>
          </div>

          <div className="row">
          <div className="col-xl-6">
          <div className="d-flex align-items-center bg-light-danger rounded p-5 mb-9" >
            <span className="svg-icon svg-icon-danger mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/Communication/Group-chat.svg"
                )}
              ></SVG>
            </span>

            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
               Active Sale Agents Count
              </a>
              {/* <span className="text-muted font-weight-bold">Approved</span> */}
            </div>

            <span className="font-weight-bolder text-warning py-1 font-size-lg">
              {shop}
            </span>
          </div>
          </div>
          
          <div className="col-xl-6">
          <div className="d-flex align-items-center bg-light-info rounded p-5" >
            <span className="svg-icon svg-icon-info mr-5 svg-icon-lg">
              <SVG
                src={toAbsoluteUrl("/media/svg/icons/General/Attachment2.svg")}
              ></SVG>
            </span>

            <div className="d-flex flex-column flex-grow-1 mr-2">
              <a
                href="#"
                className="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"
              >
                Deactivate Sale Agents Count
              </a>
              {/* <span className="text-muted font-weight-bold">Due in 2 Days</span> */}
            </div>

            <span className="font-weight-bolder text-success py-1 font-size-lg">
              {shop1}
            </span>
          </div>
          </div>
          </div>

        </div>
      </div>
    </>
  );
}
