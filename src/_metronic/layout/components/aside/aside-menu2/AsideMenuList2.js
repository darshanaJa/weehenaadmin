/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import {
  FaReceipt,
  FaUserCheck,
  FaMapMarkerAlt,
  FaFileExport,
  FaWarehouse,
  FaTicketAlt,
  FaMoneyBillAlt
  
  } from "react-icons/fa";

export function AsideMenuList2({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard/sales", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard/sales">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>


        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          <h4 className="menu-text">Sales</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/sales/tickets", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/tickets">
            <span className="svg-icon menu-icon">
            {/* <SVG
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Communication/Group.svg"
                        )}
                      /> */}
                      <FaReceipt />
            </span>
            <span className="menu-text"> Sales Tickets </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/sales/agent", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/agent">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/file-done.svg"
                )}
              /> */}
              {/* <SVG
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Communication/Group.svg"
                        )}
                      /> */}
                      <FaUserCheck />
            </span>
            <span className="menu-text"> Sale Agents </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/sales/track", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/track">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/file-done.svg"
                )}
              /> */}
              <FaMapMarkerAlt />
            </span>
            <span className="menu-text"> Track Sale Agents </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/sales/requests", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/requests">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/Protected-file.svg"
                )}
              /> */}
              <FaFileExport />
            </span>
            <span className="menu-text"> Price Request </span>
          </NavLink>
        </li>

        {/* <li
          className={`menu-item ${getMenuItemActive("/sales/history", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/history">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/export.svg"
                )}
              />
            </span>
            <span className="menu-text"> Sale History </span>
          </NavLink>
        </li> */}

        <li
          className={`menu-item ${getMenuItemActive("/sales/shops", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/shops">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/shopping/Barcode-read.svg"
                )}
              /> */}
              <FaWarehouse />
            </span>
            <span className="menu-text"> Customers </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/sales/payments", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/payments">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/shopping/Barcode-read.svg"
                )}
              /> */}
              <FaMoneyBillAlt />
            </span>
            <span className="menu-text"> Payments </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/sales/reatails", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/sales/reatails">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/shopping/Barcode-read.svg"
                )}
              /> */}
              <FaTicketAlt />
            </span>
            <span className="menu-text"> Retail Sales </span>
          </NavLink>
        </li>


    
      </ul>

     
    </>
  );
}




// /* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
// import React from "react";
// import { useLocation } from "react-router";
// import { NavLink } from "react-router-dom";
// import SVG from "react-inlinesvg";
// import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";

// export function AsideMenuList2({ layoutProps }) {
//   const location = useLocation();
//   const getMenuItemActive = (url, hasSubmenu = false) => {
//     return checkIsActive(location, url)
//       ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
//       : "";
//   };

//   return (
//     <>
//       {/* begin::Menu Nav */}
//       <ul className={`menu-nav ${layoutProps.ulClasses}`}>
//         {/*begin::1 Level*/}
//         <li
//           className={`menu-item ${getMenuItemActive("/dashboard", false)}`}
//           aria-haspopup="true"
//         >
//           <NavLink className="menu-link" to="/dashboard">
//             <span className="svg-icon menu-icon">
//               <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
//             </span>
//             <span className="menu-text">Dashboard</span>
//           </NavLink>
//         </li>


//         {/* Applications */}
//         {/* begin::section */}
//         <li className="menu-section ">
//           <h4 className="menu-text">Sales</h4>
//           <i className="menu-icon flaticon-more-v2"></i>
//         </li>

//         <li
//           className={`menu-item ${getMenuItemActive("/sales/tickets", false)}`}
//           aria-haspopup="true"
//         >
//           <NavLink className="menu-link" to="/sales/tickets">
//             <span className="svg-icon menu-icon">
//               <SVG
//                 src={toAbsoluteUrl(
//                   "/media/svg/icons/shopping/sale1.svg"
//                 )}
//               />
//             </span>
//             <span className="menu-text"> Sales Tickets </span>
//           </NavLink>
//         </li>

//         <li
//           className={`menu-item ${getMenuItemActive("/sales/agent", false)}`}
//           aria-haspopup="true"
//         >
//           <NavLink className="menu-link" to="/sales/agent">
//             <span className="svg-icon menu-icon">
//               <SVG
//                 src={toAbsoluteUrl(
//                   "/media/svg/icons/files/file-done.svg"
//                 )}
//               />
//             </span>
//             <span className="menu-text"> Sale Agents </span>
//           </NavLink>
//         </li>

//         <li
//           className={`menu-item ${getMenuItemActive("/sales/history", false)}`}
//           aria-haspopup="true"
//         >
//           <NavLink className="menu-link" to="/sales/history">
//             <span className="svg-icon menu-icon">
//               <SVG
//                 src={toAbsoluteUrl(
//                   "/media/svg/icons/files/export.svg"
//                 )}
//               />
//             </span>
//             <span className="menu-text"> Sale History </span>
//           </NavLink>
//         </li>

//         <li
//           className={`menu-item ${getMenuItemActive("/sales/shops", false)}`}
//           aria-haspopup="true"
//         >
//           <NavLink className="menu-link" to="/sales/shops">
//             <span className="svg-icon menu-icon">
//               <SVG
//                 src={toAbsoluteUrl(
//                   "/media/svg/icons/shopping/Barcode-read.svg"
//                 )}
//               />
//             </span>
//             <span className="menu-text"> Shops </span>
//           </NavLink>
//         </li>


    
//       </ul>

     
//     </>
//   );
// }
