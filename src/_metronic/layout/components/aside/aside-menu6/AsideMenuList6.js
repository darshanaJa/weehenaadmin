/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import {
  FaBuilding,
  FaAddressCard,
  FaAtlas,
  FaChalkboardTeacher
  
  } from "react-icons/fa";

export function AsideMenuList6({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard/hr", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard/hr">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>


        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          <h4 className="menu-text">HR</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

     

        <li
          className={`menu-item ${getMenuItemActive("/hr/departments", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/hr/departments">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/Folder-check.svg"
                )}
              /> */}
              <FaBuilding />
            </span>
            <span className="menu-text"> Department </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/hr/employee", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/hr/employee">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Clipboard.svg"
                )}
              /> */}
              <FaAddressCard />
            </span>
            <span className="menu-text"> Employee Details </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/hr/position", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/hr/position">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Shield-disabled.svg"
                )}
              /> */}
              <FaAtlas />
            </span>
            <span className="menu-text"> Position </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/hr/gatekeeper", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/hr/gatekeeper">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Shield-protected.svg"
                )}
              /> */}
              <FaChalkboardTeacher />
            </span>
            <span className="menu-text"> Gate Keeper </span>
          </NavLink>
        </li>


    
      </ul>

     
    </>
  );
}