/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import {
  FaToriiGate,
  FaListAlt,
  FaCalendar,
  FaAlignCenter,
  } from "react-icons/fa";

export function AsideMenuList4({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard/buyback", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard/buyback">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>


        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          <h4 className="menu-text">Buy Back</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

     

        <li
          className={`menu-item ${getMenuItemActive("/buyback/farm", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/buyback/farm">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/Compilation.svg"
                )}
              /> */}
              <FaToriiGate />
            </span>
            <span className="menu-text"> Farm </span>
          </NavLink>
        </li>

        {/* <li
          className={`menu-item ${getMenuItemActive("/buyback/chicken", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" exact to="/buyback/chicken">
            <span className="svg-icon menu-icon">
              <FaDungeon />
            </span>
            <span className="menu-text"> Chicken </span>
          </NavLink>
        </li> */}

        <li
          className={`menu-item ${getMenuItemActive("/buyback/coop", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link"  to="/buyback/coop">
            <span className="svg-icon menu-icon">
              <FaListAlt />
            </span>
            <span className="menu-text"> Chicken Coop </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/buyback/batch", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link"  to="/buyback/batch">
            <span className="svg-icon menu-icon">
            <FaAlignCenter />
            </span>
            <span className="menu-text"> Chicken Batch </span>
          </NavLink>
        </li>

        {/* <li
          className={`menu-item ${getMenuItemActive("/buyback/batch", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/buyback/batch">
            <span className="svg-icon menu-icon">
              <FaAlignCenter />
            </span>
            <span className="menu-text"> Chicken Batch </span>
          </NavLink>
        </li> */}

        <li
          className={`menu-item ${getMenuItemActive("/buyback/chcoopbatch", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link"  to="/buyback/chcoopbatch">
            <span className="svg-icon menu-icon">
            <FaCalendar />
            </span>
            <span className="menu-text"> Chicken Coop Batch </span>
          </NavLink>
        </li>

        {/* <li
          className={`menu-item ${getMenuItemActive("/buyback/chickencoopbatch", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" exact to="/buyback/chcoopbatch">
            <span className="svg-icon menu-icon">
             
              <FaCalendar />
            </span>
            <span className="menu-text"> Chicken Coop Batch </span>
          </NavLink>
        </li> */}

        {/* <li
          className={`menu-item ${getMenuItemActive("/stocks/stockbatch", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/stocks/stockbatch">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Bookmark.svg"
                )}
              />
            </span>
            <span className="menu-text"> Stock Batch</span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/stocks/pricehistory", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/stocks/pricehistory">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Clipboard.svg"
                )}
              />
            </span>
            <span className="menu-text"> Stock Price History </span>
          </NavLink>
        </li> */}


    
      </ul>

     
    </>
  );
}