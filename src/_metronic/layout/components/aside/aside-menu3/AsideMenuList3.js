/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import {
  FaStore,
  FaQrcode,
  FaRedoAlt,
  FaStream,
  FaTools
  
  } from "react-icons/fa";

export function AsideMenuList2({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard/stocks", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard/stocks">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>


        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          <h4 className="menu-text">Stocks</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

     

        <li
          className={`menu-item ${getMenuItemActive("/stocks/mainstocks", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/stocks/mainstocks">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/file-done.svg"
                )}
              /> */}
              <FaStore />
            </span>
            <span className="menu-text"> Main Stocks </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/stocks/stockbatch", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/stocks/stockbatch">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Bookmark.svg"
                )}
              /> */}
              <FaQrcode />
            </span>
            <span className="menu-text"> Stock Batch</span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/stocks/pricehistory", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/stocks/pricehistory">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Clipboard.svg"
                )}
              /> */}
              <FaRedoAlt />
            </span>
            <span className="menu-text"> Stock Price History </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/stocks/sparepartstock", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/stocks/sparepartstock">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Clip.svg"
                )}
              /> */}
              <FaStream />
            </span>
            <span className="menu-text"> Spare Part Stock </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/stocks/sparepartgrn", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/stocks/sparepartgrn">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/general/Expand-arrows.svg"
                )}
              /> */}
              <FaTools />
            </span>
            <span className="menu-text"> Spare Part Grn </span>
          </NavLink>
        </li>

    
      </ul>

     
    </>
  );
}

