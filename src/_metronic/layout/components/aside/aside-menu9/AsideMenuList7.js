/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import { FiAlertOctagon } from "react-icons/fi";

export function AsideMenuList7({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard/permissions", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard/permissions">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>


        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          <h4 className="menu-text">In and Out</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

     

        <li
          className={`menu-item ${getMenuItemActive("/inandout/vechicalegate", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/inandout/vechicalegate">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/Compilation.svg"
                )}
              /> */}
              <FiAlertOctagon />
            </span>
            <span className="menu-text"> Vechicale Gate </span>
          </NavLink>
        </li>

        

        {/* <li
          className={`menu-item ${getMenuItemActive("/transport/services", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/transport/services">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/General/Other1.svg"
                )}
              />
            </span>
            <span className="menu-text"> Services </span>
          </NavLink>
        </li> */}

      </ul>

     
    </>
  );
}
