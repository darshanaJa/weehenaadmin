/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import {
  FaKey,
  FaFileInvoice
  } from "react-icons/fa";

export function AsideMenuList7({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard/permissions", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard/permissions">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>


        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          <h4 className="menu-text">System Access</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

     

        <li
          className={`menu-item ${getMenuItemActive("/access/permisions", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/access/permisions">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/Compilation.svg"
                )}
              /> */}
              <FaKey />
            </span>
            <span className="menu-text"> Permission </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/access/modules", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/access/modules">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/Compilation.svg"
                )}
              /> */}
              <FaFileInvoice />
            </span>
            <span className="menu-text"> Module </span>
          </NavLink>
        </li>

        {/* <li
          className={`menu-item ${getMenuItemActive("/transport/services", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/transport/services">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/General/Other1.svg"
                )}
              />
            </span>
            <span className="menu-text"> Services </span>
          </NavLink>
        </li> */}

      </ul>

     
    </>
  );
}
