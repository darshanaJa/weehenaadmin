/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useMemo, useState } from "react";
import objectPath from "object-path";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
// import SVG from "react-inlinesvg";
import { useHtmlClassService } from "../../_core/MetronicLayout";
// import { toAbsoluteUrl } from "../../../_helpers";
import { AsideSearch } from "./AsideSearch";
import { AsideMenu } from "./aside-menu/AsideMenu";
import { AsideMenu2 } from "./aside-menu2/AsideMenue2"
import { AsideMenu3 } from "./aside-menu3/AsideMenue3"
import { AsideMenu4 } from "./aside-menu4/AsideMenue4"
import { AsideMenu5 } from "./aside-menu5/AsideMenue5"
import { AsideMenu6 } from "./aside-menu6/AsideMenue6"
import { AsideMenu7 } from "./aside-menu7/AsideMenue7"
import { AsideMenu8 } from "./aside-menu8/AsideMenue7"
import { AsideMenu9 } from "./aside-menu9/AsideMenue7";
import { LanguageSelectorDropdown } from "../extras/dropdowns/LanguageSelectorDropdown";
import { QuickUserToggler } from "../extras/QuickUserToggler";
import { Brand } from "../brand/Brand";
import { KTUtil } from "./../../../_assets/js/components/util";
import { NavLink } from "react-router-dom";
import { 
  FaLayerGroup,
  FaBalanceScale,
  FaCheckCircle,
  FaClipboard,
  FaUnlockAlt,
  // FaArchway,
  FaStreetView,
  FaTruck
  } from "react-icons/fa";
// import { FiBookmark } from "react-icons/fi";

export function Aside() {
  const uiService = useHtmlClassService();

  const layoutProps = useMemo(() => {
    return {
      asideClassesFromConfig: uiService.getClasses("aside", true),
      asideSecondaryDisplay: objectPath.get(
        uiService.config,
        "aside.secondary.display"
      ),
      asideSelfMinimizeToggle: objectPath.get(
        uiService.config,
        "aside.self.minimize.toggle"
      ),
      extrasSearchDisplay: objectPath.get(
        uiService.config,
        "extras.search.display"
      ),
      extrasNotificationsDisplay: objectPath.get(
        uiService.config,
        "extras.notifications.display"
      ),
      extrasQuickActionsDisplay: objectPath.get(
        uiService.config,
        "extras.quick-actions.display"
      ),
      extrasQuickPanelDisplay: objectPath.get(
        uiService.config,
        "extras.quick-panel.display"
      ),
      extrasLanguagesDisplay: objectPath.get(
        uiService.config,
        "extras.languages.display"
      ),
      extrasUserDisplay: objectPath.get(
        uiService.config,
        "extras.user.display"
      ),
    };
  }, [uiService]);

  const tabs = {
    tabId1: "kt_aside_tab_1",
    tabId2: "kt_aside_tab_2",
    tabId3: "kt_aside_tab_3",
    tabId4: "kt_aside_tab_4",
    tabId5: "kt_aside_tab_5",
    tabId6: "kt_aside_tab_6",
    tabId7: "kt_aside_tab_7",
    tabId8: "kt_aside_tab_8",
    tabId9: "kt_aside_tab_9",
    tabId10: "kt_aside_tab_10",
  };
  const [activeTab, setActiveTab] = useState(tabs.tabId4);
  const handleTabChange = (id) => {
    setActiveTab(id);
    const asideWorkspace = KTUtil.find(
      document.getElementById("kt_aside"),
      ".aside-secondary .aside-workspace"
    );
    if (asideWorkspace) {
      KTUtil.scrollUpdate(asideWorkspace);
    }
  };

  return (
    <>
      {/* begin::Aside */}
      <div
        id="kt_aside"
        className={`aside aside-left d-flex ${layoutProps.asideClassesFromConfig}`}
      >
        {/* begin::Primary */}
        <div className="aside-primary d-flex flex-column align-items-center flex-row-auto">
          <Brand />
          {/* begin::Nav Wrapper */}
          <div className="aside-nav d-flex flex-column align-items-center flex-column-fluid py-5 scroll scroll-pull">
            {/* begin::Nav */}
            <ul className="list-unstyled flex-column" role="tablist">
              {/* begin::Item */}
             
              {/* <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">Metronic Features</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/features"> 
                  <a
                    href="#" 
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId2 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId2}`}
                    onClick={() => handleTabChange(tabs.tabId2)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                     
                      <FaBalanceScale />
                    </span>
                  </a>
                  </NavLink>
                </OverlayTrigger>
              </li> */}
              {/* end::Item */}

              {/* begin::Item */}
              <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">Stocks</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/stocks"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId4 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId4}`}
                    onClick={() => handleTabChange(tabs.tabId4)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                      {/* <SVG 
                        src={toAbsoluteUrl(
                          "/media/svg/icons/shopping/Wallet.svg"
                        )}
                      /> */}
                      <FaCheckCircle />
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li>
              {/* end::Item */}



               {/* begin::Item */}
               <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">Sales</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/sales"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId3 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId3}`}
                    onClick={() => handleTabChange(tabs.tabId3)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                      {/* <SVG 
                        src={toAbsoluteUrl(
                          "/media/svg/icons/shopping/Chart-bar3.svg"
                        )}
                      /> */}
                      {/* <SVG
                        src={toAbsoluteUrl("/media/svg/icons/Communication/Group.svg")}
                      /> */}
                      <FaBalanceScale />
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li>
              {/* end::Item */}

              {/* begin::Item */}
              <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">Buy Back</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/buyback"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId5 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId5}`}
                    onClick={() => handleTabChange(tabs.tabId5)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                    <FaLayerGroup />
                      {/* <SVG src={toAbsoluteUrl("/media/svg/icons/shopping/ATM.svg")}/> */}
                      {/* <SVG src={toAbsoluteUrl("/media/svg/icons/Communication/Group.svg")}/> */}
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li>
              {/* end::Item */}

              {/* begin::Item */}
              <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">Supplier</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/suplier"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId6 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId6}`}
                    onClick={() => handleTabChange(tabs.tabId6)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                      {/* <SVG 
                        src={toAbsoluteUrl("/media/svg/icons/shopping/Gift.svg")}
                      /> */}
                      <FaClipboard />
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li>
              {/* end::Item */}

              {/* begin::Item */}
              <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">HR</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/hr"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId7 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId7}`}
                    onClick={() => handleTabChange(tabs.tabId7)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                      {/* <SVG 
                        src={toAbsoluteUrl(
                          "/media/svg/icons/shopping/Sort1.svg"
                        )}
                      /> */}
                      <FaStreetView />
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li>
              {/* end::Item */}


              {/* begin::Item */}
              <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">System Access</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/permisions"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId9 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId9}`}
                    onClick={() => handleTabChange(tabs.tabId9)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                      {/* <SVG 
                        src={toAbsoluteUrl("/media/svg/icons/shopping/ATM.svg")}
                      /> */}
                      <FaUnlockAlt />
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li>
              {/* end::Item */}

              {/* begin::Item */}
              {/* <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">In and Out</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/inandout"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId10 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId10}`}
                    onClick={() => handleTabChange(tabs.tabId10)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                      
                      <FaArchway />
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li> */}
              {/* end::Item */}
    

              {/* begin::Item */}
              <li
                className="nav-item mb-3"
                data-toggle="tooltip"
                data-placement="rigth"
                data-container="body"
                data-boundary="window"
                title="Metronic Features"
              >
                <OverlayTrigger
                  placement="right"
                  overlay={
                    <Tooltip id="metronic-features">Transport</Tooltip>
                  }
                >
                  <NavLink to="/dashboard/suplier"> 
                  <a
                    // href="/dashboard/sales" 
                   
                    className={`nav-link btn btn-icon btn-clean btn-lg ${activeTab ===
                      tabs.tabId8 && "active"}`}
                    data-toggle="tab"
                    data-target={`#${tabs.tabId8}`}
                    onClick={() => handleTabChange(tabs.tabId8)}
                    role="tab"
                  >
                    <span className="svg-icon svg-icon-lg">
                      {/* <SVG 
                        src={toAbsoluteUrl(
                          "/media/svg/icons/Map/Compass.svg"
                        )}
                      /> */}
                      <FaTruck />
                    </span>
                  </a>
                  </NavLink> 
                </OverlayTrigger>
              </li>
              {/* end::Item */}
            </ul>
            {/* end::Nav */}
          </div>
          {/* end::Nav Wrapper */}

          {/* begin::Footer */}
          <div className="aside-footer d-flex flex-column align-items-center flex-column-auto py-4 py-lg-10">
            {/* begin::Aside Toggle */}
            {layoutProps.asideSecondaryDisplay &&
              layoutProps.asideSelfMinimizeToggle && (
                <>
                  <OverlayTrigger
                    placement="right"
                    overlay={<Tooltip id="toggle-aside">Toggle Aside</Tooltip>}
                  >
                    <span
                      className="aside-toggle btn btn-icon btn-primary btn-hover-primary shadow-sm"
                      id="kt_aside_toggle"
                    >
                      <i className="ki ki-bold-arrow-back icon-sm" />
                    </span>
                  </OverlayTrigger>
                </>
              )}
            {/* end::Aside Toggle */}

            {/* begin::Search */}
            {/* {layoutProps.extrasSearchDisplay && (
              <OverlayTrigger
                placement="right"
                overlay={<Tooltip id="toggle-search">Quick Search</Tooltip>}
              >
                <a
                  href="#"
                  className="btn btn-icon btn-clean btn-lg mb-1"
                  id="kt_quick_search_toggle"
                >
                  <span className="svg-icon svg-icon-lg">
                    <SVG
                      src={toAbsoluteUrl("/media/svg/icons/General/Search.svg")}
                    />
                  </span>
                </a>
              </OverlayTrigger>
            )} */}
            {/* end::Search */}

            {/* begin::Notifications */}
            {/* {layoutProps.extrasNotificationsDisplay && (
              <OverlayTrigger
                placement="right"
                overlay={
                  <Tooltip id="toggle-notifications">Notifications</Tooltip>
                }
              >
                <a
                  href="#"
                  className="btn btn-icon btn-clean btn-lg mb-1 position-relative"
                  id="kt_quick_notifications_toggle"
                  data-placement="right"
                  data-container="body"
                  data-boundary="window"
                >
                  <span className="svg-icon svg-icon-lg">
                    <SVG
                      src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")}
                    />
                  </span>
                </a>
              </OverlayTrigger>
            )} */}
            {/* end::Notifications */}

            {/* begin::Quick Actions */}
            {/* {layoutProps.extrasQuickActionsDisplay && (
              <OverlayTrigger
                placement="right"
                overlay={
                  <Tooltip id="toggle-quick-actions">Quick Actions</Tooltip>
                }
              >
                <a
                  href="#"
                  className="btn btn-icon btn-clean btn-lg mb-1"
                  id="kt_quick_actions_toggle"
                >
                  <span className="svg-icon svg-icon-lg">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/Media/Equalizer.svg"
                      )}
                    />
                  </span>
                </a>
              </OverlayTrigger>
            )} */}
            {/* end::Quick Actions */}

            {/* begin::Quick Panel */}
            {/* {layoutProps.extrasQuickPanelDisplay && (
              <OverlayTrigger
                placement="right"
                overlay={<Tooltip id="toggle-quick-panel">Quick Panel</Tooltip>}
              >
                <a
                  href="#"
                  className="btn btn-icon btn-clean btn-lg mb-1 position-relative"
                  id="kt_quick_panel_toggle"
                  data-placement="right"
                  data-container="body"
                  data-boundary="window"
                >
                  <span className="svg-icon svg-icon-lg">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/Layout/Layout-4-blocks.svg"
                      )}
                    />
                  </span>
                  <span className="label label-sm label-light-danger label-rounded font-weight-bolder position-absolute top-0 right-0 mt-1 mr-1">
                    3
                  </span>
                </a>
              </OverlayTrigger>
            )} */}
            {/* end::Quick Panel */}

            {/* begin::Languages*/}
            {layoutProps.extrasLanguagesDisplay && <LanguageSelectorDropdown />}
            {/* end::Languages */}

            {/* begin::User*/}
            {layoutProps.extrasUserDisplay && <QuickUserToggler />}
            {/* end::User */}
          </div>
          {/* end::Footer */}
        </div>
        {/* end::Primary */}

        {layoutProps.asideSecondaryDisplay && (
          <>
            {/* begin::Secondary */}
            <div className="aside-secondary d-flex flex-row-fluid">
              {/* begin::Workspace */}
              <div className="aside-workspace scroll scroll-push my-2">
                <div className="tab-content">
                  <AsideSearch isActive={activeTab === tabs.tabId1} />
                  <AsideMenu isActive={activeTab === tabs.tabId2} />
                  <AsideMenu2 isActive={activeTab === tabs.tabId3} />
                  <AsideMenu3 isActive={activeTab === tabs.tabId4} />
                  <AsideMenu4 isActive={activeTab === tabs.tabId5} />
                  <AsideMenu5 isActive={activeTab === tabs.tabId6} />
                  <AsideMenu6 isActive={activeTab === tabs.tabId7} />
                  <AsideMenu7 isActive={activeTab === tabs.tabId8} />
                  <AsideMenu8 isActive={activeTab === tabs.tabId9} />
                  <AsideMenu9 isActive={activeTab === tabs.tabId10} />
                </div>
              </div>
              {/* end::Workspace */}
            </div>
            {/* end::Secondary */}
          </>
        )}
      </div>
      {/* end::Aside */}
    </>
  );
}

