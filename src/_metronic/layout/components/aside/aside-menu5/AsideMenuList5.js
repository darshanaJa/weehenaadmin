/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl, checkIsActive } from "../../../../_helpers";
import {
  FaUserFriends,
  FaUserMd
  
  } from "react-icons/fa";

export function AsideMenuList5({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open `
      : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive("/dashboard/suplier", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/dashboard/suplier">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl("/media/svg/icons/Design/Layers.svg")} />
            </span>
            <span className="menu-text">Dashboard</span>
          </NavLink>
        </li>


        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          <h4 className="menu-text">Supplier</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>

     

        <li
          className={`menu-item ${getMenuItemActive("/suplier/suplier", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/suplier/suplier">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/files/Protected-file.svg"
                )}
              /> */}
              <FaUserFriends />
            </span>
            <span className="menu-text"> Supplier </span>
          </NavLink>
        </li>

        <li
          className={`menu-item ${getMenuItemActive("/suplier/sparepartsuplier", false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/suplier/sparepartsuplier">
            <span className="svg-icon menu-icon">
              {/* <SVG
                src={toAbsoluteUrl(
                  "/media/svg/icons/Media/Media-library1.svg"
                )}
              /> */}
              <FaUserMd />
            </span>
            <span className="menu-text"> Spare part Supplier </span>
          </NavLink>
        </li>

      </ul>

     
    </>
  );
}
