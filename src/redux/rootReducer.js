import {all} from "redux-saga/effects";
import {combineReducers} from "redux";

import * as auth from "../app/modules/Auth/_redux/authRedux";
import {customersSlice} from "../app/pages/_redux/customers/customersSlice";
import {productsSlice} from "../app/pages/_redux/products/productsSlice";
import {agentsSlice} from "../app/pages/_redux/agent/agentsSlice";
import {shopsSlice} from "../app/pages/_redux/shops/shopsSlice"
import {remarksSlice} from "../app/pages/_redux/remarks/remarksSlice";
import {salesTicketsSlice} from "../app/pages/_redux/salesTickets/salesTicketsSlice";
import {StockBatchsSlice} from "../app/pages/_redux/StockBatch/StockBatchsSlice"
import {MainStocksSlice} from "../app/pages/_redux/MainStock/MainStocksSlice";
import {PricesHistorySlice} from "../app/pages/_redux/PriceHistory/PricesHistorySlice";
import {salesHistorySlice} from "../app/pages/_redux/salesHistory/salesHistorySlice";
import {specificationsSlice} from "../app/pages/_redux/specifications/specificationsSlice";
import { FarmsSlice } from "../app/pages/_redux/Farm/FarmsSlice";
import { SupliersSlice } from "../app/pages/_redux/Suplier/SupliersSlice";
import { ItemsSlice } from "../app/pages/_redux/Items/ItemsSlice";
import { DepartmentsSlice } from "../app/pages/_redux/Department/DepartmentsSlice";
import { EmployeesSlice } from "../app/pages/_redux/Employee/EmployeesSlice";
import { PositionsSlice } from "../app/pages/_redux/Position/PositionsSlice";
import { VechicalesSlice } from "../app/pages/_redux/Vechicale/VechicalesSlice";
import { RequestsSlice } from "../app/pages/_redux/Requests/RequestsSlice";
import { SpareSuppliersSlice } from "../app/pages/_redux/SpareSupplier/SpareSuppliersSlice";
import { SpareStocksSlice } from "../app/pages/_redux/SpareStock/SpareStocksSlice";
import { ChickensSlice } from "../app/pages/_redux/Chicken/ChickensSlice";
import { ServicesSlice } from "../app/pages/_redux/Service/ServicesSlice";
import { SparegrnsSlice } from "../app/pages/_redux/Sparegrn/SparegrnsSlice";
import { GateKeepersSlice } from "../app/pages/_redux/GateKeeper/GateKeepersSlice";
import { AccessSlice } from "../app/pages/_redux/Acces/AccessSlice";
import { ModulesSlice } from "../app/pages/_redux/Module/ModulesSlice";
import { ChickenBatchesSlice } from "../app/pages/_redux/ChickenBatch/ChickenBatchesSlice";
import { ChickenCopBatchesSlice } from "../app/pages/_redux/ChickenCopBatch/ChickenCopBatchesSlice";
import { ChickensAgesSlice } from "../app/pages/_redux/ChickensAge/ChickensAgesSlice";
import { VechicleGatesSlice } from "../app/pages/_redux/VechicleGate/VechicleGatesSlice";
import { PaymentsSlice } from "../app/pages/_redux/Payment/PaymentsSlice";
import { CreditsSlice } from "../app/pages/_redux/Credit/CreditsSlice";

export const rootReducer = combineReducers({
  auth: auth.reducer,
  customers: customersSlice.reducer,
  Farms: FarmsSlice.reducer,
  Vechicales: VechicalesSlice.reducer,
  Requests: RequestsSlice.reducer,
  Supliers: SupliersSlice.reducer,
  products: productsSlice.reducer,
  Departments:DepartmentsSlice.reducer,
  Employees:EmployeesSlice.reducer,
  Positions:PositionsSlice.reducer,
  remarks: remarksSlice.reducer,
  Items:ItemsSlice.reducer,
  Agents: agentsSlice.reducer,
  StockBatchs: StockBatchsSlice.reducer,
  PricesHistory: PricesHistorySlice.reducer,
  MainStocks: MainStocksSlice.reducer,
  Shops: shopsSlice.reducer,
  SalesTickets: salesTicketsSlice.reducer,
  SalesHistory:salesHistorySlice.reducer,
  specifications: specificationsSlice.reducer,
  SpareStocks:SpareStocksSlice.reducer,
  Chickens:ChickensSlice.reducer,
  Services:ServicesSlice.reducer,
  SpareSuppliers:SpareSuppliersSlice.reducer,
  Sparegrns:SparegrnsSlice.reducer,
  GateKeepers:GateKeepersSlice.reducer,
  Access:AccessSlice.reducer,
  Modules:ModulesSlice.reducer,
  ChickenBatches:ChickenBatchesSlice.reducer,
  ChickenCopBatches:ChickenCopBatchesSlice.reducer,
  ChickensAges:ChickensAgesSlice.reducer,
  VechicleGates:VechicleGatesSlice.reducer,
  Payments:PaymentsSlice.reducer,
  Credits:CreditsSlice.reducer

});

export function* rootSaga() {
  yield all([auth.saga()]);
}
